package it.uniroma2.art.coda.pf4j;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.provisioning.ComponentIndex;
import it.uniroma2.art.coda.provisioning.ComponentIndexingException;
import it.uniroma2.art.coda.provisioning.ComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.provisioning.ConverterContractDescription;
import it.uniroma2.art.coda.provisioning.ConverterDescription;
import org.pf4j.PluginManager;
import org.pf4j.PluginState;
import org.pf4j.PluginStateEvent;
import org.pf4j.PluginStateListener;
import org.pf4j.PluginWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PF4JComponentProvider implements ComponentProvider, PluginStateListener {

    private static final Logger logger = LoggerFactory.getLogger(PF4JComponentProvider.class);


    private PluginManager pluginManager;
    private boolean stopPluginsOnClose;

    // A component index
    private final ComponentIndex componentIndex;

    // Plugin to converter
    private final Multimap<PluginWrapper, Converter> plugin2converterMap = HashMultimap.create();

    // Global URI bindings
    private final Map<String, String> uri2converterGlobalBindings = new ConcurrentHashMap<>();

    // Map each contract URI to conforming converter instances
    private final Multimap<String, Converter> contractURI2converterMap = HashMultimap.create();

    // Map convert URIs to the corresponding converter instance
    private final Map<String, Converter> uri2converterMap = new HashMap<>();

    // Cached converter instances for previously used URIs (either converter or contract URIs)
    private final Map<String, Object> uri2ObjectMap = new ConcurrentHashMap<>();


    public PF4JComponentProvider(PluginManager pluginManager) {
        this(pluginManager, false);
    }

    public PF4JComponentProvider(PluginManager pluginManager, boolean stopPluginsOnClose) {
        this.pluginManager = pluginManager;
        this.stopPluginsOnClose = stopPluginsOnClose;
        this.componentIndex = new ComponentIndex();
    }

    @Override
    public void open() throws ComponentIndexingException {
        for (Converter converter : this.pluginManager.getExtensions(Converter.class)) {
            PluginWrapper pluginWrapper = this.pluginManager.whichPlugin(converter.getClass());
            indexConverter(pluginWrapper, converter);
        }

        this.pluginManager.addPluginStateListener(this);
    }


    // Indexes a new plugin
    protected synchronized void indexPlugin(PluginWrapper pluginWrapper) throws ComponentIndexingException {

        for (Converter converter : this.pluginManager.getExtensions(Converter.class, pluginWrapper.getPluginId())) {
            indexConverter(pluginWrapper, converter);
        }


    }

    protected synchronized void indexConverter(PluginWrapper pluginWrapper, Converter converter) throws ComponentIndexingException {
        indexConverter(converter);
        if (pluginWrapper != null) {
            plugin2converterMap.put(pluginWrapper, converter);
        }
    }


    // Indexes a new converter discovered in the OSGi service registry
    protected synchronized void indexConverter(Converter converterObj)
            throws ComponentIndexingException {
        String converterURI = converterObj.getConverterURI();

        if (contractURI2converterMap.containsKey(converterURI)) {
            logger.warn(String.format(
                    "Converter \"%s\" registered under the URI <%s> already in use by a different converter. Ignored.", converterURI));
        }

        Class<?> converterClazz = converterObj.getClass();
        ConverterDescription converterDescription = componentIndex.indexConverter(converterClazz);
        uri2converterMap.put(converterURI, converterObj);

        for (ConverterContractDescription contractDescr : converterDescription.getImplementedContracts()) {
            String contractURI = contractDescr.getContractURI();
            contractURI2converterMap.put(contractURI, converterObj);
        }
    }

    protected synchronized void forgetPlugin(PluginWrapper plugin) {
        for (Converter converter : plugin2converterMap.removeAll(plugin)) {
            uri2converterMap.values().remove(converter);
            contractURI2converterMap.values().remove(converter);
            uri2ObjectMap.values().remove(converter);

            componentIndex.forgetConverter(converter.getClass());
        }
    }

    @Override
    public synchronized void setGlobalContractBinding(String contractURI, String converterURI) {
        if (uri2ObjectMap.containsKey(contractURI)) {
            uri2ObjectMap.remove(contractURI);
        }

        uri2converterGlobalBindings.put(contractURI, converterURI);
    }

    @Override
    public void close() {

        this.pluginManager.removePluginStateListener(this);

        if (this.stopPluginsOnClose) {
            this.pluginManager.stopPlugins();
        }
    }

    @Override
    public Collection<ConverterContractDescription> listConverterContracts() {
        return componentIndex.listConverterContracts();
    }

    @Override
    public Collection<ConverterDescription> listConverters() {
        return componentIndex.listConverters();
    }

    @Override
    public Object lookup(String dependencyURI) throws ComponentProvisioningException {
        Object service = lookupLocallyAvailable(dependencyURI);
        if (service != null) {
            return service;
        } else {
            throw new ComponentProvisioningException(String.format("Cannot satisfy the required dependency <%s> with a locally " +
                    "available component, and remote component provisioning is disabled.", dependencyURI));
        }
    }

    private synchronized Object lookupLocallyAvailable(String dependencyURI) {
        String converter = uri2converterGlobalBindings.get(dependencyURI);

        if (converter != null) {
            return uri2converterMap.get(converter);
        } else {
            Object immediateConverter = uri2converterMap.get(dependencyURI);
            if (immediateConverter != null) {
                return immediateConverter;
            }

            if (dependencyURI != null) {
                Collection<Converter> matchingConverters = contractURI2converterMap.get(dependencyURI);

                if (!matchingConverters.isEmpty()) {
                    return matchingConverters.iterator().next();
                }

            }

            return null;
        }
    }


    @Override
    public void pluginStateChanged(PluginStateEvent event) {
        if (event.getPluginState() == PluginState.STOPPED) {
            forgetPlugin(event.getPlugin());
        } else if (event.getPluginState() == PluginState.STARTED) {
            try {
                System.out.println("plugin started: " + event.getPlugin().getPluginId());
                indexPlugin(event.getPlugin());
            } catch (ComponentIndexingException e) {
                logger.error("Error while indexing plugin: " + event.getPlugin().getPluginId(), e);
            }
        }
    }

}
