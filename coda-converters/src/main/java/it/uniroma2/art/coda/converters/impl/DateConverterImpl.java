package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.commons.DateTimeUtils;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.DateConverter;
import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.pf4j.Extension;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;

/**
 * Implementation of {@link DateConverter}. Its identifier is {@value #CONVERTER_URI}.
 */
@Extension(points = {Converter.class})
public class DateConverterImpl implements DateConverter {
	
	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "date";
	
	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value)
			throws ConverterConfigurationException, ConversionException  {
		//input featurePath not provided => return current date
		if (value == null) {
			String formattedDate = DateTimeFormatter.ofPattern(DateTimeUtils.DATE_PATTERN_ISO_8601).format(LocalDate.now());
			return SimpleValueFactory.getInstance().createLiteral(formattedDate, XSD.DATE);
		}
		//otherwise try to infer format of value
		DateTimeFormatter inputFormatter = DateTimeUtils.inferFormatForDate(value);
		if (inputFormatter == null) {
			//input value not recognized with any pattern
			throw new ConverterConfigurationException("Cannot parse the input featurePath value " + value);
		} else {
			//format found, convert value into standard format
			try {
				//this code should never raise exception, since inputFormatter if not null, is for sure a valid formatter
				TemporalAccessor parsedDate = inputFormatter.parse(value);
				String formattedDate = DateTimeFormatter.ofPattern(DateTimeUtils.DATE_PATTERN_ISO_8601).format(parsedDate);
				return SimpleValueFactory.getInstance().createLiteral(formattedDate, XSD.DATE);
			} catch (DateTimeParseException e) {
				throw new ConversionException(e);
			}
		}
	}

}
