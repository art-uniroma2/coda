package it.uniroma2.art.coda.converters.contracts;

import org.eclipse.rdf4j.model.Literal;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.DatatypeCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 * Date converter contract. Its identifier is {@value #CONTRACT_URI}.
 */
@RDFCapability(RDFCapabilityType.literal)
//@DatatypeCapability({XMLSchema.DATE})
@DatatypeCapability("http://www.w3.org/2001/XMLSchema#date")
@Description("Generates a literal with datatype xsd:date. The input value is parsed " +
		"(compatibly with a set of recognized patterns) and is formatted according to the standard format (ISO 8601) yyyy-MM-dd. " +
		"If no input value is provided, the converter generates the current date. " +
		"If the input value cannot be parsed, the converter throws a ConverterConfigurationException.")
public interface DateConverter extends Converter {
	
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "date";
	
	/**
	 * Gets a value and returns it as typed literal (xsd:date) formatted according to the date 
	 * pattern (yyyy-MM-dd).
	 * If value is not provided returns the current datetime.
	 * @param ctx
	 * @return
	 * @throws ConverterException 
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) throws ConverterException;

}
