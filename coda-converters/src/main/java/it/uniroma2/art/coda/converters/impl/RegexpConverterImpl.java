package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.RegexpConverter;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.apache.commons.math3.exception.NotANumberException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.pf4j.Extension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Extension(points = {Converter.class})
public class RegexpConverterImpl implements RegexpConverter {

    public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "regexp";

    @Override
    public IRI produceURI(CODAContext ctx, String value, String regex, String template)
            throws ConverterException {

        String[] groupValueArray = prepareGroupValue(regex, value);

        if(groupValueArray == null || groupValueArray.length==0) {
            // the exception ConverterException is not thrown anymore, instead the value NULL is returned when the
            // regex did not match, so no value was created
            //throw  new ConverterException("Problem with the regex "+regex+" and the value "+value);

            return null;
        }

        String newText = parseTemplateWithRegexResults(template, groupValueArray);

        try {
            new URL(newText);//if this doesn't throw exception, then the value is already a URL
            return SimpleValueFactory.getInstance().createIRI(newText);
        } catch (MalformedURLException e) {
            if (newText.matches(DefaultConverterImpl.MAIL_REGEX)){
                return SimpleValueFactory.getInstance().createIRI("mailto:"+newText);
            } else {
                newText = newText.replaceAll("\\s","_"); //sanitize the input value
                return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace()+newText);
            }
        }

    }

    @Override
    public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, String regex,
                                  String template) throws ConverterException {
        String[] groupValueArray = prepareGroupValue(regex, value);
        if(groupValueArray == null || groupValueArray.length==0){
            throw  new ConverterException("The regex could not match the entire input value");
        }

        String newText = parseTemplateWithRegexResults(template, groupValueArray);

        Literal newLiteral;
        SimpleValueFactory simpleValueFactory = SimpleValueFactory.getInstance();
        if(lang!=null && !lang.isEmpty()){
            newLiteral = simpleValueFactory.createLiteral(newText, lang);
        } else if(datatype!=null && !datatype.isEmpty()){
            newLiteral =simpleValueFactory.createLiteral(newText, simpleValueFactory.createIRI(datatype));
        } else {
            newLiteral = simpleValueFactory.createLiteral(newText);
        }
        return newLiteral;
    }

    private String[] prepareGroupValue(String regex, String text){
        //create the Regex (which should match the entire text)
        if (!regex.startsWith("^")) {
            regex = "^"+regex;
        }
        if (!regex.endsWith("$")){
            regex += "$";
        }
        Pattern pattern = Pattern.compile(regex);
        //apply the regex to the input value
        Matcher matcher = pattern.matcher(text);
        //count the group number
        int groupCount = matcher.groupCount();

        boolean find = matcher.find();
        if (!find) {
            //the regex did not matched the input value, so return null
            return null;
        }

        //create the array containing the value for each group from the regex
        String[] groupValueArray = new String[groupCount];
        for(int i=1; i<=groupCount; ++i){
            String groupValue = matcher.group(i);
            groupValueArray[i-1] = groupValue;
        }

        return  groupValueArray;
    }

    private String parseTemplateWithRegexResults(String template, String[] groupValueArray) {
        StringBuilder sb = new StringBuilder();

        //split the template using the $ and check if the next character(s) are number (they should be a number less
        // than groupValueArray.length , especially for multiple two digit number)
        String[] templateArray = template.split("\\$");
        boolean first = true;
        for(String templatePart : templateArray){
            if(first) {
                // the first element is everything before the fist $, so leave it as it is
                first = false;
                sb.append(templatePart);
                continue;
            }
            if (templatePart.isEmpty()) {
                //empty string, so just add the $
                sb.append("$");
                continue;
            }
            // check if it start with a number (in case of two numbers, check that the number is smaller than groupValueArray.length )
            int number1, number2, startPosRest=0;
            String prefixString = "";

            number1 = checkNumberForGroup(templatePart.substring(0,1), groupValueArray.length);
            if (number1>0) {
                // it was a number and it is a possible number according to the group count, check if also the
                // following character is a number (if there is a second character in templatePart)
                if(templatePart.length()>1) {
                    number2 = checkNumberForGroup(templatePart.substring(0,2), groupValueArray.length);
                    if (number2>0) {
                        // use number2 since it is a valid number for the group position
                        sb.append(groupValueArray[number2-1]);
                        startPosRest=2;
                    } else {
                        // use number1 since number2 is not a valid number
                        sb.append(groupValueArray[number1-1]);
                        startPosRest=1;
                    }
                } else {
                    // just a single character, so get the value from groupValueArray
                    sb.append(groupValueArray[number1-1]);
                    startPosRest=1;
                }
            } else {
                // there was no number after $, so just add the $
                prefixString = "$";
            }
            // add the rest of the string
            if (startPosRest<templatePart.length()) {
                sb.append(prefixString).append(templatePart.substring(startPosRest));
            }

        }
        return sb.toString();
    }

    private int checkNumberForGroup(String numberString, int groupCount){
        try {
            int number = Integer.parseInt(numberString);
            if(number<=groupCount) {
                return number;
            } else {
                return -1;
            }
        } catch (NumberFormatException e) {
            return -1;
        }
    }

}
