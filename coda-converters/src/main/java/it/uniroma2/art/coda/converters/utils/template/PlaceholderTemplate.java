package it.uniroma2.art.coda.converters.utils.template;

public class PlaceholderTemplate implements TemplateInterface {
    private String id;
    private String modifier = null; // it can be: null, "U" or "l"

    public PlaceholderTemplate(String id) {
        this.id = id;
    }

    public PlaceholderTemplate(String id, String modifier) {
        this.id = id;
        this.modifier = modifier;
    }

    public String getId() {
        return id;
    }

    public String getModifier() {
        return modifier;
    }


}
