package it.uniroma2.art.coda.converters.contracts;

import org.eclipse.rdf4j.model.Literal;

import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.DatatypeCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 * Time converter contract. Its identifier is {@value #CONTRACT_URI}.
 */
@RDFCapability(RDFCapabilityType.literal)
//@DatatypeCapability({XmlSchema.TIME})
@DatatypeCapability("http://www.w3.org/2001/XMLSchema#time")
@Description("Generates a literal with datatype xsd:time. " +
		"The input value is parsed (compatibly with a set of recognized patterns) " +
		"and is formatted according to the standard format (ISO 8601) hh:mm:ss. " +
		"If no input value is provided, the converter generates the current time. " +
		"If the input value cannot be parsed, the converter throws a ConverterConfigurationException.\n" +
		"The converter takes optional parameters:\n" +
		"\t- an offset, which admitted values are:\n" +
		"\t\t- undefined: the output time will not contain any offset, if the input value has offset it will be ignored.\n" +
		"\t\t- Z: Zulu timezone. The \"Z\" timezone is simply added at the end of the output time.\n" +
		"\t\t- <hh>:<mm>: an offset, specified in hours and minutes, that is applied to the input value, or replaced if the latter already contains an offset.\n" +
		"\t\t- reuse: is applied the same offset of the input.\n" +
		"\t- an additionalOffset: an offset specified in hours and minutes (hh:mm). In case the offset parameter is <hh>:<mm> the additionalOffset is added, in case the offset is reuse, it adds the additionalOffset to the offset of the input (in case is missing, is considered as +00:00). In every other cases a ConverterConfigurationException is thrown.\n" +
		"If invalid parameters are passed, the converter throws a ConverterConfigurationException.")
public interface TimeConverter extends Converter {
	
String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "time";
	
	/**
	 * Gets a value and returns it as typed literal (xsd:time) formatted according to the time pattern
	 * (HH:mm:ss). If value is not provided returns the current datetime.
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value
	 * @return
	 * @throws ConversionException 
	 * @throws ConverterConfigurationException 
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) throws ConverterConfigurationException, ConversionException;

	/**
	 * Gets a value and returns it as typed literal (xsd:time) formatted according to the time pattern
	 * (HH:mm:ss) and with the given offset. If value is not provided returns the current datetime.
	 * @param ctx
	 * @param offset admitted values:
	 * <li>undefined: the output time does not contain any offset, if the input value has offset it is lost</li>
	 * <li>Z: Zulu timezone</li>
	 * <li>&lt;hh&gt;:&lt;mm&gt;: the given offset, specified in hours and minutes, 
	 * 	is applied to the input value, or replaced if the latter already contains a offset</li>
	 * <li>reuse: is applied the same offset of the input</li>
	 * @return
	 * @throws ConverterConfigurationException 
	 * @throws ConversionException e
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, @Parameter(name = "offset") String offset)
			throws ConverterConfigurationException, ConversionException;
	
	/**
	 * Gets a value and returns it as typed literal (xsd:time) formatted according to the time pattern
	 * (HH:mm:ss) with the given offset added to the offset of the input. This converter works only 
	 * with the offset "reuse" or in +/-HH:mm format
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value value of the featurePath to convert
	 * @param offset admitted value:
	 * <li>&lt;hh&gt;:&lt;mm&gt;: the given offset, specified in hours and minutes, 
	 * 	is applied to the input value, or replaced if the latter already contains a offset</li>
	 * <li>reuse: is applied the same offset of the input</li>
	 * @param additionalOffset offset expressed in format +/-HH:mm that is added to the original offset.
	 * This parameter is compatible only when offset parameter is REUSE
	 * @return
	 * @throws ConverterConfigurationException 
	 * @throws ConversionException 
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value,
			@Parameter(name = "offset") String offset, @Parameter(name = "additionalOffset") String additionalOffset)
			throws ConverterConfigurationException, ConversionException;

}
