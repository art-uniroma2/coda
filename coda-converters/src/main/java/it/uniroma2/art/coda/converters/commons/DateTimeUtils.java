package it.uniroma2.art.coda.converters.commons;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.uniroma2.art.coda.exception.ConverterConfigurationException;

/**
 * Utility class related to the management of date/time values.
 *
 */
public class DateTimeUtils {

	public static final String DATE_PATTERN_ISO_8601 = "yyyy-MM-dd";
	public static final String TIME_PATTERN_ISO_8601 = "HH:mm:ss";
	public static final String DATETIME_PATTERN_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss";

	public static final String OFFSET_PARAM_UNDEFINED = "undefined";
	public static final String OFFSET_PARAM_Z = "Z";
	public static final String OFFSET_PARAM_REUSE = "reuse";
	public static final String OFFSET_PARAM_PATTERN = "XXX";

	// ============= DATE PATTERNS =============

	/*
	 * List of pattern used to parse a date value. They differs for order of year, month, day, and separator
	 * ("-", "/", ".", " " whitespace)
	 */
	private static final List<String> datePatternsDigit = new ArrayList<String>();

	static {
		datePatternsDigit.add("yyyy-M-d"); // separator -
		datePatternsDigit.add("d-M-yyyy");
		datePatternsDigit.add("M-d-yyyy");
		datePatternsDigit.add("yyyy/M/d"); // separator /
		datePatternsDigit.add("d/M/yyyy");
		datePatternsDigit.add("M/d/yyyy");
		datePatternsDigit.add("yyyy.M.d"); // separator .
		datePatternsDigit.add("d.M.yyyy");
		datePatternsDigit.add("M.d.yyyy");
		datePatternsDigit.add("yyyy M d"); // separator whitespace
		datePatternsDigit.add("d M yyyy");
		datePatternsDigit.add("M d yyyy");
	}

	/*
	 * List equal to the previous, with the month expressed as text instead of as number. MMM stands for short
	 * form (e.g. Jan), MMMM is the full form (e.g. January). This list depends also from Locale that
	 * specifies the language in which the month is expressed.
	 */
	private static final List<String> datePatternsText = new ArrayList<String>();

	static {
		datePatternsText.add("yyyy-MMM-d"); // separator -
		datePatternsText.add("yyyy-MMMM-d");
		datePatternsText.add("d-MMM-yyyy");
		datePatternsText.add("d-MMMM-yyyy");
		datePatternsText.add("MMM-d-yyyy");
		datePatternsText.add("MMMM-d-yyyy");
		datePatternsText.add("yyyy/MMM/d"); // separator /
		datePatternsText.add("yyyy/MMMM/d");
		datePatternsText.add("d/MMM/yyyy");
		datePatternsText.add("d/MMMM/yyyy");
		datePatternsText.add("MMM/d/yyyy");
		datePatternsText.add("yyyy.MMM.d"); // separator .
		datePatternsText.add("yyyy.MMMM.d");
		datePatternsText.add("d.MMM.yyyy");
		datePatternsText.add("d.MMMM.yyyy");
		datePatternsText.add("MMM.d.yyyy");
		datePatternsText.add("MMMM.d.yyyy");
		datePatternsText.add("yyyy MMM d"); // separator whitespace
		datePatternsText.add("yyyy MMMM d");
		datePatternsText.add("d MMM yyyy");
		datePatternsText.add("d MMMM yyyy");
		datePatternsText.add("MMM d yyyy");
		datePatternsText.add("MMMM d yyyy");
	}

	// ============= TIME PATTERNS =============

	/*
	 * List of pattern used to parse a time value. They differs for separator (":", "."), presence or absence
	 * of seconds (ss) and milliseconds (S), the 12h (hh) or 24h (HH) format with "a" to indicate am/pm marker
	 */
	private static final List<String> timePatterns = new ArrayList<String>();

	static {
		// no timezone
		timePatterns.add("h:m:s a"); // separator :
		timePatterns.add("H:m:s");
		timePatterns.add("h:m a");
		timePatterns.add("H:m");
		timePatterns.add("h:m:s.SSS a");
		timePatterns.add("H:m:s.SSS");
		timePatterns.add("h.m.s a"); // separator .
		timePatterns.add("H.m.s");
		timePatterns.add("h.m a");
		timePatterns.add("H.m");
		timePatterns.add("h.m.s.SSS a");
		timePatterns.add("H.m.s.SSS");

	}

	// ============= DATETIME PATTERNS =============

	private static final List<String> datetimePatterns = new ArrayList<String>();

	static {
		datetimePatterns.add("yyyy-M-d'T'H:m:s");
	}

	private static final List<String> timeOffsetPatterns = new ArrayList<String>();

	static {
		/*
		 * matches all offsets X, XX, XXX, XXXX, XXXXX X just the hour, such as '+01' XX hour and minute,
		 * without a colon, such as '+0130' XXX hour and minute, with a colon, such as '+01:30' XXXX hour and
		 * minute and optional second, without a colon, such as '+013015' XXXXX hour and minute and optional
		 * second, with a colon, such as '+01:30:15'
		 */
		timeOffsetPatterns.add("X");
		timeOffsetPatterns.add("XX");
		timeOffsetPatterns.add("XXX");
		timeOffsetPatterns.add("XXXX");
		timeOffsetPatterns.add("XXXXX");
		timeOffsetPatterns.add(" X"); // with space between previous pattern and offset
		timeOffsetPatterns.add(" XX");
		timeOffsetPatterns.add(" XXX");
		timeOffsetPatterns.add(" XXXX");
		timeOffsetPatterns.add(" XXXXX");
	}

	/**
	 * Tries to infer the pattern of the input value in order to generate a date, so tries to parse it using
	 * date and datetime patterns (no time patterns since an input time value cannot be converted to a date)
	 * 
	 * @param value
	 * @return
	 */
	public static DateTimeFormatter inferFormatForDate(String value) {
		DateTimeFormatter formatter;
		formatter = inferDateFormat(value);
		if (formatter != null) {
			return formatter;
		}
		formatter = inferDatetimeFormat(value, false);
		if (formatter != null) {
			return formatter;
		}
		return null;
	}

	/**
	 * Tries to infer the pattern of the input value in order to generate a time, so tries to parse it using
	 * time and datetime patterns (no date patterns since an input date value cannot be converted to a time)
	 * 
	 * @param value
	 * @param onlyWithOffset
	 *            tells if this method should look only for pattern with timezone offset
	 * @return
	 */
	public static DateTimeFormatter inferFormatForTime(String value, boolean onlyWithOffset) {
		DateTimeFormatter formatter;
		formatter = inferTimeFormat(value, onlyWithOffset);
		if (formatter != null) {
			return formatter;
		}
		formatter = inferDatetimeFormat(value, onlyWithOffset);
		if (formatter != null) {
			return formatter;
		}
		return null;
	}

	/**
	 * Tries to infer the pattern of the input value in order to generate a datetime, so tries to parse it
	 * using date, time and datetime patterns
	 * 
	 * @param value
	 * @param onlyWithOffset
	 *            tells if this method should look only for pattern with timezone offset
	 * @return
	 */
	public static DateTimeFormatter inferFormatForDatetime(String value, boolean onlyWithOffset) {
		DateTimeFormatter formatter;
		if (!onlyWithOffset) {
			formatter = inferDateFormat(value);
			if (formatter != null) {
				return formatter;
			}
		}
		formatter = inferTimeFormat(value, onlyWithOffset);
		if (formatter != null) {
			return formatter;
		}
		formatter = inferDatetimeFormat(value, onlyWithOffset);
		if (formatter != null) {
			return formatter;
		}
		return null;
	}

	/**
	 * Tries to infer the date pattern for the given value, then returns a DateTimeFormatter with the inferred
	 * pattern. This method takes for granted that the input value is a date, so tries to parse it with only
	 * the available date patterns. Returns null if no pattern can parse the input.
	 * 
	 * @param value
	 * @return
	 */
	public static DateTimeFormatter inferDateFormat(String value) {
		if (value == null)
			return null;
		for (int i = 0; i < datePatternsDigit.size(); i++) {
			DateTimeFormatter formatter = new DateTimeFormatterBuilder()
					.appendPattern(datePatternsDigit.get(i)).toFormatter();
			try {
				formatter.parse(value);
				// System.out.println("Found pattern " + datePatternsDigit.get(i));
				return formatter;
			} catch (DateTimeParseException e) {
			}
		}
		// no compliant pattern found in digitsInputPattern
		for (int i = 0; i < datePatternsText.size(); i++) {
			Locale[] localeList = Locale.getAvailableLocales();
			for (int j = 0; j < localeList.length; j++) {
				DateTimeFormatter formatter = new DateTimeFormatterBuilder().parseCaseInsensitive()
						.appendPattern(datePatternsText.get(i)).toFormatter(localeList[j]);
				try {
					formatter.parse(value);
					return formatter;
				} catch (DateTimeParseException e) {
				}
			}
		}
		return null;
	}

	/**
	 * Tries to infer the time pattern for the given value, then returns a DateTimeFormatter with the inferred
	 * pattern. This method takes for granted that the input value is a time, so tries to parse it with only
	 * the available date patterns. Returns null if no pattern can parse the input.
	 * 
	 * @param value
	 * @param onlyWithOffset
	 *            tells if this method should look only for pattern with timezone offset
	 * @return
	 */
	public static DateTimeFormatter inferTimeFormat(String value, boolean onlyWithOffset) {
		if (value == null)
			return null;
		DateTimeFormatter formatter;
		for (int i = 0; i < timePatterns.size(); i++) {
			// with offset
			for (int j = 0; j < timeOffsetPatterns.size(); j++) {
				formatter = new DateTimeFormatterBuilder().appendPattern(timePatterns.get(i))
						.appendPattern(timeOffsetPatterns.get(j)).toFormatter();
				try {
					formatter.parse(value);
					// System.out.println("Found pattern " + (timePatterns.get(i) +
					// timeOffsetPatterns.get(j)));
					return formatter;
				} catch (DateTimeParseException e) {
				}
			}
			if (!onlyWithOffset) {
				formatter = new DateTimeFormatterBuilder().appendPattern(timePatterns.get(i)).toFormatter();
				try {
					formatter.parse(value);
					// System.out.println("Found pattern " + timePatterns.get(i));
					return formatter;
				} catch (DateTimeParseException e) {
				}
			}
		}
		return null;
	}

	/**
	 * Tries to infer the datetime pattern for the given value, then returns a DateTimeFormatter with the
	 * inferred pattern. This method takes for granted that the input value is a datetime, so tries to parse
	 * it with only the available datetime patterns. Returns null if no pattern can parse the input.
	 * 
	 * @param value
	 * @param onlyWithOffset
	 *            tells if this method should look only for pattern with timezone offset
	 * @return
	 */
	public static DateTimeFormatter inferDatetimeFormat(String value, boolean onlyWithOffset) {
		if (value == null)
			return null;
		DateTimeFormatter formatter;
		for (int i = 0; i < datetimePatterns.size(); i++) {
			// with offset
			for (int j = 0; j < timeOffsetPatterns.size(); j++) {
				formatter = new DateTimeFormatterBuilder().appendPattern(datetimePatterns.get(i))
						.appendPattern(timeOffsetPatterns.get(j)).toFormatter();
				try {
					formatter.parse(value);
					// System.out.println("Found pattern " + (datetimePatterns.get(i) +
					// timeOffsetPatterns.get(j)));
					return formatter;
				} catch (DateTimeParseException e) {
				}
			}
			if (!onlyWithOffset) {
				formatter = new DateTimeFormatterBuilder().appendPattern(datetimePatterns.get(i))
						.toFormatter();
				try {
					formatter.parse(value);
					// System.out.println("Found pattern " + datetimePatterns.get(i));
					return formatter;
				} catch (DateTimeParseException e) {
				}
			}
		}
		return null;
	}

	/**
	 * Applies an offset to a time
	 * 
	 * @param inputTime
	 *            input value used to get the offset in case of offset param is "reuse"
	 * @param formattedTime
	 *            time (already formatted according to ISO TIME) to which apply the offset
	 * @param offset
	 *            admitted values:
	 *            <li>undefined: the output time does not contain any offset, if the input value has offset it
	 *            is lost</li>
	 *            <li>Z: Zulu timezone</li>
	 *            <li>&lt;hh&gt;:&lt;mm&gt;: the given offset, specified in hours and minutes, is applied to
	 *            the input value, or added if the latter already contains an offset</li>
	 *            <li>reuse: is applied the same offset of the input</li>
	 * @return
	 * @throws ConverterConfigurationException
	 */
	public static String applyOffsetToTime(String inputTime, String formattedTime, String offset)
			throws ConverterConfigurationException {
		if (offset.equals(OFFSET_PARAM_UNDEFINED)) {
			return formattedTime;
		} else if (offset.equals(OFFSET_PARAM_REUSE)) {
			// reuse the offset of the input, so I need to infer it
			ZoneOffset zoneOffset = getOffsetFromTime(inputTime);
			if (zoneOffset == null) {// no offset found for the input time
				return formattedTime; // return same formatted time without offset
			} else {
				return formattedTime + zoneOffset;
			}
		} else if (offset.equals(OFFSET_PARAM_Z)) {
			return formattedTime + "Z";
		} else {
			// try to parse offset (in case offset is +/-HH:mm
			try {
				ZoneOffset parsedOffset = parseOffset(offset);
				return formattedTime + parsedOffset;
			} catch (DateTimeParseException e) {
				throw new ConverterConfigurationException("Invalid offset parameter '" + offset + "'");
			}
		}
	}

	/**
	 * Applies an offset to a datetime
	 * 
	 * @param inputDatetime
	 *            input value used to get the offset in case of offset param is "reuse"
	 * @param formattedDatetime
	 *            datetime (already formatted according to ISO DATETIME) to which apply the offset
	 * @param offset
	 *            admitted values:
	 *            <li>undefined: the output time does not contain any offset, if the input value has offset it
	 *            is lost</li>
	 *            <li>Z: Zulu timezone</li>
	 *            <li>&lt;hh&gt;:&lt;mm&gt;: the given offset, specified in hours and minutes, is applied to
	 *            the input value, or added if the latter already contains an offset</li>
	 *            <li>reuse: is applied the same offset of the input</li>
	 * @return
	 * @throws ConverterConfigurationException
	 */
	public static String applyOffsetToDatetime(String inputDatetime, String formattedDatetime, String offset)
			throws ConverterConfigurationException {
		if (offset.equals(OFFSET_PARAM_UNDEFINED)) {
			return formattedDatetime;
		} else if (offset.equals(OFFSET_PARAM_REUSE)) {
			// reuse the offset of the input, so I need to infer it
			ZoneOffset zoneOffset = getOffsetFromDatetime(inputDatetime);
			if (zoneOffset == null) {// no offset found for the input datetime
				return formattedDatetime; // return same formatted datetime without offset
			} else {
				return formattedDatetime + zoneOffset;
			}
		} else if (offset.equals(OFFSET_PARAM_Z)) {
			return formattedDatetime + "Z";
		} else {
			// try to parse offset (in case offset is +/-HH:mm
			try {
				ZoneOffset parsedOffset = parseOffset(offset);
				return formattedDatetime + parsedOffset;
			} catch (DateTimeParseException e) {
				throw new ConverterConfigurationException("Invalid offset parameter '" + offset + "'");
			}
		}
	}

	/**
	 * Tries to detect offset from time. Returns offset if it's found in the input time, null otherwise.
	 * 
	 * @param time
	 * @return
	 */
	public static ZoneOffset getOffsetFromTime(String time) {
		DateTimeFormatter formatterWithOffset = inferFormatForTime(time, true);
		if (formatterWithOffset == null) {// no formatter with offset found for the input time
			return null;
		} else {
			return ZoneOffset.from(formatterWithOffset.parse(time));
		}
	}

	/**
	 * Tries to detect offset from datetime. Returns offset if it's found in the input datetime, null
	 * otherwise.
	 * 
	 * @param time
	 * @return
	 */
	public static ZoneOffset getOffsetFromDatetime(String datetime) {
		DateTimeFormatter formatterWithOffset = inferFormatForDatetime(datetime, true);
		if (formatterWithOffset == null) {// no formatter with offset found for the input datetime
			return null;
		} else {
			return ZoneOffset.from(formatterWithOffset.parse(datetime));
		}
	}

	public static ZoneOffset parseOffset(String offset) {
		return ZoneOffset.from(DateTimeFormatter.ofPattern(OFFSET_PARAM_PATTERN).parse(offset));
	}

	public static ZoneOffset sumOffsets(ZoneOffset offset1, ZoneOffset offset2) {
		return ZoneOffset.ofTotalSeconds(offset1.getTotalSeconds() + offset2.getTotalSeconds());
	}
}
