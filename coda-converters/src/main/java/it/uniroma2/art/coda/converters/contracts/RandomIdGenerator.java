package it.uniroma2.art.coda.converters.contracts;

import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 * A converter compliant with the {@link RandomIdGenerator} contract generates resource identifiers in a
 * random manner, i.e. producing different outputs for the same arguments.
 * 
 * Its identifier is {@value #CONTRACT_URI}.
 * 
 */
@Description("Randomic converter. Generates a URI concatenating a prefix with a 8-digits hexadecimal randomic char sequence. The converter takes two input parameters:\n" +
		"\t- xRole: tells the nature of the resource. Available\n" +
		"\t- args: a map of further optional arguments. They depends on the xRole parameter as follow:\n" +
		"\t\t- concept (for skos:Concepts)\n" +
		"\t\t\t- label: the accompanying preferred label of the skos:Concept (or literal form of the accompanying xLabel)skos:Concept\n" +
		"\t\t\t- schemes: the concept schemes to which the concept is being attached at the moment of its creation (serialized as a Turtle collection)\n" +
		"\t\t- conceptScheme (for skos:ConceptSchemes)\n" +
		"\t\t\t- label: the accompanying preferred label of the skos:Concept (or literal form of the accompanying xLabel)\n" +
		"\t\t- skosCollection (for skos:Collections)\n" +
		"\t\t\t- label: the accompanying preferred label of the skos:Collection (or literal form of the accompanying xLabel)\n" +
		"\t\t- xLabel (for skosxl:Labelss)\n" +
		"\t\t\t- lexicalForm: the lexical form of the skosxl:Label\n" +
		"\t\t\t- lexicalizedResource: the resource to which the skosxl:Label will be attached to\n" +
		"\t\t\t- lexicalizationProperty: the property used for attaching the label\n" +
		"\t\t- xNote (for reified skos:notes)\n" +
		"\t\t\t- value: the content of the note\n" +
		"\t\t\t- annotatedResource: the resource being annotated\n" +
		"\t\t\t- noteProperty: the property used for annotation\n" +
		"\t\t- A custom prefix that will be placed befor the randomic sequence.")
public interface RandomIdGenerator extends Converter {

	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "randIdGen";

	public static class XRoles {
		public static final String CONCEPT = "concept";
		public static final String CONCEPTSCHEME = "conceptScheme";
		public static final String XLABEL = "xLabel";
		public static final String XNOTE = "xNote";
		public static final String SKOSCOLLECTION = "skosCollection";
	};

	public static class PARAMETERS {
		public static final String LABEL = "label";
		public static final String VALUE = "value";
		public static final String SCHEMES = "schemes";
		public static final String LEXICALFORM = "lexicalForm";
		public static final String LEXICALIZEDRESOURCE = "lexicalizedResource";
		public static final String ANNOTATEDRESOURCE = "annotatedResource";
		public static final String LEXICALIZATION_PROPERTY = "lexicalizationProperty";
		public static final String NOTE_PROPERTY = "noteProperty";
	};

	/**
	 * Returns a resource identifier produced randomly from the given inputs. The parameter {@code xRole}
	 * holds the nature of the resource that will be identified with the given URI. Depending on the value of
	 * the parameter {@code xRole}, a conforming converter may generate differently shaped URIs, possibly
	 * using specific arguments passed via the map {@code args}.
	 * 
	 * For each specific {@code xRole}, the client should provide some agreed-upon parameters to the
	 * converters. This contract defines the following parameters:
	 * 
	 * <ul>
	 * <li><code>concept</code> (for <code>skos:Concept</code>s)
	 * <ul>
	 * <li><code>label</code> (optional): the accompanying preferred label of the <i>skos:Concept</i> (or
	 * literal form of the accompanying xLabel)</li> <i>skos:Concept</i></li>
	 * <li><code>schemes</code> (optional): the concept schemes to which the concept is being
	 * attached at the moment of its creation (serialized as a Turtle collection)</li>
	 * </ul>
	 * </li>
	 * <li><code>conceptScheme</code> (for <code>skos:ConceptScheme</code>s)
	 * <ul>
	 * <li><code>label</code> (optional): the accompanying preferred label of the <i>skos:Concept</i> (or
	 * literal form of the accompanying xLabel)</li> <i>skos:Concept</i></li>
	 * </ul>
	 * </li>
	 * <li><code>skosCollection</code> (for <code>skos:Collection</code>s)
	 * <ul>
	 * <li><code>label</code> (optional): the accompanying preferred label of the <i>skos:Collection</i> (or
	 * literal form of the accompanying xLabel)</li>
	 * </ul>
	 * </li>
	 * <li><code>xLabel</code> (for <code>skosxl:Labels</code>s)
	 * <ul>
	 * <li><code>lexicalForm</code>: the lexical form of the <i>skosxl:Label</i></li>
	 * <li><code>lexicalizedResource</code>: the resource to which the <i>skosxl:Label</i> will be attached to
	 * </li>
	 * <li><code>lexicalizationProperty</code>: the property used for attaching the label</li>
	 * </ul>
	 * </li>
	 * <li><code>xNote</code></li> (for reified <code>skos:note</code>s)
	 * <ul>
	 * <li><code>value</code>: the content of the note</li>
	 * <li><code>annotatedResource</code>: the resource being annotated</li>
	 * <li><code>noteProperty</code>: the property used for annotation</li>
	 * </ul>
	 * </li></li>
	 * </ul>
	 *
	 * The parameters requested by additional <code>xRole</code>s are defined elsewhere by the party defining
	 * that <code>xRole</code>.
	 * 
	 * Users of this contract should always supply values for the required parameters associated with an
	 * <code>xRole</code>; therefore, they should not attempt to generate a URI for an <code>xRole</code>
	 * unless they known what arguments are requested.
	 * 
	 * Conversely, it is a duty of the specific converter implementing this contract to verify that all
	 * relevant information has been provided by the client. In fact, it is suggested that converters are
	 * implemented defensively, that is to say they should:
	 * 
	 * <ul>
	 * <li>complain only about the absence of absolutely required parameters</li>
	 * <li>handle unknown <code>xRole</code>s gracefully, by means some fallback strategy</li>
	 * </ul>
	 *
	 * @param ctx
	 *            the conversion context
	 * @param value
	 *            the value seeding the generation of the identifier (ignored in this case)
	 * @param xRole
	 *            an extension of the notion of role (see {@link RDFResourceRolesEnum}), allowing for finer
	 *            grained distinctions
	 * @param args
	 *            additional arguments, depending on the specific type of identifier
	 * @return
	 * @throws ConverterException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.IGNORED)
	IRI produceURI(CODAContext ctx, String value, @Parameter(name = "xRole") String xRole,
			@Parameter(name = "args") Map<String, Value> args) throws ConverterException;

	/**
	 * An overload of {@link #produceURI(CODAContext, String, String, Map)} without an explicit parameter map.
	 * 
	 * @param ctx
	 * @param value
	 * @param xRole
	 * @return
	 * @throws ConverterException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.IGNORED)
	IRI produceURI(CODAContext ctx, String value, @Parameter(name = "xRole") String xRole)
			throws ConverterException;

	/**
	 * An overload of {@link #produceURI(CODAContext, String, String, Map)} without an explicit xRole and
	 * parameter map.
	 * 
	 * @param ctx
	 * @param value
	 * @return
	 * @throws ConverterException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.IGNORED)
	IRI produceURI(CODAContext ctx, String value) throws ConverterException;

}
