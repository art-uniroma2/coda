package it.uniroma2.art.coda.converters.contracts;

/**
 * Constants related to the identification of the converters and their contracts.
 */
public interface ContractConstants {
	/**
	 * Base URI associated with standard contracts.
	 */
	String CODA_CONTRACTS_BASE_URI = "http://art.uniroma2.it/coda/contracts/";

	/**
	 * Base URI associated with standard converters.
	 */
	String CODA_CONVERTERS_BASE_URI = "http://art.uniroma2.it/coda/converters/";
}
