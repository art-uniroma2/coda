package it.uniroma2.art.coda.converters.contracts;

import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

import java.util.Map;

/**
 * A converter compliant with the {@link FormatterConverter} contract generates resource identifiers by replacing placeholder with values
 * passed as arguments
 *
 * Its identifier is {@value #CONTRACT_URI}.
 * 
 */
@Description("Generates a resource by compiling a <template> expression (1st parameter of the converter)." +
		"\nThe <template> is composed of placeholders that are replaced by values obtained from the other arguments passed to the converter, " +
		"in the same order of presentation. The arguments are processed in different way, depending on the name of the placeholder." +
		"\nThis is the list of available placeholders:" +
		"\n%s : string representation of the input. For IRIs = the string representation of the IRI, for literals it is the lexical form" +
		"\n%n : local name in case of IRI" +
		"\n%d : datatype IRI in case of literals" +
		"\n%l : lang in case of language tagged literal" +
		"\n!s : the value from the feature path or the previous converter in a converter chain")
public interface FormatterConverter extends Converter {

	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "formatter";


	/**
	 * Produce an IRI by replacing che placeholders in the input text with the values coming from the feature path
	 * and the input args
	 * @param ctx
	 * @param value
	 * @param newString
	 * @param args
	 * @return
	 * @throws ConverterConfigurationException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
	IRI produceURI(CODAContext ctx, String value, @Parameter(name = "template") String newString,
			@Parameter(name = "args")Value ... args) throws ConverterConfigurationException;


	/**
	 * Produce a Literal (with the given language and/or datatype) by replacing che placeholders in the input text with the values coming from the feature path
	 * and the input args
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value
	 * @param text
	 * @param args
	 * @return
	 * @throws ConverterConfigurationException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, @Parameter(name = "template") String text,
		@Parameter(name = "args")Value ... args) throws ConverterConfigurationException;
}
