package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.commons.DateTimeUtils;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.TimeConverter;
import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.pf4j.Extension;

import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;

/**
 * Implementation of the {@link TimeConverter} contract. Its identifier is {@value #CONVERTER_URI}.
 */
@Extension(points = {Converter.class})
public class TimeConverterImpl implements TimeConverter {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "time";


	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value)
			throws ConverterConfigurationException, ConversionException {
		if (value == null) {// input featurePath not provided => return current time
			String formattedTime = DateTimeFormatter.ofPattern(DateTimeUtils.TIME_PATTERN_ISO_8601)
					.format(LocalTime.now());
			return SimpleValueFactory.getInstance().createLiteral(formattedTime, XSD.TIME);
		}
		// otherwise try to infer format of value
		DateTimeFormatter inputFormatter = DateTimeUtils.inferFormatForTime(value, false);
		if (inputFormatter == null) {
			// input value not recognized with any pattern
			throw new ConverterConfigurationException("Cannot parse the input featurePath value " + value);
		} else {
			// format found, convert value into standard format
			try {
				// this code should never raise exception, since inputFormatter if not null, is for sure a
				// valid formatter
				TemporalAccessor parsedTime = inputFormatter.parse(value);
				String formattedTime = DateTimeFormatter.ofPattern(DateTimeUtils.TIME_PATTERN_ISO_8601)
						.format(parsedTime);
				return SimpleValueFactory.getInstance().createLiteral(formattedTime, XSD.TIME);
			} catch (DateTimeParseException e) {
				throw new ConversionException(e);
			}
		}
	}

	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value,
			String offset) throws ConverterConfigurationException, ConversionException {
		DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(DateTimeUtils.TIME_PATTERN_ISO_8601);
		if (value == null) {// input featurePath not provided => return current time
			String formattedTime = outputFormatter.format(LocalTime.now());
			String timeWithOffset = DateTimeUtils.applyOffsetToTime(value, formattedTime, offset);
			return SimpleValueFactory.getInstance().createLiteral(timeWithOffset, XSD.TIME);
		}
		// otherwise try to infer format of value
		DateTimeFormatter inputFormatter = DateTimeUtils.inferFormatForTime(value, false);
		if (inputFormatter == null) {
			// input value not recognized with any pattern
			throw new ConverterConfigurationException("Cannot parse the input featurePath value " + value);
		} else {// format found, convert value into standard format
			try {
				// this code should never raise exception, since inputFormatter if not null, is for sure a
				// valid formatter
				TemporalAccessor parsedTime = inputFormatter.parse(value);
				String formattedTime = outputFormatter.format(parsedTime);
				String timeWithOffset = DateTimeUtils.applyOffsetToTime(value, formattedTime, offset);
				return SimpleValueFactory.getInstance().createLiteral(timeWithOffset, XSD.TIME);
			} catch (DateTimeParseException e) {
				throw new ConversionException(e);
			}
		}
	}

	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value,
			String offset, String additionalOffset)
					throws ConverterConfigurationException, ConversionException {
		// Checks: value not null, offset param not "undefined" or "Z", additionalOffset valid (format
		// +/-HH:mm)
		if (value == null) {
			throw new ConverterConfigurationException("This converter with the additional offset "
					+ additionalOffset + " cannot be used without input value");
		}
		if (offset.equals(DateTimeUtils.OFFSET_PARAM_UNDEFINED)
				|| offset.equals(DateTimeUtils.OFFSET_PARAM_Z)) {
			throw new ConverterConfigurationException("This converter with the additional offset "
					+ additionalOffset + " cannot be used with offset " + offset);
		}
		ZoneOffset additionalZoneOffset;
		try {
			additionalZoneOffset = DateTimeUtils.parseOffset(additionalOffset);
		} catch (DateTimeParseException e) {
			throw new ConverterConfigurationException("Invalid offset parameter '" + additionalOffset + "'");
		}
		// params checks passed
		DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(DateTimeUtils.TIME_PATTERN_ISO_8601);
		DateTimeFormatter inputFormatter = DateTimeUtils.inferFormatForTime(value, false);
		TemporalAccessor formattedTime = inputFormatter.parse(value);
		if (offset.equals(DateTimeUtils.OFFSET_PARAM_REUSE)) {
			DateTimeFormatter formatterWithOffset = DateTimeUtils.inferFormatForTime(value, true);
			if (formatterWithOffset == null) {// no formatter with offset found for the input time => input
												// has no offset
				// simply add the additional offset to the input time
				String timeWithOffset = DateTimeUtils.applyOffsetToTime(value,
						outputFormatter.format(formattedTime), additionalOffset);
				return SimpleValueFactory.getInstance().createLiteral(timeWithOffset, XSD.TIME);
			} else { // formatter with offset found for the input time => input has offset
				// sum the offset of the input with the additional one
				ZoneOffset inputOffset = DateTimeUtils.getOffsetFromTime(value);
				ZoneOffset totalOffset = DateTimeUtils.sumOffsets(inputOffset, additionalZoneOffset);
				String timeWithOffset = DateTimeUtils.applyOffsetToTime(value,
						outputFormatter.format(formattedTime), totalOffset + "");
				return SimpleValueFactory.getInstance().createLiteral(timeWithOffset, XSD.TIME);
			}
		} else {
			try {
				ZoneOffset zoneOffsetParam = DateTimeUtils.parseOffset(offset);
				ZoneOffset totalOffset = DateTimeUtils.sumOffsets(zoneOffsetParam, additionalZoneOffset);
				String timeWithOffset = DateTimeUtils.applyOffsetToTime(value,
						outputFormatter.format(formattedTime), totalOffset + "");
				return SimpleValueFactory.getInstance().createLiteral(timeWithOffset, XSD.TIME);
			} catch (DateTimeParseException e) {
				throw new ConverterConfigurationException("Invalid offset parameter '" + offset + "'");
			}
		}
	}

}
