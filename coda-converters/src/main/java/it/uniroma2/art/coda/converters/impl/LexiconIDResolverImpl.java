package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.LexiconIDResolverConverter;
import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.MultipleResourcesRetrieved;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.pf4j.Extension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@Extension(points = {Converter.class})
public class LexiconIDResolverImpl implements LexiconIDResolverConverter {

    //private static final String RDFS_MODEL = "RDFS";
    //private static final String SKOS_MODEL = "SKOS";
    //private static final String SKOSXL_MODEL = "SKOSXL";
    //private static final String ONTOLEX_MODEL = "ONTOLEX";
    //private static final String ALL_MODEL = "ALL";
    //private static final String CTX_MODEL = "CTX";

    private static final UIMACODAUtilities.LexModelForSearch DEFAULT_LEX_MODEL_OPTION = UIMACODAUtilities.LexModelForSearch.CTX_MODEL;


    public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "lexiconIDResolver";

    public static final String MAIL_REGEX = "[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*"
            + "@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";


    @Override
    public IRI produceURI(CODAContext ctx, String value, Value object) throws MultipleResourcesRetrieved, ConverterConfigurationException {
        return produceURI(ctx, value, object, DEFAULT_LEX_MODEL_OPTION.getLexModelForSearch());
    }

    @Override
    public IRI produceURI(CODAContext ctx, String value, Value object, String lexModel)
            throws MultipleResourcesRetrieved, ConverterConfigurationException {
        return produceURI(ctx, value, object, lexModel, null);
    }

    @Override
    public IRI produceURI(CODAContext ctx, String value, Value object, String lexModel, IRI fallbackIRI)
            throws MultipleResourcesRetrieved, ConverterConfigurationException {

        //check that the lexModel is one of the supported one
        if (!lexModel.equals(UIMACODAUtilities.LexModelForSearch.RDFS_MODEL.getLexModelForSearch()) &&
                !lexModel.equals(UIMACODAUtilities.LexModelForSearch.SKOS_MODEL.getLexModelForSearch()) &&
                !lexModel.equals(UIMACODAUtilities.LexModelForSearch.SKOSXL_MODEL.getLexModelForSearch()) &&
                !lexModel.equals(UIMACODAUtilities.LexModelForSearch.ONTOLEX_MODEL.getLexModelForSearch()) &&
                !lexModel.equals(UIMACODAUtilities.LexModelForSearch.ALL_MODEL.getLexModelForSearch()) &&
                !lexModel.equals(UIMACODAUtilities.LexModelForSearch.CTX_MODEL.getLexModelForSearch())) {
            String msg = lexModel + " is not a supported option for the Lexical Model parameter to use. Please one of:"+
                    UIMACODAUtilities.LexModelForSearch.RDFS_MODEL.getLexModelForSearch()+", "+
                    UIMACODAUtilities.LexModelForSearch.SKOS_MODEL.getLexModelForSearch()+", "+
                    UIMACODAUtilities.LexModelForSearch.SKOSXL_MODEL.getLexModelForSearch()+", "+
                    UIMACODAUtilities.LexModelForSearch.ALL_MODEL.getLexModelForSearch()+", "+
                    UIMACODAUtilities.LexModelForSearch.CTX_MODEL.getLexModelForSearch()+", ";
            throw new ConverterConfigurationException(msg);
        }

        String propPath;
        if (lexModel.equals(UIMACODAUtilities.LexModelForSearch.RDFS_MODEL.getLexModelForSearch())) {
            // use rdfs:label
            propPath = getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch.RDFS_MODEL);
        } else if (lexModel.equals(UIMACODAUtilities.LexModelForSearch.SKOS_MODEL.getLexModelForSearch())) {
            //use skos:prefLabel
            propPath = getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch.SKOS_MODEL);
        } else if (lexModel.equals(UIMACODAUtilities.LexModelForSearch.SKOSXL_MODEL.getLexModelForSearch())) {
            propPath = getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch.SKOSXL_MODEL);
        } else if ((lexModel.equals(UIMACODAUtilities.LexModelForSearch.ONTOLEX_MODEL.getLexModelForSearch()))) {
            //currently, not supported
            throw new ConverterConfigurationException(UIMACODAUtilities.LexModelForSearch.ONTOLEX_MODEL.getLexModelForSearch()+" is currently not supported as the Lexical path to use");
        } else if (lexModel.equals(UIMACODAUtilities.LexModelForSearch.ALL_MODEL.getLexModelForSearch())) {
            propPath = getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch.ALL_MODEL);
        } else { // CTX_MODEL
            UIMACODAUtilities.LexModelForSearch lexModelForSearch = ctx.getLexModelForSearch();
            propPath = getPredFromLexModelForSearch(lexModelForSearch);
        }

        //execute a SPARQL query, using the sparqlPropPath, as the PropertyPath and the value in objValue as the Object
        // of such SPARQL triple, trying to obtain a value for the Subject

        RepositoryConnection conn = ctx.getRepositoryConnection();

        // get the prefix-namespace from the CODAContext
        Map<String, String> prefixToNamespaceMap =
                ctx.getPlaceholderStruct().getOwnerRule().getProjectionRulesModel().getPrefixToNamespaceMap();
        StringBuilder sb = new StringBuilder();
        for (String prefix : prefixToNamespaceMap.keySet()) {
            sb.append("PREFIX "+prefix+": <"+prefixToNamespaceMap.get(prefix)+">\n");
        }

        //create the text for the SPARQL query
        sb.append("SELECT ?resource \n").append("WHERE {\n").append("?resource "+propPath+" "+normalizeObj(NTriplesUtil.toNTriplesString(object)))
                .append(" .\n").append("}");

        String query = sb.toString();

        TupleQuery tupleQuery = conn.prepareTupleQuery(query);
        TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
        IRI resource = null;
        while (tupleQueryResult.hasNext()) {
            if (resource!=null) {
                // multiple resource were retrieved, so throw an exception
                throw new MultipleResourcesRetrieved(propPath, NTriplesUtil.toNTriplesString(object));
            }
            resource = (IRI) tupleQueryResult.next().getValue("resource");
        }

        if (resource!=null) {
            // a resource was retrieved from the dataset, so return it
            return resource;
        }

        if (fallbackIRI!=null) {
            //since no resouce was found and the input fallbackIRI is not null, return fallbackIRI
            return fallbackIRI;
        }

        // fallbackIRI is null, so use the input value

        ValueFactory vf = SimpleValueFactory.getInstance();
        try {
            new URL(value);//if this doesn't throw exception, then the value is already a URL
            return vf.createIRI(value);
        } catch (MalformedURLException e) {
            try { //not a URL, try to parse as NTriple IRI
                return NTriplesUtil.parseURI(value, vf);
            } catch (IllegalArgumentException e1) { //not NTriple
                if (value.matches(MAIL_REGEX)){
                    return vf.createIRI("mailto:"+value);
                } else {
                    value = value.replaceAll("\\s","_"); //sanitize the input value
                    return vf.createIRI(ctx.getDefaultNamespace()+value);
                }
            }
        }
    }



    private String normalizeObj(String objInput) {
        return objInput;
    }

    
    private String getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch lexModelForSearch) throws ConverterConfigurationException {
        if (lexModelForSearch.equals(UIMACODAUtilities.LexModelForSearch.RDFS_MODEL)) {
            // use rdfs:label
            return  "<http://www.w3.org/2000/01/rdf-schema#label>";
        } else if (lexModelForSearch.equals(UIMACODAUtilities.LexModelForSearch.SKOS_MODEL)) {
            //use skos:prefLabel
            return "<http://www.w3.org/2004/02/skos/core#prefLabel>";
        } else if (lexModelForSearch.equals(UIMACODAUtilities.LexModelForSearch.SKOSXL_MODEL)) {
            return "<http://www.w3.org/2008/05/skos-xl#prefLabel>/<http://www.w3.org/2008/05/skos-xl#literalForm>";
        } else if ((lexModelForSearch.equals(UIMACODAUtilities.LexModelForSearch.ONTOLEX_MODEL))) {
            //currently, not supported
            throw new ConverterConfigurationException(UIMACODAUtilities.LexModelForSearch.ONTOLEX_MODEL.getLexModelForSearch()+" is currently not supported as the Lexical path to use");
        } else { // UIMACODAUtilities.LexModelForSearch.ALL_MODEL
            return "(" +
                    getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch.RDFS_MODEL) +
                    " | " +
                    getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch.SKOS_MODEL) +
                    " | " +
                    getPredFromLexModelForSearch(UIMACODAUtilities.LexModelForSearch.SKOSXL_MODEL) +
                    ")";
        }
    }

}
