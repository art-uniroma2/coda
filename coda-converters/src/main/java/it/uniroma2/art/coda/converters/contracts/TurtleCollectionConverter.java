package it.uniroma2.art.coda.converters.contracts;

import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.DatatypeCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 *  TURTLE collection converter contract. Its identifier is {@value #CONTRACT_URI}.
 */
@RDFCapability(RDFCapabilityType.literal)
@DatatypeCapability("http://www.w3.org/2001/XMLSchema#string")
@Description("Produces the TURTLE serialization (as an xsd:string) of the collection formed by the provided items")
public interface TurtleCollectionConverter extends Converter {

	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "turtleCollection";

	/**
	 * Produces the TURTLE serialization (as an xsd:string) of the collection formed by the provided items.
	 * 
	 * @param ctx
	 * @return
	 * @throws ConversionException
	 * @throws ConverterConfigurationException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.IGNORED)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, Value... items);

}
