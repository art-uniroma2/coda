package it.uniroma2.art.coda.converters.utils.template;

public class ValueTemplate implements TemplateInterface {

    private String value;
    private String modifier = null; // it can be: null, "U" or "l"

    public ValueTemplate(String value) {
        this.value = value;
    }

    public ValueTemplate(String value, String modifier) {
        this.value = value;
        this.modifier = modifier;
    }

    public String getValue() {
        return value;
    }

    public String getModifier() {
        return modifier;
    }
}
