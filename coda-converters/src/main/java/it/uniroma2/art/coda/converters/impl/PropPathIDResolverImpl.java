package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.PropPathIDResolverConverter;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.MultipleResourcesRetrieved;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.pf4j.Extension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@Extension(points = {Converter.class})
public class PropPathIDResolverImpl implements PropPathIDResolverConverter {

    public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "propPathIDResolver";

    public static final String MAIL_REGEX = "[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*"
            + "@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";


    @Override
    public IRI produceURI(CODAContext ctx, String value, Value object, String propPath)
            throws MultipleResourcesRetrieved, ConverterConfigurationException {
        return produceURI(ctx, value, object, propPath, null);
    }

    @Override
    public IRI produceURI(CODAContext ctx, String value, Value object, String propPath, IRI fallbackIRI)
            throws MultipleResourcesRetrieved, ConverterConfigurationException {

        //execute a SPARQL query, using the sparqlPropPath, as the PropertyPath and the value in objValue as the Object
        // of such SPARQL triple, trying to obtain a value for the Subject

        RepositoryConnection conn = ctx.getRepositoryConnection();

        // get the prefix-namespace from the CODAContext
        Map<String, String> prefixToNamespaceMap =
                ctx.getPlaceholderStruct().getOwnerRule().getProjectionRulesModel().getPrefixToNamespaceMap();
        StringBuilder sb = new StringBuilder();
        for (String prefix : prefixToNamespaceMap.keySet()) {
            sb.append("PREFIX "+prefix+": <"+prefixToNamespaceMap.get(prefix)+">\n");
        }

        //create the text for the SPARQL query
        sb.append("SELECT ?resource \n").append("WHERE {\n").append("?resource "+propPath+" "+normalizeObj(NTriplesUtil.toNTriplesString(object)))
                .append(" .\n").append("}");

        String query = sb.toString();

        TupleQuery tupleQuery = conn.prepareTupleQuery(query);
        TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
        IRI resource = null;
        while (tupleQueryResult.hasNext()) {
            if (resource!=null) {
                // multiple resource were retrieved, so throw an exception
                throw new MultipleResourcesRetrieved(propPath, NTriplesUtil.toNTriplesString(object));
            }
            resource = (IRI) tupleQueryResult.next().getValue("resource");
        }

        if (resource!=null) {
            // a resource was retrieved from the dataset, so return it
            return resource;
        }

        if (fallbackIRI!=null) {
            //since no resouce was found and the input fallbackIRI is not null, return fallbackIRI
            return fallbackIRI;
        }

        // fallbackIRI is null, so use the input value

        ValueFactory vf = SimpleValueFactory.getInstance();
        try {
            new URL(value);//if this doesn't throw exception, then the value is already a URL
            return vf.createIRI(value);
        } catch (MalformedURLException e) {
            try { //not a URL, try to parse as NTriple IRI
                return NTriplesUtil.parseURI(value, vf);
            } catch (IllegalArgumentException e1) { //not NTriple
                if (value.matches(MAIL_REGEX)){
                    return vf.createIRI("mailto:"+value);
                } else {
                    value = value.replaceAll("\\s","_"); //sanitize the input value
                    return vf.createIRI(ctx.getDefaultNamespace()+value);
                }
            }
        }
    }


    private String normalizeObj(String objInput) {
        return objInput;
    }


}
