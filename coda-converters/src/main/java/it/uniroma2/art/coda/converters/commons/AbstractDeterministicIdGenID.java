package it.uniroma2.art.coda.converters.commons;

import it.uniroma2.art.coda.interfaces.CODAContext;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Base class of deterministic random id generators. The main characteristics of these generators is that they
 * compute pseudo-random values, which can be reproduced deterministically based on the input provided to the
 * converter. Using the approximated formula from Wikipedia (http://en.wikipedia.org/wiki/Birthday_problem),
 * (under the assumption that the output of an HMAC is uniformly distributed) we computed that 16 hex digits
 * provide less than 0.1% probability of a collision in 100 * 10^6 generated ID:
 * 
 * hex digits = ln(-((100*10^6) * (100*10^6 - 1)) / (2 * ln(1- 0.001))) / ln(16)
 */
public abstract class AbstractDeterministicIdGenID {

	private static final byte[] KEY_BYTES = { (byte) 0x9c, (byte) 0xf5, (byte) 0xe3, (byte) 0xac, (byte) 0x9b,
			(byte) 0xc3, (byte) 0x7a, (byte) 0x52, (byte) 0x30, (byte) 0x24, (byte) 0xa8, (byte) 0x84,
			(byte) 0xa0, (byte) 0x3e, (byte) 0x7f, (byte) 0x62, (byte) 0x41, (byte) 0xad, (byte) 0x26,
			(byte) 0xb8 };

	private String prefix;
	private int trunc;

	public AbstractDeterministicIdGenID(String prefix, int trunc) {
		this.prefix = prefix;
		this.trunc = trunc;
	}

	public IRI produceURI(CODAContext ctx, String value) {
		try {

			SecretKeySpec key = new SecretKeySpec(KEY_BYTES, "HmacSHA1");

			Mac mac = Mac.getInstance("HmacSHA1");
			try {
				mac.init(key);
			} catch (InvalidKeyException e) {
				throw new RuntimeException("This should have never been happened", e);
			}

			byte[] digest = mac.doFinal(value.getBytes());

			if (trunc != 4 && trunc != 8 && trunc != 12)
				trunc = 16;

			String randomId = HEXUtils.toHexString(digest, 0, trunc / 2);

			return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace() + prefix + randomId);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(
					"This should never happen, since implementations are required to provide SHA-1", e);
		}
	}

}
