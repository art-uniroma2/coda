package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.DefaultConverter;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.pf4j.Extension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

//@Converter(contract=DefaultContract.class)
@Extension(points = {Converter.class})
public class DefaultConverterImpl implements DefaultConverter {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "default";
	
	public static final String MAIL_REGEX = "[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*"
			+ "@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";

	@Override
	public IRI produceURI(CODAContext ctx, String value) {
		/*
		 * 3 scenarios for input value:
		 * - it is an IRI (like "http://baseuri/localName") => creates an rdf4j IRI with value as IRI
		 * - it is an NT-serialized IRI (like "<http://baseuri/localName>") => creates an rdf4j IRI parsing the value
		 * - none of the previous => creates an rdf4j IRI appending (sanitized) value to the default namespace
		 */
		ValueFactory vf = SimpleValueFactory.getInstance();
		try {
			new URL(value);//if this doesn't throw exception, then the value is already a URL
			return vf.createIRI(value);
		} catch (MalformedURLException e) {
			try { //not a URL, try to parse as NTriple IRI
				return NTriplesUtil.parseURI(value, vf);
			} catch (IllegalArgumentException e1) { //not NTriple
				if (value.matches(MAIL_REGEX)){
					return vf.createIRI("mailto:"+value);
				} else {
					value = value.replaceAll("\\s","_"); //sanitize the input value
					return vf.createIRI(ctx.getDefaultNamespace()+value);
				}
			}
		}
	}

	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) {
		/*
		 * 2 scenario for input value:
		 * - it is an NT-serialized Literal (like "example"@en) => creates an rdf4j Literal parsing the value
		 * 	 and check compatibility with the input datatype or language
		 * - it is a simple String => creates an rdf4j with the value as label and with the given lang/datatype
		 */
		ValueFactory vf = SimpleValueFactory.getInstance();
		Literal literal;
		try { //first try to parse value as NT-serialized literal
			literal = NTriplesUtil.parseLiteral(value, vf);
		} catch (IllegalArgumentException e) { //parsing failed => create literal with the given value as label
			if (datatype != null) {
				return vf.createLiteral(value, vf.createIRI(datatype));
			} else if (lang != null) {
				return vf.createLiteral(value, lang);
			} else {
				return vf.createLiteral(value);
			}
		}
		/*
		 * NT parsing succeed.
		 * Check if the language or datatype (if any) of the parsed Literal are compliant with the input parameters.
		 * If both lang and datatype are not provided to the converter => return the parsed literal as it is
		 */
		if (lang != null) { //lang provided to the converter
			Optional<String> parsedLang = literal.getLanguage();
			if (parsedLang.isPresent()) { //provided also to the input value, cross check them
				if (!lang.equals(parsedLang.get())) {
					throw new IllegalArgumentException("Converter language (" + lang +
							") is not compliant with the language of the input value (" + value + ")");
				}
			} else { //not provided to input value => set the one of the converter
				literal = vf.createLiteral(literal.getLabel(), lang);
			}
		} else if (datatype != null) { //datatype provided to the converter
			IRI parsedDt = literal.getDatatype();
			if (parsedDt.equals(XSD.STRING)) {
				//xsd:string is the default dt assigned by RDFJ => prioritize the datatype provided to converter
				literal = vf.createLiteral(literal.getLabel(), vf.createIRI(datatype));
			} else if (!datatype.equals(parsedDt.stringValue())) {
				throw new IllegalArgumentException("Converter datatype (" + datatype +
						") is not compliant with the datatype of the input value (" + value + ")");
			}
		}
		return literal;
	}
}
