package it.uniroma2.art.coda.converters.commons;

import it.uniroma2.art.coda.interfaces.CODAContext;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import java.util.UUID;

/**
 * Base class of random id generators. The main characteristics of these generators is that they ignore the
 * value provided as input to the converter, which produces non reproducible random values. Using the
 * approximated formula from Wikipedia (http://en.wikipedia.org/wiki/Birthday_problem), (under the assumption
 * that the output of an HMAC is uniformly distributed) we computed that 16 hex digits provide less than 0.1%
 * probability of a collision in 100 * 10^6 generated ID:
 * 
 * hex digits = ln(-((100*10^6) * (100*10^6 - 1)) / (2 * ln(1- 0.001))) / ln(16)
 */
public abstract class AbstractRandomIdGenID {

	private String prefix;
	private int trunc;

	public AbstractRandomIdGenID(String prefix, int trunc) {
		this.prefix = prefix;
		this.trunc = trunc;
	}

	public IRI produceURI(CODAContext ctx, String value) {
		UUID uuid = UUID.randomUUID();
		String rawSequence = uuid.toString().replace("-", "");

		String selectedDigits = "";
		// Extract trunc hex digits (ignoring digits with at least partially fixed content)
		if (trunc == 4 || trunc == 8 || trunc == 12) {
			selectedDigits = rawSequence.substring(0, trunc);
		} else { // default case: 16 digits
			selectedDigits = rawSequence.substring(0, 12) + rawSequence.substring(13, 16)
					+ rawSequence.substring(17, 18);
		}

		return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace() + prefix + selectedDigits);
	}

}
