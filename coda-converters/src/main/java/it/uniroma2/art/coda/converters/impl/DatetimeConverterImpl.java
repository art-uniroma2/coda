package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.commons.DateTimeUtils;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.DatetimeConverter;
import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.pf4j.Extension;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;

/**
 * Implementation of {@link DatetimeConverter}. Its identifier is {@value #CONVERTER_URI}.
 */
@Extension(points = {Converter.class})
public class DatetimeConverterImpl implements DatetimeConverter {
	
public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "datetime";
	
	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) throws ConverterConfigurationException, ConversionException {
		if (value == null) {//input featurePath not provided => return current datetime
			String formattedTime = DateTimeFormatter.ofPattern(DateTimeUtils.DATETIME_PATTERN_ISO_8601).format(LocalDateTime.now());
			return SimpleValueFactory.getInstance().createLiteral(formattedTime, XSD.DATETIME);
		}
		// otherwise try to infer format of value
		System.out.println("datetime with input " + value);
		DateTimeFormatter inputFormatter = DateTimeUtils.inferFormatForDatetime(value, false);
		if (inputFormatter == null) {
			// input value not recognized with any pattern
			throw new ConverterConfigurationException("Cannot parse the input featurePath value " + value);
		} else {
			// format found, convert value into standard format
			try {
				// this code should never raise exception, since inputFormatter if not null, is for sure a valid formatter
				TemporalAccessor parsedDatetime = inputFormatter.parse(value);
				String formattedDatetime = DateTimeFormatter.ofPattern(DateTimeUtils.DATETIME_PATTERN_ISO_8601).format(parsedDatetime);
				return SimpleValueFactory.getInstance().createLiteral(formattedDatetime, XSD.DATETIME);
			} catch (DateTimeParseException e) {
				throw new ConversionException(e);
			}
		}
	}

	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value,
			String offset) throws ConverterConfigurationException, ConversionException {
		DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(DateTimeUtils.DATETIME_PATTERN_ISO_8601);
		if (value == null) {//input featurePath not provided => return current time
			String formattedTime = outputFormatter.format(LocalDateTime.now());
			String timeWithOffset = DateTimeUtils.applyOffsetToDatetime(value, formattedTime, offset);
			return SimpleValueFactory.getInstance().createLiteral(timeWithOffset, XSD.DATETIME);
		}
		// otherwise try to infer format of value
		DateTimeFormatter inputFormatter = DateTimeUtils.inferFormatForDatetime(value, false);
		if (inputFormatter == null) {
			// input value not recognized with any pattern
			throw new ConverterConfigurationException("Cannot parse the input featurePath value " + value);
		} else {//format found, convert value into standard format
			try {
				// this code should never raise exception, since inputFormatter if not null, is for sure a valid formatter
				TemporalAccessor parsedDatetime = inputFormatter.parse(value);
				String formattedDatetime = outputFormatter.format(parsedDatetime);
				String datetimeWithOffset = DateTimeUtils.applyOffsetToDatetime(value, formattedDatetime, offset);
				return SimpleValueFactory.getInstance().createLiteral(datetimeWithOffset, XSD.DATETIME);
			} catch (DateTimeParseException e) {
				throw new ConversionException(e);
			}
		}
	}
	
	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, String offset, String additionalOffset)
			throws ConverterConfigurationException, ConversionException {
		//Checks: value not null, offset param not "undefined" or "Z", additionalOffset valid (format +/-HH:mm)
		if (value == null) {
			throw new ConverterConfigurationException("This converter with the additional offset "
					+ additionalOffset + " cannot be used without input value");
		}
		if (offset.equals(DateTimeUtils.OFFSET_PARAM_UNDEFINED) || offset.equals(DateTimeUtils.OFFSET_PARAM_Z)) {
			throw new ConverterConfigurationException("This converter with the additional offset "
					+ additionalOffset + " cannot be used with offset " + offset);
		}
		ZoneOffset additionalZoneOffset;
		try {
			additionalZoneOffset = DateTimeUtils.parseOffset(additionalOffset);
		} catch (DateTimeParseException e) {
			throw new ConverterConfigurationException("Invalid offset parameter '" + additionalOffset + "'");
		}
		//params checks passed 
		DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(DateTimeUtils.DATETIME_PATTERN_ISO_8601);
		DateTimeFormatter inputFormatter = DateTimeUtils.inferFormatForDatetime(value, false);
		TemporalAccessor formattedDatetime = inputFormatter.parse(value);
		if (offset.equals(DateTimeUtils.OFFSET_PARAM_REUSE)) {
			DateTimeFormatter formatterWithOffset = DateTimeUtils.inferFormatForDatetime(value, true);
			if (formatterWithOffset == null) {//no formatter with offset found for the input datetime => input has no offset
				//simply add the additional offset to the input datetime
				String datetimeWithOffset = DateTimeUtils.applyOffsetToDatetime(value, outputFormatter.format(formattedDatetime), additionalOffset);
				return SimpleValueFactory.getInstance().createLiteral(datetimeWithOffset, XSD.DATETIME);
			} else { //formatter with offset found for the input datetime => input has offset
				//sum the offset of the input with the additional one
				ZoneOffset inputOffset = DateTimeUtils.getOffsetFromDatetime(value);
				ZoneOffset totalOffset = DateTimeUtils.sumOffsets(inputOffset, additionalZoneOffset);
				String datetimeWithOffset = DateTimeUtils.applyOffsetToDatetime(value, outputFormatter.format(formattedDatetime), totalOffset+"");
				return SimpleValueFactory.getInstance().createLiteral(datetimeWithOffset, XSD.DATETIME);
			}
		} else {
			try {
				ZoneOffset zoneOffsetParam = DateTimeUtils.parseOffset(offset);
				ZoneOffset totalOffset = DateTimeUtils.sumOffsets(zoneOffsetParam, additionalZoneOffset);
				String timeWithOffset = DateTimeUtils.applyOffsetToDatetime(value, outputFormatter.format(formattedDatetime), totalOffset+"");
				return SimpleValueFactory.getInstance().createLiteral(timeWithOffset, XSD.DATETIME);
			} catch (DateTimeParseException e) {
				throw new ConverterConfigurationException("Invalid offset parameter '" + offset + "'");
			}
		}
	}

}
