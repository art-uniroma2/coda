package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.LangStringConverter;
import it.uniroma2.art.coda.interfaces.CODAContext;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.pf4j.Extension;

/**
 * Implementation of {@link LangStringConverter}. Its identifier is {@value #CONVERTER_URI}.
 */
@Extension(points = {Converter.class})
public class LangStringConverterImpl implements LangStringConverter {
	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "langString";


	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value,
			String langArg) {

		return SimpleValueFactory.getInstance().createLiteral(value, langArg);
	}

	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) {

		ValueFactory vf = SimpleValueFactory.getInstance();
		Literal literal;
		try { //try to parse value as NT-serialized literal
			literal = NTriplesUtil.parseLiteral(value, vf);
		} catch (IllegalArgumentException e) { //parsing failed, so throw an exception
			throw new IllegalArgumentException("Converter langString is not compliant with the input value (" + value + ")");
		}

		//check that the created literal has a language
		if (literal.getLanguage().isEmpty()) {
			//it has no language, so throw an exception
			throw new IllegalArgumentException("Converter langString is not compliant with the input value (" + value
					+ ") since it should have a language");
		}

		return literal;
	}


}
