package it.uniroma2.art.coda.converters.contracts;

import org.eclipse.rdf4j.model.Literal;

import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.DatatypeCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 * Date-time converter contract. Its identifier is {@value #CONTRACT_URI}.
 */
@RDFCapability(RDFCapabilityType.literal)
//@DatatypeCapability({ XmlSchema.DATETIME })
@DatatypeCapability("http://www.w3.org/2001/XMLSchema#dateTime")
@Description("Generates a literal with datatype xsd:dateTime. " +
		"The input value is parsed (compatibly with a set of recognized patterns) " +
		"and is formatted according to the standard format (ISO 8601) yyyy-MM-ddThh:mm:ss. " +
		"If no input value is provided, the converter generates the current datetime. " +
		"If the input value cannot be parsed, the converter throws a ConverterConfigurationException.\n" +
		"The converter takes the same optional parameters of the coda:time converter.")
public interface DatetimeConverter extends Converter {
	
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "datetime";

	/**
	 * Gets a value and returns it as typed literal (xsd:datetime) formatted according to the datetime
	 * pattern (yyyy-MM-dd'T'HH:mm:ss). If value is not provided returns the current datetime.
	 * 
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value
	 * @return
	 * @throws ConverterConfigurationException 
	 * @throws ConversionException 
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) throws ConverterConfigurationException, ConversionException;

	/**
	 * Gets a value and returns it as typed literal (xsd:datetime) formatted according to the datetime
	 * pattern (yyyy-MM-dd'T'HH:mm:ss) and with the given offset.
	 * If value is not provided returns the current datetime.
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value
	 * @param offset admitted values:
	 * <li>undefined: the output time does not contain any offset, if the input value has timezone it is lost</li>
	 * <li>Z: Zulu timezone</li>
	 * <li>&lt;hh&gt;:&lt;mm&gt;: the given offset, specified in hours and minutes, 
	 * 	is applied to the input value, or replaced if the latter already contains a timezone</li>
	 * <li>reuse: is applied the same offset of the input</li>
	 * 
	 * @throws ConverterConfigurationException
	 * @throws ConversionException 
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, @Parameter(name = "offset") String offset)
			throws ConverterConfigurationException, ConversionException;
	
	/**
	 * Gets a value and returns it as typed literal (xsd:datetime) formatted according to the datetime
	 * pattern (yyyy-MM-dd'T'HH:mm:ss) with the given offset added to the offset of the input.
	 * This converter works only with the offset "param" or given in +/-HH:mm format.
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value value of the featurePath to convert
	 * @param offset admitted value:
	 * <li>&lt;hh&gt;:&lt;mm&gt;: the given offset, specified in hours and minutes, 
	 * 	is applied to the input value, or replaced if the latter already contains a offset</li>
	 * <li>reuse: is applied the same offset of the input</li>
	 * @param additionalOffset offset expressed in format +/-HH:mm that is added to the original offset.
	 * This parameter is compatible only when offset parameter is REUSE
	 * @return
	 * @throws ConverterConfigurationException 
	 * @throws ConversionException 
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value,
			@Parameter(name = "offset") String offset, @Parameter(name = "additionalOffset") String additionalOffset)
			throws ConverterConfigurationException, ConversionException;

}
