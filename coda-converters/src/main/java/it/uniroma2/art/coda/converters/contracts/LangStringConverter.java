package it.uniroma2.art.coda.converters.contracts;

import it.uniroma2.art.coda.interfaces.annotations.converters.DatatypeCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapability;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import org.eclipse.rdf4j.model.Literal;

import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 * Language tagged literal converter contract. Its identifier is {@value #CONTRACT_URI}.
 */
@RDFCapability(RDFCapabilityType.literal)
@DatatypeCapability("http://www.w3.org/1999/02/22-rdf-syntax-ns#langString")
@Description("Produces a language tagged literal language tag provided as parameter.")
public interface LangStringConverter extends Converter {

	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "langString";

	/**
	 * Produces a language tagged literal based on the arguments corresponding to the parameters
	 * <code>value</code> and <code>langArg</code>. The motivation of this contract is that the parameter
	 * <code>lang</code> can only be set statically in the PEARL document, while the value of the additional
	 * parameter <code>langArg</code> can be determined at runtime.
	 * 
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value
	 * @param langArg
	 * @return
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, @Parameter(name = "langArg") String langArg);

	/**
	 * Produces a language tagged literal based on the arguments corresponding to the parameters
	 * <code>value</code>. The motivation of this contract is to have the possibility to call this converter without
	 * passing a parameter (similar to what it is done with the default converter, but in this case it can ONLY be used
	 * to generate a Literal and such literal should have a language)
	 *
	 * @param ctx
	 * @param datatype
	 * @param lang
	 * @param value
	 * @return
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value);
}
