package it.uniroma2.art.coda.converters.utils.template;

public class StaticValueTemplate implements TemplateInterface{

    private String text;

    public StaticValueTemplate(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
