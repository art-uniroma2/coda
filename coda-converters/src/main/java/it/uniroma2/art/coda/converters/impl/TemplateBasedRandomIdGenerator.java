package it.uniroma2.art.coda.converters.impl;

import com.google.common.net.UrlEscapers;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.RandomIdGenerator;
import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.util.OntoUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFCollections;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.rio.turtle.TurtleParser;
import org.pf4j.Extension;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Template-based implementation of the {@link RandomIdGenerator} contract. Its identifier is
 * {@value #CONVERTER_URI}.
 */
@Extension(points = {Converter.class})
public class TemplateBasedRandomIdGenerator implements RandomIdGenerator {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "templateBasedRandIdGen";
	public static final Properties defaultPropertyValues;
	public static final Properties defaultPropertyDescriptions;

	public static final String PARAM_URI_RND_CODE_GENERATOR = "uriRndCodeGenerator";
	public static final String PARAM_URI_RND_CODE_LENGTH = "uriRndCodeLength";

	public static final String LANG_NOT_PRESENT = "null";

	static {
		defaultPropertyValues = new Properties();
		defaultPropertyValues.setProperty("concept", "c_${rand()}");
		defaultPropertyValues.setProperty("xLabel", "xl_${lexicalForm.language}_${rand()}");
		defaultPropertyValues.setProperty("xNote", "xNote_${rand()}");
		defaultPropertyValues.setProperty("fallback", "${xRole}_${rand()}");

		defaultPropertyDescriptions = new Properties();
		defaultPropertyDescriptions.setProperty("concept", "Template for SKOS concepts");
		defaultPropertyDescriptions.setProperty("xLabel", "Template for SKOS eXtended Labels");
		defaultPropertyDescriptions.setProperty("xNote", "Template for SKOS eXtended Notes");
		defaultPropertyDescriptions.setProperty("fallback",
				"Fallback template for any unknown genre of resource");
	}

	public enum RandCode {
		DATETIMEMS, UUID, TRUNCUUID4, TRUNCUUID8, TRUNCUUID12, DIGIT, XDIGIT, ALNUM;
	}

	// predefined random code
	public static final String DEFAULT_RANDOM_CODE_GENERATOR = RandCode.TRUNCUUID8.name();

	// predefined random code length (when applicable)
	public static final int DEFAULT_RANDOM_CODE_LENGTH = 8;

	// rand() rand(DIGIT) rand(DIGIT,8)
	private static final String RAND_REGEX = "rand\\((" +
			String.format("(?<randomCode>%s)", EnumUtils.getEnumList(RandCode.class).stream().map(Object::toString).collect(Collectors.joining("|"))) +
			"(\\s*,\\s*(?<randomLength>[1-9]\\d*))?" +
			")?\\)";

	private static final Pattern RAND_PATTERN = Pattern.compile(RAND_REGEX);

	private static final String XROLE = "xRole";

	private static final Pattern PLACEHOLDER_PATTERN = Pattern
			.compile("([a-zA-Z]+)(?:\\[([1-9]*\\d)\\])?(?:\\.([a-zA-Z]+))?");
	private static final Pattern PLACEHOLDER_START_PATTERN = Pattern.compile("\\$(\\$)?\\{");

	private static final int MAX_GENERATION_ATTEMPTS = 5;

	/*
	 * Borrowed from the class UUID.
	 *
	 * The random number generator used by this class to create random
	 * numbers. In a holder class to defer initialization until needed.
	 */
	private static class Holder {
		static final SecureRandom numberGenerator = new SecureRandom();
		static final RandomStringGenerator digitGenerator = new RandomStringGenerator.Builder().withinRange(new char[][]{{'0', '9'}}).usingRandom(numberGenerator::nextInt).build();
		static final RandomStringGenerator xigitGenerator = new RandomStringGenerator.Builder().withinRange(new char[][]{{'0', '9'}, {'a', 'f'}}).usingRandom(numberGenerator::nextInt).build();
		static final RandomStringGenerator alnumGenerator = new RandomStringGenerator.Builder().withinRange(new char[][]{{'0', '9'}, {'a', 'z'}}).usingRandom(numberGenerator::nextInt).build();
	}

	private String getRandomPart(CODAContext ctx, Matcher randomPartMatcher) {
		String randomCode = randomPartMatcher.group("randomCode");
		if (StringUtils.isAnyBlank(randomCode)) {
			randomCode = getDefaultRandomCode(ctx);
		}

		String randomLengthStr = randomPartMatcher.group("randomLength");
		int randomCodeLength;
		if (StringUtils.isNoneBlank(randomLengthStr)) {
			randomCodeLength = Integer.parseInt(randomLengthStr);
		} else {
			randomCodeLength = getDefaultRandomCodeLength(ctx);
		}


		String randomValue;
		if (randomCode.equalsIgnoreCase(RandCode.DATETIMEMS.name())) {
			randomValue = new java.util.Date().getTime() + "";
		} else if (randomCode.equalsIgnoreCase(RandCode.UUID.name())) {
			randomValue = UUID.randomUUID().toString();
		} else if (randomCode.equalsIgnoreCase(RandCode.TRUNCUUID4.name())) {
			randomValue = UUID.randomUUID().toString().substring(0, 4);
		} else if (randomCode.equalsIgnoreCase(RandCode.TRUNCUUID12.name())) {
			randomValue = UUID.randomUUID().toString().substring(0, 13);
		} else if (randomCode.equalsIgnoreCase(RandCode.DIGIT.name())) {
			randomValue = Holder.digitGenerator.generate(randomCodeLength);
		} else if (randomCode.equalsIgnoreCase(RandCode.XDIGIT.name())) {
			randomValue = Holder.xigitGenerator.generate(randomCodeLength);
		} else if (randomCode.equalsIgnoreCase(RandCode.ALNUM.name())) {
			randomValue = Holder.alnumGenerator.generate(randomCodeLength);
		} else {// default value TRUNCUUID8
			randomValue = UUID.randomUUID().toString().substring(0, 8);
		}
		return randomValue;
	}

	protected String getDefaultRandomCode(CODAContext ctx) {
		Properties props = ctx.getConverterProperties(CONVERTER_URI);
		String randomCode = null;
		if (props != null) {
			randomCode = props.getProperty(PARAM_URI_RND_CODE_GENERATOR);
		}

		if (randomCode == null) {
			randomCode = DEFAULT_RANDOM_CODE_GENERATOR;
		}
		return randomCode;
	}

	protected int getDefaultRandomCodeLength(CODAContext ctx) {
		Properties props = ctx.getConverterProperties(CONVERTER_URI);

		int randomCodeLength = 0;

		if (props != null) {
			randomCodeLength = NumberUtils.toInt(props.getProperty(PARAM_URI_RND_CODE_LENGTH), 0);
		}

		if (randomCodeLength <= 0) {
			randomCodeLength = DEFAULT_RANDOM_CODE_LENGTH;
		}
		return randomCodeLength;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.coda.contracts.RandomIdGenerator#produceURI(it.uniroma2.art.coda.interfaces.CODAContext
	 * , java.lang.String, java.util.Map)
	 */
	@Override
	public IRI produceURI(CODAContext ctx, String value, String xRole, Map<String, Value> args)
			throws ConversionException {
		IRI uriRes = null; // null should never be returned

		boolean newResourceGenerated = false;

		int attempts = 0;

		while (!newResourceGenerated && attempts < MAX_GENERATION_ATTEMPTS) {
			++attempts;
			String localName = "";

			String currentTemplate = getTemplate(ctx, xRole);

			while (currentTemplate.length() > 0) {
				// S{ is an escaped placeholder, $${ is a not escaped placeholder
				if (StringUtils.startsWithAny(currentTemplate,"${", "$${")) {
					// get placeholder
					// If ${, then escaped with 2 characters to be skipped
					boolean phEscaped = true;
					int phBegin = 2;

					// If $${, then not escaped with 3 characters to be skipped
					if (currentTemplate.startsWith("$${")) {
						phEscaped = false;
						phBegin = 3;
					}

					int phEnd = currentTemplate.indexOf("}");

					if (phEnd == -1) {
						throw new IllegalArgumentException("Missing closing brace");
					}

					String ph = currentTemplate.substring(phBegin, phEnd);

					// retrieve the value to replace the placeholder
					String phSubst;

					Matcher randPartMatcher = RAND_PATTERN.matcher(ph);

					if (randPartMatcher.matches()) {
						phSubst = getRandomPart(ctx, randPartMatcher);
					} else {
						phSubst = getPlaceholderValue(ph, xRole, args);
						if (phSubst == null) {
							throw new IllegalArgumentException(
									"The placeholder \"" + ph + "\" is not present in the valueMapping");
						}
					}
					if (phEscaped) {
						phSubst = escapeValue(phSubst);
					}
					localName = localName + phSubst; // compose the result
					// remove the parsed part
					currentTemplate = currentTemplate.substring(phEnd + 1);
				} else {
					// concat the fixed part of the template
					Matcher m = PLACEHOLDER_START_PATTERN.matcher(currentTemplate);

					int literalEnd;

					if (m.find()) {
						literalEnd = m.start();
					} else {
						literalEnd = currentTemplate.length();
					}

					localName = localName + currentTemplate.substring(0, literalEnd);
					currentTemplate = currentTemplate.substring(literalEnd);
				}
			}

			RepositoryConnection conn = ctx.getRepositoryConnection();
			// ARTResource[] graphs = ctx.getRGraphs();
			uriRes = conn.getValueFactory().createIRI(ctx.getDefaultNamespace() + localName);
			// resRand.setArtURIResource(uriRes);

			try {
				// check if the newly created resource is already present in the repository
				if (!OntoUtils.existsResource(conn, uriRes)) {
					newResourceGenerated = true;
				}
			} catch (RepositoryException e) {
				// throw new URIGenerationException(e);
				throw new RuntimeException(e);
			}
		}

		if (!newResourceGenerated) {
			throw new ConversionException("Exceeded the allowed number of attempts to create a resource URI."
					+ "This may be determined by the absence of a random part in the template associated with the xRole "
					+ xRole
					+ "(or in the fallback template, if the given xRole is not provided with a specific template");
		}

		return uriRes;
	}

	/**
	 * This method escapes a raw string into something usable as a path segment in a URL. Additionally, it replaces any
	 * number of contiguous spaces with a single underscore.
	 * @param rawString
	 * @return
	 */
	protected String escapeValue(String rawString) {
		return UrlEscapers.urlPathSegmentEscaper().escape(rawString.trim().replaceAll("\\s+", "_"));
	}

	/**
	 * Returns a placeholder value as string. This method is invoked for placeholders like the following:
	 * <ol>
	 *     <li><code>xRole</code>: it returns the xRole provided as argument to {@link #produceURI(CODAContext, String, String)}</li>
	 *     <li><code>paramN</code>: it returns the value of <code>paramN</code> in the map bound to the <code>args</code> parameter of {@link #produceURI(CODAContext, String, String)}</li>
	 *     <li><code>paramN.propX</code>: it returns the value of the property <code>propX</code> of the object <code>paramN</code> in the map bound to the <code>args</code> parameter of {@link #produceURI(CODAContext, String, String)}</li>
	 *     <li><code>paramN[M]</code>: like #2, but accessing the M-th element of a string literal interpreted as a Value collection</li>
	 *     <li><code>paramN[M].propX</code>: like #3, but accessing the M-th element of a string literal interpreted as a Value collection</li>
	 * </ol>
	 * The conversion to string id done by the operation {@link #object2string(Object)}.
	 * @param ph
	 * @param xRole
	 * @param args
	 * @return
	 */
	protected String getPlaceholderValue(String ph, String xRole, Map<String, Value> args) {
		if (Objects.equals(ph, XROLE)) {
			return xRole;
		}

		Matcher matcher = PLACEHOLDER_PATTERN.matcher(ph);
		if (!matcher.matches()) {
			return null;
		}

		String firstLevel = matcher.group(1);
		String indexString = matcher.group(2);
		String secondLevel = matcher.group(3);

		// get the value from the args map using the key contained in firstLevel
		Value firstLevelObj = args.get(firstLevel);

		if (firstLevelObj == null) {
			return null;
		}

		if (indexString != null) {

			int index = Integer.valueOf(indexString);

			if (firstLevelObj instanceof Literal) {
				Literal firstLevelLiteral = (Literal) firstLevelObj;
				if (!XSD.STRING.equals(firstLevelLiteral.getDatatype())) {
					return null;
				}

				List<? extends Value> items = parseCollectionString(firstLevelLiteral.getLabel());

				if (index >= items.size()) {
					return null;
				}

				firstLevelObj = items.get(index);

			}
		}

		if (secondLevel == null) {
			return object2string(firstLevelObj);
		}

		try {
			Method[] methods = firstLevelObj.getClass().getMethods();

			//create the name of the method using the value in secondLevel. E.g. from "language" the method is getLanguage()
			String nameWithHas = "get" + Character.toUpperCase(secondLevel.charAt(0))
					+ secondLevel.substring(1);

			Method methodWithLiteralName = null;
			Method methodWithHasName = null;

			for (Method m : methods) {
				if (m.getParameterTypes().length != 0)
					continue;

				String methodName = m.getName();
				if (Objects.equals(methodName, secondLevel)) {
					methodWithLiteralName = m;
					break;
				} else if (Objects.equals(methodName, nameWithHas)) {
					methodWithHasName = m;
				}
			}

			Method m = methodWithLiteralName != null ? methodWithLiteralName : methodWithHasName;

			if (m == null) {
				return null;
			}

			Object secondLevelObject = m.invoke(firstLevelObj);

			String valueToReturn = object2string(secondLevelObject);
			if (valueToReturn==null && m.getName().contains("getLanguage")) {
				// the method was the getLanguage() but this RDF_Literal did not have a language so return LANG_NOT_PRESENT
				return LANG_NOT_PRESENT;
			}
			return valueToReturn;
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			return null;
		}
	}

	/**
	 * Parses a Tutle-serialized collection into a {@link List} of {@link Value}.
	 *
	 * @param collectionString
	 * @return
	 */
	protected List<Value> parseCollectionString(String collectionString) {
		List<Value> items = new ArrayList<>();

		TurtleParser parser = new TurtleParser();
		Model statements = new LinkedHashModel();
		parser.setRDFHandler(new StatementCollector(statements));
		StringBuilder sb = new StringBuilder("[] ").append(NTriplesUtil.toNTriplesString(RDF.VALUE))
				.append(" ").append(collectionString).append(" . ");

		try {
			statements = Rio.parse(new StringReader(sb.toString()), "http://example.org/", RDFFormat.TURTLE);
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			e.printStackTrace();
			return null;
		}

		RDFCollections.asValues(statements,
				Models.objectResource(statements.filter(null, RDF.VALUE, null)).get(), items);

		return items;
	}

	/**
	 * Converts an object to a string. Instances of {@link Optional} are treated as a possibly <code>null</code> reference
	 * of the managed type.
	 *
	 * @param obj
	 * @return For an RDF4J's {@link Value} it returns its {@link Value#stringValue()}; otherwise, for a non-null object
	 * reference, it returns {@link Object#toString()}. Finally, for a <code>null</code> argument, it returns null.
	 */
	protected String object2string(Object obj) {
		if (obj instanceof Optional<?>) {
			//obj = ((Optional<?>) obj).orElseGet(null); // not working because of the orElseGet(null) which trows a java.lang.NullPointerException
			if (((Optional<?>) obj).isPresent()) {
				obj = ((Optional<?>) obj).get();
			} else {
				obj = null;
			}

		}

		String rawString;
		if (obj instanceof Value) {
			rawString = ((Value) obj).stringValue();
		} else if (obj != null) {
			rawString = obj.toString();
		} else {
			rawString = null;
		}

		return rawString;
	}

	/**
	 * Returns the template associated with the given <code>xRole</code>.
	 * @param ctx
	 * @param xRole
	 * @return It returns the mapping returned by {@link CODAContext#getConverterProperties(String)} with argument
	 * {@value #CONVERTER_URI}; if none is found, it returns the predefined template. If no template is found, it looks
	 * up the <code>fallback</code> template in the first mapping (if any); if none is found, it considers the
	 * predefined <code>fallback</code> template.
	 */
	private String getTemplate(CODAContext ctx, String xRole) {
		Properties props = ctx.getConverterProperties(CONVERTER_URI);

		String template = null;

		// Looks up a template for the given xRole
		if (props != null) {
			template = props.getProperty(xRole, defaultPropertyValues.getProperty(xRole));
		} else {
			template = defaultPropertyValues.getProperty(xRole);
		}

		// If no template is found, considers the fallback template
		if (template == null) {
			if (props != null) { // checks whether the fallback template has been overridden
				template = props.getProperty("fallback", defaultPropertyValues.getProperty("fallback"));
			} else {
				template = defaultPropertyValues.getProperty("fallback");
			}
		}

		return template;
	}

	@Override
	public IRI produceURI(CODAContext ctx, String value, String xRole) throws ConverterException {
		return produceURI(ctx, value, xRole, Collections.<String, Value>emptyMap());
	}

	@Override
	public IRI produceURI(CODAContext ctx, String value) throws ConverterException {
		return produceURI(ctx, value, "undetermined");
	}

}
