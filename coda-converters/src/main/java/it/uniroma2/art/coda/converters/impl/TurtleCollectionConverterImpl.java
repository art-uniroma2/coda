package it.uniroma2.art.coda.converters.impl;

import static java.util.stream.Collectors.joining;

import java.util.Arrays;

import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.TurtleCollectionConverter;
import it.uniroma2.art.coda.interfaces.CODAContext;
import org.pf4j.Extension;

/**
 * Implementation of the {@link TurtleCollectionConverter} contract. Its identifier is
 * {@value #CONVERTER_URI}.
 */
@Extension(points = {Converter.class})
public class TurtleCollectionConverterImpl implements TurtleCollectionConverter {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "turtleCollection";

	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value,
			Value... items) {
		return SimpleValueFactory.getInstance().createLiteral(
				Arrays.stream(items).map(NTriplesUtil::toNTriplesString).collect(joining(" ", "(", ")")));
	}

}
