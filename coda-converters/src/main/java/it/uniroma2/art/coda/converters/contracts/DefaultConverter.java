package it.uniroma2.art.coda.converters.contracts;

import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 * Default converter contract. Its identifier is {@value #CONTRACT_URI}.
 */
@Description("Default converter.\n" +
		"If it is used with the 'uri' capability, it generates a URI concatenating the baseUri to the given input. " +
		"If the input string is already a URI, it returns the same.\n" +
		"Otherwise, if it is used with the 'literal' capability, it simply returns the given input as a Literal.")

public interface DefaultConverter extends Converter{
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "default";

	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	IRI produceURI(CODAContext ctx, String value);

	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value);
}
