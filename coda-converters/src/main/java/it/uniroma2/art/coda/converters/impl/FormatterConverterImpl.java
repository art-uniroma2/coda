package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.FormatterConverter;
import it.uniroma2.art.coda.converters.utils.template.PlaceholderTemplate;
import it.uniroma2.art.coda.converters.utils.template.StaticValueTemplate;
import it.uniroma2.art.coda.converters.utils.template.TemplateInterface;
import it.uniroma2.art.coda.converters.utils.template.ValueTemplate;
import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.pf4j.Extension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Implementation of the {@link FormatterConverter} contract. Its identifier is
 * {@value #CONVERTER_URI}.
 */
@Extension(points = {Converter.class})
public class FormatterConverterImpl implements FormatterConverter {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "formatter";

	public static final String PARAM_STRING = "%s";
	public static final String FEAT_STRING = "!s";
	public static final String PARAM_LOCAL = "%n";
	public static final String PARAM_DATATYPE = "%d";
	public static final String PARAM_LANG = "%l";
	//remember that these params could have an additional ^U or ^l to UPPER_CASE or to LOWER_CASE the value that they read
	// so, for example, we could have %s^U , %l^U or %s^l
	public static final String MOD_UPPERCASE = "U";
	public static final String MOD_LOWERCASE = "l";
	public static final String MOD_INTRO = "^";


	private List<String> placeholderList = new ArrayList<>();

	@Override
	public IRI produceURI(CODAContext ctx, String value, String text,
			Value ... args) throws ConverterConfigurationException {

		initializePlchldList();

		List<TemplateInterface> templateInterfaceList = parseTemplate(text);
		String newText = replaceTextInTemplate(templateInterfaceList, value,  args);

		//transform the text (after the replacement) into a URI (using the same approach as the one used in
		// DefaultConverterImpl )
		try {
			new URL(newText);//if this doesn't throw exception, then the value is already a URL
			return SimpleValueFactory.getInstance().createIRI(newText);
		} catch (MalformedURLException e) {
			if (newText.matches(DefaultConverterImpl.MAIL_REGEX)){
				return SimpleValueFactory.getInstance().createIRI("mailto:"+newText);
			} else {
				newText = newText.replaceAll("\\s","_"); //sanitize the input value
				return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace()+newText);
			}
		}
	}


	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, String text,
			Value... args) throws ConverterConfigurationException {

		initializePlchldList();

		//String newText = replaceText(text, value , args);
		List<TemplateInterface> templateInterfaceList = parseTemplate(text);
		String newText = replaceTextInTemplate(templateInterfaceList, value, args);
		Literal newLiteral;
		SimpleValueFactory simpleValueFactory = SimpleValueFactory.getInstance();
		if(lang!=null && !lang.isEmpty()){
			newLiteral = simpleValueFactory.createLiteral(newText, lang);
		} else if(datatype!=null && !datatype.isEmpty()){
			newLiteral =simpleValueFactory.createLiteral(newText, simpleValueFactory.createIRI(datatype));
		} else {
			newLiteral = simpleValueFactory.createLiteral(newText);
		}
		return newLiteral;
	}


	private void initializePlchldList() {
		if(placeholderList.isEmpty()) {
			// the placeholderList is empty, so add the placeholders
			placeholderList.add(PARAM_STRING);
			placeholderList.add(PARAM_LOCAL);
			placeholderList.add(PARAM_DATATYPE);
			placeholderList.add(PARAM_LANG);
		}
	}

	private boolean isValidPlchd(String plchld) {
		initializePlchldList(); // just to be sure that the list is not empty
		if(!plchld.startsWith("%")) {
			plchld = "%"+plchld;
		}
		return placeholderList.contains(plchld);
	}

	private boolean isValidValuePlchd(String plchld) {
		if(plchld.equals("s")){
			return true;
		}
		return false;
	}

	private List<TemplateInterface> parseTemplate(String template) throws ConverterConfigurationException {
		List<TemplateInterface> templateInterfaceList = new ArrayList<>();

		//split the template text using %
		String[] templateFirstSplit = template.split("%");
		boolean first = true;
		for(String templatePart : templateFirstSplit){
			if(first){
				first = false;
				//the first element is everything that is before the first %
				if(templatePart.isEmpty()) {
					//it is empty, do nothing
					continue;
				}
				templateInterfaceList.addAll(splitForValue(templatePart, false));
			} else {
				// the text after a %, so check that it is really a placeholder and not another character
				// and also manage the possible derivations, such, for %X, %X^U and %X^l
				boolean isPlch;
				if(isValidPlchd(templatePart.substring(0,1))) {
					// it is a placeholder
					isPlch = true;
				} else {
					// it is not a placeholder, so add the % to the templatePart
					templatePart = "%"+templatePart;
					isPlch = false;
				}
				templateInterfaceList.addAll(splitForValue(templatePart, isPlch));
			}
		}
		return  templateInterfaceList;
	}

	// split the input text using the ! (used for finding !s and its derivations such as !s^C or !s^l)
	// this text, in original , could start with a placeholder or not (if firstIsPlch is true it did start with a placeholder, if false,
	// no placeholder)
	private List<TemplateInterface> splitForValue(String text, boolean firstIsPlch)
			throws ConverterConfigurationException {
		List<TemplateInterface> templateInterfaceList = new ArrayList<>();
		String[] textArray = text.split("!");
		boolean first = true;
		for(String textPart : textArray){
			if(first) {
				// the text before the first !
				first = false;
				if(textPart.isEmpty()) {
					// the text started with a ! so, with the split, if the first element of the array is empty,
					// do nothing
					continue;
				}
				// the first element is not empty, se there could be a PlaceholderTemplate or just a StaticValueTemplate
				if(!firstIsPlch) {
					// it is only a StaticValueTemplate
					templateInterfaceList.add(new StaticValueTemplate(textPart));
				} else {
					// first a PlaceholderTemplate and the rest, if any, is a and StaticValueTemplate
					String placeholderName = textPart.substring(0,1);
					int nextPos = 1; // this value can be 3 if a modifier is found
					String modifier = extractModifier(textPart);
					if (modifier!=null) {
						//a modifier was found
						templateInterfaceList.add(new PlaceholderTemplate(placeholderName, modifier));
						nextPos = 3;
					} else {
						//no modifier was found
						templateInterfaceList.add(new PlaceholderTemplate(placeholderName));
					}
					// check if there is still other text to process
					if(textPart.length()>nextPos) {
						//now the StaticValueTemplate
						templateInterfaceList.add(new StaticValueTemplate(textPart.substring(nextPos)));
					}
				}
			} else {
				//it is probably a ValueTemplate followed by one StaticValueTemplate (if it has more than one, or three,
				// characters)
				processValueTemplateAndStaticValueTemplate(templateInterfaceList, textPart);
			}
		}
		return templateInterfaceList;
	}

	private void processValueTemplateAndStaticValueTemplate(List<TemplateInterface> templateInterfaceList, String textPart) {
		if (textPart.length()==0) {
			// the textPart is empty, so just return
			return;
		}
		String valuePlchdId = textPart.substring(0,1);

		if(isValidValuePlchd(valuePlchdId)) {
			// it is a ValueTemplate, check if there is a modifier
			int nextPos = 1; // this value can be 3 if a modifier is found
			String modifier = extractModifier(textPart);
			if (modifier!=null) {
				// a modifier was found
				templateInterfaceList.add(new ValueTemplate(valuePlchdId, modifier));
				nextPos = 3;
			} else {
				//no modifier was found
				templateInterfaceList.add(new ValueTemplate(valuePlchdId));
			}
			// check if there is still other text to process
			if(textPart.length()>nextPos) {
				templateInterfaceList.add(new StaticValueTemplate(textPart.substring(nextPos)));
			}
		} else {
			// it is not a ValueTemplate, but just a StaticValueTemplate starting with !
			templateInterfaceList.add(new StaticValueTemplate("!"+textPart));
		}
	}

	private String extractModifier(String text) {
		// check if the text has at least 3 characters and if the second one is "^" and the third one is either
		// "C" or "l"
		// if a modifiers is found, return such modifier, otherwise return false
		if (text.length()<3) {
			// the input text it is too short, it cannot contain a modifier
			return null;
		}
		if (text.startsWith(MOD_INTRO, 1) &&
				(text.startsWith(MOD_UPPERCASE, 2) || text.startsWith(MOD_LOWERCASE, 2))) {
			// found the modifier, so return it
			return text.substring(2,3);
		}
		// the modifier was not found
		return null;

	}

	private String replaceTextInTemplate(List<TemplateInterface> templateInterfaceList, String value,
											   Value... args) throws ConverterConfigurationException {
		StringBuilder sb = new StringBuilder();
		int pos=0;
		for (TemplateInterface templateInterface : templateInterfaceList){
			//depending on the type of templateInterface, behave accordingly
			if (templateInterface instanceof StaticValueTemplate){
				//just add the value, as it is
				sb.append(((StaticValueTemplate) templateInterface).getText());
			} else if (templateInterface instanceof ValueTemplate) {
				//it is about the inputValue, check if there is a modifier or not.
				// if a modifier is present
				sb.append(getValueWithModifierIfPresent(((ValueTemplate) templateInterface).getModifier(), value));
			} else if (templateInterface instanceof PlaceholderTemplate) {
				PlaceholderTemplate placeholderTemplate = (PlaceholderTemplate) templateInterface;
				// placeholder use by the formatter converter
				Value iriOrLiteral = args[pos++];
				sb.append(getValueFromIriOrLiteral(placeholderTemplate, iriOrLiteral, pos));
			}
		}
		return sb.toString();
	}

	private String getValueWithModifierIfPresent(String modifier, String text) {
		if (modifier !=null) {
			//there is a modifier, so behave accordingly
			if (modifier.equals(MOD_LOWERCASE)) {
				return text.toLowerCase(Locale.ROOT);
			} else  if (modifier.equals(MOD_UPPERCASE)) {
				return text.toUpperCase(Locale.ROOT);
			} else {
				// this should never happen, but, just to be sure, return the value as it is
				return text;
			}
		} else {
			// no modifier, so just return the value as it is
			return text;
		}
	}

	private String getValueFromIriOrLiteral(PlaceholderTemplate placeholderTemplate, Value iriOrLiteral, int cont)
			throws ConverterConfigurationException {
		String replancedText = "";
		String expr = "%"+placeholderTemplate.getId();
		if(expr.equals(PARAM_STRING)){
			if(iriOrLiteral instanceof IRI){
				replancedText = ((IRI) iriOrLiteral).stringValue();
			} else if(iriOrLiteral instanceof Literal) {
				replancedText = ((Literal) iriOrLiteral).stringValue();
			} else {
				throw new ConverterConfigurationException("There is a problem with the "+cont+" parameter, only IRIs and Literals are supported");
			}
		} else if(expr.equals(PARAM_LOCAL)){
			if(iriOrLiteral instanceof IRI) {
				replancedText = ((IRI)iriOrLiteral).getLocalName();
			}  else if (iriOrLiteral instanceof Literal){
				String temp = ((Literal) iriOrLiteral).getLabel();
				throw new ConverterConfigurationException("'"+temp+"' is not an IRI, so it is not possible to get its local name");
			} else {
				throw new ConverterConfigurationException("There is a problem with the "+cont+" parameter, only IRIs and Literals are supported");
			}
		} else if(expr.equals(PARAM_DATATYPE)){
			if(iriOrLiteral instanceof IRI) {
				String temp = ((IRI) iriOrLiteral).stringValue();
				throw new ConverterConfigurationException("'"+temp+"' is not a Literal, so it is not possible to get its datatype");
			}  else if (iriOrLiteral instanceof Literal){
				replancedText = ((Literal)iriOrLiteral).getDatatype().stringValue();
			} else {
				throw new ConverterConfigurationException("There is a problem with the "+cont+" parameter, only IRIs and Literals are supported");
			}
		} else if(expr.equals(PARAM_LANG)){
			if(iriOrLiteral instanceof IRI) {
				String temp = ((IRI) iriOrLiteral).stringValue();
				throw new ConverterConfigurationException("'"+temp+"' is not a Literal, so it is not possible to get its language");
			}  else if (iriOrLiteral instanceof Literal){
				replancedText = ((Literal) iriOrLiteral).getLanguage().isPresent() ? ((Literal) iriOrLiteral).getLanguage().get(): null;
				if(replancedText == null){
					throw new ConverterConfigurationException("'"+((Literal) iriOrLiteral).getLabel()+"' is a Literal with no language");
				}
			} else {
				throw new ConverterConfigurationException("There is a problem with the "+cont+" parameter, only IRI and Literal are supported");
			}
		} else {
			throw new ConverterConfigurationException("'"+expr+"' is not supported");
		}

		//transform the replancedText, according to the modifier, if present
		return getValueWithModifierIfPresent(placeholderTemplate.getModifier(), replancedText);
	}

}
