package it.uniroma2.art.coda.converters.contracts;

import it.uniroma2.art.coda.exception.ConverterConfigurationException;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.Description;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

@Description("Produces a resource by replacing placeholder in the template with values passed as arguments:" +
        "\n$NUM : the value matched in the regex according to the group applications of regexes (similar to what it is done in Java replaceAll)")
public interface RegexpConverter extends Converter {

    String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "regexp";

    /**
     * Produce an IRI by replacing che placeholders in the input text with the values coming from the feature path
     * and the input args
     * @param ctx
     * @param value
     * @param regex
     * @param template
     * @return
     * @throws ConverterConfigurationException
     */
    @FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
    IRI produceURI(CODAContext ctx, String value, @Parameter(name = "regex") String regex,
                   @Parameter(name = "template") String template) throws ConverterException;

    /**
     * Produce a Literal (with the given language and/or datatype) by replacing che placeholders in the input text with the values coming from the feature path
     * and the input args
     * @param ctx
     * @param datatype
     * @param lang
     * @param value
     * @return
     * @throws ConverterConfigurationException
     */
    @FeaturePathArgument(requirementLevel = RequirementLevels.OPTIONAL)
    Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value, @Parameter(name = "regex") String regex,
                           @Parameter(name = "template") String template) throws ConverterException;
}
