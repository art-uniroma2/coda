package it.uniroma2.art.coda.ae;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import it.uniroma2.art.coda.pf4j.PF4JComponentProvider;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.turtle.TurtleWriter;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.structures.CODATriple;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;

/**
 * A UIMA Analysis Engine that can be used in a UIMA processing chain to add CODA capabilities. This analysis
 * engine executes a given projection document (expressed in PEARL) on each input CAS, producing an RDF file
 * describing the resulting triples.
 * <p>
 * This analysis engine can be managed using uimaFIT. In the code below, we first instantiate a descriptor for
 * the analysis engine, and then we combine it with other descriptors to run a simple pipeline.
 * 
 * <pre>
 * CollectionReaderDescription readerDesc = ...;
 * 
 * AnalysisEngineDescription firstAEDesc = ...;
 * 
 * AnalysisEngineDescription secondAEDesc = ...;
 * 
 * AnalysisEngineDescription codaAEDesc = AnalysisEngineFactory.createEngineDescription(CODAAnalysisEngine.class,
 *      CODAAnalysisEngine.PARAM_PLUGINS_ROOTS, "arrays of paths to directories where to look for plugins",
 *      CODAAnalysisEngine.PARAM_BASE_URI, "base uri",
 *      CODAAnalysisEngine.PARAM_PROJECTION_DOCUMENT, "path to a projection document",
 *      CODAAnalysisEngine.PARAM_OUTPUT_FILE, "path to the output file");
 *      
 * SimplePipeline.runPipeline(readerDesc, firstAEDesc, secondAEDesc, codaAEDesc);
 * </pre>
 * 
 * The <em>bundles</em> directory has to contain the OSGi bundles providing the converters required to resolve
 * the utilized projection document. The <em>OSGi cache</em> directory is used by CODA for internal purposes.
 */
public class CODAAnalysisEngine extends JCasAnnotator_ImplBase {

	public static final String PARAM_PLUGINS_ROOTS = "pluginsRoots";

	public static final String PARAM_BASE_URI = "baseURI";

	private static final String PARAM_DEFAULT_NAMESPACE = "defaultNamespace";

	public static final String PARAM_PROJECTION_DOCUMENT = "projectionDocument";

	public static final String PARAM_OUTPUT_FILE = "outputFile";

	public static final String PARAM_OUTPUT_FORMAT = "outputFormat";

	@ConfigurationParameter(name = PARAM_PLUGINS_ROOTS, mandatory = true)
	private File[] pluginsRoots;

	@ConfigurationParameter(name = PARAM_BASE_URI, mandatory = true)
	private String baseUri;

	@ConfigurationParameter(name = PARAM_DEFAULT_NAMESPACE, mandatory = false)
	private String defaultNamespace;

	@ConfigurationParameter(name = PARAM_PROJECTION_DOCUMENT, mandatory = true)
	private File projectionDocument;

	@ConfigurationParameter(name = PARAM_OUTPUT_FILE, mandatory = true)
	private File outputFile;

	@ConfigurationParameter(name = PARAM_OUTPUT_FORMAT, mandatory = false)
	private String outputFormat = "TURTLE";

	//private RDFModel rdfModel;
	//private RDFModel outputModel;

	private RepositoryConnection connection;
	private RepositoryConnection outputConnection;
	
	private CODACore codaCore;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		if (defaultNamespace == null) {
			if (!baseUri.endsWith("/") && !baseUri.endsWith("#")) {
				defaultNamespace = baseUri + "#";
			} else {
				defaultNamespace = baseUri;
			}
		}

		try {
			Repository rep = new SailRepository(new MemoryStore());
			rep.init();
			connection = rep.getConnection();
			connection.setNamespace("", defaultNamespace);
			
			Repository outputRep = new SailRepository(new MemoryStore());
			outputRep.init();
			outputConnection = rep.getConnection();


			PluginManager pluginManager = new DefaultPluginManager(Stream.of(pluginsRoots).map(File::toPath).collect(Collectors.toList()));
			pluginManager.loadPlugins();
			pluginManager.startPlugins();

			PF4JComponentProvider cp = new PF4JComponentProvider(pluginManager, true);

			codaCore = new CODACore(cp);

			codaCore.initialize(connection);

			codaCore.setProjectionRulesModel(projectionDocument);
		} catch (Exception e) {
			throw new ResourceInitializationException(e);
		}

	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		try {
			codaCore.setJCas(aJCas, true);

			while (codaCore.isAnotherAnnotationPresent()) {
				SuggOntologyCoda suggestions = codaCore.processNextAnnotation();
				List<CODATriple> triples = suggestions.getAllInsertARTTriple();

				for (CODATriple t : triples) {
					connection.add(connection.getValueFactory().createStatement(t.getSubject(), 
							t.getPredicate(), t.getObject()));
					
					
					BNode stmBNode = outputConnection.getValueFactory().createBNode();
					
					outputConnection.add(outputConnection.getValueFactory().createStatement(stmBNode, RDF.TYPE, RDF.STATEMENT));
					outputConnection.add(outputConnection.getValueFactory().createStatement(stmBNode, RDF.SUBJECT, t.getSubject()));
					outputConnection.add(outputConnection.getValueFactory().createStatement(stmBNode, RDF.PREDICATE, t.getPredicate()));
					outputConnection.add(outputConnection.getValueFactory().createStatement(stmBNode, RDF.OBJECT, t.getObject()));
				}
			}
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}

	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		try {
			super.collectionProcessComplete();
			
			RDFHandler rdfHandler = new TurtleWriter(new OutputStreamWriter(
					new FileOutputStream(outputFile)));
			outputConnection.export(rdfHandler);
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
	}

	@Override
	public void destroy() {
		try {
			codaCore.stopAndClose();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		super.destroy();
	}
}
