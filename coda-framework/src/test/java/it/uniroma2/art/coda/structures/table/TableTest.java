package it.uniroma2.art.coda.structures.table;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public class TableTest {

	TablePlaceholdersBindingsVariables table;
	
	public TableTest() {
		table = new TablePlaceholdersBindingsVariables();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TableTest tableTest = new TableTest();
		tableTest.prepareTable();
		tableTest.printTable();

	}

	
	private void prepareTable(){
		/*
		 * This test works with the following pearl rules:
		 * 
		 * rule PlantRule {
		 * 		nodes = {
		 * 			commonName ....
		 * 			latinName ....
		 * 		}
		 * }
		 * 
		 * rule InsectRule {
		 * 		nodes = {
		 * 			commonName ....
		 * 			latinName ....
		 * 		}
		 * }
		 * 
		 * rule PestOf {
		 * 		binding = {
		 * 			plant 		plantList		PlantRule
		 * 			insect		insectList		InsectRule
		 * 		}
		 * 		nodes = {
		 * 			semamanticRelation ....
		 * 		}
		 * } 
		 * 
		 */
		
		
		
		ValuesFromAnAnnotation valueFromMainAnnotation = new ValuesFromAnAnnotation("mainRule");
		
		//there are two values associated to the placeholder semanticRelation
		IRI semRel1 = SimpleValueFactory.getInstance().createIRI("http://example.org#isPestOf1");
		IRI semRel2 = SimpleValueFactory.getInstance().createIRI("http://example.org#isPestOf2");
		
		ValueForTablePlaceholder valueForTablePlaceholder1 = 
				new ValueForTablePlaceholder(null, "$semamanticRelation", semRel1);
		ValueForTablePlaceholder valueForTablePlaceholder2 = 
				new ValueForTablePlaceholder(null, "$semamanticRelation", semRel2);
		
		List <ValueForTable> placeholder1List = new ArrayList<ValueForTable>();
		placeholder1List.add(valueForTablePlaceholder1);
		placeholder1List.add(valueForTablePlaceholder2);
		valueFromMainAnnotation.addSingleValueListToList(placeholder1List);
		
		//there are two couple of values associated to the binding plant
		IRI plant1CommonName = SimpleValueFactory.getInstance().createIRI("http://example.org#grano");
		IRI plant1LatinName = SimpleValueFactory.getInstance().createIRI("http://example.org#granus");
		IRI plant2CommonName = SimpleValueFactory.getInstance().createIRI("http://example.org#albero");
		IRI plant2LatinName = SimpleValueFactory.getInstance().createIRI("http://example.org#arbustus");
		
		Map<String, Value> idToValueMapPlant1 = new HashMap<String, Value>();
		idToValueMapPlant1.put("$plant.commonName", plant1CommonName);
		idToValueMapPlant1.put("$plant.latinName", plant1LatinName);
		ValueForTableBinding valueForBindingPlant1 = new ValueForTableBinding(null, idToValueMapPlant1);
		
		
		Map<String, Value> idToValueMapPlant2 = new HashMap<String, Value>();
		idToValueMapPlant2.put("$plant.commonName", plant2CommonName);
		idToValueMapPlant2.put("$plant.latinName", plant2LatinName);
		ValueForTableBinding valueForBindingPlant2 = new ValueForTableBinding(null, idToValueMapPlant2);
		
		List <ValueForTable> bindingPlantList = new ArrayList<ValueForTable>();
		bindingPlantList.add(valueForBindingPlant1);
		bindingPlantList.add(valueForBindingPlant2);
		
		valueFromMainAnnotation.addSingleValueListToList(bindingPlantList);
		
		//there are two couple of values associated to the binding insect
		IRI insect1CommonName = SimpleValueFactory.getInstance().createIRI("http://example.org#zanzara");
		IRI insect1LatinName = SimpleValueFactory.getInstance().createIRI("http://example.org#mosquitus");
		IRI insect2CommonName = SimpleValueFactory.getInstance().createIRI("http://example.org#bruco");
		IRI insect2LatinName = SimpleValueFactory.getInstance().createIRI("http://example.org#vermus");
		
		Map<String, Value> idToValueMapInsect1 = new HashMap<String, Value>();
		idToValueMapInsect1.put("$insect.commonName", insect1CommonName);
		idToValueMapInsect1.put("$insect.latinName", insect1LatinName);
		ValueForTableBinding valueForBindingInsect1 = new ValueForTableBinding(null, idToValueMapInsect1);
		
		
		Map<String, Value> idToValueMapInsect2 = new HashMap<String, Value>();
		idToValueMapInsect2.put("$insect.commonName", insect2CommonName);
		idToValueMapInsect2.put("$insect.latinName", insect2LatinName);
		ValueForTableBinding valueForBindingInsect2 = new ValueForTableBinding(null, idToValueMapInsect2);
			
		List <ValueForTable> binding2List = new ArrayList<ValueForTable>();
		binding2List.add(valueForBindingInsect1);
		binding2List.add(valueForBindingInsect2);
		
		valueFromMainAnnotation.addSingleValueListToList(binding2List);
		
		List<ValuesFromAnAnnotation> vfaaList = new ArrayList<ValuesFromAnAnnotation>();
		vfaaList.add(valueFromMainAnnotation);
		
		
		List<List<ValuesFromAnAnnotation>> valuesFromAnnotationListList = 
				new ArrayList<List<ValuesFromAnAnnotation>>();
		valuesFromAnnotationListList.add(vfaaList);
		
		table.constructPlaceholderBindingMap(valuesFromAnnotationListList);
		
		
		//TODO fix this using the new method to create the table
		//table.constructPlaceholderBindingMap(placeholderBindingList);
		
}
	
	private void printTable(){
		table.printPlaceholderBindingTable();
	}
}
