package it.uniroma2.art.coda.structures.table;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;


public class SimpleTableTest {

	TablePlaceholdersBindingsVariables table;

	public SimpleTableTest() {
		table = new TablePlaceholdersBindingsVariables();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SimpleTableTest tableTest = new SimpleTableTest();
		tableTest.prepareTable();
		tableTest.printTable();

	}

	private void prepareTable() {


		// there are two values associated to the placeholder first
		IRI first1 = SimpleValueFactory.getInstance().createIRI("http://example.org#isPestOf1");
		IRI first2 = SimpleValueFactory.getInstance().createIRI("http://example.org#isPestOf2");

		ValueForTablePlaceholder valueForTablePlaceholder1_1 = new ValueForTablePlaceholder(null, "$first",
				first1);
		ValueForTablePlaceholder valueForTablePlaceholder1_2 = new ValueForTablePlaceholder(null, "$first",
				first2);

		List<ValueForTable> placeholder1List = new ArrayList<ValueForTable>();
		placeholder1List.add(valueForTablePlaceholder1_1);
		placeholder1List.add(valueForTablePlaceholder1_2);

		// there are two values associated to the placeholder second
		IRI second1 = SimpleValueFactory.getInstance().createIRI("http://example.org#knows1");
		IRI second2 = SimpleValueFactory.getInstance().createIRI("http://example.org#knows2");

		ValueForTablePlaceholder valueForTablePlaceholder2_1 = new ValueForTablePlaceholder(null, "$second",
				second1);
		ValueForTablePlaceholder valueForTablePlaceholder2_2 = new ValueForTablePlaceholder(null, "$second",
				second2);

		List<ValueForTable> placeholder2List = new ArrayList<ValueForTable>();
		placeholder2List.add(valueForTablePlaceholder2_1);
		placeholder2List.add(valueForTablePlaceholder2_2);


		ValuesFromAnAnnotation valueFromMainAnnotation = new ValuesFromAnAnnotation("mainRule");
		valueFromMainAnnotation.addSingleValueListToList(placeholder1List);
		valueFromMainAnnotation.addSingleValueListToList(placeholder2List);
		
		List<ValuesFromAnAnnotation> vfaaList = new ArrayList<ValuesFromAnAnnotation>();
		vfaaList.add(valueFromMainAnnotation);
		
		List<List<ValuesFromAnAnnotation>> valuesFromAnnotationListList = 
				new ArrayList<List<ValuesFromAnAnnotation>>();
		valuesFromAnnotationListList.add(vfaaList);
		
		table.constructPlaceholderBindingMap(valuesFromAnnotationListList);
		
		
		/*List<List<ValueForTable>> placeholderBindingList = new ArrayList<List<ValueForTable>>();
		placeholderBindingList.add(placeholder1List);
		placeholderBindingList.add(placeholder2List);*/

		//TODO fix this using the new method to create the table
		//table.constructPlaceholderBindingMap(placeholderBindingList);

	}

	private void printTable() {
		System.out.println("Printing table");
		table.printPlaceholderBindingTable();
	}
}
