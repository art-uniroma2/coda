package it.uniroma2.art.coda.provisioning.classpath;

import it.uniroma2.art.coda.provisioning.ComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.provisioning.ConverterContractDescription;
import it.uniroma2.art.coda.provisioning.ConverterDescription;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ComponentProviderMock implements ComponentProvider {

	private Map<String, Object> contract2converterMapping;

	public ComponentProviderMock() {
		this.contract2converterMapping = new HashMap<String, Object>();
	}

	public ComponentProviderMock(Map<String, Object> contract2converterMapping) {
		this.contract2converterMapping = contract2converterMapping;
	}

	@Override
	public Object lookup(String contract) throws ComponentProvisioningException {
		Object coverter = contract2converterMapping.get(contract);

		if (coverter == null) {
			throw new ComponentProvisioningException(
					"No converter has been found that matches the contract: " + contract);
		}

		return coverter;
	}

	@Override
	public void close() {
	}

	@Override
	public void setGlobalContractBinding(String contract, String converter) {
		throw new UnsupportedOperationException(
				"This method has not been implemented in this mocked component provider");
	}

	@Override
	public Collection<ConverterContractDescription> listConverterContracts() {
		throw new UnsupportedOperationException(
				"This method has not been implemented in this mocked component provider");
	}
	
	@Override
	public Collection<ConverterDescription> listConverters() {
		throw new UnsupportedOperationException(
				"This method has not been implemented in this mocked component provider");
	}

	@Override
	public void open() {
		// Nothing to do
	}

}
