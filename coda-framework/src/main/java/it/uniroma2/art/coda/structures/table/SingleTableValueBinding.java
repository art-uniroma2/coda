package it.uniroma2.art.coda.structures.table;

import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.pearl.model.BindingStruct;

public class SingleTableValueBinding extends SingleTableValue {

	BindingStruct bindingStruct;
	
	public SingleTableValueBinding(String id, Value value, BindingStruct bindingStruct) {
		super(id, value);
		this.bindingStruct = bindingStruct;
	}

	public BindingStruct getBindingStruct() {
		return bindingStruct;
	}
	

}
