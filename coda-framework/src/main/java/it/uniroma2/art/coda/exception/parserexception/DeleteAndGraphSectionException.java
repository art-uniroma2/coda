package it.uniroma2.art.coda.exception.parserexception;

public class DeleteAndGraphSectionException extends PRParserException {

	private String ruleId;
	
	private static final long serialVersionUID = 1L;

	public DeleteAndGraphSectionException(String ruleId) {
		super();
		this.ruleId = ruleId;
	}

	public DeleteAndGraphSectionException(Exception e, String plcName, String ruleId) {
		super(e);
		this.ruleId = ruleId;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	@Override
	public String getErrorAsString() {
		return "There are both the GRAPH and the DELETE section in the rule: "+ ruleId+", this should not "
				+ "be possible, please change the GRAPH section with an INSERT one";
	}
}
