package it.uniroma2.art.coda.core;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueIri;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASRuntimeException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.BooleanArray;
import org.apache.uima.jcas.cas.ByteArray;
import org.apache.uima.jcas.cas.DoubleArray;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.FSList;
import org.apache.uima.jcas.cas.FloatArray;
import org.apache.uima.jcas.cas.FloatList;
import org.apache.uima.jcas.cas.IntegerArray;
import org.apache.uima.jcas.cas.IntegerList;
import org.apache.uima.jcas.cas.LongArray;
import org.apache.uima.jcas.cas.NonEmptyFSList;
import org.apache.uima.jcas.cas.NonEmptyFloatList;
import org.apache.uima.jcas.cas.NonEmptyIntegerList;
import org.apache.uima.jcas.cas.NonEmptyStringList;
import org.apache.uima.jcas.cas.ShortArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.cas.StringList;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.pearl.model.ConverterArgumentExpression;
import it.uniroma2.art.coda.pearl.model.ConverterMapArgument;
import it.uniroma2.art.coda.pearl.model.ConverterMention;
import it.uniroma2.art.coda.pearl.model.ConverterPlaceholderArgument;
import it.uniroma2.art.coda.pearl.model.ConverterRDFLiteralArgument;
import it.uniroma2.art.coda.pearl.model.ConverterRDFURIArgument;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import it.uniroma2.art.coda.pearl.model.ProjectionOperator;
import it.uniroma2.art.coda.pearl.model.ProjectionOperator.NodeType;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.provisioning.ComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.structures.StringOrFeatureStruct;

/**
 * This class contains some methods that can be used as utilities regarding the use of UIMA in CODA
 * 
 * @author Andrea Turbati
 * 
 */
public class UIMACODAUtilities {

	private static Logger logger = LoggerFactory.getLogger(UIMACODAUtilities.class);

	private final static String COVERED_TEXT = "coveredText";
	private final static String COMPLETE_ANNOTATION = "completeAnnotation";

	/*
	 * private static final String STRINGARRAY = "uima.cas.StringArray"; private static final String
	 * INTEGERARRAY = "uima.cas.IntegerArray"; private static final String LONGARRAY = "uima.cas.LongArray";
	 * private static final String BOOLEANARRAY = "uima.cas.BooleanArray"; private static final String
	 * DOUBLEARRAY = "uima.cas.DoubleArray"; private static final String FLOATARRAY = "uima.cas.FloatArray";
	 * private static final String FSARRAY = "uima.cas.FSArray"; private static final String
	 * NONEMPTYSTRINGLIST = "uima.cas.NonEmptyStringList"; private static final String STRINGLIST =
	 * "uima.cas.StringList"; private static final String NONEMPTYFSLIST = "uima.cas.NonEmptyFSList"; private
	 * static final String FSLIST = "uima.cas.FSList";
	 */

	// /**
	// * Get a value (normally the first if more than one is present) in a particular annotation following the
	// * feature path contained in the placeholder
	// *
	// * @param ann
	// * the annotation where the value will be taken from
	// * @param placeholder
	// * the placeholder containing the feature path
	// * @return the one (just one) corresponding to the feature path or null if there is no value
	// */
	// public static String getValueOfFeatureFromFeatPathOneValue(Annotation ann, PlaceholderStruct
	// placeholder) {
	// String uimaTypeAndFeat = placeholder.getType()+placeholder.getFeaturePath();
	// return getValueOfFeatureFromFeatPathOneValue(ann, uimaTypeAndFeat);
	// }

	/**
	 * Get a value (normally the first if more than one is present) in a particular annotation following the
	 * feature path represented by uimaTypeAndFeat
	 *
	 * @param ann
	 *            the annotation where the value will be taken from
	 * @param uimaTypeAndFeat
	 *            the feature path
	 * @return the one (just one) corresponding to the feature path or null if there is no value
	 */
	public static StringOrFeatureStruct getValueOfFeatureFromFeatPathOneValue(Annotation ann,
																			  String uimaTypeAndFeat) {
		List<StringOrFeatureStruct> valueList = getValuesOfFeatureFromFeatPath(ann, uimaTypeAndFeat);
		if (valueList == null)
			return null;
		else
			return valueList.get(0);
	}

	/**
	 * Get a list of values in a particular annotation following the feature path (or uri) contained in the
	 * placeholder. It is able to resolve also the if/if then/else construct when present
	 *
	 * @param ann
	 *            the annotation where the value will be taken from
	 * @param placeholder
	 *            the placeholder containing the feature path
	 * @return the list of values corresponding to the feature path or null if there is no value
	 */
	/*
	 * public static List<ValueOrUri> getValuesOrUriOfPlaceholder(Annotation annotation, PlaceholderStruct
	 * placeholder, ParserPR projectionRuleParserSelected, Map<PlaceholderStruct, String>
	 * placeholderAliasToValueMap) { String uimaTypeAndFeat=""; String uri=""; List<ValueOrUri> valueOrUriList
	 * = new ArrayList<ValueOrUri>(); if(placeholder.hasIfElse()){ // the placeholder has an if/else
	 * structure, so I need to test each condition to know // which uima typeFeat (or uri) should I use
	 * if(projectionRuleParserSelected == null || placeholderAliasToValueMap == null) return null;
	 * List<IfElseStruct> ifElseList = placeholder.getIfElseList(); for (int i = 0; i < ifElseList.size();
	 * ++i) { IfElseStruct ifElse = ifElseList.get(i); if (ifElse.isElse() == true) { // the condition is an
	 * else, so there is no boolean condition to check (and by // construction it is the last in the list) if
	 * (ifElse.isUimaTypeAndFeatSet()) uimaTypeAndFeat = ifElse.getUimaTypeAndFeat(); else uri =
	 * ifElse.getOntoResUri(); break; } if (projectionRuleParserSelected.checkCondition(ifElse,
	 * placeholderAliasToValueMap, annotation) == true) { // the boolean condition is true, so I can use the
	 * uimaTypeAndFeat of this // IfElseStruct if (ifElse.isUimaTypeAndFeatSet()) uimaTypeAndFeat =
	 * ifElse.getUimaTypeAndFeat(); else uri = ifElse.getOntoResUri(); break; } } if
	 * (uimaTypeAndFeat.compareTo("") == 0 && uri.compareTo("") == 0) return valueOrUriList; // no uimaType or
	 * URI could be used because each boolean condition in the // if/else was false else if(uri.compareTo("")
	 * != 0){ // a uri was specified and not a feature path ValueOrUri valueOrUri = new ValueOrUri(uri, true);
	 * valueOrUriList.add(valueOrUri); return valueOrUriList; } } else uimaTypeAndFeat =
	 * placeholder.getTypeAndFeat()+":"+p;
	 * 
	 * List <String> valueList = getValuesOfFeatureFromFeatPath(annotation, uimaTypeAndFeat); for(String value
	 * : valueList) valueOrUriList.add(new ValueOrUri(value, false)); return valueOrUriList; }
	 */

	/**
	 * Get a list of values in a particular annotation following the feature path represented by
	 * uimaTypeAndFeat
	 *
	 * @param ann
	 *            the annotation where the value will be taken from
	 * @param uimaTypeAndFeat
	 *            the feature path
	 * @return the list of values corresponding to the feature path or null if there is no value
	 */
	public static List<StringOrFeatureStruct> getValuesOfFeatureFromFeatPath(Annotation ann,
																			 String uimaTypeAndFeat) {

		// List<String> featurePathValueList = new ArrayList<String>();

		if (uimaTypeAndFeat == "")
			return null;

		// split the uimaTypeAndFeat first with : (Type And Features), then the Features with / (path)
		String[] typeAndFeatArray = uimaTypeAndFeat.split(":");

		// there is a feature path, so split the single component of the path (feature) in an array
		String[] featurePathElementNamesArray = typeAndFeatArray[1].split("/");

		List<StringOrFeatureStruct> valuesList = getValueOfFeature(ann, featurePathElementNamesArray, 0);
		if (valuesList == null || valuesList.isEmpty()) {
			return null;
		}
		return valuesList;
	}

	private static List<StringOrFeatureStruct> getValueOfFeature(FeatureStructure featStruct, String[] path,
																 int posFeatPath) {

		if (posFeatPath > path.length)
			return null;
		if (featStruct == null)
			return null;
		String featName = path[posFeatPath];

		// check if this last element is COMPLETE_ANNOTATION, in this case return the current annotation
		if (path.length == posFeatPath + 1 && featName.compareTo(COMPLETE_ANNOTATION) == 0) {
			List<StringOrFeatureStruct> valueList = new ArrayList<StringOrFeatureStruct>();
			StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(featStruct);
			valueList.add(stringOrFeatureStruct);
			return valueList;
		}

		// check if this last element is "coveredText" in this case call directly getCoveredText without
		// doing anything else
		if (path.length == posFeatPath + 1 && featName.compareTo(COVERED_TEXT) == 0
				&& featStruct instanceof Annotation) {
			List<StringOrFeatureStruct> valueList = new ArrayList<StringOrFeatureStruct>();
			StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(
					((Annotation) featStruct).getCoveredText());
			valueList.add(stringOrFeatureStruct);
			return valueList;
		}

		Feature feature = featStruct.getType().getFeatureByBaseName(featName.split("\\[")[0]);

		if (feature == null) {
			return null;
		}

		// check if we are analyzing the last element of the array
		if (path.length == posFeatPath + 1) {
			// if is a primitive return it as aString
			if (feature.getRange().isPrimitive()) {
				List<StringOrFeatureStruct> valueList = new ArrayList<StringOrFeatureStruct>();
				String value = featStruct.getFeatureValueAsString(feature);

				if (value == null) {
					return null;
				}

				StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(value);
				valueList.add(stringOrFeatureStruct);
				return valueList;
			} else if (isPrimitiveArray(feature)) { // check if it an array of primitive value
				return getPrimitiveValuesFromArray(feature, featStruct);

			} else if (isPrimitiveList(feature)) { // check if it a list of primitive value
				return getPrimitiveValuesFromList(feature, featStruct);

			} else {
				// return null; //TODO check
				List<StringOrFeatureStruct> valueList = new ArrayList<StringOrFeatureStruct>();
				FeatureStructure featStructCurrent = featStruct.getFeatureValue(feature);

				if (featStructCurrent == null) {
					return null;
				}

				StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(featStructCurrent);
				valueList.add(stringOrFeatureStruct);
				return valueList;
			}
		}

		// check if the uimaType is an Array
		if (feature.getRange().isArray()) {
			boolean posSpecified = false;
			int posList = -1;
			if (featName.contains("[")) {
				posList = Integer.parseInt(featName.split("\\[")[1].split("\\]")[0]);
				posSpecified = true;
			}
			return getValueOfFeatureFromArray(featStruct.getFeatureValue(feature), posFeatPath, path,
					posSpecified, posList);
		}
		// check if the uimaType is a List
		else if (feature.getRange().getName().startsWith("uima.cas")
				&& feature.getRange().getName().endsWith("List")) {
			boolean posSpecified = false;
			int posList = -1;
			if (featName.contains("[")) {
				posList = Integer.parseInt(featName.split("\\[")[1].split("\\]")[0]);
				posSpecified = true;
			}
			return getValueOfFeatureFromList(featStruct.getFeatureValue(feature), posFeatPath, path,
					posSpecified, posList);
		}
		// the featureStruct is not a List nor an Array, so it must be a complex one
		List<StringOrFeatureStruct> stringValueArray = new ArrayList<StringOrFeatureStruct>();
		FeatureStructure featureValue;
		try {
			featureValue = featStruct.getFeatureValue(feature);
		} catch (CASRuntimeException ex) {
			// There was an exception, mainly because it was a primitive feature before, so it cannot
			// be further navigate. In this case return null
			// TODO decide if, instead of null, an exception should be thrown
			logger.error(ex.getMessage());
			return null;
		}
		List<StringOrFeatureStruct> valueList = getValueOfFeature(featureValue, path, posFeatPath + 1);
		if (valueList == null) {
			return null;
		}
		stringValueArray.addAll(valueList);
		return stringValueArray;

	}

	private static boolean isPrimitiveArray(Feature feature) {
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_BOOLEAN_ARRAY))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_BYTE_ARRAY))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_DOUBLE_ARRAY))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_FLOAT_ARRAY))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_INTEGER_ARRAY))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_LONG_ARRAY))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_SHORT_ARRAY))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_STRING_ARRAY))
			return true;

		return false;
	}

	private static List<StringOrFeatureStruct> getPrimitiveValuesFromArray(Feature feature,
																		   FeatureStructure featStruct) {
		List<StringOrFeatureStruct> list = new ArrayList<StringOrFeatureStruct>();

		if (feature.getRange().getName().equals(CAS.TYPE_NAME_BOOLEAN_ARRAY)) {
			// the feature is a BooleanArray
			BooleanArray ba = (BooleanArray) featStruct.getFeatureValue(feature);
			if (ba != null) {
				String array[] = ba.toStringArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_BYTE_ARRAY)) {
			// the feature is a ByteArray
			ByteArray ba = (ByteArray) featStruct.getFeatureValue(feature);
			if (ba != null) {
				String array[] = ba.toStringArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_DOUBLE_ARRAY)) {
			// the feature is a DoubleArray
			DoubleArray da = (DoubleArray) featStruct.getFeatureValue(feature);
			if (da != null) {
				String array[] = da.toStringArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_FLOAT_ARRAY)) {
			// the feature is a FloatArray
			FloatArray fa = (FloatArray) featStruct.getFeatureValue(feature);
			if (fa != null) {
				String array[] = fa.toStringArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_INTEGER_ARRAY)) {
			// the feature is a IntegerArray
			IntegerArray ia = (IntegerArray) featStruct.getFeatureValue(feature);
			if (ia != null) {
				String array[] = ia.toStringArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_LONG_ARRAY)) {
			// the feature is a LongArray
			LongArray la = (LongArray) featStruct.getFeatureValue(feature);
			if (la != null) {
				String array[] = la.toStringArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_SHORT_ARRAY)) {
			// the feature is a ShortArray
			ShortArray sa = (ShortArray) featStruct.getFeatureValue(feature);
			if (sa != null) {
				String array[] = sa.toStringArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_STRING_ARRAY)) {
			// the feature is a StringArray
			StringArray sa = (StringArray) featStruct.getFeatureValue(feature);
			if (sa != null) {
				String array[] = sa.toArray();
				for (int i = 0; i < array.length; ++i) {
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(array[i]);
					list.add(stringOrFeatureStruct);
				}
			}
		}

		return list;
	}

	private static boolean isPrimitiveList(Feature feature) {
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_FLOAT_LIST))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_INTEGER_LIST))
			return true;
		if (feature.getRange().getName().equals(CAS.TYPE_NAME_STRING_LIST))
			return true;

		return false;
	}

	private static List<StringOrFeatureStruct> getPrimitiveValuesFromList(Feature feature,
																		  FeatureStructure featStruct) {
		List<StringOrFeatureStruct> list = new ArrayList<StringOrFeatureStruct>();

		if (feature.getRange().getName().equals(CAS.TYPE_NAME_FLOAT_LIST)) {
			// the feature is a FloatList
			FloatList tempFloatList = (FloatList) featStruct.getFeatureValue(feature);
			while (true) {
				if (tempFloatList instanceof NonEmptyFloatList) {
					// list.add(((NonEmptyFloatList) tempFloatList).getHead()+"");
					String valueString = ((NonEmptyFloatList) tempFloatList).getHead() + "";
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(valueString);
					list.add(stringOrFeatureStruct);
					tempFloatList = ((NonEmptyFloatList) tempFloatList).getTail();
				} else
					break;
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_INTEGER_LIST)) {
			// the feature is a IntegerList
			IntegerList tempIntegerList = (IntegerList) featStruct.getFeatureValue(feature);
			while (true) {
				if (tempIntegerList instanceof NonEmptyIntegerList) {
					// list.add(((NonEmptyIntegerList) tempIntegerList).getHead()+"");
					String valueString = ((NonEmptyIntegerList) tempIntegerList).getHead() + "";
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(valueString);
					list.add(stringOrFeatureStruct);
					tempIntegerList = ((NonEmptyIntegerList) tempIntegerList).getTail();
				} else
					break;
			}
		} else if (feature.getRange().getName().equals(CAS.TYPE_NAME_STRING_LIST)) {
			// the feature is a StringList
			StringList tempStringList = (StringList) featStruct.getFeatureValue(feature);
			while (true) {
				if (tempStringList instanceof NonEmptyStringList) {
					// list.add(((NonEmptyStringList) tempStringList).getHead());
					String valueString = ((NonEmptyStringList) tempStringList).getHead() + "";
					StringOrFeatureStruct stringOrFeatureStruct = new StringOrFeatureStruct(valueString);
					list.add(stringOrFeatureStruct);
					tempStringList = ((NonEmptyStringList) tempStringList).getTail();
				} else
					break;
			}
		}

		return list;
	}

	private static List<StringOrFeatureStruct> getValueOfFeatureFromList(FeatureStructure featStruct,
																		 int posFeatPath, String[] path, boolean posSpecified, int posList) {
		if (featStruct == null)
			return null;
		Type uimaType = featStruct.getType();
		if ((uimaType.getName().compareTo(CAS.TYPE_NAME_NON_EMPTY_FS_LIST) == 0)) {
			FSList fsList = (FSList) featStruct;
			if (posSpecified) {
				FeatureStructure nextFeatStruct = getElemFromListAtPos(fsList, posList);
				if (nextFeatStruct == null)
					return null;
				return getValueOfFeature(nextFeatStruct, path, posFeatPath + 1);
			} else {
				List<StringOrFeatureStruct> valueList = new ArrayList<StringOrFeatureStruct>();

				List<FeatureStructure> featStructList = getAllElemFromList(fsList);
				for (FeatureStructure featStructFromList : featStructList) {
					List<StringOrFeatureStruct> valueListTemp = getValueOfFeature(featStructFromList, path,
							posFeatPath + 1);
					valueList.addAll(valueListTemp);
				}
				return valueList;
			}
		} /*
			 * else if ((uimaType.getName().compareTo(NONEMPTYSTRINGLIST) == 0)) { StringList stringList =
			 * (StringList) featStruct; if (posFeatPath >= path.length) return null; if (posSpecified) {
			 * String value = getElemFromListAtPos(stringList, posList); if (value == null) return null;
			 * List<String> valueList = new ArrayList<String>(); valueList.add(value); return valueList; }
			 * else { return getAllElemFromList(stringList); } }
			 */
		return null;

	}

	private static FeatureStructure getElemFromListAtPos(FSList fsList, int pos) {
		FSList tempList = fsList;
		for (int i = 0; i < pos; ++i) {
			if (tempList instanceof NonEmptyFSList)
				tempList = ((NonEmptyFSList) tempList).getTail();
			else
				return null;
		}
		if (tempList instanceof NonEmptyFSList)
			return ((NonEmptyFSList) tempList).getHead();
		else
			return null;
	}

	private static List<FeatureStructure> getAllElemFromList(FSList fsList) {
		FSList tempFSList = fsList;
		List<FeatureStructure> featStructList = new ArrayList<FeatureStructure>();
		while (true) {
			if (tempFSList instanceof NonEmptyFSList) {
				featStructList.add(((NonEmptyFSList) tempFSList).getHead());
				tempFSList = ((NonEmptyFSList) tempFSList).getTail();
			} else
				break;
		}
		return featStructList;
	}

	/*
	 * private static String getElemFromListAtPos(StringList stringList, int pos) { StringList tempList =
	 * stringList; for (int i = 0; i < pos; ++i) { if (tempList instanceof NonEmptyStringList) tempList =
	 * ((NonEmptyStringList) tempList).getTail(); else return null; } if (tempList instanceof
	 * NonEmptyStringList) return ((NonEmptyStringList) tempList).getHead(); else return null; }
	 */

	/*
	 * private static List<String> getAllElemFromStringList(StringList stringList) { StringList tempStringList
	 * = stringList; List<String> listOfString = new ArrayList<String>(); while (true) { if (tempStringList
	 * instanceof NonEmptyStringList) { listOfString.add(((NonEmptyStringList) tempStringList).getHead());
	 * tempStringList = ((NonEmptyStringList) tempStringList).getTail(); } else break; } return listOfString;
	 * }
	 */

	private static List<StringOrFeatureStruct> getValueOfFeatureFromArray(FeatureStructure featStruct,
																		  int posFeatPath, String[] path, boolean posSpecified, int posArray) {
		// Type uimaType = featStruct.getType();
		if (featStruct == null) { // TODO check
			return null;
		}
		List<StringOrFeatureStruct> valueList = new ArrayList<StringOrFeatureStruct>();

		FSArray fsArray = (FSArray) featStruct;
		if (posSpecified) {
			if (fsArray.size() > posArray) {
				return getValueOfFeature(fsArray.get(posArray), path, posFeatPath + 1);
			} else
				return null;
		} else {
			for (int i = 0; i < fsArray.size(); ++i) {
				List<StringOrFeatureStruct> valueListTemp = getValueOfFeature(fsArray.get(i), path,
						posFeatPath + 1);
				if (valueListTemp != null)
					valueList.addAll(valueListTemp);
			}
		}
		// }
		return valueList;
	}

	/*
	 * private static List<String> getValueOfFeatureGeneric(FeatureStructure featStruct, int posFeatPath,
	 * String[] path) { Type uimaType = featStruct.getType();
	 * 
	 * return null;
	 * 
	 * }
	 */

	public static Value getRDFValueFromUIMAValue(PlaceholderStruct placeholder, ComponentProvider cp,
												 CODAContext ctx) throws ComponentProvisioningException, ConverterException {
		return getRDFValueFromUIMAValue(placeholder, null, cp, ctx);
	}

	public static Value getRDFValueFromUIMAValue(PlaceholderStruct placeholder,
												 StringOrFeatureStruct valueOfFeatStruct, ComponentProvider cp, CODAContext ctx)
			throws ComponentProvisioningException, ConverterException {

		Object value = null;
		if (placeholder.hasFeaturePath()) {
			if (valueOfFeatStruct.hasFeatureStruct()) {
				value = valueOfFeatStruct.getFeatureStruct();
			} else { // valueOfFeatStruct.hasStringValue()
				value = valueOfFeatStruct.getStringValue();
			}
		}

		Optional<IRI> nsOverride = overrideDefaultNS(placeholder, ctx);
		ctx.setCodaMemoizedAnnotation(getMemoizedAnnotationIfPresent(placeholder));
		ctx.setPlaceholderStruct(placeholder);

		try {
			return getRDFValuesFromUIMAValue(placeholder.hasFeaturePath(), value, placeholder.getRDFType(),
					placeholder.getLiteralDatatype(), placeholder.getLiteralLang(),
					placeholder.getConverterList(), cp, ctx, null, null, getMemoizationMapNameIfAny(placeholder))
					.stream().findAny().orElse(null);
		} finally {
			if (nsOverride.isPresent()) {
				ctx.setDefaultNamespaceOverride(null);
			}
		}
	}

	private static Optional<IRI> overrideDefaultNS(PlaceholderStruct placeholder, CODAContext ctx) {
		Optional<IRI> nsOverride = CollectionUtils.emptyIfNull(placeholder.getAnnotationList()).stream()
				.filter(ann -> PearlParserDescription.DEFAULT_NAMESPACE_ANN.equals(ann.getName()))
				.map(ann -> (ParamValueIri) ann.getParamValueList("value").iterator().next())
				.map(ParamValueIri::getIri)
				.findAny();

		nsOverride.map(IRI::stringValue).ifPresent(ctx::setDefaultNamespaceOverride);
		return nsOverride;
	}

	public static List<Value> getRDFValuesFromUIMAValue(PlaceholderStruct placeholder,
														StringOrFeatureStruct valueOfFeatStruct, ComponentProvider cp, CODAContext ctx,
														Multimap<String, Object> placeholder2valueMap,
														Map<String, Map<String, Value>> memoizedProjectionsMap)
			throws ComponentProvisioningException, ConverterException {
		Object initialValue = null;

		if (placeholder.hasFeaturePath()) {
			if (valueOfFeatStruct.hasFeatureStruct()) {
				initialValue = valueOfFeatStruct.getFeatureStruct();
			} else { // valueOfFeatStruct.hasStringValue()
				initialValue = valueOfFeatStruct.getStringValue();
			}
		}

		Optional<IRI> nsOverride = overrideDefaultNS(placeholder, ctx);
		ctx.setCodaMemoizedAnnotation(getMemoizedAnnotationIfPresent(placeholder));
		ctx.setPlaceholderStruct(placeholder);

		try {
			return getRDFValuesFromUIMAValue(placeholder.hasFeaturePath(), initialValue, placeholder.getRDFType(),
					placeholder.getLiteralDatatype(), placeholder.getLiteralLang(),
					placeholder.getConverterList(), cp, ctx, placeholder2valueMap, memoizedProjectionsMap,
					getMemoizationMapNameIfAny(placeholder));
		} finally {
			if (nsOverride.isPresent()) {
				ctx.setDefaultNamespaceOverride(null);
			}
		}
	}

	public static String getMemoizationMapNameIfAny(PlaceholderStruct ph) {
		it.uniroma2.art.coda.pearl.model.annotation.Annotation memoizedAnn = getMemoizedAnnotationIfPresent(ph);
		if (memoizedAnn==null) {
			return null;
		}
		Map<String, List<ParamValueInterface>> paramMap = memoizedAnn.getParamMap();
		List<ParamValueInterface> valueForDefaultValue = paramMap.get(PearlParserDescription.DEFAULT_NAME_FOR_PARAM);
		for (ParamValueInterface paramValueInterface : valueForDefaultValue) {
			return paramValueInterface.asString();
		}
		return null;
	}

	public static it.uniroma2.art.coda.pearl.model.annotation.Annotation getMemoizedAnnotationIfPresent(PlaceholderStruct ph){
		List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> codaAnnotationList = ph.getAnnotationList();
		for (it.uniroma2.art.coda.pearl.model.annotation.Annotation codaAnnotation : codaAnnotationList) {
			String codaAnnotationName = codaAnnotation.getName();
			if (codaAnnotationName.equals(PearlParserDescription.MEMOIZED_ANN)) {
				return codaAnnotation;
			}
		}
		return null;
	}

	public static boolean getMemoizationIgnoreCaseOrDefaultValue(it.uniroma2.art.coda.pearl.model.annotation.Annotation codaAnnotation) {
		if (codaAnnotation==null) {
			return false;
		}
		List<ParamValueInterface> paramValueInterfaceList = codaAnnotation.getParamValueList(PearlParserDescription.MEMOIZED_ANN_PARAM_IGNORECASE);
		for (ParamValueInterface paramValueInterface : paramValueInterfaceList) {
			String value = normalizeStringToBoolean(paramValueInterface.asString());
			return Boolean.parseBoolean(value);
		}
		// no value, nor the default value has been found so return null (in theory this should never happen)
		return false;
	}

	public static boolean checkExistenceAnnotation(String annName, PlaceholderStruct placeholderStruct) {
		List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> plchldAnnList = placeholderStruct.getAnnotationList();
		for (it.uniroma2.art.coda.pearl.model.annotation.Annotation annotation : plchldAnnList) {
			if (annotation.getName().equals(annName)) {
				//found the annotation
				return true;
			}
		}
		//annotation not found in the placeholderStruct
		return false;
	}


	public static boolean getBooleanValueFromAnnValueFromPlhldOrDefeaultValueFromDef(String annName, String paramName,
																					 PlaceholderStruct placeholderStruct, boolean valueToReturnOtherwise) {
		//check, in this order, if:
		// - the placeholder has the desired annotation, if so, get its value or, if no value is specified, its default value
		// - the rule has the desired annotation, if so, get its value or, if no value is specified, its default value
		// - otherwise return the input valueToReturnOtherwise

		it.uniroma2.art.coda.pearl.model.annotation.Annotation desiredCodaAnn = null;

		//check the placeholder
		List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> plchldAnnList = placeholderStruct.getAnnotationList();
		for (it.uniroma2.art.coda.pearl.model.annotation.Annotation annotation : plchldAnnList) {
			if (annotation.getName().equals(annName)) {
				desiredCodaAnn = annotation;
				//found the annotation
				break;
			}
		}

		if (desiredCodaAnn!=null) {
			//the annotation was found, so get its value
			List<ParamValueInterface> paramValueInterfaceList = desiredCodaAnn.getParamValueList(paramName);
			for (ParamValueInterface paramValueInterface : paramValueInterfaceList) {
				String value = paramValueInterface.asString().replaceAll("'", "");
				return !(value.equalsIgnoreCase("false")); //Boolean.parseBoolean(value);
			}
		}

		// the annotation was not found in the placeholder, so search it in the rule
		ProjectionRule projectionRule = placeholderStruct.getOwnerRule();
		plchldAnnList = projectionRule.getAnnotationList();
		for (it.uniroma2.art.coda.pearl.model.annotation.Annotation annotation : plchldAnnList) {
			if (annotation.getName().equals(annName)) {
				desiredCodaAnn = annotation;
				//found the annotation
				break;
			}
		}

		if (desiredCodaAnn!=null) {
			//the annotation was found, so get its value
			List<ParamValueInterface> paramValueInterfaceList = desiredCodaAnn.getParamValueList(paramName);
			for (ParamValueInterface paramValueInterface : paramValueInterfaceList) {
				String value = paramValueInterface.asString().replaceAll("'", "");
				return !(value.equalsIgnoreCase("false")); // Boolean.parseBoolean(value);
			}
		}

		// the annotation was not found in either the placeholder nor in the rule, so get its default value from the annotation definition
        /*
		if (placeholderStruct.getOwnerRule().getProjectionRulesModel()!=null) {
			AnnotationDefinition annotationDefinition =
					placeholderStruct.getOwnerRule().getProjectionRulesModel().getAnnotationDefinition(annName);
			if (annotationDefinition != null) {
				// found the annotation definition, so now get its default value
				ParamDefinition paramDefinition = annotationDefinition.getParamDefinition(paramName);
				if (paramDefinition.hasDefault()) {
					String defaultValue = normalizeStringToBoolean(paramDefinition.getDefaultValue().asString());
					return Boolean.parseBoolean(defaultValue);
				} else {
					return valueToReturnOtherwise;
				}
			}
		}
		 */

		return valueToReturnOtherwise;
	}

	private static String normalizeStringToBoolean(String text){
		if (text.startsWith("'") && text.endsWith("'")) {
			text = text.replaceAll("'", "");
		} else  if (text.startsWith("\"") && text.endsWith("\"")) {
			text = text.replaceAll("\"", "");
		}
		return text;
	}

	private static String removePunctuation(String text, PlaceholderStruct placeholderStruct) {
		//check in the placeholderStruct if the REMOVE_PUNCTUATION_ANN has a value in it and such value is not "true"
		// or false (if the value is "false", then the removePunctuation is not even applied and the text is returned
		// as it is)
		// If the value if different that "true" and "false", then this value is the characters
		// that are considered as punctuation to remove. If it has no value, then use the default one(".,:;!?")

		//get the value in the annotation
		it.uniroma2.art.coda.pearl.model.annotation.Annotation desiredCodaAnn = null;
		List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> plchldAnnList = placeholderStruct.getAnnotationList();
		for (it.uniroma2.art.coda.pearl.model.annotation.Annotation annotation : plchldAnnList) {
			if (annotation.getName().equals(PearlParserDescription.REMOVE_PUNCTUATION_ANN)) {
				desiredCodaAnn = annotation;
				//found the annotation
				break;
			}
		}
		if (desiredCodaAnn==null) {
			//REMOVE_PUNCTUATION_ANN not found in the placeholderStruct, so check if it exists in the rule
			ProjectionRule projectionRule = placeholderStruct.getOwnerRule();
			List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> ruleAnnList = projectionRule.getAnnotationList();
			for (it.uniroma2.art.coda.pearl.model.annotation.Annotation annotation : ruleAnnList) {
				if (annotation.getName().equals(PearlParserDescription.REMOVE_PUNCTUATION_ANN)) {
					desiredCodaAnn = annotation;
					//found the annotation
					break;
				}
			}
			if (desiredCodaAnn==null) {
				// REMOVE_PUNCTUATION_ANN not found even in the rule, so return the text as it is
				return text;
			}
		}

		//REMOVE_PUNCTUATION_ANN was found, so now see if it has a value

		String sequenceOfPuncuation = null;
		List<ParamValueInterface> paramValueInterfaceList = desiredCodaAnn.getParamValueList(PearlParserDescription.DEFAULT_NAME_FOR_PARAM);
		for (ParamValueInterface paramValueInterface : paramValueInterfaceList) {
			sequenceOfPuncuation = paramValueInterface.asString().replaceAll("'", "");
		}
		//check if the value was found or not, and behave accordingly
		boolean useDefault;
		if (sequenceOfPuncuation!=null && !sequenceOfPuncuation.isEmpty()) {
			//a value was found, check if:
			// - it is false -> then return text as it is
			// - it is true -> use the default list punctuations to remove
			// - something different, so it is the list of punctuation to use
			if (sequenceOfPuncuation.equals("false")) {
				// return the input text as it is
				return text;
			} else if (sequenceOfPuncuation.equals("true")) {
				// it is true, so use the default list of punctuation
				useDefault = true;
			} else {
				useDefault = false;
				String regex;
				//use the passed sequence
				regex = "";
				boolean first = true;
				for (int i = 0; i < sequenceOfPuncuation.length(); ++i) {
					if (!first) {
						regex += "|";
					}
					first = false;
					regex += "(\\Q" + sequenceOfPuncuation.charAt(i) + "\\E)";
				}
				text = text.replaceAll(regex, " ");
				text = text.replaceAll("[" + sequenceOfPuncuation + "]$", "");
				text = text.replaceAll("[" + sequenceOfPuncuation + "]", " ");
			}
		} else {
			//no value was found, so use the default list of punctuation to remove
			useDefault = true;
		}


		if (useDefault) {
			// the default list of punctuation to remove should be used

			text = text.replaceAll("(\\. )|(, )|(: )|(; )|(! )|(\\? )", " ");
			text = text.replaceAll("[.,:;!?]$", "");
			text = text.replaceAll("[.,:;!?]", " ");
		}

		return text;
	}


	/**
	 * Computes a (possibly empty) collection of ART Nodes through the execution of a converter. The value of
	 * <code>initialValue</code> is passed to the converter regardless of whether it is <code>null</code>.
	 * Usually, the client should not invoke this method if the initial value is <code>null</code>, unless the
	 * target converter does not require an input argument (as in the case of a random identifier generator).
	 * Conversely, if a converter has an additional argument containing a non initialize placeholder, this
	 * Immediately stops the computation, which returns an empty list.
	 *
	 * @param hasFeaturePath
	 *            indicates if the originating node is associated with a feature path
	 * @param initialValue
	 *            the initial value (extracted from the feature path) (may be <code>null</code>)
	 * @param rdfType
	 *            the kind of node to produce (i.e. either "literal" or "uri")
	 * @param literalDatatype
	 *            the datatype of a literal (may be <code>null</code>)
	 * @param literalLang
	 *            the language tag of a literal (may be <code>null</code>)
	 * @param converterList
	 *            the list of converter mentions to evaluate
	 * @param cp
	 *            the provider of CODA components
	 * @param ctx
	 *            the context for the execution of the converter
	 * @param placeholder2valueMap
	 *            a structure containing the nodes associated to placeholders
	 * @param memoizedProjectionsMaps
	 *            memoized projections
	 * @param memoizationMapName
	 *            optional name of a memoization map o enable memoization
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public static List<Value> getRDFValuesFromUIMAValue(boolean hasFeaturePath, Object initialValue,
														String rdfType, String literalDatatype, String literalLang, List<ConverterMention> converterList,
														ComponentProvider cp, CODAContext ctx, Multimap<String, Object> placeholder2valueMap,
														Map<String, Map<String, Value>> memoizedProjectionsMaps, String memoizationMapName)
			throws ComponentProvisioningException, ConverterException {

		//check the initialValue if it is not null
		if (initialValue!=null) {
			//initialValue is not null, now check if it is a String and a not-empty one
			if ((initialValue instanceof  String) && (!((String) initialValue).isEmpty()) ) {
				// initialValue it is a not-empty String, so check if the normalization Annotations (REMOVE_PUNCTUATION_ANN,
				// LOWER_CASE_ANN, CAPITALIZE_ANN, REMOVE_DUP_SPACES_ANN and TRIM_ANN)
				if (getBooleanValueFromAnnValueFromPlhldOrDefeaultValueFromDef(PearlParserDescription.REMOVE_PUNCTUATION_ANN, PearlParserDescription.DEFAULT_NAME_FOR_PARAM,
						ctx.getPlaceholderStruct(), false)) {
					// remove punctuation
					//initialValue = removePunctuation((String) initialValue, ctx.getPlaceholderStruct());
					initialValue = removePunctuation((String) initialValue, ctx.getPlaceholderStruct());
				}
				if (getBooleanValueFromAnnValueFromPlhldOrDefeaultValueFromDef(PearlParserDescription.LOWER_CASE_ANN, PearlParserDescription.DEFAULT_NAME_FOR_PARAM,
						ctx.getPlaceholderStruct(), false)) {
					// lowerCase the Initial Value
					initialValue = ((String) initialValue).toLowerCase(Locale.ROOT);
				}
				if (getBooleanValueFromAnnValueFromPlhldOrDefeaultValueFromDef(PearlParserDescription.UPPER_CASE_ANN, PearlParserDescription.DEFAULT_NAME_FOR_PARAM,
						ctx.getPlaceholderStruct(), false)) {
					// Capitalize the Initial Value
					initialValue = ((String) initialValue).toUpperCase(Locale.ROOT);
				}
				if (getBooleanValueFromAnnValueFromPlhldOrDefeaultValueFromDef(PearlParserDescription.REMOVE_DUP_SPACES_ANN, PearlParserDescription.DEFAULT_NAME_FOR_PARAM,
						ctx.getPlaceholderStruct(), Boolean.parseBoolean(PearlParserDescription.REMOVE_DUP_SPACES_ANN_PARAM_IGNORECASE_DEFAULT_VALUE))) {
					// remove multiple whitespaces from the Initial Value
					initialValue = ((String) initialValue).replaceAll("  +", " ");
				}
				if (getBooleanValueFromAnnValueFromPlhldOrDefeaultValueFromDef(PearlParserDescription.TRIM_ANN, PearlParserDescription.DEFAULT_NAME_FOR_PARAM,
						ctx.getPlaceholderStruct(), Boolean.parseBoolean(PearlParserDescription.TRIM_ANN_PARAM_IGNORECASE_DEFAULT_VALUE))) {
					// trim the Initial Value
					initialValue = ((String) initialValue).trim();
				}

			}
		}


		List<Object> outerValueList = new ArrayList<>();

		for (ProjectionOperator projectionOperator : new ProjectionOperator(NodeType.valueOf(rdfType),
				literalLang, literalDatatype, converterList)
				.instantiateWithPlaceholderValues(placeholder2valueMap)) {

			// if the Memoized annotation had the ignorecase=true (explicitly or it was its default value), then check if
			// initialValue is a String. Is so, set i to lowerCase to use it as the memoizationKey (so in the memoizedProjections)

			String memoizationKey = null;

			if (memoizationMapName != null) {
				//since memoizationMapName is not null, it means that the Memoized annotation was used, so check if
				// ignoreCase is true or false
				boolean ignoreCase = getMemoizationIgnoreCaseOrDefaultValue(ctx.getCodaMemoizedAnnotation());

				memoizationKey = calculateMemoizationKey(projectionOperator, initialValue, ignoreCase);

				Map<String, Value> memoizedProjections = memoizedProjectionsMaps.computeIfAbsent(memoizationMapName, k -> new HashMap<>());
				Object memoizedValue = memoizedProjections.get(memoizationKey);
				if (memoizedValue != null) {
					outerValueList.add(memoizedValue);
					continue;
				}
			}

			Object currentValue = initialValue;

			for (ConverterMention converter : projectionOperator.getConverterMentions()) {

				List<Class<?>> argumentTypes = new ArrayList<>();
				List<Object> argumentValues = new ArrayList<>();

				argumentTypes.add(CODAContext.class);
				argumentValues.add(ctx);

				if (rdfType.equalsIgnoreCase("literal")) {
					argumentTypes.add(String.class);
					argumentTypes.add(String.class);

					argumentValues.add(literalDatatype);
					argumentValues.add(literalLang);
				}

				argumentTypes.add(currentValue != null ? currentValue.getClass() : null);
				argumentValues.add(currentValue);

				converter.getAdditionalArguments().stream().forEach(arg -> {
					argumentValues.add(arg.getGroundObject());
					argumentTypes.add(arg.getArgumentType());
				});

				Object converterObj = cp.lookup(converter.getURI());

				Method m = searchSuitableTarget(converterObj.getClass(),
						rdfType.equalsIgnoreCase("literal") ? "produceLiteral" : "produceURI", argumentTypes);

				if (m == null) {
					throw new ConverterException("The converter '" + converterObj.getClass().getName()
							+ "' does not have a suitable method to invoke");
				}

				try {
					try {
						List<Object> aValueList = adaptArgumentList(m, argumentValues);

						Object value = m.invoke(converterObj, (Object[]) aValueList.toArray());

						currentValue = value;
					} catch (Exception e) {
						throw e;
					}

				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					Throwable throwable = e.getCause();
					if (throwable instanceof ConverterException) {
						throw (ConverterException)throwable;
					}
					throw new ConverterException(e);
				}
			}

			outerValueList.add(currentValue);

			if (memoizationMapName != null) {
				//Map<Pair<ProjectionOperator, Object>, Value> memoizedProjections = memoizedProjectionsMaps.computeIfAbsent(memoizationMapName, k -> new HashMap<>());
				Map<String, Value> memoizedProjectionsMap = memoizedProjectionsMaps.computeIfAbsent(memoizationMapName, k -> new HashMap<>());
				memoizedProjectionsMap.put(memoizationKey, (Value) currentValue);
			}
		}

		return (List<Value>) (Object) outerValueList;
	}

	private static String calculateMemoizationKey(ProjectionOperator projectionOperator,
												  Object initialValue, boolean ignoreCase) {
		StringBuilder sb = new StringBuilder();
		//create a String containing all the information about the projectionOperator and initialValue
		sb.append(projectionOperator.getNodeType()).append("_").append(projectionOperator.getDatatype().orElse(null))
				.append("_").append(projectionOperator.getLanguage().orElse(null));
		List<ConverterMention> converterMentionList = projectionOperator.getConverterMentions();
		for (ConverterMention converterMention : converterMentionList) {
			sb.append("_").append(converterMention.getURI());
			for ( ConverterArgumentExpression converterArgumentExpression : converterMention.getAdditionalArguments()) {
				sb.append("_").append(converterArgumentExpression.toString());
			}
		}
		sb.append("_").append(initialValue);
		return ignoreCase ? sb.toString().toLowerCase() : sb.toString();
	}


	private static List<Class<?>> getArgumentTypes(List<Object> valueList) {
		Set<Class<?>> typeSet = new HashSet<Class<?>>();

		for (Object v : valueList) {
			typeSet.add(v != null ? v.getClass() : null);
		}

		return Lists.newArrayList(typeSet);
	}

	public static <T> void appendValuesToMultiList(List<List<T>> multiList, List<? extends T> values) {

		T singleAddition = null;

		if (values.size() == 1) {
			singleAddition = values.get(0);
		}

		if (multiList.isEmpty()) {
			multiList.add(new ArrayList<>());
		}

		if (singleAddition != null) {
			for (List<T> aList : multiList) {
				aList.add(singleAddition);
			}
		} else {
			ListIterator<List<T>> it = multiList.listIterator();

			while (it.hasNext()) {
				List<T> aList = it.next();

				it.remove();

				for (T v : values) {
					ArrayList<T> newList = new ArrayList<>(aList);
					newList.add(v);
					it.add(newList);
				}
			}
		}
	}

	public static List<Object> evaluateConverterArgumentExpression(ConverterArgumentExpression expr,
																   Multimap<String, Object> placeholder2valueMap) throws ConverterException {
		return explodeSingleConverterArgument(expr, placeholder2valueMap);
	}

	/**
	 * Returns a method of the provided class having a signature compatible with the given method name and
	 * argument types (see {@link #checkMethodSuitability(Method, String, List)}).
	 *
	 * @param clazz
	 * @param methodName
	 * @param argumentTypes
	 * @return a suitable method, or <code>null</code> if none exists
	 */
	public static Method searchSuitableTarget(Class<?> clazz, String methodName,
											  List<Class<?>> argumentTypes) {

		for (Method method : clazz.getMethods()) {
			if (checkMethodSuitability(method, methodName, argumentTypes))
				return method;
		}

		return null;
	}

	/**
	 * Returns a method of the provided class having a signature compatible with the given method name and
	 * each provided combination of argument types (see {@link #checkMethodSuitability(Method, String, List)}.
	 *
	 * @param clazz
	 * @param methodName
	 * @param argumentMultiTypes
	 * @return a suitable method, or <code>null</code> if none exists
	 */
	private static Method searchSuitableTargetForMultiArgs(Class<? extends Object> clazz, String methodName,
														   List<List<Class<?>>> argumentMultiTypes) {
		MethodLookup: for (Method method : clazz.getMethods()) {
			for (List<Class<?>> argumentTypes : argumentMultiTypes) {
				if (!checkMethodSuitability(method, methodName, argumentTypes))
					continue MethodLookup;
			}

			return method;
		}

		return null;
	}

	/**
	 * Checks that a method signature is compatible with the given method name and argument types. When an
	 * argument is <code>null</code>, the corresponding item inside the list <code>argumentTypes</code> is
	 * also <code>null</code>. The compatibility between a method parameter type and the corresponding
	 * argument type depends on whether the former can be assigned from the latter (see:
	 * {@link #canBeAssignedFrom(Class, Class)})
	 *
	 * @param method
	 * @param methodName
	 * @param argumentTypes
	 * @return
	 */
	public static boolean checkMethodSuitability(Method method, String methodName,
												 List<Class<?>> argumentTypes) {
		if (!method.getName().equals(methodName)) {
			return false;
		}

		Class<?>[] parameterTypes = method.getParameterTypes();

		int boundary = Math.min(parameterTypes.length, argumentTypes.size());

		int i = 0;
		for (; i < boundary; i++) {

			if (!canBeAssignedFrom(parameterTypes[i], argumentTypes.get(i))) {
				break;
			}

		}

		// at this point, the variable "i" holds the position of the first argument (if any) that is not
		// assignable to the corresponding parameter.

		// If the variable "i" denotes an argument, then three conditions must be met:
		// - the corresponding parameter is the last parameter
		// - the corresponding parameter is array-typed
		// - the i-th argument and all the following ones must be assignable to the array component type

		// If the variable "i" does not denote an argument, then the corresponding parameter must be an
		// an array, which will be assigned with an empty array. If there is no corresponding parameter,
		// the all parameters matched the arguments.

		if (i < argumentTypes.size()) {

			if (i != parameterTypes.length - 1)
				return false;

			Class<?> lastParameterType = parameterTypes[i];

			if (!lastParameterType.isArray())
				return false;

			Class<?> componentType = lastParameterType.getComponentType();

			for (int j = i; j < argumentTypes.size(); j++) {
				if (!canBeAssignedFrom(componentType, argumentTypes.get(j))) {
					return false;
				}
			}
		} else {
			if (i == parameterTypes.length - 1) {
				if (!parameterTypes[i].isArray()) {
					return false;
				}
			} else if (i != parameterTypes.length) {
				return false;
			}
		}

		return true;
	}

	/**
	 * A generalization of {@link Class#isAssignableFrom(Class)} taking into consideration specific promotion
	 * opportunities for RDF4J Nodes and the compatibility of the <code>null</code> value with any reference
	 * type. The parameter <code>source</code> may be <code>null</code>, to indicate the type of a
	 * <code>null</code> value.
	 *
	 * @param dest
	 * @param source
	 * @return
	 */
	private static boolean canBeAssignedFrom(Class<?> dest, Class<?> source) {
		// If the source type is null (i.e. argument is null), then the assignment is
		// possible for any reference type
		if (source == null) {
			return Object.class.isAssignableFrom(dest);
		}

		if (dest.isAssignableFrom(source)) {
			return true;
		}

		if (source == Literal.class) {
			return dest == String.class;
		} else if (source == String.class) {
			return dest.isAssignableFrom(Literal.class);
		} else if (IRI.class.isAssignableFrom(source)) {
			return dest == String.class;
		} else if (Literal.class.isAssignableFrom(source)) {
			return dest == String.class;
		}

		return false;
	}

	public static List<Object> adaptArgumentList(Method m, List<Object> argumentList)
			throws ConverterException {
		Class<?>[] parameterTypes = m.getParameterTypes();
		java.lang.reflect.Type[] genericParameterTypes = m.getGenericParameterTypes();

		for (int i = 0; i < argumentList.size(); i++) {
			java.lang.reflect.Type aType;

			if (i >= (genericParameterTypes.length - 1)) {
				aType = genericParameterTypes[genericParameterTypes.length - 1];

				if (aType instanceof GenericArrayType) {
					aType = ((GenericArrayType) aType).getGenericComponentType();
				}
			} else {
				aType = genericParameterTypes[i];
			}

			Object arg = argumentList.get(i);

			if (arg == null)
				continue;

			Class<?> argType = arg.getClass();

			if (aType instanceof ParameterizedType) {
				ParameterizedType parameterizedType = ((ParameterizedType) aType);
				Class<?> rawClass = (Class<?>) parameterizedType.getRawType();
				Class<?> valueRawClass = (Class<?>) parameterizedType.getActualTypeArguments()[1];
				boolean isMap = Map.class.isAssignableFrom(rawClass);

				if (isMap) {
					argumentList.set(i, promoteMap((Map<String, Object>) arg, valueRawClass));
				}
			} else if (aType == String.class && Literal.class.isAssignableFrom(argType)) {
				argumentList.set(i, Literal.class.cast(arg).getLabel());
			} else if (aType == String.class && IRI.class.isAssignableFrom(argType)) {
				argumentList.set(i, IRI.class.cast(arg).stringValue());
			} else if (((Class<?>) aType).isAssignableFrom(Literal.class) && argType == String.class) {
				argumentList.set(i, SimpleValueFactory.getInstance().createLiteral((String) arg));

			}
		}

		if (parameterTypes.length > 0) {
			Class<?> lastParamType = parameterTypes[parameterTypes.length - 1];

			if (lastParamType.isArray()) {
				if (argumentList.size() >= parameterTypes.length) {
					List<Object> firstList = new ArrayList<Object>(
							argumentList.subList(0, parameterTypes.length - 1));
					List<Object> secondList = argumentList.subList(parameterTypes.length - 1,
							argumentList.size());
					Object arrayArg = Array.newInstance(lastParamType.getComponentType(), secondList.size());
					for (int i = 0; i < secondList.size(); i++) {
						Array.set(arrayArg, i, secondList.get(i));
					}
					firstList.add(arrayArg);
					return firstList;
				} else {
					List<Object> newArgs = new ArrayList<Object>(argumentList);
					newArgs.add(Array.newInstance(lastParamType.getComponentType(), 0));
					return newArgs;
				}
			}
		}

		return argumentList;
	}

	public static List<String> getSuperTypes(Annotation annotation, Class<?> annotationClass) {
		Class<?> inputAnnotationClass;
		if (annotationClass == null) {
			inputAnnotationClass = annotation.getClass();
		} else {
			// if annotationClass is not null, then assign it to inputAnnotationClass
			inputAnnotationClass = annotationClass;
		}

		List<String> superAnnotationClassList = new ArrayList<String>();

		// this part works when the annotation was generated dynamically by UIMAfit mechanism but the
		// Annotation class exists directly as a Java class (so the source code of the Annotation class was generated
		// and compiled)
		if (inputAnnotationClass.getName().compareTo(Annotation.class.getName()) != 0) {
			Class<?> tempClass = inputAnnotationClass.getSuperclass();
			superAnnotationClassList.add(tempClass.getName());
			if (tempClass.getName().compareTo(Annotation.class.getName()) != 0
					&& tempClass.getName().compareTo(TOP.class.getName()) != 0) {
				superAnnotationClassList.addAll(getSuperTypes(null, tempClass));
			}
		}

		// this part work with annotation created using the UIMAfit mechanism of dynamically defined an annotation and
		// its FS (so there is no Java source code nor an XML file describing such Annotaions/FS )
		if (annotation!=null) {
			CAS cas = annotation.getCAS();
			TypeSystem ts = cas.getTypeSystem();
			boolean foundTop = false;
			Type currentType = annotation.getType();
			while (!foundTop) {
				currentType = ts.getParent(currentType);
				if (currentType==null) {
					// there is no parent of the current type, so stop the while
					foundTop=true;
				} else {
					// found a parent, so save its name
					String typeName = currentType.getName();
					superAnnotationClassList.add(typeName);
				}
			}
		}
		// return the list containing the super types
		return superAnnotationClassList;
	}

	@SuppressWarnings("unchecked")
	private static List<Object> explodeSingleConverterArgument(ConverterArgumentExpression arg,
															   Multimap<String, Object> placeholder2valueMap) throws ConverterException {

		if (arg instanceof ConverterRDFLiteralArgument) {
			return Collections
					.<Object>singletonList(ConverterRDFLiteralArgument.class.cast(arg).getLiteralValue());
		} else if (arg instanceof ConverterRDFURIArgument) {
			return Collections.<Object>singletonList(ConverterRDFURIArgument.class.cast(arg).getURI());
		} else if (arg instanceof ConverterPlaceholderArgument) {
			List<Object> rv = new ArrayList<Object>();

			if (placeholder2valueMap == null) {
				throw new ConverterException("Cannot evaluate non-const converter argument: $"
						+ ConverterPlaceholderArgument.class.cast(arg).getPlaceholderId());
			}
			Collection<Object> placeholderValues = placeholder2valueMap
					.get(ConverterPlaceholderArgument.class.cast(arg).getPlaceholderId());

			for (Object vt : placeholderValues) {
				rv.add(vt);
			}
			return rv;
		} else if (arg instanceof ConverterMapArgument) {
			ConverterMapArgument mapArg = (ConverterMapArgument) arg;
			Map<String, ConverterArgumentExpression> mapArgExpr = mapArg.getMap();

			List<Map<String, Object>> tempRv = new ArrayList<Map<String, Object>>();
			tempRv.add(new HashMap<String, Object>());

			for (Entry<String, ConverterArgumentExpression> entry : mapArgExpr.entrySet()) {
				List<Object> explodedValues = explodeSingleConverterArgument(entry.getValue(),
						placeholder2valueMap);

				List<Map<String, Object>> newRv = new ArrayList<Map<String, Object>>();

				for (Object v : explodedValues) {

					for (Map<String, ?> aMap : tempRv) {
						HashMap<String, Object> newMap = Maps.newHashMap(aMap);
						newMap.put(entry.getKey(), v);
						newRv.add(newMap);
					}

				}

				tempRv = newRv;
			}

			List<Map<String, Object>> rv = new ArrayList<Map<String, Object>>();

			for (Map<String, Object> aMap : tempRv) {
				rv.add(homogenizeMap(aMap));
			}

			return (List<Object>) (Object) rv;
		} else {
			throw new ConverterException("Unsupported converter argument type: " + arg.getClass().getName());
		}
	}

	private static Map<String, Object> homogenizeMap(Map<String, Object> aMap) throws ConverterException {
		Class<?> topClass = null;
		for (Object value : aMap.values()) {
			if (topClass == null) {
				topClass = value.getClass();
			} else {
				Class<?> valueClass = value.getClass();

				if (!topClass.isAssignableFrom(valueClass)) {

					if (topClass == String.class) {
						topClass = valueClass;
					} else {
						if (Resource.class.isAssignableFrom(topClass)) {
							if (!Resource.class.isAssignableFrom(valueClass)) {
								topClass = Value.class;
							} else {
								topClass = Resource.class;
							}
						} else if (Literal.class.isAssignableFrom(topClass)) {
							topClass = Value.class;
						}
					}
				}
			}
		}

		Map<String, Object> newMap = new HashMap<String, Object>();

		for (Entry<String, Object> entry : aMap.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (topClass.isAssignableFrom(valueClass)) {
				newMap.put(key, value);
			} else if (valueClass == String.class && topClass.isAssignableFrom(Literal.class)) {
				newMap.put(key, SimpleValueFactory.getInstance().createLiteral(value.toString()));
			} else {
				throw new ConverterException("Cannot converter argument");
			}
		}

		return newMap;
	}

	private static Map<String, Object> promoteMap(Map<String, Object> aMap, Class<?> target)
			throws ConverterException {
		Map<String, Object> newMap = new HashMap<String, Object>();

		for (Entry<String, Object> entry : aMap.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (target.isAssignableFrom(valueClass)) {
				newMap.put(key, value);
			} else if (valueClass == String.class && target.isAssignableFrom(Literal.class)) {
				newMap.put(key, SimpleValueFactory.getInstance().createLiteral(value.toString()));
			} else {
				throw new ConverterException("Cannot converter argument");
			}
		}

		return newMap;

	}

	public static boolean validateFeathPathInProjRuleModel(ProjectionRulesModel prModel, JCas jcas)
			throws UnassignableFeaturePathException {
		TypeSystem typeSystem = jcas.getTypeSystem();

		// iterate on every projection rule and get all the feature paths
		for (String prId : prModel.getProjRule().keySet()) {
			ProjectionRule pr = prModel.getProjRuleFromId(prId);
			// first of all, get the type to which the rule is associated to
			String uimaTypeString = pr.getUIMAType();
			Type uimaFirstLevelType = typeSystem.getType(uimaTypeString);

			for (String plcId : pr.getPlaceholderMap().keySet()) {
				Type currentType = uimaFirstLevelType;
				PlaceholderStruct plcStruct = pr.getPlaceholderMap().get(plcId);
				String uimaFeatPath = plcStruct.getFeaturePath();

				if (uimaFeatPath == null || uimaFeatPath.isEmpty()) {
					// no faeture path is specified, this is possible and not an error
					continue;
				}

				// split the single component of the path (feature) in an array
				String[] featurePathElementNamesArray = uimaFeatPath.split("/");

				for (String featName : featurePathElementNamesArray) {

					if (uimaFeatPath.equals(COMPLETE_ANNOTATION) || uimaFeatPath.equals(COVERED_TEXT)) {
						// this two value are special feature path, so there is no need to check them, because
						// they do not really exist in a UIMA annotation
						continue;
					}

					// TODO check if this is correct or not
					if (featName.equals(COMPLETE_ANNOTATION) || featName.equals(COVERED_TEXT)) {
						// this two value are special feature path, so there is no need to check them, because
						// they do not really exist in a UIMA annotation
						continue;
					}

					Feature feature = currentType.getFeatureByBaseName(featName);
					if (feature == null) {
						throw new UnassignableFeaturePathException("The feature path " + uimaFeatPath
								+ " is not" + " valid for uima type " + uimaTypeString, plcId, pr.getId(),
								uimaFeatPath, uimaTypeString);
					}
					currentType = feature.getRange();
					if (currentType == null) {
						// this should never happen, but better be sure than sorry
						throw new UnassignableFeaturePathException("The feature path " + uimaFeatPath
								+ " is not" + " valid for uima type " + uimaTypeString, plcId, pr.getId(),
								uimaFeatPath, uimaTypeString);
					}
					// check it the type is an array
					if (currentType.isArray()) {
						// TODO find a way to manage arrays
						return true;
					} else if (currentType.getName().startsWith("uima.cas")
							&& currentType.getName().endsWith("List")) { // or a list

						// TODO find a way to manage lists
						return true;

						/*
						 * FeatureDescription featureDescription =
						 * TypeSystemUtil.feature2FeatureDescription(feature); String typeStringOfList =
						 * featureDescription.getElementType();
						 * 
						 * if(typeStringOfList == null){ return true; } currentType =
						 * typeSystem.getType(typeStringOfList);
						 * 
						 * if(currentType == null){ //this should never happen, but better be sure than sorry
						 * throw new
						 * UnassignableFeaturePathException("The feature path "+uimaFeatPath+" is not" +
						 * " valid for uima type "+uimaTypeString, plcId, pr.getId(), uimaFeatPath,
						 * uimaTypeString); }
						 */
					}

				}
			}
		}
		return true;
	}

	public enum LexModelForSearch {

		RDFS_MODEL("RDFS"),
		SKOS_MODEL("SKOS"),
		SKOSXL_MODEL("SKOSXL"),
		ONTOLEX_MODEL("ONTOLEX"),
		ALL_MODEL("ALL"),
		CTX_MODEL("CTX");


		LexModelForSearch(String lexModelForSearch) {
			this.lexModelForSearch = lexModelForSearch;
		}

		public String getLexModelForSearch() {
			return lexModelForSearch;
		}

		private String lexModelForSearch;

	}
}
