package it.uniroma2.art.coda.pearl.model;

import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemPlaceholder;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElement;
import it.uniroma2.art.coda.structures.DependsOnInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.uima.jcas.tcas.Annotation;

/**
 * @author Andrea Turbati
 */

public class ProjectionRule extends BaseProjectionRule{

	private String uimaTypeName;
	
	private double confidenceValue; // TODO choose a better name
	private List<ConditionStruct> conditionStructList;
	private Map<String, PlaceholderStruct> placeholdersMap; // this map contains the placeholders coming from
	// the nodes and the alias part of the projection rule
	private Map<String, BindingStruct> bindingsMap;
	private boolean isGraphSection = false;
	private Collection<GraphElement> graphInsertList;
	private Collection<GraphElement> graphDeleteList;
	//private Collection<String> varList;
	private Collection<GraphElement> whereList;
	private Map<String, String> parametersMap;

	private List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> annotationList;


	// the key to the outer map is the dependecy type, the inner map has as a key the ruleId, and its value
	// is the DependencyInfo
	private Map<String, Map<String, DependsOnInfo>> dependencyMap; 
	
	private Map<String, List<String>> aliasIdToRealIdMap;
	private Map<String, List<String>> aliasIdToUsedPlchldMap;
	
	
	private boolean laziness;
	
	private boolean isForRegex;


	public ProjectionRule(String id, ProjectionRulesModel projectionRulesModel) {
		super(id, projectionRulesModel);
		initialize();
	}

	private void initialize() {
		this.uimaTypeName = null;
		this.confidenceValue = 0;
		this.laziness = false;
		this.isForRegex = false;
		this.parametersMap = new HashMap<>();
		this.dependencyMap = new HashMap<>();
		this.aliasIdToRealIdMap = new HashMap<>();
		this.aliasIdToUsedPlchldMap = new HashMap<>();
		this.conditionStructList = new ArrayList<>();
		this.placeholdersMap = new LinkedHashMap<>();
		this.bindingsMap = new HashMap<>();
		this.graphInsertList = new ArrayList<>();
		this.graphDeleteList = new ArrayList<>();
		this.whereList = new ArrayList<>();
		this.annotationList = new ArrayList<>();
	}

	public boolean isIdNull() {
		return id == null;
	}

	public String getUIMAType() {
		return uimaTypeName;
	}

	public double getConfidenceValue() {
		return confidenceValue;
	}

	public boolean addPlaceholder(PlaceholderStruct placeholderStruct) {
		String placeholderName = placeholderStruct.getName();
		if (placeholdersMap.containsKey(placeholderName))
			return false;
		placeholdersMap.put(placeholderName, placeholderStruct);
		return true;
	}

	public Map<String, PlaceholderStruct> getPlaceholderMap() {
		return placeholdersMap;
	}

	
	public boolean isGraphSection(){
		return isGraphSection;
	}
	
	public void addElementToInsertGraph(GraphElement graphElement, boolean isGraphSection) {
		if(!this.isGraphSection) {
			this.isGraphSection = isGraphSection;
		}
		graphInsertList.add(graphElement);
	}
	
	public void addElementsToInsertGraph(Collection<GraphElement> graphElements, boolean isGraphSection) {
		if(!this.isGraphSection) {
			this.isGraphSection = isGraphSection;
		}
		graphInsertList.addAll(graphElements);
	}

	public Collection<GraphElement> getInsertGraphList() {
		return graphInsertList;
	}
	
	public void addElementToDeleteGraph(GraphElement graphElement) {
		graphDeleteList.add(graphElement);
	}
	
	public void addElementsToDeleteGraph(Collection<GraphElement> graphElements) {
		graphDeleteList.addAll(graphElements);
	}

	public Collection<GraphElement> getDeleteGraphList() {
		return graphDeleteList;
	}

	public void addElementToWhere(GraphElement graphElement) {
		whereList.add(graphElement);
	}
	
	public void addElementsToWhere(Collection<GraphElement> graphElements) {
		whereList.addAll(graphElements);
	}

	public Collection<GraphElement> getWhereList() {
		return whereList;
	}

	public void addDependsOn(DependsOnInfo dependsOnInfo){
		String dependecyType = dependsOnInfo.getDependsOnType();
		if(!dependencyMap.containsKey(dependecyType)){
			dependencyMap.put(dependecyType, new HashMap<String, DependsOnInfo>());
		}
		Map<String, DependsOnInfo> innerDependencyMap = dependencyMap.get(dependecyType);
		String dependsOnAliasRuleId = dependsOnInfo.getAliasRuleId();
		if(!aliasIdToRealIdMap.containsKey(dependsOnAliasRuleId)){
			aliasIdToRealIdMap.put(dependsOnAliasRuleId, dependsOnInfo.getDependsOnRuleIdList());
		}
		innerDependencyMap.put(dependsOnAliasRuleId, dependsOnInfo);
	}
	
	public Map<String, Map<String, DependsOnInfo>> getDependOnMap(){
		return dependencyMap;
	}
	
	public List<String> getRealIdFromAliasId(String aliasId){
		return aliasIdToRealIdMap.get(aliasId);
	}
	
	public void addPlchldUsedWithAltId(String altId, String plchld){
		
		if(altId.startsWith("$")){
			altId = altId.substring(1);
		}
		
		if(!aliasIdToUsedPlchldMap.containsKey(altId)){
			aliasIdToUsedPlchldMap.put(altId, new ArrayList<String>());
		}
		List<String> plchldList = aliasIdToUsedPlchldMap.get(altId);
		if(!plchldList.contains(plchld)){
			plchldList.add(plchld);
		}
	}
	
	public List<String> getPlchldUsedFromAltId(String aliasId){
		return aliasIdToUsedPlchldMap.get(aliasId);
	}
	
	public boolean addParameter(String attName, String attValue) {
		if (parametersMap.containsKey(attValue))
			return false;
		parametersMap.put(attName, attValue);
		return true;
	}

	public Map<String, String> getParametersMap() {
		return parametersMap;
	}

	public void setLaziness(boolean laziness) {
		this.laziness  = laziness;
	}
	
	public boolean isLazy() {
		return laziness;
	}
	
	public void setIfForRegex(boolean isForRegex){
		this.isForRegex = isForRegex;
	}
	
	public boolean isForRegex(){
		return isForRegex;
	}

	public void setUimaTypeName(String uimaTypeName) {
		this.uimaTypeName = uimaTypeName;
	}
	
	public void addCondition(ConditionStruct conditionStruct){
		conditionStructList.add(conditionStruct);
	}
	
	public List<ConditionStruct> getConditionStructList(){
		return conditionStructList;
	}
	
	public boolean evaluateConditions(Annotation ann){
		for(ConditionStruct conditionStruct : conditionStructList){
			if(conditionStruct.evaluteCondition(ann) == false){
				return false;
			}
		}
		return true;
	}

	public void addBinding(BindingStruct bindingStruct) {
		bindingsMap.put(bindingStruct.getName(), bindingStruct);
	}
	
	public Map<String, BindingStruct> getBindingsMap(){
		return bindingsMap;
	}
	
	public void resolveIsMandatoryCheck(){
		//iterate over each triple in the INSERT graph section to see all the placeholder which are mandatory
		// so they appear in at least one triple outside an OPTIONAL graph
		for(GraphElement graphElement : graphInsertList){
			resolveIsMandatoryCheck_inner(graphElement);
		}
		//do the same check for the DELETE Graph section
		for(GraphElement graphElement : graphDeleteList){
			resolveIsMandatoryCheck_inner(graphElement);
		}
	}
	
	private void resolveIsMandatoryCheck_inner(GraphElement graphElement){
		if (graphElement instanceof GraphStruct) {
			GraphStruct graphStruct = (GraphStruct) graphElement;
			GraphSingleElement subElem = graphStruct.getSubject();
			GraphSingleElement predElem = graphStruct.getPredicate();
			GraphSingleElement objElem = graphStruct.getObject();
			// check if these elements are placeholder or not
			if (subElem instanceof GraphSingleElemPlaceholder) {
				GraphSingleElemPlaceholder subjPlchldElem = (GraphSingleElemPlaceholder) subElem;
				if (!subjPlchldElem.isFromDependsOnRule() && !subjPlchldElem.isFromBindingsRule()) {
					String subjPlchhldName = subjPlchldElem.getName().substring(1);
					PlaceholderStruct subPlchldStruct = placeholdersMap.get(subjPlchhldName);
					subPlchldStruct.setIsMandatoryInGraphSection(true);
				}
			}
			if (predElem instanceof GraphSingleElemPlaceholder) {
				GraphSingleElemPlaceholder predPlchldElem = (GraphSingleElemPlaceholder) predElem;
				if (!predPlchldElem.isFromDependsOnRule() && !predPlchldElem.isFromBindingsRule()) {
					String predPlchhldName = predPlchldElem.getName().substring(1);
					PlaceholderStruct predPlchldStruct = placeholdersMap.get(predPlchhldName);
					predPlchldStruct.setIsMandatoryInGraphSection(true);
				}
			}
			if (objElem instanceof GraphSingleElemPlaceholder) {
				GraphSingleElemPlaceholder objPlchldElem = (GraphSingleElemPlaceholder) objElem;
				if (!objPlchldElem.isFromDependsOnRule() && !objPlchldElem.isFromBindingsRule()) {
					String objPlchhldName = objPlchldElem.getName().substring(1);
					PlaceholderStruct objPlchldStruct = placeholdersMap.get(objPlchhldName);
					objPlchldStruct.setIsMandatoryInGraphSection(true);
				}
			}
		}
	}

	public void setAnnotations(List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> annotationList){
		this.annotationList = annotationList;
	}

	public boolean hasAnnotations(){
		if (annotationList == null)
			return false;
		if (annotationList.isEmpty())
			return false;
		return true;
	}

	public List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> getAnnotationList(){
		return annotationList;
	}
}
