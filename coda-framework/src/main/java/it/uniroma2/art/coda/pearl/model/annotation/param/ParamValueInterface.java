package it.uniroma2.art.coda.pearl.model.annotation.param;

public interface ParamValueInterface {

    String toString();

    String asString();

    String getType();
}
