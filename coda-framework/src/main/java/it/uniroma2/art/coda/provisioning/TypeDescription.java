package it.uniroma2.art.coda.provisioning;

/**
 * A description of a type. This interface is intended to be extended in order to support different genera of
 * types.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface TypeDescription {
	/**
	 * Returns the name of this type. The format of the returned string depends on the actual nature of the
	 * type (e.g. Java type).
	 * 
	 * @return
	 */
	String getName();
}
