package it.uniroma2.art.coda.pearl.model.annotation.param;

import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class ParamValueLiteral implements  ParamValueInterface{

    private Literal literal;

    public ParamValueLiteral(Literal literal) {
        this.literal = literal;
    }

    public Literal getLiteral() {
        return literal;
    }

    @Override
    public String toString() {
        return NTriplesUtil.toNTriplesString(literal);
    }

    @Override
    public String asString() {
        return NTriplesUtil.toNTriplesString(literal);
    }

    @Override
    public String getType() {
        return "literal";
    }
}
