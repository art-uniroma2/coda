package it.uniroma2.art.coda.structures;

import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.pearl.model.BaseProjectionRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.uima.jcas.tcas.Annotation;

/**
 * @author Andrea Turbati
 */

public class SuggOntologyCoda {

	private Annotation ann;
	private Map<String, List<CODATriple>> suggestedBaseRuleIdToInsertARTTtripleMap;
	private Map<String, List<CODATriple>> suggestedBaseRuleIdToDeleteARTTtripleMap;
	private Map<String, BaseProjectionRule> baseProjectionRuleMap;

	private Map<String, NodeAssignment> nodeNameToNodeAssignmentMap;

	// the structure created by the projection disambiguation containing the selected projection rules
	//SelectedProjectionRules selProjRules;
	// all the structure returned by CODACore (merging the other structure and adding the missing information)
	//CodaFinalSuggestions codaFinalSuggs;

	
	public SuggOntologyCoda(Annotation ann, boolean addNodeAssignmentMap){
		this.ann = ann;
		suggestedBaseRuleIdToInsertARTTtripleMap = new HashMap<>();
		suggestedBaseRuleIdToDeleteARTTtripleMap = new HashMap<>();
		baseProjectionRuleMap = new HashMap<>();
		if (addNodeAssignmentMap) {
			nodeNameToNodeAssignmentMap = new HashMap<>();
		} else {
			nodeNameToNodeAssignmentMap = null;
		}
	}
	
	
	public void addProjectionRule(BaseProjectionRule bpr){
		baseProjectionRuleMap.put(bpr.getId(), bpr);
	}
	
	public Set<String> getBaseProjectionRulesIdList(){
		return baseProjectionRuleMap.keySet();
	}
	
	public BaseProjectionRule getBaseProjectionRulesFromId(String baseProjRuleId){
		return baseProjectionRuleMap.get(baseProjRuleId);
	}
	
	//all the methods to deal with insert Triples
	public void addSuggestedInsertTriple(String baseRuleId, CODATriple codaTriple){
		if(!suggestedBaseRuleIdToInsertARTTtripleMap.containsKey(baseRuleId)){
			suggestedBaseRuleIdToInsertARTTtripleMap.put(baseRuleId, new ArrayList<CODATriple>());
		}
		suggestedBaseRuleIdToInsertARTTtripleMap.get(baseRuleId).add(codaTriple);
	}
	
	public void addSuggestedInsertTripleList(String baseRuleId, List<CODATriple> codaTripleList){
		if(!suggestedBaseRuleIdToInsertARTTtripleMap.containsKey(baseRuleId)){
			suggestedBaseRuleIdToInsertARTTtripleMap.put(baseRuleId, new ArrayList<CODATriple>());
		}
		suggestedBaseRuleIdToInsertARTTtripleMap.get(baseRuleId).addAll(codaTripleList);
	}
	

	
	public List<CODATriple> getInsertARTTripleListFromBaseProjRuleId(String baseProjRuleId){
		List<CODATriple> rv = suggestedBaseRuleIdToInsertARTTtripleMap.get(baseProjRuleId);
		if (rv != null) {
			return rv;
		} else {
			return new ArrayList<>();
		}
	}
	
	public List<CODATriple> getAllInsertARTTriple(){
		List<CODATriple> codaTripleList = new ArrayList<CODATriple>();
		for(String baseRuleId : suggestedBaseRuleIdToInsertARTTtripleMap.keySet()){
			codaTripleList.addAll(suggestedBaseRuleIdToInsertARTTtripleMap.get(baseRuleId));
		}
		return codaTripleList;
	}
	
	//all the methods to deal with delete Triples
	public void addSuggestedDeleteTriple(String baseRuleId, CODATriple codaTriple) {
		if (!suggestedBaseRuleIdToDeleteARTTtripleMap.containsKey(baseRuleId)) {
			suggestedBaseRuleIdToDeleteARTTtripleMap.put(baseRuleId, new ArrayList<CODATriple>());
		}
		suggestedBaseRuleIdToDeleteARTTtripleMap.get(baseRuleId).add(codaTriple);
	}

	public void addSuggestedDeleteTripleList(String baseRuleId, List<CODATriple> codaTripleList) {
		if (!suggestedBaseRuleIdToDeleteARTTtripleMap.containsKey(baseRuleId)) {
			suggestedBaseRuleIdToDeleteARTTtripleMap.put(baseRuleId, new ArrayList<CODATriple>());
		}
		suggestedBaseRuleIdToDeleteARTTtripleMap.get(baseRuleId).addAll(codaTripleList);
	}

	public List<CODATriple> getDeleteARTTripleListFromBaseProjRuleId(String baseProjRuleId) {
		List<CODATriple> rv = suggestedBaseRuleIdToDeleteARTTtripleMap.get(baseProjRuleId);
		if (rv != null) {
			return rv;
		} else {
			return new ArrayList<>();
		}
	}

	public List<CODATriple> getAllDeleteARTTriple() {
		List<CODATriple> codaTripleList = new ArrayList<CODATriple>();
		for (String baseRuleId : suggestedBaseRuleIdToDeleteARTTtripleMap.keySet()) {
			codaTripleList.addAll(suggestedBaseRuleIdToDeleteARTTtripleMap.get(baseRuleId));
		}
		return codaTripleList;
	}

	/**
	 * @param selProjRules
	 *            the selected projection rules
	 * @param bindLabels
	 *            the suggestions from the Identity Resolution Component
	 * @param resSmartSuggsStruct
	 *            the suggestions form the Smart Suggestion Component
	 * @param codaFinalSuggs
	 *            the final suggestions from CODA
	 * @param ann
	 *            the annotation that triggered all these suggestions
	 */
	//public SuggOntologyCoda(SelectedProjectionRules selProjRules,
	//		CodaFinalSuggestions codaFinalSuggs, Annotation ann) {
	//	this.ann = ann;
	//	this.selProjRules = selProjRules;
	//	this.codaFinalSuggs = codaFinalSuggs;
	//}

	/**
	 * Get the selected projection rules
	 * 
	 * @return the selected projection rules
	 */
	//public SelectedProjectionRules getSelProjRules() {
	//	return selProjRules;
	//}

	/**
	 * Get the final CODA suggestions
	 * 
	 * @return the final CODA suggestions
	 */
	//public CodaFinalSuggestions getCodaFinalSuggs() {
	//	return codaFinalSuggs;
	//}

	/**
	 * Get the annotation to which all this suggestions belongs to
	 * 
	 * @return the annotation to which all this suggestions belongs to
	 */
	public Annotation getAnnotation() {
		return ann;
	}


	public Map<String, NodeAssignment> getNodeNameToNodeAssignmentMap() throws ValueNotPresentDueToConfigurationException {
		if (nodeNameToNodeAssignmentMap == null) {
			throw new ValueNotPresentDueToConfigurationException("nodeNameToNodeAssignmentMap is not present since" +
					"the parameter addNodeAssignmentMap was false or not passed");
		}
		return nodeNameToNodeAssignmentMap;
	}

	public void addNodeAssignmen(String nodeName, NodeAssignment nodeAssignment) throws ValueNotPresentDueToConfigurationException {
		if (nodeNameToNodeAssignmentMap == null) {
			throw new ValueNotPresentDueToConfigurationException("nodeNameToNodeAssignmentMap is not present since" +
					"the parameter addNodeAssignmentMap was false or not passed");
		}
		nodeNameToNodeAssignmentMap.put(nodeName, nodeAssignment);

	}
}
