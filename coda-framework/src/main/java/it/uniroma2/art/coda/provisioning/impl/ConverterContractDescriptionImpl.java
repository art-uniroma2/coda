package it.uniroma2.art.coda.provisioning.impl;

import java.util.Collection;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.coda.provisioning.ConverterContractDescription;
import it.uniroma2.art.coda.provisioning.SignatureDescription;
import it.uniroma2.art.coda.vocabulary.CODAONTO;

public class ConverterContractDescriptionImpl implements ConverterContractDescription {

	private final String contractURI;
	private final String contractName;
	private final String contractDescription;
	private final RDFCapabilityType rdfCapability;
	private final Set<IRI> datatypes;
	private final Collection<SignatureDescription> signatureDescriptions;

	public ConverterContractDescriptionImpl(String contractURI, String contractName,
			String contractDescription, RDFCapabilityType rdfCapability, Set<IRI> datatypes,
			Collection<SignatureDescription> signatureDescriptions) {
		this.contractURI = contractURI;
		this.contractName = contractName;
		this.contractDescription = contractDescription;
		this.rdfCapability = rdfCapability;
		this.datatypes = datatypes;
		this.signatureDescriptions = signatureDescriptions;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("contractURI", contractURI).add("contractName", contractName)
				.add("rdfCapability", rdfCapability).add("datatypes", datatypes)
				.add("signatureDescriptions", signatureDescriptions).toString();
	}

	@Override
	public String getContractURI() {
		return contractURI;
	}

	@Override
	public String getContractName() {
		return contractName;
	}

	@Override
	public String getContractDescription() {
		return contractDescription;
	}

	@Override
	public RDFCapabilityType getRDFCapability() {
		return rdfCapability;
	}

	@Override
	public Set<IRI> getDatatypes() {
		return datatypes;
	}

	@Override
	public Collection<SignatureDescription> getSignatureDescriptions() {
		return signatureDescriptions;
	}
	
	@Override
	public void toRDF(Model model) {
		SimpleValueFactory vf = SimpleValueFactory.getInstance();
		IRI contractIRI = vf.createIRI(getContractURI());
		model.add(contractIRI, RDF.TYPE, CODAONTO.CONVERTER_CONTRACT);
		model.add(contractIRI, RDFS.LABEL, vf.createLiteral(getContractName()));
		model.add(contractIRI, DCTERMS.DESCRIPTION, vf.createLiteral(getContractDescription()));
		for (IRI datatype : getDatatypes()) {
			model.add(contractIRI, CODAONTO.DATATYPE_CAPABILITY, datatype);
		}
		model.add(contractIRI, CODAONTO.RDF_CAPABILITY, vf.createLiteral(getRDFCapability().toString()));
	}
}
