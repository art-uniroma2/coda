package it.uniroma2.art.coda.pearl.model.annotation;


import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetaAnnotationGeneric extends MetaAnnotation {

	Map<String, List<ParamValueInterface>> nameToValueListMap = new HashMap<>();

	public MetaAnnotationGeneric(String name) {
		super(name);
	}

	public boolean addNameValue(String name, ParamValueInterface value){
		//check if there are already value for the input name
		if(!nameToValueListMap.containsKey(name)){
			nameToValueListMap.put(name, new ArrayList<>());
		}
		List<ParamValueInterface> valueList = nameToValueListMap.get(name);
		//check if the there is already the input value associated with the desired name
		if(!valueList.contains(value)){
			valueList.add(value);
			return true;
		} else {
			return false;
		}
	}

	public boolean addNameValueList(String name, List<ParamValueInterface> valueList){
		boolean allAdded = true;
		for(ParamValueInterface value : valueList){
			boolean singleAdded = addNameValue(name, value);
			//if, at to this point, all values have been added, them the value to allAded is set to singleAdded, otherwise (it is already false), the old value is used
			allAdded = allAdded ? singleAdded : allAdded;
		}
		return allAdded;
	}

	public  Map<String, List<ParamValueInterface>> getNameToValueListMap(){
		return nameToValueListMap;
	}

	public List<ParamValueInterface> getValueList(String name){
		return nameToValueListMap.get(name);
	}
}
