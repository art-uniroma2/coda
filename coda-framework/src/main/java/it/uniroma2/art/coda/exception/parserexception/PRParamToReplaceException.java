package it.uniroma2.art.coda.exception.parserexception;


public class PRParamToReplaceException extends PRParserException {

	private String msg ;

	private int line;
	
	private static final long serialVersionUID = 1L;

	/*public PRParamToReplaceException(RecognitionException e) {
		super();
		recognitionException = e;
	}*/
	
	public PRParamToReplaceException(int  line) {
		super();
		this.line = line;
		this.msg = "Found a parameter that has to be replaced at line: "+line;
	}
	

	@Override
	public String getErrorAsString() {
		return msg;
	}
	
	public int getLine() {
		return line;
	}
}
