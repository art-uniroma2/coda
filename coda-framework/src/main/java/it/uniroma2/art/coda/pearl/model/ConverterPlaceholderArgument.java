package it.uniroma2.art.coda.pearl.model;

import java.util.Objects;

import org.eclipse.rdf4j.model.Value;

/**
 * A placeholder used as an additional argument in the context of a {@link ConverterMention}. It is always
 * non-constant ({@link #isConstant()} == <code>false</code>), because the value of this argument expression
 * depends on the value assigned to the placeholder.
 */
public class ConverterPlaceholderArgument extends ConverterArgumentExpression {

	private String placeholderId;
	private Class<? extends Value> clazz;

	/**
	 * Constructs an argument based on the placeholder holding the given type of RDF node (i.e.
	 * {@link ARTURIResource} or {@link ARTLiteral}
	 * 
	 * @param placeholderId
	 * @param clazz
	 */
	public ConverterPlaceholderArgument(String placeholderId, Class<? extends Value> clazz) {
		this.placeholderId = placeholderId;
		this.clazz = clazz;
	}

	@Override
	public Class<?> getArgumentType() {
		return clazz;
	}

	@Override
	public boolean isConstant() {
		return false;
	}

	public String getPlaceholderId() {
		return placeholderId;
	}

	@Override
	public Object getGroundObject() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return "$" + placeholderId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(placeholderId, clazz);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != this.getClass())
			return false;
		ConverterPlaceholderArgument other = (ConverterPlaceholderArgument) obj;
		return Objects.equals(placeholderId, other.placeholderId) && Objects.equals(clazz, other.clazz);
	}

}
