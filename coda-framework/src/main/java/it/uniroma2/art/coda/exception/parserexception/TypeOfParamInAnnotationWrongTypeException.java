package it.uniroma2.art.coda.exception.parserexception;

public class TypeOfParamInAnnotationWrongTypeException extends PRParserException {

	private String annName;
	private String ruleId;
	private String paramName;
	private  String typeExpected;


	private static final long serialVersionUID = 1L;

	public TypeOfParamInAnnotationWrongTypeException(String annName, String ruleId, String paramName, String typeExpected) {
		super();
		this.annName = annName;
		this.ruleId = ruleId;
		this.paramName = paramName;
		this.typeExpected = typeExpected;
	}

	public TypeOfParamInAnnotationWrongTypeException(Exception e, String prefixName, String ruleId, String paramName, String typeExpected) {
		super(e);
		this.annName = prefixName;
		this.ruleId = ruleId;
		this.paramName = paramName;
		this.typeExpected = typeExpected;
	}
	
	public String getAnnName(){
		return annName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	public String getParamName() {
		return paramName;
	}

	public String getTypeExpected() {
		return typeExpected;
	}

	@Override
	public String getErrorAsString() {
		return "param "+paramName+" in annotation "+ annName +" in rule "+ruleId+" has not the type "+typeExpected;
	}
}
