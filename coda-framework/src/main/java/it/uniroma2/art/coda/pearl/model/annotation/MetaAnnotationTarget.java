package it.uniroma2.art.coda.pearl.model.annotation;


import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;

import java.util.ArrayList;
import java.util.List;


public class MetaAnnotationTarget extends MetaAnnotation {

	List<String> valueList = new ArrayList<>();

	public MetaAnnotationTarget() {
		super(PearlParserDescription.TARGET_ANN_DECL);
	}

	public List<String> getTargetList(){
		return valueList;
	}

	public boolean addTargetValue(String value){
		if(!valueList.contains(value)){
			valueList.add(value);
			return  true;
		} else {
			return false;
		}
	}

	public boolean addTargetValueList(List<ParamValueInterface> valueList){
		boolean allAdded = true;
		for(ParamValueInterface value : valueList){
			boolean singleAdded = addTargetValue(value.toString());
			//if, at to this point, all values have been added, them the value to allAded is set to singleAdded, otherwise (it is already false), the old value is used
			allAdded = allAdded ? singleAdded : allAdded;
		}
		return allAdded;
	}
}
