package it.uniroma2.art.coda.exception;

/**
 * This class represent a generic Exception regarding the reading of the Projection
 * Rule File written in XML
 * 
 * @author Andrea Turbati
 */
public class ProjectionRuleModelNotSet extends Exception {
    
    private static final long serialVersionUID = -8543980667727905823L;

	/**
	 * @param msg
	 */
	public ProjectionRuleModelNotSet(String msg) {
        super(msg);
    }
    
    /**
     * @param e
     */
    public ProjectionRuleModelNotSet(Exception e) {
        super(e);
    }

}
