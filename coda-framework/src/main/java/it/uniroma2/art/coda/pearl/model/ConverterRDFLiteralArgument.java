package it.uniroma2.art.coda.pearl.model;

import java.util.Objects;

import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

import it.uniroma2.art.coda.util.OntoUtils;

/**
 * An RDF literal used as an additional argument in the context of a {@link ConverterMention}. It is always
 * considered constant (i.e. {@link #isConstant()} == <code>true</code>).
 */
public class ConverterRDFLiteralArgument extends ConverterArgumentExpression {

	private Literal literalValue;

	/**
	 * Constructs an argument based on the NT serialization of a literal value.
	 * 
	 * @param literalNT
	 */
	public ConverterRDFLiteralArgument(String literalNT) {
		this.literalValue = OntoUtils.createLiteral(literalNT, null);
	}

	/**
	 * Returns the underlying literal term
	 * 
	 * @return
	 */
	public Literal getLiteralValue() {
		return literalValue;
	}

	public Class<Literal> getArgumentType() {
		return Literal.class;
	}

	@Override
	public boolean isConstant() {
		return true;
	}
	
	@Override
	public Object getGroundObject() {
		return getLiteralValue();
	}

	/**
	 * Factory method that constructs a (simple) literal argument based on a string.
	 * 
	 * @param literalForm
	 * @return
	 */
	public static ConverterRDFLiteralArgument fromString(String literalForm) {
		return new ConverterRDFLiteralArgument(
				NTriplesUtil.toNTriplesString(SimpleValueFactory.getInstance().createLiteral(literalForm)));
	}

	@Override
	public String toString() {
		return NTriplesUtil.toNTriplesString(literalValue);
	}

	@Override
	public int hashCode() {
		return literalValue.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != this.getClass())
			return false;
		ConverterRDFLiteralArgument other = (ConverterRDFLiteralArgument) obj;
		return Objects.equals(literalValue, other.literalValue);
	}

}
