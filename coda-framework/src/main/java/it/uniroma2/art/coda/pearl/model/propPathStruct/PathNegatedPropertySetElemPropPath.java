package it.uniroma2.art.coda.pearl.model.propPathStruct;

import java.util.ArrayList;
import java.util.List;

public class PathNegatedPropertySetElemPropPath implements GenericElemPropPath{

    private List<PathOneInPropertySetElemPropPath> pathOneInPropertySetElemPropPathList = new ArrayList<>();

    public PathNegatedPropertySetElemPropPath() {
    }

    public void addPathOneInPropertySetElemPropPath(PathOneInPropertySetElemPropPath pathOneInPropertySetElemPropPath) {
        this.pathOneInPropertySetElemPropPathList.add(pathOneInPropertySetElemPropPath);
    }

    public List<PathOneInPropertySetElemPropPath> getPathOneInPropertySetElemPropPathList() {
        return pathOneInPropertySetElemPropPathList;
    }

    @Override
    public String getValueAsString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(" (");
        boolean first = true;
        for (PathOneInPropertySetElemPropPath pathOneInPropertySetElemPropPath : pathOneInPropertySetElemPropPathList) {
            if (!first) {
                stringBuilder.append(" | ");
            }
            first = false;
            stringBuilder.append(pathOneInPropertySetElemPropPath.getValueAsString());
        }
        stringBuilder.append(") ");

        return stringBuilder.toString();
    }


}
