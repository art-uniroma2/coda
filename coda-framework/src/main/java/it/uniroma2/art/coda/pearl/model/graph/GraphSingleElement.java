package it.uniroma2.art.coda.pearl.model.graph;

import it.uniroma2.art.coda.pearl.model.ProjectionRule;

/**
 * This abstract class is the generic single element of a triple
 * 
 * @author Andrea Turbati
 * 
 */
public abstract class GraphSingleElement {
	private ProjectionRule prRule;

	/**
	 * 
	 * @param prRule
	 *            the projection Rule this element belongs to
	 */
	public GraphSingleElement(ProjectionRule prRule) {
		this.prRule = prRule;
	}


	/**
	 * Get the projection rule
	 * 
	 * @return the projection rule this element belongs to
	 */
	public ProjectionRule getPRRule() {
		return prRule;
	}
	
	public abstract String getValueAsString();
}
