package it.uniroma2.art.coda.exception;

/**
 * This class represent a generic Exception regarding the reading of the Projection
 * Rule File written in XML
 * 
 * @author Andrea Turbati
 */
public class RDFModelNotSetException extends Exception {
    
    private static final long serialVersionUID = -8543980667727905823L;

	/**
	 * @param msg
	 */
	public RDFModelNotSetException(String msg) {
        super(msg);
    }
    
    /**
     * @param e
     */
    public RDFModelNotSetException(Exception e) {
        super(e);
    }

}
