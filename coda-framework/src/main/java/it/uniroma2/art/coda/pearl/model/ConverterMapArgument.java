package it.uniroma2.art.coda.pearl.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

/**
 * A map used as an additional argument in the context of a {@link ConverterMention}. A map argument is
 * considered constant (i.e. {@link #isConstant()} == <code>true</code>), only if its values are themselves
 * constants.
 */
public class ConverterMapArgument extends ConverterArgumentExpression {

	private Map<String, ConverterArgumentExpression> map;

	/**
	 * Constructs an argument based on the provided map of argument expressions.
	 * 
	 * @param map
	 */
	public ConverterMapArgument(Map<String, ConverterArgumentExpression> map) {
		this.map = map;
	}

	@Override
	public Class<?> getArgumentType() {
		return Map.class;
	}

	@Override
	public boolean isConstant() {
		for (ConverterArgumentExpression v : map.values()) {
			if (!v.isConstant())
				return false;
		}

		return true;
	}

	/**
	 * Returns the underlying map of converter expressions
	 * 
	 * @return
	 */
	public Map<String, ConverterArgumentExpression> getMap() {
		return map;
	}

	@Override
	public Object getGroundObject() {
		if (!isConstant())
			throw new UnsupportedOperationException();

		return map.entrySet().stream()
				.collect(Collectors.toMap(Entry::getKey, entry -> entry.getValue().getGroundObject()));
	}

	/**
	 * Factory method that constructs a map argument based on a map, the value of which are (constant) RDF
	 * nodes.
	 * 
	 * @param args
	 * @return
	 */
	public static ConverterMapArgument fromNodesMap(Map<String, Value> args) {
		Map<String, ConverterArgumentExpression> temp = new HashMap<String, ConverterArgumentExpression>();

		for (Entry<String, Value> entry : args.entrySet()) {
			Value value = entry.getValue();
			ConverterArgumentExpression valueExpr = null;

			if (value instanceof IRI) {
				valueExpr = new ConverterRDFURIArgument(((IRI) value).stringValue());
			} else if (value instanceof Literal) {
				valueExpr = new ConverterRDFLiteralArgument(NTriplesUtil.toNTriplesString((Literal) value));
			}

			if (valueExpr != null) {
				temp.put(entry.getKey(), valueExpr);
			}
		}

		return new ConverterMapArgument(temp);
	}

	@Override
	public String toString() {
		String text = "{";
		boolean first = true;
		for (String key : map.keySet()) {
			if (!first) {
				text += ",";
			}
			first = false;
			ConverterArgumentExpression converterArgumentExpression = map.get(key);
			text += key + "=" + converterArgumentExpression.toString();
		}

		text += "}";

		return text;
	}

	@Override
	public int hashCode() {
		return map.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != this.getClass())
			return false;
		ConverterMapArgument other = (ConverterMapArgument) obj;
		return map.equals(other.map);
	}

}
