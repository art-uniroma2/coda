package it.uniroma2.art.coda.exception.parserexception;

/**
 * This class represent a generic Exception regarding the reading of the Projection
 * Rule File written in PEARL
 * 
 * @author Andrea Turbati
 */
public abstract class PRParserException extends Exception {
    
    private static final long serialVersionUID = -8543980667727905853L;

	public PRParserException() {
        super();
    }
    
    /**
     * @param e
     */
    public PRParserException(Exception e) {
        super(e);
    }
    
    public abstract String getErrorAsString();

}
