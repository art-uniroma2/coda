package it.uniroma2.art.coda.pearl.model.annotation.param;

public class ParamValueString implements  ParamValueInterface{

    private String string;

    public ParamValueString(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public String asString() {
        return "'"+string+"'";
    }

    @Override
    public String getType() {
        return "string";
    }
}
