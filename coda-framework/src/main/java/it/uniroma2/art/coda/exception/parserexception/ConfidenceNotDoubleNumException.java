package it.uniroma2.art.coda.exception.parserexception;

public class ConfidenceNotDoubleNumException extends PRParserException {

	private String ruleId;
	private String value;
	private String wrongType;

	private static final long serialVersionUID = 1L;

	public ConfidenceNotDoubleNumException(String ruleId, String value, String wrongType) {
		super();
		this.ruleId = ruleId;
		this.value = value;
		this.wrongType = wrongType;
	}

	public ConfidenceNotDoubleNumException(Exception e, String ruleId, String value, String wrongType) {
		super(e);
		this.ruleId = ruleId;
		this.value = value;
		this.wrongType = wrongType;
	}
	
	public String getValue(){
		return value;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	public String getWrongType() {
		return wrongType;
	}

	@Override
	public String getErrorAsString() {
		return "the confidence value  "+value+" in rule "+ruleId+" cannot be converted into a decimal number, having type "+wrongType;
	}
}
