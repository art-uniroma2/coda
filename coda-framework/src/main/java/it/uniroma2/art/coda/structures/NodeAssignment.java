package it.uniroma2.art.coda.structures;

import it.uniroma2.art.coda.exception.WrongAskedValueException;
import org.eclipse.rdf4j.model.Value;

import java.util.List;

public class NodeAssignment {

    private String nodeName;
    private Value singleValue;
    private List<Value> valueList;
    private boolean hasSingleValue;

    public NodeAssignment(String nodeName, Value singleValue) {
        this.nodeName = nodeName;
        this.singleValue = singleValue;
        this.valueList = null;
        this.hasSingleValue = true;
    }

    public NodeAssignment(String nodeName, List<Value> valueList) {
        this.nodeName = nodeName;
        this.singleValue = null;
        this.valueList = valueList;
        this.hasSingleValue = false;
    }

    public String getNodeName() {
        return nodeName;
    }

    public boolean isSingleValuePresent() {
        return hasSingleValue;
    }

    public Value getSingleValue() throws WrongAskedValueException {
        if (!hasSingleValue) {
            throw new WrongAskedValueException("The SingleValue is not present, so the ValueList should have been asked");
        }
        return singleValue;
    }

    public List<Value> getValueList() throws WrongAskedValueException {
        if (hasSingleValue) {
            throw new WrongAskedValueException("The ValueList is not present, so the SingleValue should have been asked");
        }
        return valueList;
    }
}
