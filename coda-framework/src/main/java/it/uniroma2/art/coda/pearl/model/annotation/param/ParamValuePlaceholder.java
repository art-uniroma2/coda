package it.uniroma2.art.coda.pearl.model.annotation.param;

public class ParamValuePlaceholder implements  ParamValueInterface{

    String plchldName;

    public ParamValuePlaceholder(String plchldName) {
        this.plchldName = plchldName;
    }

    public String getPlchldName() {
        return plchldName;
    }

    @Override
    public String toString() {
        return plchldName;
    }

    @Override
    public String asString() {
        return "$"+plchldName;
    }

    @Override
    public String getType() {
        return "placeholder";
    }
}
