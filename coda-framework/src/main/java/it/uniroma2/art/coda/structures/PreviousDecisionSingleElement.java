package it.uniroma2.art.coda.structures;

import java.util.List;

/**
 * This class stored a single decision (all the added triple and all the suggestion form CODA regardina single
 * annotation)
 * 
 * @author Andrea Turbati
 * 
 */
public class PreviousDecisionSingleElement {
	// the triple added by the external program using the suggestions provided by CODA
	private List<CODATriple> addedTripleList;
	// all the structure created by each modules inside CODA
	private SuggOntologyCoda suggOntCoda;

	/**
	 * @param addedTripleList
	 *            the list of added triple to the ontology
	 * @param suggOntCoda
	 *            all the suggestion from CODA about a single annotation
	 */
	public PreviousDecisionSingleElement(List<CODATriple> addedTripleList, SuggOntologyCoda suggOntCoda) {
		this.addedTripleList = addedTripleList;
		this.suggOntCoda = suggOntCoda;
	}

	/**
	 * Get all the added triple for this annotation
	 * 
	 * @return a list of all the added triple regarding an annotation
	 */
	public List<CODATriple> getTripleList() {
		return addedTripleList;
	}

	/**
	 * Get all the CODA suggestions regarding an annotation
	 * 
	 * @return all the CODA suggestions regarding an annotation
	 */
	public SuggOntologyCoda getsuggOntCoda() {
		return suggOntCoda;
	}
}
