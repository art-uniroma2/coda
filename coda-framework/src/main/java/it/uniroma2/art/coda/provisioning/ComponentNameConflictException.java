package it.uniroma2.art.coda.provisioning;

public class ComponentNameConflictException extends ComponentIndexingException {

	private String componentURI;

	public ComponentNameConflictException(String componentURI) {
		this.componentURI = componentURI;
	}
	
	@Override
	public String getMessage() {
		return String.format("The URI <%s> is already bound to a different component", componentURI);
	}
	
	private static final long serialVersionUID = 6382826106154865010L;
	
	
}
