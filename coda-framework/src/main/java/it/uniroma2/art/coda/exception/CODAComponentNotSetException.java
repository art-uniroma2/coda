package it.uniroma2.art.coda.exception;

/**
 * This class represent a generic Exception regarding a CODA Component not properly set
 * 
 * @author Andrea Turbati
 *
 */

public class CODAComponentNotSetException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public CODAComponentNotSetException(String msg){
		super(msg);
	}

	
	public CODAComponentNotSetException(Exception e){
		super(e);
	}
}
