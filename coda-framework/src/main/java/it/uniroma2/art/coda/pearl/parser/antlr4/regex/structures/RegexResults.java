package it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.uima.jcas.tcas.Annotation;

public class RegexResults {

	private int numberOfAnn=0;
	private int lastAnnEnd=0;
	
//	Map<String, String> internalIdToRuleIdMap;
	Map<String, List<Annotation>> internalIdToAnnotationListMap;

	
	
	public RegexResults() {
//		internalIdToRuleIdMap = new HashMap<String, String>();
		internalIdToAnnotationListMap = new HashMap<String, List<Annotation>>();
	}

	public int getNumberOfAnn() {
		return numberOfAnn;
	}

	private void setNumberOfAnn(int numberOfAnn) {
		this.numberOfAnn = numberOfAnn;
	}

	public int getLastAnnEnd() {
		return lastAnnEnd;
	}

	private void setLastAnnEnd(int lastAnnEnd) {
		this.lastAnnEnd = lastAnnEnd;
	}
	
//	public String getRuleIdFromInternalId(String internalId){
//		return internalIdToRuleIdMap.get(internalId);
//	}
	
	public List<Annotation> getAnnotationListFromInternalId(String internalId){
		return internalIdToAnnotationListMap.get(internalId);
	}
	
	//public void addInternalIdRuleIdAnnotation(String internalId, String ruleId, Annotation annotation){
	public void addInternalIdRuleIdAnnotation(String internalId, Annotation annotation){
//		internalIdToRuleIdMap.put(internalId, ruleId);
		if(internalIdToAnnotationListMap.get(internalId) == null){
			internalIdToAnnotationListMap.put(internalId, new ArrayList<Annotation>());
		}
		internalIdToAnnotationListMap.get(internalId).add(annotation);
		setNumberOfAnn(getNumberOfAnn()+1);
		if(lastAnnEnd < annotation.getEnd()){
			setLastAnnEnd(annotation.getEnd());
		}
	}
	
	public RegexResults copy(){
		RegexResults regexResults = new RegexResults();
		for(String internalId : internalIdToAnnotationListMap.keySet()){
			for(Annotation ann : internalIdToAnnotationListMap.get(internalId)){
				regexResults.addInternalIdRuleIdAnnotation(internalId, ann);
			}
		}
		return regexResults;
	}
	
	public List<Annotation> getAllAnnotationsMatched(){
		List<Annotation> annList = new ArrayList<Annotation>();
		
		for(String key : internalIdToAnnotationListMap.keySet()){
			List<Annotation> tempAnnList = internalIdToAnnotationListMap.get(key);
			annList.addAll(tempAnnList);
		}
		
		return annList;
	}
	
}
