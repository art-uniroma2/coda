package it.uniroma2.art.coda.pearl.model.graph;

import it.uniroma2.art.coda.pearl.model.ProjectionRule;

/**
 * This class is the single element for a placeholder
 * 
 * @author Andrea Turbati
 */
public class GraphSingleElemPlaceholder extends GraphSingleElement {

	private String name;
	private boolean isFromDepRule;
	private boolean isFromBindRule;

	/**
	 * @param name
	 * @param ownerRule
	 *            the projection rule where this element belongs to
	 * @param isFromOtherRule
	 *            true if this placeholder is from a dependency rule (not a binding), false otherwise
	 * @param isFromBindRule
	 *            true if this placeholder is from a binding rule (not a dependency), false otherwise
	 */
	public GraphSingleElemPlaceholder(String name, ProjectionRule ownerRule, boolean isFromOtherRule,
			boolean isFromBindRule) {
		super(ownerRule);
		this.name = name;
		this.isFromDepRule = isFromOtherRule;
		this.isFromBindRule = isFromBindRule;
	}

	public String getName() {
		return name;
	}

	public boolean isFromDependsOnRule() {
		return isFromDepRule;
	}
	
	public boolean isFromBindingsRule() {
		return isFromBindRule;
	}

	@Override
	public String getValueAsString() {
		return getName();
	}

}
