package it.uniroma2.art.coda.pearl.model.propPathStruct;

import java.util.ArrayList;
import java.util.List;

public class PathSequenceElemPropPath implements GenericElemPropPath {

    private List<PathEltOrInverseElemPropPath> pathEltOrInverseElemPropPathList = new ArrayList();

    public PathSequenceElemPropPath() {
    }

    public void addPathEltOrInverseElemPropPath(PathEltOrInverseElemPropPath pathEltOrInverseElemPropPath) {
        pathEltOrInverseElemPropPathList.add(pathEltOrInverseElemPropPath);
    }

    public List<PathEltOrInverseElemPropPath> getPathEltOrInverseElemPropPathList() {
        return pathEltOrInverseElemPropPathList;
    }

    @Override
    public String getValueAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        boolean first = true;
        for (PathEltOrInverseElemPropPath pathEltOrInverseElemPropPath : pathEltOrInverseElemPropPathList) {
            if (!first) {
                stringBuilder.append("/");
            }
            stringBuilder.append(pathEltOrInverseElemPropPath.getValueAsString());
            first = false;
        }
        stringBuilder.append(" ");

        return stringBuilder.toString();
    }
}
