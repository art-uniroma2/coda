package it.uniroma2.art.coda.structures;


import it.uniroma2.art.coda.pearl.model.ProjectionRule;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains all the projection rules choosen by a Projection Rule Disambiguation component
 * 
 * @author Andrea Turbati
 */
public class SelectedProjectionRules extends InterComponentObject {
	private List<ProjectionRule> projRuleList;

	public SelectedProjectionRules() {
		projRuleList = new ArrayList<ProjectionRule>();
	}

	/**
	 * Add a projection rule to the list
	 * 
	 * @param projRule
	 *            the projection rule to be added
	 * @return true if the list did not already contained the projection rule, false otherwise
	 */
	public boolean addProjRule(ProjectionRule projRule) {
		if (projRuleList.contains(projRule))
			return false;
		projRuleList.add(projRule);
		return true;
	}

	/**
	 * Get the projection rule list
	 * 
	 * @return the projection rule list
	 */
	public List<ProjectionRule> getProjRuleList() {
		return projRuleList;
	}
}