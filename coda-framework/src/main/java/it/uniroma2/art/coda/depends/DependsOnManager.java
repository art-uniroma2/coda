package it.uniroma2.art.coda.depends;

import java.util.HashMap;
import java.util.Map;

public class DependsOnManager {

	private Map<String, DependsAbstactClass> dependsImplMap;
	
	public DependsOnManager() {
		dependsImplMap = new HashMap<String, DependsAbstactClass>();
	}
	
	public void addDependsImpl(DependsAbstactClass dependsImpl){
		dependsImplMap.put(dependsImpl.getName(), dependsImpl);
	}
	
	public DependsAbstactClass getDependsImpl(String depImplName){
		return dependsImplMap.get(depImplName);
	}
}
