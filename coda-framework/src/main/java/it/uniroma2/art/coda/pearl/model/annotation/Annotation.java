package it.uniroma2.art.coda.pearl.model.annotation;

import it.uniroma2.art.coda.exception.parserexception.MissingMandatoryParamInAnnotationException;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Annotation  {
	
	private AnnotationDefinition annotationDefinition;
	private String name;
	private List<String> targetList;
	private Map<String, List<ParamValueInterface>> paramMap;



	public Annotation(String name, AnnotationDefinition annotationDefinition) {
		this.name = name;
		this.annotationDefinition = annotationDefinition;
		paramMap = new HashMap<>();
		targetList = new ArrayList<>();
		
	}
	
	public AnnotationDefinition getAnnotationDefinition(){
		return annotationDefinition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addTarget(String target){
		if(!targetList.contains(target)){
			targetList.add(target);
		}
	}

	public List<String> getTargetList(){
		return targetList;
	}

	public void addParams(String name, List<ParamValueInterface> valueList){
		if(!paramMap.containsKey(name)){
			paramMap.put(name, valueList);

			List<ParamValueInterface> valueListRedux = new ArrayList<>();
			for(ParamValueInterface input : valueList){
				boolean add = true;
				for(ParamValueInterface existing : valueListRedux){
					if(existing.asString().equals(input.asString())){
						add = false;
					}
				}
				if(add){
					valueListRedux.add(input);
				}
			}
			paramMap.put(name, valueListRedux);

		} else{
			//the map already contains this name, so add the values (avoid duplicates)
			List<ParamValueInterface> existingValueList = paramMap.get(name);
			for(ParamValueInterface input : valueList){
				boolean add = true;
				for(ParamValueInterface existing : existingValueList){
					if(existing.asString().equals(input.asString())){
						add = false;
					}
				}
				if(add){
					existingValueList.add(input);
				}

			}
		}
	}

	public List<ParamValueInterface> getParamValueList(String name){
		if(paramMap.get(name)==null){
			//if the annotation do not have such param, check if the annotationDefinition has a default value for such param
			if(annotationDefinition.getParamDefinition(name) != null && annotationDefinition.getParamDefinition(name).hasDefault){
				List<ParamValueInterface> pviList = new ArrayList<>();
				pviList.add(annotationDefinition.getParamDefinition(name).getDefaultValue());
				return pviList;
			}
		}
		return paramMap.get(name);
	}

	public Map<String, List<ParamValueInterface>> getParamMap(){
		Map<String, List<ParamValueInterface>> completeParamMap = new HashMap<>();
		//return not just the param with an explicit value, but also the default one, of no explicit value has been set
		for(ParamDefinition paramDefinition : annotationDefinition.getParamDefinitionList()){
			if(!paramMap.containsKey(paramDefinition.getName())){
				//the explicit paramMap does not have a value for the parameter, so add the default one
				if(paramDefinition.hasDefault){
					List<ParamValueInterface> pviList = new ArrayList<>();
					pviList.add(paramDefinition.getDefaultValue());
					completeParamMap.put(paramDefinition.getName(), pviList);
				} else {
					//this should never happen
				}
			} else{
				completeParamMap.put(paramDefinition.getName(), paramMap.get(paramDefinition.getName()));
			}
		}
		return completeParamMap;
	}




}
