package it.uniroma2.art.coda.structures.table;


import java.util.ArrayList;
import java.util.List;

public class ValuesFromAnAnnotation {
	private List<List<ValueForTable>> singleTableValueListList;
	private boolean isFromMainAnnotation;
	private boolean isFromDepAnnotation;
	private String depType;
	private String ruleId;

	public ValuesFromAnAnnotation(String ruleId) {
		initialize(true, false, ruleId, null);
	}
	
	public ValuesFromAnAnnotation(String ruleId, String depType) {
		initialize(false, true, ruleId, depType);
	}

	public void initialize(boolean isFromMainAnnotation, boolean isFromDepAnnotation, String ruleid,
			String depType) {
		singleTableValueListList = new ArrayList<List<ValueForTable>>();

	}

	
	public List<List<ValueForTable>> getSingleTableValueListList(){
		return singleTableValueListList;
	}

	public void addSingleValueListToList(List<ValueForTable> vftList) {
		
		singleTableValueListList.add(vftList);
		
	}

	public boolean isFromMainAnnotation() {
		return isFromMainAnnotation;
	}

	public boolean isFromDepAnnotation() {
		return isFromDepAnnotation;
	}

	public String getDepType() {
		return depType;
	}

	public String getRuleId() {
		return ruleId;
	}

}
