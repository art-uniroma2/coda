package it.uniroma2.art.coda.pearl.model;

public class IfElseStruct {
	private String condType; // this should be "if", "else if" or "else"
	private String uimaTypeAndFeat;
	private boolean isUimaTypeAndFeatSet;
	private String ontoResUri;
	private PlaceholderStruct condAlias; // null for else type
	private String condOper; // null for else type
	private String condValue; // null for else type
	private boolean isElse; // true for else type, false otherwise

	/**
	 * @param condType
	 *            the conditional type ("if", "else if" or "else")
	 * @param value
	 *            the uimaTypeAndFeat or the uri of resource in the ontology
	 * @param isValueAUimaTypeAndFeat
	 *            true if the value is a uimaTypeAndFeat, false for a uri of a resource of an ontology
	 * @param condAlias
	 *            the alias of the condition or null for the "else" type
	 * @param condOper
	 *            the operator of the condition or null for the "else" type
	 * @param condValue
	 *            the value of the condition or null for the "else" type
	 */
	public IfElseStruct(String condType, String value, boolean isValueAUimaTypeAndFeat,
			PlaceholderStruct condAlias, String condOper, String condValue) {
		this.condType = condType;
		this.isUimaTypeAndFeatSet = isValueAUimaTypeAndFeat;
		if (isValueAUimaTypeAndFeat) {
			uimaTypeAndFeat = value;
			ontoResUri = null;
		} else {
			ontoResUri = value;
			uimaTypeAndFeat = null;
		}
		this.condAlias = condAlias;
		this.condOper = condOper;
		this.condValue = condValue;
		isElse = (condAlias == null);
	}

	/**
	 * Get the type of the condition
	 * 
	 * @return the type of the condition
	 */
	public String getCondType() {
		return condType;
	}

	/**
	 * Check if the value is a uimaTypeAndFeat
	 * 
	 * @return true if the value is a uimaTypeAndFeat false otherwise
	 */
	public boolean isUimaTypeAndFeatSet() {
		return isUimaTypeAndFeatSet;
	}

	/**
	 * Get the uimaTypeTypeAndFeat. This should be used only if {@link #isUimaTypeAndFeatSet()} returns true
	 * 
	 * @return the uimaTypeAndFeat
	 */
	public String getUimaTypeAndFeat() {
		return uimaTypeAndFeat;
	}

	/**
	 * Get the uri of the resource of the ontology. This should be used only if
	 * {@link #isUimaTypeAndFeatSet()} returns false
	 * 
	 * @return the uri of the resource of the ontology
	 */
	public String getOntoResUri() {
		return ontoResUri;
	}

	/**
	 * Get the Alias of the condition
	 * 
	 * @return the alias of the condition
	 */
	public PlaceholderStruct getCondAlias() {
		return condAlias;
	}

	/**
	 * Get the boolean operator of the condition
	 * 
	 * @return the boolean operator of the condition
	 */
	public String getCondOper() {
		return condOper;
	}

	/**
	 * Check if the type is "Else"
	 * 
	 * @return true of the type is "Else", false otherwise
	 */
	public boolean isElse() {
		return isElse;
	}

	/**
	 * Get the value (uimaTypeAndFeat or URI) of the condition
	 * 
	 * @return the value (uimaTypeAndFeat or URI) of the condition
	 */
	public String getCondValue() {
		return condValue;
	}

}
