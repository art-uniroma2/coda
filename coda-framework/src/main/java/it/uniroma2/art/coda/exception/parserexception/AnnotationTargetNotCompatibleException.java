package it.uniroma2.art.coda.exception.parserexception;

public class AnnotationTargetNotCompatibleException extends PRParserException {

	private String annName;
	private String ruleId;
	private String targetNotCompatible;

	private static final long serialVersionUID = 1L;

	public AnnotationTargetNotCompatibleException(String annName, String ruleId, String targetNotCompatible) {
		super();
		this.annName = annName;
		this.ruleId = ruleId;
		this.targetNotCompatible = targetNotCompatible;
	}

	public AnnotationTargetNotCompatibleException(Exception e, String prefixName, String ruleId, String targetNotCompatible) {
		super(e);
		this.annName = prefixName;
		this.ruleId = ruleId;
		this.targetNotCompatible = targetNotCompatible;
	}
	
	public String getAnnName(){
		return annName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	public String getTargetNotCompatible(){
		return targetNotCompatible;
	}

	@Override
	public String getErrorAsString() {
		return "annotation "+ annName +" in rule "+ruleId+" has at least one target not compatible with its Annotation Definition. " +
				"Target not compatible:  "+targetNotCompatible;
	}
}
