package it.uniroma2.art.coda.provisioning.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.coda.provisioning.ConverterContractDescription;
import it.uniroma2.art.coda.provisioning.ConverterDescription;
import it.uniroma2.art.coda.provisioning.SignatureDescription;
import it.uniroma2.art.coda.vocabulary.CODAONTO;

public class ConverterDescriptionImpl extends ConverterContractDescriptionImpl
		implements ConverterDescription {

	private List<ConverterContractDescription> implementedContracts;

	public ConverterDescriptionImpl(String contractURI, String contractName, String contractDescription,
			RDFCapabilityType rdfCapability, Set<IRI> datatypes,
			List<ConverterContractDescription> implementedContracts,
			Collection<SignatureDescription> signatureDescriptions) {
		super(contractURI, contractName, contractDescription, rdfCapability, datatypes,
				signatureDescriptions);
		this.implementedContracts = implementedContracts;
	}

	@Override
	public List<ConverterContractDescription> getImplementedContracts() {
		return implementedContracts;
	}

	@Override
	public void toRDF(Model model) {
		super.toRDF(model);
		SimpleValueFactory vf = SimpleValueFactory.getInstance();
		IRI contractIRI = vf.createIRI(getContractURI());
		Models.setProperty(model, contractIRI, RDF.TYPE, CODAONTO.CONVERTER);

		for (ConverterContractDescription contract : getImplementedContracts()) {
			model.add(contractIRI, CODAONTO.IMPLEMENTED_CONTRACT, vf.createIRI(contract.getContractURI()));
		}
	}
}
