package it.uniroma2.art.coda.exception;

public class ConverterConfigurationException extends ConverterException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1512898145726471372L;

	/**
	 * @param msg
	 */
	public ConverterConfigurationException(String msg) {
        super(msg);
    }
    
    /**
     * @param e
     */
    public ConverterConfigurationException(Exception e) {
        super(e);
    }

}
