package it.uniroma2.art.coda.core;

import java.io.File;
import java.io.InputStream;
import java.nio.file.NotDirectoryException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import it.uniroma2.art.coda.exception.PlaceholderValueWrongTypeForParamInAnnotationException;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceGenericErrorException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceNotDoubleNumException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceValueNotAcceptableException;
import it.uniroma2.art.coda.exception.parserexception.TypeOfParamInAnnotationWrongTypeException;
import it.uniroma2.art.coda.exception.parserexception.UnsupportedTypeInParamDefinitionException;
import it.uniroma2.art.coda.pearl.model.annotation.ParamDefinition;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueDouble;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInteger;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueIri;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueLiteral;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValuePlaceholder;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueString;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemPropPath;
import it.uniroma2.art.coda.provisioning.ComponentIndexingException;
import it.uniroma2.art.coda.structures.CODATriple;
import it.uniroma2.art.coda.structures.NodeAssignment;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.UnsupportedQueryLanguageException;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import it.uniroma2.art.coda.depends.DependsAbstactClass;
import it.uniroma2.art.coda.depends.DependsBetween;
import it.uniroma2.art.coda.depends.DependsFollowing;
import it.uniroma2.art.coda.depends.DependsLast;
import it.uniroma2.art.coda.depends.DependsLastOneOf;
import it.uniroma2.art.coda.depends.DependsNext;
import it.uniroma2.art.coda.depends.DependsOnManager;
import it.uniroma2.art.coda.depends.DependsPrevious;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.DuplicateRuleIdException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.ParserPR;
import it.uniroma2.art.coda.interfaces.ProjectionRulesDeciderInterface;
import it.uniroma2.art.coda.pearl.model.BindingStruct;
import it.uniroma2.art.coda.pearl.model.ConverterArgumentExpression;
import it.uniroma2.art.coda.pearl.model.ConverterMention;
import it.uniroma2.art.coda.pearl.model.GraphElement;
import it.uniroma2.art.coda.pearl.model.GraphStruct;
import it.uniroma2.art.coda.pearl.model.OptionalGraphStruct;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import it.uniroma2.art.coda.pearl.model.ProjectionOperator;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.pearl.model.RegexProjectionRule;
import it.uniroma2.art.coda.pearl.model.annotation.AnnotationDefinition;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemBNode;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemLiteral;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemPlaceholder;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemUri;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemVar;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElement;
import it.uniroma2.art.coda.pearl.parser.PearlParserAntlr4;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.RegexResults;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.StateFSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.TransitionFSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;
import it.uniroma2.art.coda.provisioning.ComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.provisioning.ConverterContractDescription;
import it.uniroma2.art.coda.provisioning.ConverterDescription;
import it.uniroma2.art.coda.structures.DependsOnInfo;
import it.uniroma2.art.coda.structures.StringOrFeatureStruct;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import it.uniroma2.art.coda.structures.table.SingleTableValue;
import it.uniroma2.art.coda.structures.table.SingleTableValueVariable;
import it.uniroma2.art.coda.structures.table.TablePlaceholdersBindingsVariables;
import it.uniroma2.art.coda.structures.table.ValueForTable;
import it.uniroma2.art.coda.structures.table.ValueForTableBinding;
import it.uniroma2.art.coda.structures.table.ValueForTablePlaceholder;
import it.uniroma2.art.coda.structures.table.ValuesFromAnAnnotation;

/**
 * This class represents the entry point to the CODA framework and the external tool should only use this
 * directly.
 * 
 * @author <a href="mailto:turbati@info.uniroma2.it">Andrea Turbati</a>
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class CODACore {

	private final boolean debugMode = false;

	private final String VAR_SYMBOL = "?";
	public static final String PLACEHOLDER_SYMBOL = "$";
	public static final String PLACEHOLDER_DEPENDS_SYMBOL = "..";
	public static final String PLACEHOLDER_DEPENDS_REGEX = "\\..";

	private FSIterator<Annotation> iterAnnotation = null;
	private JCas jcas = null;

	// private PreviousDecisions prevDecision;

	// private RDFModel rdfModel = null;
	// private ModelFactory<?> modelFactory = null;

	private RepositoryConnection connection = null;

	private boolean feathPathInProjRulesValidated = false;

	private CODAContext ctx;
	private boolean useSuperTypesDefault = false;

	// These maps contains all the possible implementation of the five modules
	// with their ID
	private HashMap<String, ProjectionRulesDeciderInterface> projectionRulesDeciderMap = null;

	// private ParserPR projectionRuleParserSelected = null;
	// These are the selected implementations to be used (one for every module)
	// private ProjectionRulesDeciderInterface projectionRuleDeciderSelected =
	// null;

	// These int represent the maximum number of solution that each module
	// should return, 0 means no limit
	private int maxNumAnswerPD = 0;

	// This is a model of the ProjectionRule
	ProjectionRulesModel prModel = null;

	// dependency Info
	DependsOnManager dependsOnManager = null;

	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(CODACore.class);

	// a temporary directory where the framework will store internal stuff (e.g.
	// the repoDir for the Linked Data resolution)
	private ComponentProvider componentProvider;

	private Map<String, Properties> converter2propertiesMap;

	private Map<String, Map<String, Value>> memoizedProjectionsMaps; // multiple, named memoization maps

	public CODACore(ComponentProvider componentProvider) {
		this.componentProvider = componentProvider;
		this.converter2propertiesMap = new HashMap<>();
		this.memoizedProjectionsMaps = new HashMap<>();
	}

	/**
	 * Initializes the framework. This method should be called just after the constructor, before all other
	 * methods.
	 *
	 * @param connection
	 *            a RepositoryConnection to an ontology
	 * @param useSuperTypes
	 *            tells an annotation triggers a rule matching a super type
	 */
	public void initialize(RepositoryConnection connection, boolean useSuperTypes) throws ComponentIndexingException {

		this.useSuperTypesDefault = useSuperTypes;
		setUseSuperTypes(useSuperTypes);

		setRepositoryConnection(connection);

		setAllImplementedDependency();

		componentProvider.open();
	}

	private void setAllImplementedDependency() {
		dependsOnManager = new DependsOnManager();

		dependsOnManager.addDependsImpl(new DependsLast());
		dependsOnManager.addDependsImpl(new DependsNext());
		dependsOnManager.addDependsImpl(new DependsBetween());
		dependsOnManager.addDependsImpl(new DependsPrevious());
		dependsOnManager.addDependsImpl(new DependsFollowing());
		dependsOnManager.addDependsImpl(new DependsLastOneOf());
	}

	/**
	 * Sets whether or not an annotation triggers a rule matching a super type
	 *
	 * @param useSuperTypes
	 */
	public void setUseSuperTypes(boolean useSuperTypes) {
		this.useSuperTypesDefault = useSuperTypes;
	}

	/**
	 * An overload of {@link #initialize(RepositoryConnection, boolean)}, with the third parameter set to
	 * <code>true</code>.
	 *
	 * @param connection
	 */
	public void initialize(RepositoryConnection connection) throws ComponentIndexingException {
		initialize(connection, true);
	}

	/**
	 * Sets the PEARL document that will drive the projection process. This method replace the existing
	 * ProjectionRulesModel with the new one
	 * 
	 * @param prFile
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel setProjectionRulesModel(File prFile) throws PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // new parser using ANTLRv4
		parser.initialize(this);
		this.prModel = parser.parsePearlDocument(prFile, true);
		feathPathInProjRulesValidated = false;
		return prModel;
	}

	/**
	 * Sets the PEARL document that will drive the projection process. This method replace the existing
	 * ProjectionRulesModel with the new one
	 * 
	 * @param prInputStream
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel setProjectionRulesModel(InputStream prInputStream) throws PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // new parser using ANTLRv4
		parser.initialize(this);
		this.prModel = parser.parsePearlDocument(prInputStream, true);
		feathPathInProjRulesValidated = false;
		return prModel;
	}

	/**
	 * Add the PEARL info from the input file into the current ProjectionRuleModel. If the ProjectionRuleModel was null,
	 * it creates a new ProjectionRuleModel
	 * @param prFile
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel addProjectionRuleModel(File prFile)throws PRParserException {
		return addProjectionRuleModel(prFile, false);
	}

	/**
	 * Add the PEARL info from the input file into the current ProjectionRuleModel. If the ProjectionRuleModel was null,
	 * it creates a new ProjectionRuleModel
	 * @param prFile
	 * @param ruleShouldExists
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel addProjectionRuleModel(File prFile, boolean ruleShouldExists)throws PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser;
		if(prModel!=null) {
			//if the prModel is not null, then passed it
			parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser", prModel); // new parser using ANTLRv4
		} else {
			parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // new parser using ANTLRv4
		}
		parser.initialize(this);
		ProjectionRulesModel prmTemp = parser.parsePearlDocument(prFile, ruleShouldExists);
		if(this.prModel==null){
			//the ProjectionRulesModel is empty, so set as the one just created
			this.prModel = prmTemp;
		} else {
			//the ProjectionRulesModel is not empty, so add to it the info from the one just parsed
			this.prModel.addProjectionRuleModel(prmTemp);
		}
		feathPathInProjRulesValidated = false;
		return prModel;
	}

	/**
	 * Add the PEARL info from the input stream into the current ProjectionRuleModel. If the ProjectionRuleModel was null,
	 * it creates a new ProjectionRuleModel
	 * @param prInputStream
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel addProjectionRuleModel(InputStream prInputStream) throws PRParserException {
		return addProjectionRuleModel(prInputStream, false);
	}

	/**
	 * Add the PEARL info from the input stream into the current ProjectionRuleModel. If the ProjectionRuleModel was null,
	 * it creates a new ProjectionRuleModel
	 * @param prInputStream
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel addProjectionRuleModel(InputStream prInputStream, boolean ruleShouldExists) throws PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser;
		if(prModel!=null) {
			//if the prModel is not null, then passed it
			parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser", prModel); // new parser using ANTLRv4
		} else {
			parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // new parser using ANTLRv4
		}
		parser.initialize(this);
		ProjectionRulesModel prmTemp = parser.parsePearlDocument(prInputStream, ruleShouldExists);
		if(this.prModel==null){
			//the ProjectionRulesModel is empty, so set as the one just created
			this.prModel = prmTemp;
		} else {
			//the ProjectionRulesModel is not empty, so add to it the info from the one just parsed
			this.prModel.addProjectionRuleModel(prmTemp);
		}
		feathPathInProjRulesValidated = false;
		return prModel;
	}

	/**
	 * Add all PEARL info from the list of File into the current ProjectionRuleModel. Before adding all these info, the current ProjectionRuleModel is cleaned.
	 * The order of the list is important
	 * @param perFileList
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel setAllProjectionRulelModelFromFileList(List<File> perFileList) throws PRParserException {
		this.cleanProjectionRulesModel();
		for(File prFile : perFileList){
			addProjectionRuleModel(prFile);
		}
		return prModel;
	}

	/**
	 * Add all PEARL info from the list of InputStream into the current ProjectionRuleModel. Before adding all these info, the current ProjectionRuleModel is cleaned.
	 * The order of the list is important
	 * @param prInputSreamList
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionRulesModel setAllProjectionRulelModelFromInputStreamList(List<InputStream> prInputSreamList) throws PRParserException {
		this.cleanProjectionRulesModel();
		for(InputStream prImputStream : prInputSreamList){
			addProjectionRuleModel(prImputStream);
		}
		return prModel;

	}


	/**
	 * Add all the ProjectionRules placed inside *.pr files in a give directory in the current
	 * ProjectionRulesModel, if <code>isRecursive</code> is set to true, then subdirectories are inspected as
	 * well
	 * 
	 * @param dir
	 *            the start directory where to look for *pr files
	 * @param isRecursive
	 *            true if subdirectories should be used as well
	 * @return the ProjectionRulesModel with all the added Projection Rules
	 * @throws NotDirectoryException
	 * @throws PRParserException
	 */
	public ProjectionRulesModel addAllRulesFromDirectory(File dir, boolean isRecursive)
			throws NotDirectoryException, PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv4
		parser.initialize(this);
		ProjectionRulesModel prm = addAllRulesFromDirectory(dir, isRecursive, parser);
		if (this.prModel == null) {
			this.prModel = prm;
		} else {
			this.prModel.addProjectionRuleModel(prm);
		}
		return this.prModel;
	}

	protected ProjectionRulesModel addAllRulesFromDirectory(File dir, boolean isRecursive, ParserPR parser)
			throws NotDirectoryException, PRParserException {
		parser.initialize(this);
		ProjectionRulesModel prm = null;
		if (!dir.isDirectory()) {
			throw new NotDirectoryException(dir.getAbsolutePath());
		}
		File[] prFileArray = dir.listFiles();
		for (File prFile : prFileArray) {
			ProjectionRulesModel tempPRM = null;
			if (prFile.getName().endsWith(".pr")) {
				tempPRM = parser.parsePearlDocument(prFile, true);
			} else if (prFile.isDirectory() && isRecursive) {
				tempPRM = addAllRulesFromDirectory(prFile, isRecursive, parser);
			}
			if (tempPRM != null) {
				if (prm == null) {
					// it is the first ProjectionRulesModel, so just set as the one that will contain all
					// the projection rules
					prm = tempPRM;
				} else {
					prm.addProjectionRuleModel(tempPRM);
				}
			}
		}
		return prm;
	}

	/**
	 * Add all the ProjectionRules placed inside prInputStream in a give directory in the current
	 * ProjectionRulesModel
	 * 
	 * @param prInputStream
	 * @return
	 * @throws DuplicateRuleIdException
	 * @throws PRParserException
	 */
	public ProjectionRulesModel addProjectionRules(InputStream prInputStream)
			throws DuplicateRuleIdException, PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // new parser using ANTLRv4
		parser.initialize(this);
		if (this.prModel == null) {
			this.prModel = parser.parsePearlDocument(prInputStream, true);
		} else {
			this.prModel.addProjectionRuleModel(parser.parsePearlDocument(prInputStream, true));
		}
		feathPathInProjRulesValidated = false;
		return prModel;
	}

	/**
	 * Add all the ProjectionRules placed inside prFile in a give directory in the current
	 * ProjectionRulesModel
	 * 
	 * @param prFile
	 * @return
	 * @throws DuplicateRuleIdException
	 * @throws PRParserException
	 */
	public ProjectionRulesModel addProjectionRules(File prFile)
			throws DuplicateRuleIdException, PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // new parser using ANTLRv4
		parser.initialize(this);
		if (this.prModel == null) {
			this.prModel = parser.parsePearlDocument(prFile, false);
		} else {
			this.prModel.addProjectionRuleModel(parser.parsePearlDocument(prFile, false));
		}
		feathPathInProjRulesValidated = false;
		return prModel;
	}

	/**
	 * add to the first ProjectionRulesModel <code>prm1</code> all the data from the second
	 * ProjectionRulesModel <code>prm1</code>
	 * 
	 * @param prm1
	 * @param prm1
	 * @throws DuplicateRuleIdException
	 */
	public void mergeProjectionRulesModels(ProjectionRulesModel prm1, ProjectionRulesModel prm2)
			throws DuplicateRuleIdException {
		prm1.addProjectionRuleModel(prm2);
	}

	/**
	 * Clean the current ProjectionRulesModel
	 */
	public void cleanProjectionRulesModel() {
		this.prModel = new ProjectionRulesModel();
	}

	/**
	 * Set the UIMA CAS containing all the UIMA annotation
	 * 
	 * @param jcas
	 *            the jcas that will be set
	 */
	public void setJCas(JCas jcas, boolean clearMemoizedMap) {
		this.jcas = jcas;
		if (jcas != null) {
			iterAnnotation = this.jcas.getAnnotationIndex().iterator();
		}
		feathPathInProjRulesValidated = false;
		if (clearMemoizedMap) {
			clearMemoizedMap();
		}
	}

	/**
	 * Clear the memoizedProjectionsMaps
	 */
	public void clearMemoizedMap(){
		memoizedProjectionsMaps.clear();
	}

	/**
	 * Binds a contract to a converter (both indicated via their URIs).
	 * 
	 * @param contract
	 * @param converter
	 */
	public void setGlobalContractBinding(String contract, String converter) {
		componentProvider.setGlobalContractBinding(contract, converter);
	}

	/**
	 * Returns the descriptions of the currently known converter contracts.
	 * 
	 * @return
	 */
	public Collection<ConverterContractDescription> listConverterContracts() {
		return componentProvider.listConverterContracts();
	}

	/**
	 * Returns the descriptions of the currently known converters
	 * 
	 * @return
	 */
	public Collection<ConverterDescription> listConverters() {
		return componentProvider.listConverters();
	}

	/**
	 * Sets the properties associated with a converter.
	 * 
	 * @param converter
	 * @param properties
	 */
	public void setConverterProperties(String converter, Properties properties) {
		this.converter2propertiesMap.put(converter, properties);
	}

	/**
	 * Close the rdfModel and stop Felix
	 * 
	 * @throws RepositoryException
	 */
	public void stopAndClose() throws RepositoryException {
		try {
			componentProvider.close();
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	/**
	 * Set the ontology connection
	 * 
	 * @param connection
	 *            the ontology model
	 */
	public void setRepositoryConnection(RepositoryConnection connection) {
		this.connection = connection;
		// setProjectionRulesDecider(projectionRuleDeciderSelected);
		this.ctx = new CODAContext(connection, converter2propertiesMap);
	}

	/**
	 * Close the RDFModel
	 * 
	 * @throws RepositoryException
	 */
	public void closeRepositoryConnection() throws RepositoryException {
		connection.close();
	}

	/**
	 * Get the CODA context 
	 * @return
	 */
	public CODAContext getCODAContext() {
		return ctx;
	}
	
	/**
	 * Get the Projection Rule Model
	 * 
	 * @return the projection rule model
	 */
	public ProjectionRulesModel getProjRuleModel() {
		return prModel;
	}

	/**
	 * Get the AnnotationsDefinition Model
	 * 
	 * @return the projection rule model
	 * @author Gambella Riccardo
	 * @author Andrea Turbati
	 */
	public Collection<AnnotationDefinition> getDefinitionModel() {
		return prModel.getAnnotationDefinitionList();
	}

	/**
	 * This method get the Projection Rule Decider Annotation Map TODO check if it should still remain in this
	 * version
	 * 
	 * @return the Projection Disambiguation Map
	 */
	public HashMap<String, ProjectionRulesDeciderInterface> getProjectionRulesDeciderMap() {
		return projectionRulesDeciderMap;
	}

	/**
	 * This method return true if there is another UIMA annotation in the cas, false otherwise
	 * 
	 * @return true if there is another UIMA annotation in the cas, false otherwise
	 */
	public boolean isAnotherAnnotationPresent() {
		if (iterAnnotation != null)
			return iterAnnotation.hasNext();
		else
			// the iterator was not set
			return false;
	}

	/**
	 * This method set the maximum number of hint that the Projection Disambiguation can return
	 * 
	 * @param maxNumAnswerPD
	 *            the maximum number of hint that the Projection Disambiguation can return
	 */
	// OLD version
	public void setMaxNumAnswerPD(int maxNumAnswerPD) {
		this.maxNumAnswerPD = maxNumAnswerPD;
	}

	/**
	 * Processes all annotation found in the current CAS, and returns a list of the suggestions originate from
	 * each annotation.
	 * 
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 * @throws DependencyException
	 * @throws RDFModelNotSetException
	 * @throws ProjectionRuleModelNotSet
	 * @throws UnassignableFeaturePathException
	 */
	List<SuggOntologyCoda> processAllAnnotation()
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {
		return processAllAnnotation(useSuperTypesDefault, false, false);
	}

	/**
	 * Processes all annotation found in the current CAS, and returns a list of the suggestions originated
	 * from each annotation.
	 * 
	 * @param useSuperTypes
	 *            tells whether or not an annotation triggers a rule matching a supertype (overriding the
	 *            value passed to {@link #initialize(RepositoryConnection, boolean)} or
	 *            {@link #setUseSuperTypes(boolean)}
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 * @throws DependencyException
	 * @throws RDFModelNotSetException
	 * @throws ProjectionRuleModelNotSet
	 * @throws UnassignableFeaturePathException
	 */
	public List<SuggOntologyCoda> processAllAnnotation(boolean useSuperTypes)
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {

		return processAllAnnotation(useSuperTypes, false, false);
	}

	public List<SuggOntologyCoda> processAllAnnotation(boolean addNodeAssignmentMap,
													   boolean addNodeNamesInGraph)
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {

		return processAllAnnotation(useSuperTypesDefault, addNodeAssignmentMap, addNodeNamesInGraph);
	}

	/**
	 * Processes all annotation found in the current CAS, and returns a list of the suggestions originated
	 * from each annotation.
	 *
	 * @param useSuperTypes
	 *            tells whether or not an annotation triggers a rule matching a supertype (overriding the
	 *            value passed to {@link #initialize(RepositoryConnection, boolean)} or
	 *            {@link #setUseSuperTypes(boolean)}
	 * @param addNodeAssignmentMap tells whether to add the nodeAssignmentMap to each SuggOntologyCoda or not
	 * @param addNodeNamesInGraph tells whether to add the the nodeNameInGraph to each CODATriple in each SuggOntologyCoda
	 *                            or not
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 * @throws DependencyException
	 * @throws RDFModelNotSetException
	 * @throws ProjectionRuleModelNotSet
	 * @throws UnassignableFeaturePathException
	 */
	public List<SuggOntologyCoda> processAllAnnotation(boolean useSuperTypes, boolean addNodeAssignmentMap,
													   boolean addNodeNamesInGraph)
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {
		List<SuggOntologyCoda> suggOntCodaList = new ArrayList<>();
		while (isAnotherAnnotationPresent()) {
			SuggOntologyCoda suggOntologyCoda = processNextAnnotation(useSuperTypes, addNodeAssignmentMap,
					addNodeNamesInGraph);
			if (suggOntologyCoda != null) {
				suggOntCodaList.add(suggOntologyCoda);
			}
		}
		return suggOntCodaList;
	}

	/**
	 * Processes the next annotation and returns the suggestions originated from it.
	 * 
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 * @throws DependencyException
	 * @throws RDFModelNotSetException
	 * @throws ProjectionRuleModelNotSet
	 * @throws UnassignableFeaturePathException
	 */
	public SuggOntologyCoda processNextAnnotation()
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {
		return processNextAnnotation(useSuperTypesDefault, false, false);
	}

	/**
	 * Processes the next annotation and returns the suggestions originated from it.
	 * 
	 * @param useSuperTypes
	 *            tells whether or not an annotation triggers a rule matching a supertype (overriding the
	 *            value passed to {@link #initialize(RepositoryConnection, boolean)} or
	 *            {@link #setUseSuperTypes(boolean)}
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 * @throws DependencyException
	 * @throws RDFModelNotSetException
	 * @throws ProjectionRuleModelNotSet
	 * @throws UnassignableFeaturePathException
	 */
	public SuggOntologyCoda processNextAnnotation(boolean useSuperTypes)
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException, ConfidenceNotDoubleNumException,
			ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {
		return processNextAnnotation(useSuperTypes, false, false);
	}

	public SuggOntologyCoda processNextAnnotation(boolean addNodeAssignmentMap,
												  boolean addNodeNamesInGraph)
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException, ConfidenceNotDoubleNumException,
			ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {
		return processNextAnnotation(useSuperTypesDefault, addNodeAssignmentMap, addNodeNamesInGraph);
	}

	/**
	 * Processes the next annotation and returns the suggestions originated from it.
	 *
	 * @param useSuperTypes
	 *            tells whether or not an annotation triggers a rule matching a supertype (overriding the
	 *            value passed to {@link #initialize(RepositoryConnection, boolean)} or
	 *            {@link #setUseSuperTypes(boolean)}
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 * @throws DependencyException
	 * @throws RDFModelNotSetException
	 * @throws ProjectionRuleModelNotSet
	 * @throws UnassignableFeaturePathException
	 */
	public SuggOntologyCoda processNextAnnotation(boolean useSuperTypes, boolean addNodeAssignmentMap,
												  boolean addNodeNamesInGraph)
			throws ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException,
			RepositoryException, MalformedQueryException, QueryEvaluationException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException, ConfidenceNotDoubleNumException,
			ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {
		if (connection == null) {
			throw new RDFModelNotSetException("The Repository Connection has not be properly set");
		}
		if (!isAnotherAnnotationPresent())
			return null;

		Annotation annotation = iterAnnotation.next();
		String annotationTypeName = annotation.getType().getName();

		SuggOntologyCoda suggOntCoda = new SuggOntologyCoda(annotation, addNodeAssignmentMap);

		if (prModel == null) {
			throw new ProjectionRuleModelNotSet("The projection rule model was not properly set");
		}

		if (!feathPathInProjRulesValidated) {
			feathPathInProjRulesValidated = UIMACODAUtilities.validateFeathPathInProjRuleModel(prModel, jcas);
			// set the value to true, so next time this check is not performed
			feathPathInProjRulesValidated = true;
		}

		// get the candidate rules (check only among the STANDARD RULES, so no lazy or forRegex rules)
		Collection<ProjectionRule> candidatedRules = prModel
				.getStandardProjectionRulesByTypeName(annotationTypeName);
		// check if the super types should be used or not
		if (useSuperTypes) {
			List<String> superTypesList = UIMACODAUtilities.getSuperTypes(annotation, null);
			for (String annotationSuperTypeName : superTypesList) {
				candidatedRules.addAll(prModel.getStandardProjectionRulesByTypeName(annotationSuperTypeName));
			}
		}
		for (ProjectionRule pr : candidatedRules) {
			// process those rules that pass the condition
			if (!pr.evaluateConditions(annotation)) {
				// at least one condition was not satisfy, so do not consider this rule
				continue;
			}

			// suggOntCoda.addProjectionRule(pr); // moved inside executeProjectionRule
			executeProjectionRule(pr, annotation, suggOntCoda, addNodeNamesInGraph, addNodeAssignmentMap);
		}

		// get the candidate regexes, the ones in which one of their starting state uses the
		// annotationTypeName
		Collection<RegexProjectionRule> candicateRegexes = prModel.getRegexesByTypename(annotationTypeName);
		// check if the super types should be used or not
		if (useSuperTypes) {
			List<String> superTypesList = UIMACODAUtilities.getSuperTypes(annotation, null);
			for (String annotationSuperTypeName : superTypesList) {
				candicateRegexes.addAll(prModel.getRegexesByTypename(annotationSuperTypeName));
			}
		}
		for (RegexProjectionRule rpr : candicateRegexes) {
			// suggOntCoda.addProjectionRule(rpr); // moved inside executeRegex
			executeRegex(rpr, jcas, suggOntCoda, iterAnnotation, annotation, useSuperTypes, addNodeNamesInGraph);
		}

		return suggOntCoda;
	}

	/**
	 * Executes a projection rule <code>pr</code> against an <code>annotation</code> storing the suggestions
	 * in the provided strucuture.
	 * 
	 * @param pr
	 * @param annotation
	 * @param suggOntCoda
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 * @throws QueryEvaluationException
	 * @throws DependencyException
	 */
	public void executeProjectionRule(ProjectionRule pr, Annotation annotation, SuggOntologyCoda suggOntCoda,
									  boolean addNodeNamesInGraph, boolean addNodeAssignmentMap)
			throws ComponentProvisioningException, ConverterException, RepositoryException,
			QueryEvaluationException, DependencyException, UnsupportedTypeInParamDefinitionException,
			TypeOfParamInAnnotationWrongTypeException, ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException,
			ConfidenceGenericErrorException, PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {

		List<CODATriple> suggestedInsertTripleWithDuplicated = new ArrayList<>();
		List<CODATriple> suggestedInsertTriple = new ArrayList<>();
		List<CODATriple> suggestedDeleteTripleWithDuplicated = new ArrayList<>();
		List<CODATriple> suggestedDeleteTriple = new ArrayList<>();

		List<List<ValuesFromAnAnnotation>> valuesFromAnnotationListList = new ArrayList<>();

		//ctx.setCurrentUIMAAnnotation(annotation);

		// this is a list because the other elements of the outer list
		// ( valueForTableListList valuesFromAnnotationListList ) are list as well
		// but this list should have just one ValuesFromAnAnnotation
		List<ValuesFromAnAnnotation> valueFromMainAnnotationList = new ArrayList<ValuesFromAnAnnotation>();

		// create the instance containing all the placeholder and binding generated from this rule
		// (the main rule)
		ValuesFromAnAnnotation valueFromMainAnnotation = new ValuesFromAnAnnotation(pr.getId());

		// Evaluate bindings
		Map<String, BindingStruct> bindingMap = pr.getBindingsMap();
		for (String bindingId : bindingMap.keySet()) {
			BindingStruct binding = bindingMap.get(bindingId);
			String uimaTypeAndFeat = pr.getUIMAType() + ":" + binding.getFeaturePath();
			String bindedRuleId = binding.getBoundRuleId();
			ProjectionRule lazyRule = prModel.getProjRuleFromId(bindedRuleId);
			// now get the placeholder of the bounded lazy rule and construct
			// the ValueForTableBinding
			Map<String, PlaceholderStruct> placeholderFromLazyMap = lazyRule.getPlaceholderMap();
			Map<String, List<StringOrFeatureStruct>> valuesFromLazyRuleListMap = new HashMap<String, List<StringOrFeatureStruct>>();
			for (String placeholderKey : placeholderFromLazyMap.keySet()) {
				PlaceholderStruct placeholderFromLazyRule = placeholderFromLazyMap.get(placeholderKey);
				String uimaTypeAndFeatInLazyRule = uimaTypeAndFeat + "/"
						+ placeholderFromLazyRule.getFeaturePath();
				List<StringOrFeatureStruct> valuesList = UIMACODAUtilities
						.getValuesOfFeatureFromFeatPath(annotation, uimaTypeAndFeatInLazyRule);
				valuesFromLazyRuleListMap.put(placeholderFromLazyRule.getName(), valuesList);
			}
			// now get the value extracted from UIMA using the feature path from
			// the binding and associated
			// then to construct the list of ValueForTableBinding
			boolean errorInBindingConstruct = false;
			if (!valuesFromLazyRuleListMap.isEmpty()) {
				int numberOfValues = -1;
				for (String key : valuesFromLazyRuleListMap.keySet()) {
					if (numberOfValues == -1)
						numberOfValues = valuesFromLazyRuleListMap.get(key).size();
					if (valuesFromLazyRuleListMap.get(key).size() != numberOfValues)
						errorInBindingConstruct = true;
				}
				if (errorInBindingConstruct) {
					// there is some problem in constructing the binding values,
					// the different list of values
					// returned from the featurePath are not even among the
					// different placeholder, so
					// it is not possible in this implementation to do the right
					// association
					// TODO do something
				} else {
					List<ValueForTable> vfBList = new ArrayList<ValueForTable>();
					for (int i = 0; i < numberOfValues; ++i) {
						Map<String, Value> idToARTNodeMap = new HashMap<String, Value>();
						for (String key : valuesFromLazyRuleListMap.keySet()) {
							PlaceholderStruct placeholder = placeholderFromLazyMap.get(key);
							// String valueFromUIMA = valuesFromLazyRuleListMap.get(key).get(i);
							StringOrFeatureStruct stringOrFeatStruct = valuesFromLazyRuleListMap.get(key)
									.get(i);

							Value artNode = UIMACODAUtilities.getRDFValueFromUIMAValue(placeholder,
									stringOrFeatStruct, componentProvider, ctx);

							// TODO check if the it used in the following map is
							// correct, it should be
							// something like $binding.placeholder
							if (artNode != null) {
								// idToARTNodeMap.put(PLACEHOLDER_SYMBOL + bindedRuleId + "." + key, artNode);
								// // OLD
								idToARTNodeMap.put(PLACEHOLDER_SYMBOL + bindingId + "." + key, artNode);
							}
						}
						ValueForTableBinding valueForBinding = new ValueForTableBinding(binding,
								idToARTNodeMap);
						// valueForTableBindingList.add(valueForBinding);
						vfBList.add(valueForBinding);
					}
					valueFromMainAnnotation.addSingleValueListToList(vfBList);
					// valueForTableListList.add(valueForTableBindingList);
				}
			}
		}

		// Evaluate placeholders
		Map<String, PlaceholderStruct> placeholderMap = pr.getPlaceholderMap();
		Multimap<String, Object> placeholder2valueMap = HashMultimap.create();

		for (String placeholderId : placeholderMap.keySet()) {
			PlaceholderStruct placeholder = placeholderMap.get(placeholderId);
			List<Value> valueListForPlaceholder = new ArrayList<>();

			List<ValueForTable> vfpList = new ArrayList<>();
			if (placeholder.hasFeaturePath()) {
				String uimaTypeAndFeat = pr.getUIMAType() + ":" + placeholder.getFeaturePath();
				List<StringOrFeatureStruct> valuesList = UIMACODAUtilities
						.getValuesOfFeatureFromFeatPath(annotation, uimaTypeAndFeat);

				// when valuesList is null, it means that no values were obtained for that particular
				// placeholder
				if (valuesList != null) {
					for (StringOrFeatureStruct valueFromUIMA : valuesList) {
						List<Value> artNodeList = UIMACODAUtilities.getRDFValuesFromUIMAValue(placeholder,
								valueFromUIMA, componentProvider, ctx, placeholder2valueMap,
								memoizedProjectionsMaps);
						for (Value artNode : artNodeList) {
							ValueForTablePlaceholder valueForTablePlaceholder = new ValueForTablePlaceholder(
									placeholder, PLACEHOLDER_SYMBOL + placeholderId, artNode);
							vfpList.add(valueForTablePlaceholder);
							placeholder2valueMap.put(placeholderId, artNode);
							if (addNodeAssignmentMap) {
								valueListForPlaceholder.add(artNode);
							}
						}

					}
					if (!vfpList.isEmpty()) {
						valueFromMainAnnotation.addSingleValueListToList(vfpList);
					}
				}
			} else {
				List<Value> artNodeList = UIMACODAUtilities.getRDFValuesFromUIMAValue(placeholder, null,
						componentProvider, ctx, placeholder2valueMap, memoizedProjectionsMaps);
				for (Value artNode : artNodeList) {
					ValueForTablePlaceholder valueForTablePlaceholder = new ValueForTablePlaceholder(
							placeholder, PLACEHOLDER_SYMBOL + placeholderId, artNode);
					vfpList.add(valueForTablePlaceholder);
					valueFromMainAnnotation.addSingleValueListToList(vfpList);
					placeholder2valueMap.put(placeholderId, artNode);
					if (addNodeAssignmentMap) {
						valueListForPlaceholder.add(artNode);
					}
				}
			}
			// if addnodeAssignmentMap is true, then add the values to the nodeAssignmentMap
			if (addNodeAssignmentMap) {
				NodeAssignment nodeAssignment;
				if (valueListForPlaceholder.isEmpty()) {
					// not values for the current placeholder
					nodeAssignment = null;
				} else if (valueListForPlaceholder.size() == 1) {
					nodeAssignment = new NodeAssignment(placeholderId, valueListForPlaceholder.get(0));
				} else { // (valueListForPlaceholder.size()>1
					nodeAssignment = new NodeAssignment(placeholderId, valueListForPlaceholder);
				}

				suggOntCoda.addNodeAssignmen(placeholderId, nodeAssignment);
			}
		}

		// add the list containing JUST the values from the Main annotation to the List_List management
		valueFromMainAnnotationList.add(valueFromMainAnnotation);
		valuesFromAnnotationListList.add(valueFromMainAnnotationList);

		// Evaluate the external placeholder (the ones derived from the dependsOn mechanism)
		// in this part DO NOT USE valueFromMainAnnotation, instead the single dependency management
		// implementation should return a list of ValuesFromAnAnnotation
		Map<String, Map<String, DependsOnInfo>> depOuterMap = pr.getDependOnMap();
		for (String depType : depOuterMap.keySet()) {
			Map<String, DependsOnInfo> depInnerMap = depOuterMap.get(depType);
			if (depInnerMap == null) {
				// this should not happen, because this map was constructed when parsing the PEARL file
				// TODO decide what to do in this case
				continue;
			}
			for (String depRuleId : depInnerMap.keySet()) {
				DependsOnInfo dependsOnInfo = depInnerMap.get(depRuleId);
				if (dependsOnInfo == null) {
					// this should not happen, because this map was constructed when parsing the PEARL file
					// TODO decide what to do in this case
					continue;
				}
				// ProjectionRule depProjRule = prModel.getProjRuleFromId(dependsOnInfo.getDependsOnRuleId());
				// now execute the dependency type using the right information
				DependsAbstactClass dependsImpl = dependsOnManager.getDependsImpl(depType);
				if (dependsImpl == null) {
					// there is no implementation for the dependency specified, decide what to do
					System.err.println("there is no implementation for the dependency " + depType);
					continue;
				}
				List<ValuesFromAnAnnotation> vfaaDepList = dependsImpl.execute(dependsOnInfo, annotation,
						jcas, prModel, pr, componentProvider, ctx);
				if (!vfaaDepList.isEmpty()) {
					valuesFromAnnotationListList.add(vfaaDepList);
				}
			}
		}

		// Construct placeholders/bindings table (containing also the placeholder taken from the
		// dependency rule)
		TablePlaceholdersBindingsVariables table = new TablePlaceholdersBindingsVariables();
		table.constructPlaceholderBindingMap(valuesFromAnnotationListList);

		// table.printPlaceholderBindingTable(); // DEBUG

		// For each row in the table, execute where
		Iterator<Map<String, SingleTableValue>> iterTempTable = table.getIteratorForPlaceholderBindingMap();
		while (iterTempTable.hasNext()) {
			Map<String, SingleTableValue> row = iterTempTable.next();
			// If there is no WHERE section (or if it is empty) construct the
			// second table from the first one
			if (pr.getWhereList().isEmpty()) {
				table.addVariableValueToPlaceholderBindingVariableMap(row.values(), null);
			} else {

				Collection<GraphElement> whereList = pr.getWhereList();
				String tempString = constructWhereQuery(whereList, row);

				// For each tuple in the where result set, create a new extended
				// row
				Map<String, List<Value>> varToArtNodeMap = new HashMap<String, List<Value>>();
				if (tempString != null) {
					String query = "SELECT *" + "\nWHERE { ";

					query += tempString;

					query += "\n}";
					TupleQuery tupleQuery;
					try {
						tupleQuery = connection.prepareTupleQuery(query);

					} catch (RepositoryException | MalformedQueryException e) {
						throw new IllegalStateException(
								"An unexpected exception was raised during the construction of the low-level SPARQL query associated with the WHERE clause of a rule",
								e);
					}
					try (TupleQueryResult tupleQueryResult = tupleQuery.evaluate()) {
						while (tupleQueryResult.hasNext()) {
							BindingSet bindingSet = tupleQueryResult.next();
							Set<String> names = bindingSet.getBindingNames();

							Iterator<String> iterNames = names.iterator();
							while (iterNames.hasNext()) {
								String name = iterNames.next();
								Value value = bindingSet.getBinding(name).getValue();
								String var = VAR_SYMBOL + name;
								if (!varToArtNodeMap.containsKey(var))
									varToArtNodeMap.put(var, new ArrayList<Value>());
								varToArtNodeMap.get(var).add(value);
							}
						}
					}
				} else {
					// there was a problem in the creation of the SPARQL query,
					// so no query is executed
				}

				// use the result of the query to construct the second and final
				// table
				List<List<SingleTableValueVariable>> valueVariableList = new ArrayList<List<SingleTableValueVariable>>();
				for (String var : varToArtNodeMap.keySet()) {
					List<SingleTableValueVariable> varSingleTableValueList = new ArrayList<SingleTableValueVariable>();
					for (Value artNode : varToArtNodeMap.get(var)) {
						varSingleTableValueList.add(new SingleTableValueVariable(var, artNode));
					}
					valueVariableList.add(varSingleTableValueList);
				}
				table.addVariableValueToPlaceholderBindingVariableMap(row.values(), valueVariableList);
			}
		}

		// Use the table construct after the SPARQL queries to create the
		// suggestions
		// iterate over the final table and for every row create the RDF graph (both Insert and Delete graph)
		Iterator<Map<String, SingleTableValue>> iterFinalTable = table
				.getIteratorForPlaceholderBindingVariableMap();
		Collection<GraphElement> graphInsertList = pr.getInsertGraphList();
		Collection<GraphElement> graphDeleteList = pr.getDeleteGraphList();
		while (iterFinalTable.hasNext()) {
			boolean graphFail = false;
			List<CODATriple> currentInsertSuggestedTriple = new ArrayList<CODATriple>();
			List<CODATriple> currentDeleteSuggestedTriple = new ArrayList<CODATriple>();
			Map<String, SingleTableValue> row = iterFinalTable.next();
			Map<String, BNode> bnodeMap = new HashMap<String, BNode>();
			// the Insert Graph
			for (GraphElement graphElement : graphInsertList) {
				if (graphElement instanceof GraphStruct) {
					GraphStruct graphStruct = (GraphStruct) graphElement;
					CODATriple suggestedCODATriple = constructCODATriple(graphStruct, row, bnodeMap, pr.getId(), addNodeNamesInGraph);
					if (suggestedCODATriple == null) {
						graphFail = true;
						break;
					}
					currentInsertSuggestedTriple.add(suggestedCODATriple);
				} else { // graphElement instanceof OptionalGraphStruct
					OptionalGraphStruct optionaGraphStruct = (OptionalGraphStruct) graphElement;

					List<CODATriple> optionalSuggestedTripleList = new ArrayList<CODATriple>();
					boolean allOptionalsucceded = resolveOptionalGraph(optionaGraphStruct,
							optionalSuggestedTripleList, row, bnodeMap, pr, addNodeNamesInGraph);

					if (allOptionalsucceded)
						currentInsertSuggestedTriple.addAll(optionalSuggestedTripleList);
				}
			}
			// the Delete Graph
			for (GraphElement graphElement : graphDeleteList) {
				if (graphElement instanceof GraphStruct) {
					GraphStruct graphStruct = (GraphStruct) graphElement;
					CODATriple suggestedCODATriple = constructCODATriple(graphStruct, row, bnodeMap, pr.getId(), addNodeNamesInGraph);
					if (suggestedCODATriple == null) {
						graphFail = true;
						break;
					}
					currentDeleteSuggestedTriple.add(suggestedCODATriple);
				} else { // graphElement instanceof OptionalGraphStruct
					OptionalGraphStruct optionaGraphStruct = (OptionalGraphStruct) graphElement;

					List<CODATriple> optionalSuggestedTripleList = new ArrayList<CODATriple>();
					boolean allOptionalsucceded = resolveOptionalGraph(optionaGraphStruct,
							optionalSuggestedTripleList, row, bnodeMap, pr, addNodeNamesInGraph);

					if (allOptionalsucceded)
						currentDeleteSuggestedTriple.addAll(optionalSuggestedTripleList);
				}
			}

			if (!graphFail) {
				// the graph did not fail, so add the suggested triple, even if they are duplicate
				suggestedInsertTripleWithDuplicated.addAll(currentInsertSuggestedTriple);
				suggestedDeleteTripleWithDuplicated.addAll(currentDeleteSuggestedTriple);
			}
		}

		// Remove the duplicate Insert RDF triple from the suggested ones
		removeDuplicate(suggestedInsertTripleWithDuplicated, suggestedInsertTriple);
		removeDuplicate(suggestedDeleteTripleWithDuplicated, suggestedDeleteTriple);

		// Add the suggestedTriple to the output structure
		suggOntCoda.addProjectionRule(pr);
		suggOntCoda.addSuggestedInsertTripleList(pr.getId(), suggestedInsertTriple);
		suggOntCoda.addSuggestedDeleteTripleList(pr.getId(), suggestedDeleteTriple);
	}

	private boolean resolveOptionalGraph(OptionalGraphStruct optionaGraphStruct,
			List<CODATriple> optionalSuggestedTripleList, Map<String, SingleTableValue> row,
			Map<String, BNode> bnodeMap, ProjectionRule pr, boolean addNodeNamesInGraph)
			throws UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException, PlaceholderValueWrongTypeForParamInAnnotationException {
		Collection<GraphElement> optionalTripleList = optionaGraphStruct.getOptionalTriples();
		for (GraphElement graphElement : optionalTripleList) {
			if (graphElement instanceof GraphStruct) {
				GraphStruct graphStruct = (GraphStruct) graphElement;
				CODATriple suggestedCODATriple = constructCODATriple(graphStruct, row, bnodeMap, pr.getId(), addNodeNamesInGraph);
				if (suggestedCODATriple == null) {
					return false;
				} else {
					optionalSuggestedTripleList.add(suggestedCODATriple);
				}
			} else {// graphElement instanceof OptionalGraphStruct
				List<CODATriple> innerOptionalSuggestedTripleList = new ArrayList<CODATriple>();
				OptionalGraphStruct innerOptionalGraphStruct = (OptionalGraphStruct) graphElement;
				boolean innerOptionalSucceded = resolveOptionalGraph(innerOptionalGraphStruct,
						innerOptionalSuggestedTripleList, row, bnodeMap, pr, addNodeNamesInGraph);
				if (innerOptionalSucceded) {
					optionalSuggestedTripleList.addAll(innerOptionalSuggestedTripleList);
				}
			}
		}
		return true;
	}

	private CODATriple constructCODATriple(GraphStruct graphStruct, Map<String, SingleTableValue> row,
			Map<String, BNode> bnodeMap, String ruleId, boolean addNodeNamesInGraph)
			throws UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceValueNotAcceptableException, ConfidenceNotDoubleNumException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException {
		GraphSingleElement subjGSE = graphStruct.getSubject();
		GraphSingleElement predGSE = graphStruct.getPredicate();
		GraphSingleElement objGSE = graphStruct.getObject();

		// Get the list of annotations related to a triple
		List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> listAnnotations = graphStruct
				.getAnnotationList();
		List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> annotationListToAddToTriple = new ArrayList<>();

		Value tempARTNode;
		Resource subjARTNode = null;
		tempARTNode = getValueOfGraph(row, subjGSE, bnodeMap);
		if (tempARTNode != null)
			subjARTNode = (Resource) tempARTNode;
		IRI predARTNode = null;
		tempARTNode = getValueOfGraph(row, predGSE, bnodeMap);
		if (tempARTNode != null)
			predARTNode = (IRI) tempARTNode;
		Value objARTNode = getValueOfGraph(row, objGSE, bnodeMap);
		if (subjARTNode == null || predARTNode == null || objARTNode == null) {
			return null;
		}

		// If the ARTTriple doesn't have annotations, create immediately the triple having confidence as 1.0
		CODATriple suggestedCODATriple;
		if (listAnnotations.isEmpty()) {
			if (addNodeNamesInGraph) {
				suggestedCODATriple = new CODATriple(subjARTNode, predARTNode, objARTNode, 1.0,
						subjGSE.getValueAsString(), predGSE.getValueAsString(), objGSE.getValueAsString());
			} else {
				suggestedCODATriple = new CODATriple(subjARTNode, predARTNode, objARTNode, 1.0);
			}
		} else {
			// Construct an array, with all the annotation specified in the definition of the triple template,
			// if some of the annotation should not be consider (used and not defined, no meta-annotation
			// RETENTION),
			// remove the annotation
			ArrayList<it.uniroma2.art.coda.pearl.model.annotation.Annotation> listAnnotationsCopy = new ArrayList<it.uniroma2.art.coda.pearl.model.annotation.Annotation>(
					listAnnotations);
			double confidence = 1.0;
			for (it.uniroma2.art.coda.pearl.model.annotation.Annotation annotation : listAnnotationsCopy) {
				//get the annotation definition from the name of the current annotation
				//AnnotationDefinition annotationDefinition = prModel.getAnnotationDefinition(annotation.getName());
				AnnotationDefinition annotationDefinition = annotation.getAnnotationDefinition();
				// check if this annotation was declared (in theory it should never happen that an annotation is not defined, an exception had to be thrown before)
				if (annotationDefinition == null) {
					// this should never happen, since if there was no AnnotationDefinition, then the
					// annotation should have been removed during the parsing phase, but just to be sure.
					// The annotation is not defined, so do not put it into the model
					logger.error("the annotation " + annotation.getName() + " is used but not defined");
					listAnnotations.remove(annotation);
				}
				// Remove the annotation if it doesn't have the metaAnnotation RETAINED_ANN_DECL .
				else if (!annotationDefinition.hasRetention()) {
					listAnnotations.remove(annotation);
					logger.error("Annotation " + annotation.getName() + "has not the Retention feature. Rejected Annotation.");
					//since the annotation has been removed, skip other checks
					continue;
				}
				// check if the annotation regards the CONFIDENCE
				if (annotation.getName().equals(PearlParserDescription.CONFIDENCE_ANN)) {
					List<ParamValueInterface> valueList = annotation.getParamValueList(PearlParserDescription.DEFAULT_NAME_FOR_PARAM);
					if(valueList.size() != 1){
						throw new ConfidenceGenericErrorException(ruleId);
					}
					ParamValueInterface paramValue = valueList.get(0);
					if(paramValue instanceof ParamValueDouble){
						confidence = ((ParamValueDouble) paramValue).getNumber();
					} else if(paramValue instanceof ParamValuePlaceholder){
						// the confidence is not a direct double, but a placeholder, so consult the placeholder
						// map to obtain the value
						String plch = "$"+((ParamValuePlaceholder) paramValue).getPlchldName();
						Value value = row.get(plch).getArtNode();
						// now try to convert it to double
						if (value != null) { // a value was found assigned to the placeholder
							try {
								confidence = Double.parseDouble(value.stringValue());
							} catch (NumberFormatException e) {
								// placeholder does not contain double
								throw new ConfidenceNotDoubleNumException(ruleId, value.stringValue(), paramValue.getType());
							}
						} else {
							//the placeholder contains nothing
							throw new ConfidenceNotDoubleNumException(ruleId, "", paramValue.getType());
						}
					} else {
						throw new ConfidenceGenericErrorException(ruleId);
					}
				}
				//check if the values in the params in the annotations are the right one (they correspond to the one in the Annotation Definition)
				// do this only if the
				it.uniroma2.art.coda.pearl.model.annotation.Annotation annotationToAdd = checkParamValueTypeCompliant(annotation, row, ruleId);
				if(annotationToAdd != null) {
					annotationListToAddToTriple.add(annotationToAdd);
				}

			}
			//check that the confidence value is between 0.0 and 1.0
			if(confidence<0.0 || confidence > 1.0){
				throw  new ConfidenceValueNotAcceptableException(ruleId, confidence);
			}
			if (addNodeNamesInGraph) {
				suggestedCODATriple = new CODATriple(subjARTNode, predARTNode, objARTNode, annotationListToAddToTriple,
						confidence, subjGSE.getValueAsString(), predGSE.getValueAsString(), objGSE.getValueAsString());
			} else {
				suggestedCODATriple = new CODATriple(subjARTNode, predARTNode, objARTNode, annotationListToAddToTriple,
						confidence);
			}
		}
		return suggestedCODATriple;
	}

	private it.uniroma2.art.coda.pearl.model.annotation.Annotation checkParamValueTypeCompliant(
			it.uniroma2.art.coda.pearl.model.annotation.Annotation annotation, Map<String, SingleTableValue> row, String ruleId)
			throws UnsupportedTypeInParamDefinitionException, ConfidenceNotDoubleNumException, PlaceholderValueWrongTypeForParamInAnnotationException, TypeOfParamInAnnotationWrongTypeException {
		//This Annotation will contain all values from the annotation. In particular, if any of the values of a parameter is a placeholder, then the
		// value contained in that placeholder will be used
		//Another important thing is that the value will be compared with the type in the annotation definition
		it.uniroma2.art.coda.pearl.model.annotation.Annotation annotationToAdd =
				new it.uniroma2.art.coda.pearl.model.annotation.Annotation(annotation.getName(), annotation.getAnnotationDefinition());
		//add the target list
		for(String target : annotation.getTargetList()){
			annotationToAdd.addTarget(target);
		}
		for(String paramName : annotation.getParamMap().keySet()) {
			List<ParamValueInterface> valueList = new ArrayList<>();
			for(ParamValueInterface pvi : annotation.getParamMap().get(paramName)){
				if(pvi instanceof  ParamValuePlaceholder){
					String plchName = "$"+((ParamValuePlaceholder) pvi).getPlchldName();
					Value valueNode = row.get(plchName).getArtNode();
					String paramType = annotation.getAnnotationDefinition().getParamDefinition(paramName).getType();
					if(valueNode != null){
						//depending on the type of the annotation, behave accordingly
						if(paramType.toLowerCase().equals(PearlParserDescription.TYPE_IRI) || paramType.toLowerCase().equals(PearlParserDescription.TYPE_IRI_ARRAY)){
							if(valueNode instanceof  IRI) {
								valueList.add(new ParamValueIri((IRI) valueNode));
							} else {
								throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
										plchName, valueNode.stringValue());
							}
						} else if (paramType.toLowerCase().equals(PearlParserDescription.TYPE_LITERAL) || paramType.toLowerCase().equals(PearlParserDescription.TYPE_LITERAL_ARRAY)){
							if(valueNode instanceof  Literal){
								valueList.add(new ParamValueLiteral((Literal) valueNode));
							} else {
								throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
										plchName, valueNode.stringValue());
							}
						} else if(paramType.toLowerCase().equals(PearlParserDescription.TYPE_DOUBLE) || paramType.toLowerCase().equals(PearlParserDescription.TYPE_DOUBLE_ARRAY)){
							if(valueNode instanceof  Literal){
								String stringValue = valueNode.stringValue();
								try {
									double doubleValue = Double.parseDouble(stringValue);
									valueList.add(new ParamValueDouble(doubleValue));
								} catch (NumberFormatException e){
									throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
											plchName, valueNode.stringValue());
								}
							} else {
								throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
										plchName, valueNode.stringValue());
							}
						} else if(paramType.toLowerCase().equals(PearlParserDescription.TYPE_INT) || paramType.toLowerCase().equals(PearlParserDescription.TYPE_INT_ARRAY)){
							if(valueNode instanceof  Literal){
								String stringValue = valueNode.stringValue();
								try {
									int intValue = Integer.parseInt(stringValue);
									valueList.add(new ParamValueInteger(intValue));
								} catch (NumberFormatException e){
									throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
											plchName, valueNode.stringValue());
								}
							} else {
								throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
										plchName, valueNode.stringValue());
							}
						} else if(paramType.toLowerCase().equals(PearlParserDescription.TYPE_STRING) || paramType.toLowerCase().equals(PearlParserDescription.TYPE_STRING_ARRAY)){
							if(valueNode instanceof  Literal){
								String stringValue = valueNode.stringValue();
								valueList.add(new ParamValueString(stringValue));
							} else {
								throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
										plchName, valueNode.stringValue());
							}
						}
					} else {
						throw new PlaceholderValueWrongTypeForParamInAnnotationException(annotation.getName(), ruleId, paramName, paramType,
								plchName, "");
					}
				} else {
					//remove the starting " and ending ", if present
					valueList.add(pvi);
				}
			}
			//before adding the list of values of the current parameter, check that the values are compliant with the one specified in the
			// annotation definition
			String type = annotation.getAnnotationDefinition().getParamDefinition(paramName).getType();
			if(!PearlParserDescription.checkParamTypeCompliant(annotation.getName(), paramName, type, valueList)){
				if(annotation.getName().equals(PearlParserDescription.CONFIDENCE_ANN) && valueList.size() == 1){
					throw new ConfidenceNotDoubleNumException(ruleId, valueList.get(0).toString(), valueList.get(0).getType());
				}
				throw  new TypeOfParamInAnnotationWrongTypeException(annotation.getName(), ruleId, paramName, type);
			}
			annotationToAdd.addParams(paramName, valueList);
		}
		//add the default value for all the missing params (all missing params should have a default value, otherwise an exception should have been thrown before)
		for(ParamDefinition paramDefinition : annotationToAdd.getAnnotationDefinition().getParamDefinitionList()){
			//check if the param is already present in the annotationToAdd
			if(annotationToAdd.getParamValueList(paramDefinition.getName()) == null){
				//the param is missing, so use the default value
				List<ParamValueInterface> valueList = new ArrayList<>();
				valueList.add(paramDefinition.getDefaultValue());
				annotationToAdd.addParams(paramDefinition.getName(), valueList);
			}
		}

		return annotationToAdd;
	}


	private Value getValueOfGraph(Map<String, SingleTableValue> row, GraphSingleElement graphSingleElement,
			Map<String, BNode> bnodeMap) {
		Value value = null;

		if (graphSingleElement instanceof GraphSingleElemPlaceholder) {
			GraphSingleElemPlaceholder gsePlaceholder = (GraphSingleElemPlaceholder) graphSingleElement;

			if (row.containsKey(gsePlaceholder.getName())) {
				value = row.get(gsePlaceholder.getName()).getArtNode();
			}
		} else if (graphSingleElement instanceof GraphSingleElemUri) {
			GraphSingleElemUri gseUri = (GraphSingleElemUri) graphSingleElement;
			value = connection.getValueFactory().createIRI(gseUri.getURI());
		} else if (graphSingleElement instanceof GraphSingleElemVar) {
			GraphSingleElemVar gseVar = (GraphSingleElemVar) graphSingleElement;
			if (row.containsKey(gseVar.getVarId())) {
				value = row.get(gseVar.getVarId()).getArtNode();
			} else if (row.containsKey(gseVar.getVarId().replace(VAR_SYMBOL, PLACEHOLDER_SYMBOL))) {
				value = row.get(gseVar.getVarId().replace(VAR_SYMBOL, PLACEHOLDER_SYMBOL)).getArtNode();
			}
		} else if (graphSingleElement instanceof GraphSingleElemBNode) {
			GraphSingleElemBNode gseBNode = (GraphSingleElemBNode) graphSingleElement;
			if (!bnodeMap.containsKey(gseBNode.getBnodeIdentifier())) {
				BNode bNode = connection.getValueFactory().createBNode();
				bnodeMap.put(gseBNode.getBnodeIdentifier(), bNode);
			}
			value = bnodeMap.get(gseBNode.getBnodeIdentifier());
		} else if (graphSingleElement instanceof GraphSingleElemLiteral) {
			GraphSingleElemLiteral gseLiteral = (GraphSingleElemLiteral) graphSingleElement;
			value = generateRDFLiteralFromGraphSingleElemLiteral(gseLiteral);
		}
		return value;
	}

	private String constructWhereQuery(Collection<GraphElement> whereList,
			Map<String, SingleTableValue> row) {
		String whereBody = "";
		String tempString = null;
		HashMap<String, BNode> bnodeMap = new HashMap<String, BNode>();
		for (GraphElement graphElement : whereList) {
			if (graphElement instanceof GraphStruct) {
				GraphStruct graphStruct = (GraphStruct) graphElement;
				tempString = constructWhereQueryTriplePart(graphStruct, row, bnodeMap);
				if (tempString == null)
					return null;
				whereBody += tempString;
			} else { // graphElement instanceof OptionalGraphStruct
				OptionalGraphStruct optionaGraphStruct = (OptionalGraphStruct) graphElement;

				whereBody += constructWhereOptional(optionaGraphStruct, row, bnodeMap, 0);

				// old from here
				/*
				 * whereBody = "\nOPTIONAL { "; for (GraphStruct graphStruct :
				 * optionaGraphStruct.getOptionalTriples()) { tempString =
				 * constructWhereQueryTriplePart(graphStruct, row, bnodeMap); if (tempString == null) return
				 * null; whereBody += tempString; }
				 * 
				 * whereBody += "\n} ";
				 */
				// to here
			}
		}
		if (whereBody.length() > 0) {
			return whereBody;
		} else {
			return null;
		}
	}

	private String constructWhereOptional(OptionalGraphStruct optionaGraphStruct,
			Map<String, SingleTableValue> row, HashMap<String, BNode> bnodeMap, int indent) {
		String indentString = "";
		for (int i = 0; i < indent; ++i) {
			indentString += "\t";
		}

		String optionalPart = "\n" + indentString + "OPTIONAL { ";

		Collection<GraphElement> optionalTripleList = optionaGraphStruct.getOptionalTriples();
		for (GraphElement graphElement : optionalTripleList) {
			if (graphElement instanceof GraphStruct) {
				GraphStruct graphStruct = (GraphStruct) graphElement;
				String tempString = constructWhereQueryTriplePart(graphStruct, row, bnodeMap);
				if (tempString == null) {
					return null; // TODO an error has occurred
				}
				optionalPart += "\t" + indentString + tempString;
			} else { // graphElement instanceof OptionalGraphStruct
				OptionalGraphStruct innerOptionalGraphStruct = (OptionalGraphStruct) graphElement;
				String tempString = constructWhereOptional(innerOptionalGraphStruct, row, bnodeMap,
						indent + 1);
				if (tempString == null) {
					return null; // TODO an error has occurred
				}
				optionalPart += tempString;
			}
		}

		optionalPart += "\n" + indentString + "} ";

		return optionalPart;
	}

	private String constructWhereQueryTriplePart(GraphStruct graphStruct, Map<String, SingleTableValue> row,
			HashMap<String, BNode> bnodeMap) {
		String query = "\n";
		String tempString;

		tempString = constructWhereQuerySingleElemOfTriplePart(graphStruct.getSubject(), row, bnodeMap);
		if (tempString == null)
			return null;
		query += tempString + " ";

		tempString = constructWhereQuerySingleElemOfTriplePart(graphStruct.getPredicate(), row, bnodeMap);
		if (tempString == null)
			return null;
		query += tempString + " ";

		tempString = constructWhereQuerySingleElemOfTriplePart(graphStruct.getObject(), row, bnodeMap);
		if (tempString == null)
			return null;
		query += tempString + " .";

		return query;
	}

	private String constructWhereQuerySingleElemOfTriplePart(GraphSingleElement graphSingleElement,
			Map<String, SingleTableValue> row, HashMap<String, BNode> bnodeMap) {
		String query = null;

		if (graphSingleElement instanceof GraphSingleElemPlaceholder) {
			GraphSingleElemPlaceholder gsePlaceholder = (GraphSingleElemPlaceholder) graphSingleElement;
			if (row.containsKey(gsePlaceholder.getName())) {
				Value artNode = row.get(gsePlaceholder.getName()).getArtNode();
				if (artNode == null)
					return null;
				query = getValueForWhere(artNode);

			}
		} else if (graphSingleElement instanceof GraphSingleElemUri) {
			GraphSingleElemUri gseUri = (GraphSingleElemUri) graphSingleElement;
			Value value = connection.getValueFactory().createIRI(gseUri.getURI());
			if (value == null)
				return null;
			query = getValueForWhere(value);
		} else if (graphSingleElement instanceof GraphSingleElemPropPath) {
			GraphSingleElemPropPath gsePropPath = (GraphSingleElemPropPath) graphSingleElement;
			query = gsePropPath.getValueAsString();
		} else if (graphSingleElement instanceof GraphSingleElemVar) {
			GraphSingleElemVar gseVar = (GraphSingleElemVar) graphSingleElement;
			query = gseVar.getVarId();
		} else if (graphSingleElement instanceof GraphSingleElemBNode) {
			GraphSingleElemBNode gseBNode = (GraphSingleElemBNode) graphSingleElement;
			if (!bnodeMap.containsKey(gseBNode.getBnodeIdentifier())) {
				BNode bnode = connection.getValueFactory().createBNode();
				bnodeMap.put(gseBNode.getBnodeIdentifier(), bnode);
			}
			Value artNode = bnodeMap.get(gseBNode.getBnodeIdentifier());
			query = getValueForWhere(artNode);
		} else if (graphSingleElement instanceof GraphSingleElemLiteral) {
			GraphSingleElemLiteral gseLiteral = (GraphSingleElemLiteral) graphSingleElement;
			// TODO deal with the language and datatype
			Value artNode = generateRDFLiteralFromGraphSingleElemLiteral(gseLiteral);
			if (artNode == null)
				return null;
			query = getValueForWhere(artNode);
		}
		return query;
	}

	private String getValueForWhere(Value value) {
		String valueStrng = null;
		if (value instanceof IRI)
			valueStrng = "<" + value.stringValue() + ">";
		else if (value instanceof Literal) {
			Literal artLiteral = (Literal) value;
			valueStrng = "\"" + artLiteral.getLabel() + "\"";
			if (artLiteral.getLanguage().isPresent())
				valueStrng += "@" + artLiteral.getLanguage().get();
			else if (artLiteral.getDatatype() != null)
				valueStrng += "^^<" + artLiteral.getDatatype().stringValue()+">";
		} else if (value instanceof BNode)
			valueStrng = "_:" + value.stringValue();
		return valueStrng;
	}

	private void removeDuplicate(List<CODATriple> suggestedTripleWithDuplicated,
			List<CODATriple> suggestedTriple) {
		List<String> uniqueTriple = new ArrayList<String>();
		for (CODATriple codaTriple : suggestedTripleWithDuplicated) {
			StringBuilder stringBuilder = new StringBuilder();
			Resource subj = codaTriple.getSubject();
			IRI pred = codaTriple.getPredicate();
			Value obj = codaTriple.getObject();
			stringBuilder.append(subj.stringValue());
			stringBuilder.append("|_|");
			stringBuilder.append(pred.stringValue());
			stringBuilder.append("|_|");
			if (obj instanceof Literal) {
				Literal objLiteral = (Literal) obj;
				stringBuilder.append(objLiteral.getLabel());
				if (objLiteral.getLanguage().isPresent())
					stringBuilder.append("@" + objLiteral.getLanguage());
				if (objLiteral.getDatatype() != null)
					stringBuilder.append("^^" + objLiteral.getDatatype().stringValue());
			} else { // obj is a resource
				stringBuilder.append(obj.stringValue());
			}

			if (!uniqueTriple.contains(stringBuilder.toString())) {
				uniqueTriple.add(stringBuilder.toString());
				suggestedTriple.add(codaTriple);
			}
		}
	}

	private Literal generateRDFLiteralFromGraphSingleElemLiteral(GraphSingleElemLiteral gseLiteral) {
		Literal literal = null;
		String completeLiteral = gseLiteral.getStringValue();
		if (completeLiteral.contains("\"@")) {
			// the literal contains a language
			String label = completeLiteral.substring(1).split("\"@")[0];
			String language = completeLiteral.split("\"@")[1];
			literal = connection.getValueFactory().createLiteral(label, language);
		} else if (completeLiteral.contains("\"^^")) {
			// the literal contanins a datatype
			String label = completeLiteral.substring(1).split("\"\\^\\^")[0];
			IRI datatype = connection.getValueFactory().createIRI(completeLiteral.split("\"\\^\\^")[1]);
			literal = connection.getValueFactory().createLiteral(label, datatype);
		} else {
			String label = completeLiteral.substring(1, completeLiteral.length() - 1);
			literal = connection.getValueFactory().createLiteral(label);
		}
		return literal;
	}

	private void executeRegex(RegexProjectionRule rpr, JCas jcas, SuggOntologyCoda suggOntCoda,
			FSIterator<Annotation> iterAnnotation, Annotation currentAnnotation, boolean useSuperTypes,
		  	boolean addNodeNamesInGraph) throws ComponentProvisioningException, ConverterException {

		// try to see if, by starting from the current annotation, you are able to arrive to a final state, so
		// if, using the current RegexProjectionRule you are able to use different successions of Annotations,
		// take the path in which the last annotation has the biggest begin (if the begin value is the same,
		// consider the end value). If both the begin and the end value are the same, consider the one which
		// uses the more annotations
		// TODO decide if what just said is the correct way to do it or not

		List<StateFSA> startStateList = rpr.getSingleRegexStruct().getDfsa().getStartStateList();
		List<RegexResults> regexResultsList = new ArrayList<RegexResults>();
		// for every start state (which could be only one, but this algorithm is able to deal with more than
		// one starting state ) call executeDFSA
		for (StateFSA startState : startStateList) {
			executeDFSA(rpr, startState, -1, true, iterAnnotation, currentAnnotation, useSuperTypes,
					new RegexResults(), regexResultsList);
		}

		// check if it was found at least one regexResults
		if (regexResultsList.size() == 0) {
			// no regexResults where found in this case
			return;
		}

		if (debugMode) {
			printAllRegexElements(regexResultsList);
		}

		// select the RegexResults, which uses the annotation that ends far away from the starting annotation,
		// if more that one RegexResults has this feature, then consider the one that has the biggest number
		// of Annotations

		// first get the max value for lastAnnEnd
		int maxEnd = 0;
		for (RegexResults tempRegexResults : regexResultsList) {
			if (maxEnd < tempRegexResults.getLastAnnEnd()) {
				maxEnd = tempRegexResults.getLastAnnEnd();
			}
		}
		// now get all the RegexResults that have that value
		List<RegexResults> candidateRegexResultsList = new ArrayList<RegexResults>();
		for (RegexResults tempRegexResults : regexResultsList) {
			if (tempRegexResults.getLastAnnEnd() == maxEnd) {
				candidateRegexResultsList.add(tempRegexResults);
			}
		}

		// now iterate over this candidateRegexResultsList to get the max value for annUsed
		int maxAnnUsed = 0;
		for (RegexResults tempRegexResults : candidateRegexResultsList) {
			if (maxAnnUsed < tempRegexResults.getNumberOfAnn()) {
				maxAnnUsed = tempRegexResults.getNumberOfAnn();
			}
		}
		// now iterate over this candidateRegexResultsList to get all the RegexResults which have annUsed
		// equals to maxAnnUsed
		List<RegexResults> finalCandidateRegexResultsList = new ArrayList<RegexResults>();
		for (RegexResults tempRegexResults : candidateRegexResultsList) {
			if (tempRegexResults.getNumberOfAnn() == maxAnnUsed) {
				finalCandidateRegexResultsList.add(tempRegexResults);
			}
		}

		// for the moment consider just the first element of the candidateRegexResultsList
		// TODO decide if it is the case to consider also other element in the candidateRegexResultsList
		RegexResults regexResults = candidateRegexResultsList.get(0);
		generateTriplesFromRegex(rpr, regexResults, jcas, suggOntCoda, addNodeNamesInGraph);

	}

	private void executeDFSA(RegexProjectionRule rpr, StateFSA currentState, int endLastUsedAnn,
			boolean firstRun, FSIterator<Annotation> currentIterAnnotation, Annotation currentAnnotation,
			boolean useSuperTypes, RegexResults regexResults, List<RegexResults> regexResultsList) {

		// clone the iterator for the annotations, so you can use the cloned one, without modifying
		// the original ones
		FSIterator<Annotation> clonedIterAnnotation = currentIterAnnotation.copy();

		String annotationTypeName = currentAnnotation.getType().getName();

		// check if the current annotation start after the last used annotation end (only if it is not
		// the first run)
		if (!firstRun && (endLastUsedAnn > currentAnnotation.getBegin())) {
			return;
		}

		// get all the possible transition (identified by their internal id) from the current state
		List<String> internalIdList = currentState.getAllTransitionInternalId();

		// this two boolean variable are used to avoid searching for a possible next annotation if all the
		// possible outgoing transitions have a maxDist and we have reached a too far annotation
		boolean onlyMaxDistTransition = true;
		boolean tooFarAnnotation = true;

		for (String internalId : internalIdList) {
			// get the uimaType associated to this transition
			String uimaType = prModel.getUimaTypeFromInternalId(rpr, internalId);

			// Do the maxDist check only if is it not the first run
			if (!firstRun) {
				// check if the associated transition to this internalId (in the regex definition all the
				// internalId should be different) has a maxDistance specified
				TransitionFSA tranition = rpr.getSingleRegexStruct().getDfsa()
						.getTransitionFromInternalId(internalId);
				// do the check only if it is not he first annnotation we are dealing wih (so
				// endLastUsedAnn>0)
				if (endLastUsedAnn > 0 && tranition.isMaxDistancePresent()) {
					int beginAnn = currentAnnotation.getBegin();
					if (endLastUsedAnn + tranition.getMaxDistance() < beginAnn) {
						// the current annotation is too distant from the last considered annotation
						continue;
					} else {
						tooFarAnnotation = false;
					}
				} else {
					onlyMaxDistTransition = false;
				}
			}

			if (uimaType == null) {
				// TODO this should never happen
				continue;
			}
			StateFSA nextState = null;

			if (annotationTypeName.compareTo(uimaType) == 0) {
				// the current annotation can be used to perform a transition from the current stateFSA
				nextState = currentState.getStateForTransition(internalId);
				if (nextState == null) {
					// TODO this should never happen
					continue;
				}
			} else if (useSuperTypes) {
				// do this checking only if the current annotation cannot be used directly to perform
				// a transition
				List<String> superTypesList = UIMACODAUtilities.getSuperTypes(currentAnnotation, null);
				for (String superUimaType : superTypesList) {
					if (superUimaType.compareTo(uimaType) == 0) {
						// the current annotation can be used to perform a transition from the current
						// stateFSA
						nextState = currentState.getStateForTransition(internalId);
						if (nextState == null) {
							// TODO this should never happen
							continue;
						}
					}
				}
			}

			// if the current annotation does not satisfy the condition inside the transition (a transition
			// is possible ) then the transition is not possible
			if (nextState != null) {
				String forRegexRuleId = rpr.getSingleRegexStruct().geteRuleIdFromInternalId(internalId);
				ProjectionRule forRegexRule = prModel.getProjRuleFromId(forRegexRuleId);
				if (!forRegexRule.isForRegex()) {
					continue; // this should never happen
				}
				if (!forRegexRule.evaluateConditions(currentAnnotation)) {
					continue;
				}

			}

			// check if it was found a possible next state
			if (nextState == null) {
				continue;
			}

			// copy RegexResults to avoid changing the original one
			RegexResults newRegexResults = regexResults.copy();
			// add the currentAnnotation to the copied RegexResults
			// (since by using it, it was possible a transition to a new state)
			newRegexResults.addInternalIdRuleIdAnnotation(internalId, currentAnnotation);

			// check if the nextState is an endOne
			if (nextState.isEndState()) {
				// the current state is an end state, so add the current regexResults to the list of valid
				// RegexResults
				regexResultsList.add(newRegexResults);
			}

			// since a nextState was found, recursive call executeDFSA on this new state (by changing
			// the parameters accordingly)
			FSIterator<Annotation> cloneOfACloneIterAnn = clonedIterAnnotation.copy();
			// check if there is another annotation
			while (cloneOfACloneIterAnn.hasNext()) {
				Annotation nextAnnotation = cloneOfACloneIterAnn.next();
				executeDFSA(rpr, nextState, currentAnnotation.getEnd(), false, cloneOfACloneIterAnn,
						nextAnnotation, useSuperTypes, newRegexResults, regexResultsList);
			}
		}

		if (!firstRun && onlyMaxDistTransition && tooFarAnnotation) {
			// we are not in the first run and
			// we have reach an annotation which is too far and all the possible transition specify a maxDist
			// which is lower than the distance we have reached, so stop searching
			return;
		}

		// call executeDFSA on the same input state, but using the next annotation, only if it is not the
		// first run
		FSIterator<Annotation> cloneOfACloneIterAnn = clonedIterAnnotation.copy();
		if (cloneOfACloneIterAnn.hasNext() && !firstRun) {
			// check if there is another annotation
			Annotation nextAnnotation = cloneOfACloneIterAnn.next();
			executeDFSA(rpr, currentState, endLastUsedAnn, firstRun, cloneOfACloneIterAnn, nextAnnotation,
					useSuperTypes, regexResults, regexResultsList);
		}

	}

	private void generateTriplesFromRegex(RegexProjectionRule rpr, RegexResults regexResults, JCas jcas,
			SuggOntologyCoda suggOntCoda, boolean addNodeNamesInGraph)
			throws ComponentProvisioningException, ConverterException {

		List<CODATriple> suggestedTripleWithDuplicated = new ArrayList<CODATriple>();
		List<CODATriple> suggestedTriple = new ArrayList<CODATriple>();

		// in this first implementation a simple approach is follow: for every template triples, each element
		// is considered, using all the possible values (if it is derived from a placeholder resolve
		// EVERYTIME the value associated to the placeholder to obtain a Value). This approach is not
		// efficient nor it can allow for complex thing, but it is only a first implementation

		// In this implementation consider in the same way the placeholder with '.' and the ones with '..'

		// TODO do a better implementation

		// get the graph part for this RegexProjectionRule
		Collection<GraphElement> graphElemList = rpr.getGraphList();

		boolean graphFail = false;

		for (GraphElement ge : graphElemList) {

			if (graphFail) {
				// at least one mandatory triple was not generated, so no triple should be generated from this
				// regex, so exit the for and stop trying to execute the graph section
				break;
			}

			if (ge instanceof OptionalGraphStruct) {
				OptionalGraphStruct ogs = (OptionalGraphStruct) ge;
				suggestedTripleWithDuplicated
						.addAll(generateTriplesFromOptionalInRegex(rpr, ogs, regexResults, addNodeNamesInGraph));
			} else {
				// it is an instance of GraphStruct
				GraphStruct gs = (GraphStruct) ge;
				GraphSingleElement subjGSE = gs.getSubject();
				GraphSingleElement predGSE = gs.getPredicate();
				GraphSingleElement objGSE = gs.getObject();

				List<Value> subjList = generateRDFNodeFromGraphSingleElementForRegex(subjGSE, rpr,
						regexResults);
				List<Value> predList = generateRDFNodeFromGraphSingleElementForRegex(predGSE, rpr,
						regexResults);
				List<Value> objList = generateRDFNodeFromGraphSingleElementForRegex(objGSE, rpr, regexResults);

				if (subjList == null || predList == null || objList == null) {
					// at least one placeholder did not produce a value, so this graph failed
					// graphFail = true;
					continue;
				}

				for (Value subj : subjList) {
					for (Value pred : predList) {
						for (Value obj : objList) {
							if (subj instanceof Resource && pred instanceof IRI) {
								CODATriple codaTriple;
								if (addNodeNamesInGraph) {
									codaTriple = new CODATriple((Resource) subj, (IRI) pred, obj, 1.0,
											subjGSE.getValueAsString(), predGSE.getValueAsString(), objGSE.getValueAsString());
								} else {
									codaTriple = new CODATriple((Resource) subj, (IRI) pred, obj, 1.0);
								}
								suggestedTripleWithDuplicated.add(codaTriple);
							} else {
								// This should never happen, the template was written using the wrong element
							}

						}
					}
				}
			}
		}
		if (graphFail) {
			// at least one mandatory triple was not generated, so no triple should be generated from this
			// regex
			suggestedTripleWithDuplicated.clear();
		}

		// Remove the duplicate RDF triple from the suggested ones
		removeDuplicate(suggestedTripleWithDuplicated, suggestedTriple);
		// Add the suggestedTriple to the output structure
		suggOntCoda.addProjectionRule(rpr);
		suggOntCoda.addSuggestedInsertTripleList(rpr.getId(), suggestedTriple);

	}

	private List<CODATriple> generateTriplesFromOptionalInRegex(RegexProjectionRule rpr,
			OptionalGraphStruct ogs, RegexResults regexResults, boolean addNodeNamesInGraph)
			throws ComponentProvisioningException, ConverterException {
		List<CODATriple> suggestedTripleWithDuplicated = new ArrayList<CODATriple>();
		boolean graphFail = false;
		for (GraphElement ge : ogs.getOptionalTriples()) {

			if (ge instanceof OptionalGraphStruct) {

				OptionalGraphStruct ogsInner = (OptionalGraphStruct) ge;

				suggestedTripleWithDuplicated
						.addAll(generateTriplesFromOptionalInRegex(rpr, ogsInner, regexResults, addNodeNamesInGraph));

				// in this implementation do nothing
				// continue;
			} else {
				// it is an instance of GraphStruct
				GraphStruct gs = (GraphStruct) ge;
				GraphSingleElement subjGSE = gs.getSubject();
				GraphSingleElement predGSE = gs.getPredicate();
				GraphSingleElement objGSE = gs.getObject();

				List<Value> subjList = generateRDFNodeFromGraphSingleElementForRegex(subjGSE, rpr,
						regexResults);
				List<Value> predList = generateRDFNodeFromGraphSingleElementForRegex(predGSE, rpr,
						regexResults);
				List<Value> objList = generateRDFNodeFromGraphSingleElementForRegex(objGSE, rpr, regexResults);

				if (subjList == null || predList == null || objList == null) {
					graphFail = true;
					continue;
				}

				for (Value subj : subjList) {
					for (Value pred : predList) {
						for (Value obj : objList) {
							if (subj instanceof Resource && pred instanceof IRI) {
								CODATriple codaTriple;
								if (addNodeNamesInGraph) {
									codaTriple = new CODATriple((Resource) subj, (IRI) pred, obj, 1.0,
											subjGSE.getValueAsString(), predGSE.getValueAsString(), objGSE.getValueAsString());
								} else {
									codaTriple = new CODATriple((Resource) subj, (IRI) pred, obj, 1.0);
								}
								suggestedTripleWithDuplicated.add(codaTriple);
							} else {
								// This should never happen, the template was written using the wrong element
							}

						}
					}
				}
			}
		}
		if (graphFail) {
			suggestedTripleWithDuplicated.clear();
		}
		return suggestedTripleWithDuplicated;

	}

	private List<Value> generateRDFNodeFromGraphSingleElementForRegex(GraphSingleElement gse,
			RegexProjectionRule rpr, RegexResults regexResults)
			throws ComponentProvisioningException, ConverterException {

		List<Value> valueList = new ArrayList<Value>();

		if (gse instanceof GraphSingleElemVar) {
			// In this implementation the variables are ignored
			return null;
		} else if (gse instanceof GraphSingleElemUri) {
			GraphSingleElemUri gseUri = (GraphSingleElemUri) gse;
			valueList.add(connection.getValueFactory().createIRI(gseUri.getURI()));
		} else if (gse instanceof GraphSingleElemBNode) {
			// In this implementation the bnodes are ignored
			return null;
		} else if (gse instanceof GraphSingleElemLiteral) {
			GraphSingleElemLiteral gseLiteral = (GraphSingleElemLiteral) gse;
			valueList.add(generateRDFLiteralFromGraphSingleElemLiteral(gseLiteral));
		} else if (gse instanceof GraphSingleElemPlaceholder) {
			GraphSingleElemPlaceholder gsePlaceholder = (GraphSingleElemPlaceholder) gse;
			// only placeholder in the form of INTERNALID.PLCHLD and INTERNALID..PLCHLD are considered
			String plchldName = gsePlaceholder.getName();

			if (!plchldName.contains(".")) {
				return null;
			}
			String[] plchldNameArray = plchldName.split("\\.");

			String internalId = plchldNameArray[0].substring(1);
			String forRegexPlchldName;
			if (plchldNameArray.length == 2) { // it is in the form INTERNALID.PLCHLD
				forRegexPlchldName = plchldNameArray[1];
			} else {// it is in the form INTERNALID..PLCHLD
				forRegexPlchldName = plchldNameArray[2];
			}

			// get the ruleId associated to this internalId
			String ruleId = rpr.getSingleRegexStruct().geteRuleIdFromInternalId(internalId);
			if (ruleId == null) {
				return null;
			}

			ProjectionRule prForRegex = prModel.getProjRuleFromId(ruleId);
			if (prForRegex == null || !prForRegex.isForRegex()) {
				return null;
			}

			// get the complete feature path (uimaType+localFeaturePath) from the selected forRegexRule
			PlaceholderStruct plchldStruct = prForRegex.getPlaceholderMap().get(forRegexPlchldName);
			if (plchldStruct == null) {
				return null;
			}

			String uimaTypeAndFeat = prForRegex.getUIMAType() + ":" + plchldStruct.getFeaturePath();

			// get from RegexProjectionRule the selected annotation associated with the desired internalId
			List<Annotation> annList = regexResults.getAnnotationListFromInternalId(internalId);
			// since annList could be null (for example when dealing with an OR, only one of the two is used)
			// check it and if it is null, then return null
			if (annList == null) {
				return null;
			}
			for (Annotation annotation : annList) {
				List<StringOrFeatureStruct> valuesList = UIMACODAUtilities
						.getValuesOfFeatureFromFeatPath(annotation, uimaTypeAndFeat);
				for (StringOrFeatureStruct stringOrFeatureStruct : valuesList) {
					valueList.add(UIMACODAUtilities.getRDFValueFromUIMAValue(plchldStruct,
							stringOrFeatureStruct, componentProvider, ctx));
				}
			}
		}

		return valueList;
	}

	// used only in debug
	private void printAllRegexElements(List<RegexResults> regexResultsList) {
		int count = 0;
		for (RegexResults regexResults : regexResultsList) {
			System.out.println("AnnotationList " + (++count));
			List<Annotation> annList = regexResults.getAllAnnotationsMatched();
			for (Annotation ann : annList) {
				System.out.println(
						"\t" + ann.getCoveredText() + " (" + ann.getBegin() + ", " + ann.getEnd() + ")");
			}
		}
	}

	/**
	 * Executes a converter to generate a literal from an String value. If the provided value is
	 * <code>null</code>, it is passed as-is to the converter: it is a responsibility of the client to assure
	 * that the converter is compatible with <code>null</code> values, or to avoid the invocation of this
	 * operation with a <code>null</code> value. Either <code>datatype</code> or <code>lang</code> should be
	 * not <code>null</code>.
	 * 
	 * @param converter
	 *            a converter mention, possibly including additional arguments
	 * @param value
	 *            a converter mention, possibly including additional arguments
	 * @param datatype
	 * @param lang
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public Value executeLiteralConverter(String converter, String value, String datatype, String lang)
			throws ComponentProvisioningException, ConverterException {
		return executeLiteralConverter(new ConverterMention(converter), value, datatype, lang);
	}

	/**
	 * Executes a converter to generate a literal from a <code>null</code> value. It is a responsibility of
	 * the client to assure that the converter is compatible with <code>null</code> values. Either
	 * <code>datatype</code> or <code>lang</code> should be not <code>null</code>.
	 * 
	 * @param converterMention
	 *            a converter mention, possibly including additional arguments
	 * @param datatype
	 * @param lang
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public Value executeLiteralConverter(ConverterMention converterMention, String datatype, String lang)
			throws ComponentProvisioningException, ConverterException {
		return UIMACODAUtilities.getRDFValuesFromUIMAValue(false, null, "literal", datatype, lang,
				Arrays.asList(converterMention), componentProvider, ctx, null, memoizedProjectionsMaps, null)
				.stream().findAny().orElse(null);
	}

	/**
	 * Executes a converter to generate a literal from an Object value. If the provided value is
	 * <code>null</code>, it is passed as-is to the converter: it is a responsibility of the client to assure
	 * that the converter is compatible with <code>null</code> values, or to avoid the invocation of this
	 * operation with a <code>null</code> value. Either <code>datatype</code> or <code>lang</code> should be
	 * not <code>null</code>.
	 * 
	 * @param converterMention
	 *            a converter mention, possibly including additional arguments
	 * @param inputValue
	 *            a converter mention, possibly including additional arguments
	 * @param datatype
	 * @param lang
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public Value executeLiteralConverter(ConverterMention converterMention, String inputValue,
			String datatype, String lang) throws ComponentProvisioningException, ConverterException {
		return UIMACODAUtilities.getRDFValuesFromUIMAValue(true, inputValue, "literal", datatype, lang,
				Arrays.asList(converterMention), componentProvider, ctx, null, memoizedProjectionsMaps, null)
				.stream().findAny().orElse(null);
	}

	/**
	 * Executes a converter to generate a URI from a String value. If the provided value is <code>null</code>,
	 * it is passed as-is to the converter: it is a responsibility of the client to assure that the converter
	 * is compatible with <code>null</code> values, or to avoid the invocation of this operation with a
	 * <code>null</code> value.
	 * 
	 * @param converter
	 *            the identifier of a converter (or an implemented contract)
	 * @param inputValue
	 *            the input value
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public Value executeURIConverter(String converter, String inputValue)
			throws ComponentProvisioningException, ConverterException {
		return executeURIConverter(new ConverterMention(converter), inputValue);
	}

	/**
	 * Executes a converter to generate a URI from a <code>null</code> value. It is a responsibility of the
	 * client to assure that the converter is compatible with <code>null</code> values.
	 * 
	 * @param converter
	 *            the identifier of a converter (or an implemented contract)
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public Value executeURIConverter(String converter)
			throws ComponentProvisioningException, ConverterException {
		return executeURIConverter(new ConverterMention(converter));
	}

	/**
	 * Executes a converter to generate a URI from a <code>null</code>. It is a responsibility of the client
	 * to assure that the converter is compatible with <code>null</code> values.
	 * 
	 * @param converterMention
	 *            a converter mention, possibly including additional arguments
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public IRI executeURIConverter(ConverterMention converterMention)
			throws ComponentProvisioningException, ConverterException {
		return (IRI) UIMACODAUtilities
				.getRDFValuesFromUIMAValue(false, null, "uri", null, null, Arrays.asList(converterMention),
						componentProvider, ctx, null, memoizedProjectionsMaps, null)
				.stream().findAny().orElse(null);
	}

	/**
	 * Executes a converter to generate a URI from an Object value. If the provided value is <code>null</code>
	 * , it is passed as-is to the converter: it is a responsibility of the client to assure that the
	 * converter is compatible with <code>null</code> values, or to avoid the invocation of this operation
	 * with a <code>null</code> value.
	 * 
	 * @param converterMention
	 *            a converter mention, possibly including additional arguments
	 * @param inputValue
	 *            the input value
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public IRI executeURIConverter(ConverterMention converterMention, String inputValue)
			throws ComponentProvisioningException, ConverterException {
		return (IRI) UIMACODAUtilities.getRDFValuesFromUIMAValue(true, inputValue, "uri", null, null,
				Arrays.asList(converterMention), componentProvider, ctx, null, memoizedProjectionsMaps, null)
				.stream().findAny().orElse(null);
	}

	/**
	 * Executes a projection operator to produce an RDF node (either a URI or a literal, depending the type of
	 * projection operator). If the provided value is <code>null</code> , it is passed as-is to the converter:
	 * it is a responsibility of the client to assure that the converter is compatible with <code>null</code>
	 * values, or to avoid the invocation of this operation with a <code>null</code> value.
	 * 
	 * @param projectionOperator
	 *            a projection operator, including an node type, optional information specific to a given
	 *            type, and a list of conveter mentions
	 * @param inputValue
	 *            the input value
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public Value executeProjectionOperator(ProjectionOperator projectionOperator, String inputValue)
			throws ComponentProvisioningException, ConverterException {
		if (projectionOperator.getNodeType() == ProjectionOperator.NodeType.uri) {
			return UIMACODAUtilities.getRDFValuesFromUIMAValue(true, inputValue, "uri", null, null,
					projectionOperator.getConverterMentions(), componentProvider, ctx, null,
					memoizedProjectionsMaps, null)
					.stream().findAny().orElse(null);
		} else {
			return UIMACODAUtilities.getRDFValuesFromUIMAValue(true, inputValue, "literal",
					projectionOperator.getDatatype().orElse(null),
					projectionOperator.getLanguage().orElse(null), projectionOperator.getConverterMentions(),
					componentProvider, ctx, null, memoizedProjectionsMaps, null)
					.stream().findAny().orElse(null);
		}
	}

	/**
	 * Executes a projection operator to produce an RDF node (either a URI or a literal, depending the type of
	 * projection operator). The input value is <code>null</code>: it is a responsibility of the client to
	 * assure that the converter is compatible with <code>null</code> values, or to avoid the invocation of
	 * this operation with a <code>null</code> value.
	 * 
	 * @param projectionOperator
	 *            a projection operator, including an node type, optional information specific to a given
	 *            type, and a list of conveter mentions
	 * @return
	 * @throws ComponentProvisioningException
	 * @throws ConverterException
	 */
	public Value executeProjectionOperator(ProjectionOperator projectionOperator)
			throws ComponentProvisioningException, ConverterException {
		if (projectionOperator.getNodeType() == ProjectionOperator.NodeType.uri) {
			return UIMACODAUtilities.getRDFValuesFromUIMAValue(false, null, "uri", null, null,
					projectionOperator.getConverterMentions(), componentProvider, ctx, null,
					memoizedProjectionsMaps, null)
					.stream().findAny().orElse(null);
		} else {
			return UIMACODAUtilities.getRDFValuesFromUIMAValue(false, null, "literal",
					projectionOperator.getDatatype().orElse(null),
					projectionOperator.getLanguage().orElse(null), projectionOperator.getConverterMentions(),
					componentProvider, ctx, null, memoizedProjectionsMaps, null)
					.stream().findAny().orElse(null);
		}
	}

	/**
	 * Parses a converter mention. The <i>textual converter mention</i> can only include constant argument
	 * expressions (see {@link ConverterArgumentExpression#isConstant()}); therefore, placeholders can not be
	 * used as argument expressions. The prefix mapping should be not <code>null</code>; however,
	 * {@link Collections#emptyMap()} can be used to create an empty map when no prefix should be used. Given
	 * a prefixMapping, a map suitable as an argument to this
	 * method can be obtained via {@link PrefixMapping#getNamespacePrefixMapping()}
	 * 
	 * @param textualConverterMention
	 * @param prefixMapping
	 * @return
	 * @throws PRParserException
	 */
	public ConverterMention parseConverterMention(String textualConverterMention,
			Map<String, String> prefixMapping) throws PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv4
		parser.initialize(this);
		return parser.parseConverterMention(textualConverterMention, prefixMapping);
	}

	/**
	 * Parses a projection operator. A projection operator is made of a node type (i.e. <code>uri</code> or
	 * <code>literal</code>), optionally followed by node specific information (e.g. language tag or
	 * datatype), and a list of converter mentions.
	 * <p>
	 * The <i>textual projection operator</i> can only include constant argument expressions (see
	 * {@link ConverterArgumentExpression#isConstant()}); therefore, placeholders can not be used as argument
	 * expressions. The prefix mapping should be not <code>null</code>; however,
	 * {@link Collections#emptyMap()} can be used to create an empty map when no prefix should be used. Given
	 * a prefixMapping, a map suitable as an argument to this
	 * method can be obtained via {@link PrefixMapping#getNamespacePrefixMapping()}
	 * 
	 * @param textualProjectionOperator
	 * @param prefixMapping
	 * @return
	 * @throws PRParserException
	 */
	public ProjectionOperator parseProjectionOperator(String textualProjectionOperator,
			Map<String, String> prefixMapping) throws PRParserException {
		// ParserPR parser = new PearlParser("ANTLRParserPR", "PEARL Parser"); // old parser using ANTLRv3
		ParserPR parser = new PearlParserAntlr4("ANTLRParserPR", "PEARL Parser"); // new parser using ANTLRv3
		parser.initialize(this);
		return parser.parseProjectionOperator(textualProjectionOperator.trim(), prefixMapping);
	}
}
