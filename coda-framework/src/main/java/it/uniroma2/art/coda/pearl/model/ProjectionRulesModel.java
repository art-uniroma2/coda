package it.uniroma2.art.coda.pearl.model;

import it.uniroma2.art.coda.exception.parserexception.DuplicateRuleIdException;
import it.uniroma2.art.coda.pearl.model.annotation.Annotation;
import it.uniroma2.art.coda.pearl.model.annotation.AnnotationDefinition;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotation;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotationGeneric;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotationTarget;
import it.uniroma2.art.coda.pearl.model.annotation.ParamDefinition;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueIri;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.FSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.SingleRegexStruct;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.StateFSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.TransitionFSA;
import it.uniroma2.art.coda.structures.DependsOnInfo;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ProjectionRulesModel {
	private static Logger logger = LoggerFactory.getLogger(ProjectionRulesModel.class);

	//a map containing the association from prefix to namespace used during the process phase (useful
	// when the PEARL is generated from the model)
	private Map<String, String> prefixToNamespaceMap;

	// a map containing just standard Projection Rules, ordered by their UIMA type
	private Map<String, Collection<ProjectionRule>> typeNameToStandardRulesMap;
	// a map containing ALL the Projection Rule (standard, lazy and forRegex)
	private Map<String, ProjectionRule> idToRuleMap;
	// a map containing the regexes
	private Map<String, RegexProjectionRule> idToRegexRuleMap;
	// Added by Gambella Riccardo
	private List<AnnotationDefinition> annotationDefinitionList;
	//a list of used prefix in the graph section
	private List<String> usedPrefixesList;
	
	public ProjectionRulesModel() {
		initialize();
	}

	public void initialize() {
		prefixToNamespaceMap = new HashMap<>();
		typeNameToStandardRulesMap = new HashMap<>();
		idToRuleMap = new HashMap<>();
		idToRegexRuleMap = new HashMap<>();
		usedPrefixesList = new ArrayList<>();
		// Added by Gambella Riccardo
		this.annotationDefinitionList = new ArrayList<>();
		
	}

	public Map<String, String> getPrefixToNamespaceMap() {
		return prefixToNamespaceMap;
	}

	public void setPrefixToNamespaceMap(Map<String, String> prefixToNamespaceMap) {
		//prepare a copy of the input map
		for(String prefix : prefixToNamespaceMap.keySet()){
			this.prefixToNamespaceMap.put(prefix, prefixToNamespaceMap.get(prefix));
		}
	}

	/**
	 * Add al the info from the input {@link ProjectionRulesModel} to the current one
	 * @param prm
	 * @throws DuplicateRuleIdException 
	 */
	public void addProjectionRuleModel(ProjectionRulesModel prm) throws DuplicateRuleIdException{
		//iterate over the two maps, idToRuleMap (for standard, lazy and forRegex) and
		// idToRegexRuleMap to get the different rules and add them to this ProjectionRulesModel
		
		if(prm == null){
			//the input ProjectionRulesModel is null, so don't do anything and just return
			return;
		}

		setPrefixToNamespaceMap(prm.getPrefixToNamespaceMap());
		
		//start with standard, lazy and forRegex
		for(ProjectionRule pr : prm.getProjRule().values()){
			addProjectionRule(pr);
		}
		
		//now iterate over the regexes
		for(RegexProjectionRule rpr : prm.getRegexRuleMap().values()){
			addRegex(rpr);
		}
		
		//finally iterate over the annotations
		for(AnnotationDefinition ad : prm.getAnnotationDefinitionList()){
			//check if the current annotationDefinition is already present, in this case, do not add it
			if(!isAnnotationPresent(ad.getName())) {
				addAnnotationDefinition(ad);
			}
		}
		
	}

	public void addProjectionRule(ProjectionRule pr) throws DuplicateRuleIdException {
		// check if is it a "standard" rule (not a lazy one nor a forRegex one)
		if (!pr.isLazy() && !pr.isForRegex()) {
			if (typeNameToStandardRulesMap.containsKey(pr.getUIMAType()) == false) {
				typeNameToStandardRulesMap.put(pr.getUIMAType(), new ArrayList<ProjectionRule>());
			}
			Collection<ProjectionRule> projectionRuleList = typeNameToStandardRulesMap.get(pr.getUIMAType());
			projectionRuleList.add(pr);
		}
		if(idToRuleMap.get(pr.getId()) != null || idToRegexRuleMap.get(pr.getId()) != null){
			throw new DuplicateRuleIdException(pr.getId());
		}
		idToRuleMap.put(pr.getId(), pr);
	}

	public void addRegex(RegexProjectionRule regexProjectionRule) throws DuplicateRuleIdException {
		if(idToRuleMap.get(regexProjectionRule.getId()) != null || 
				idToRegexRuleMap.get(regexProjectionRule.getId()) != null){
			throw new DuplicateRuleIdException(regexProjectionRule.getId());
		}
		idToRegexRuleMap.put(regexProjectionRule.getId(), regexProjectionRule);
	}

	public Collection<ProjectionRule> getStandardProjectionRulesByTypeName(String typeName) {
		Collection<ProjectionRule> result = typeNameToStandardRulesMap.get(typeName);
		if (result != null)
			return result;
		else
			return new ArrayList<ProjectionRule>();
	}

	public Map<String, Collection<ProjectionRule>> getMapProjectionRules() {
		return typeNameToStandardRulesMap;
	}

	public ProjectionRule getProjRuleFromId(String id) {
		return idToRuleMap.get(id);
	}

	public Map<String, ProjectionRule> getProjRule() {
		return idToRuleMap;
	}

	public Map<String, RegexProjectionRule> getRegexRuleMap() {
		return idToRegexRuleMap;
	}

	public RegexProjectionRule getRegexRule(String id) {
		return idToRegexRuleMap.get(id);
	}

	/**
	 * 
	 * @param definition
	 * @author Gambella Riccardo
	 */
	public void addAnnotationDefinition(AnnotationDefinition definition) {
		this.annotationDefinitionList.add(definition);
	}
	
	public void addUsedPrefix(String prefix) {
		if(!usedPrefixesList.contains(prefix)) {
			usedPrefixesList.add(prefix);
		}
	}
	
	public List<String> getUsedPrefixedList() {
		return usedPrefixesList;
	}

	/**
	 * 
	 * @return
	 * @author Gambella Riccardo
	 */
	public List<AnnotationDefinition> getAnnotationDefinitionList() {
		return annotationDefinitionList;
	}

	/**
	 * Verify if an annotation has been defined.
	 * 
	 * @param annotation
	 * @return
	 * @author Gambella Riccardo
	 */
	public boolean isAnnotationPresent(Annotation annotation) {
		String name = annotation.getName();
		return isAnnotationPresent(name);
	}
	
	/**
	 * Verify if an annotation has been defined.
	 * 
	 * @param annotationName
	 * @return
	 */
	public boolean isAnnotationPresent(String annotationName){
		if(getAnnotationDefinition(annotationName)!= null)
			return true;
		return false;
	}

	/**
	 * Return the AnntationDefition given an annotation name
	 * @param annotationName
	 * @return
	 */
	public AnnotationDefinition getAnnotationDefinition(String annotationName){
		if(annotationName.startsWith("@")){
			annotationName=annotationName.substring(1);
		}

		for (AnnotationDefinition definition : annotationDefinitionList) {
			if(definition.getName().equals(annotationName))
					return definition;
		}
		
		return null;
	}

	/**
	 * Verify if an annotation has the metaAnnotation @Duplicate.
	 * 
	 * @param annotation
	 * @return
	 * @author Gambella Riccardo
	 */
	/*public boolean isAnnotationDuplicable(GraphSingleAnnotation annotation) {
		String name = annotation.getEnhancer();
		AnnotationDefinition annDef = getAnnotationDefinition(name);
		if(annDef == null){
			// Error: If there isn't the definition of the annotation.
			// Verify before launch this method (using isPresent)
			return false;
		}
		if (!annDef.hasListMetaAnnotations())
			return false;
		for (MetaAnnotation metaAnnotation : annDef.getListMetaAnnotations()) {
			if (metaAnnotation.getName().equals("Duplicate"))
				return true;
		}
		// If there isn't the metaAnnotation @Duplicate
		return false;
	}/*

	/**
	 * Verify if an annotation is compatible with the targets expressed in the metaAnnotation.
	 * 
	 * @param annotation
	 * @return
	 */
	/*public boolean targetCompatible(GraphSingleAnnotation annotation) {
		String name = annotation.getEnhancer();
		for (AnnotationDefinition definition : annotationDefinitionList) {
			if (definition.getName().equals(name)) {
				// Verify if the annotationDefinition has the metaAnnotation
				// @Target

				// No metaAnnotations means unknown targets, so is Compatible.
				if (!definition.hasListMetaAnnotations())
					return true;
				for (MetaAnnotation metaAnnotation : definition.getListMetaAnnotations()) {
					// Has the metaAnnotation target. Semantic check.
					if (metaAnnotation.getName().equals("Target")) {
						String target = annotation.getTarget();
						if (metaAnnotation.verifyTargetCompatibility(target))
							return true;
						// If the target is not in the possible definited
						// targets,
						// the annotation has a Target incompatibility. It will
						// be discarded.
						return false;
					}
				}
				// If there isn't the metaAnnotation @Target is Compatible.
				return true;
			}
		}
		// Error: If there isn't the definition of the annotation.
		// Verify before launch this method (using isPresent)
		return false;
	}*/

	/**
	 * Return a String from which is it possible to regenerate the same model
	 *
	 * @return a String representing the current model
	 */
	public String getModelAsString(List<String> annDefToExludeList) {
		StringBuilder sb = new StringBuilder();
		String ls = System.lineSeparator();

		//add the prefixes
		for(String prefix : prefixToNamespaceMap.keySet()){
			sb.append("prefix "+prefix+": <"+prefixToNamespaceMap.get(prefix)+">"+ls);
		}
		sb.append(ls);


		//add the annotation definition
		for(AnnotationDefinition annotationDefinition : annotationDefinitionList){
			if(annDefToExludeList.contains(annotationDefinition.getName())){
				//this annotation definition is one of the annotation definition which should not be in the file
				//so just skip it
				continue;
			}

			//add the meta annotations
			for(MetaAnnotation metaAnnotation : annotationDefinition.getMetaAnnotationList()){
				sb.append("@"+metaAnnotation.getName());
				if(metaAnnotation instanceof MetaAnnotationTarget){
					MetaAnnotationTarget metaAnnotationTarget = (MetaAnnotationTarget) metaAnnotation;
					sb.append("({");
					boolean first = true;
					for(String targetValue : metaAnnotationTarget.getTargetList()){
						if(first){
							first = false;
						} else{
							sb.append(", ");
						}
						sb.append(targetValue);
					}
					sb.append("})");
				} else if(metaAnnotation instanceof MetaAnnotationGeneric) {
					MetaAnnotationGeneric metaAnnotationGeneric = (MetaAnnotationGeneric) metaAnnotation;
					Map<String, List<ParamValueInterface>> nameToValueListMap = metaAnnotationGeneric.getNameToValueListMap();
					sb.append(addParamValueInterfaceInText(nameToValueListMap));
				}
				sb.append(ls);
			}

			sb.append("Annotation "+annotationDefinition.getName());
			if(!annotationDefinition.getParamDefinitionList().isEmpty()){
				sb.append(" {"+ls) ;
				for(ParamDefinition paramDefinition : annotationDefinition.getParamDefinitionList()){
					sb.append("\t"+paramDefinition.getType()+" "+paramDefinition.getName()+"()");
					if(paramDefinition.getDefaultValue()!=null){
						sb.append(" default "+paramDefinition.getDefaultValue().asString());
					}
					sb.append(";"+ls);
				}
				sb.append("}");
			}
			sb.append(ls+ls);

		}

		//add the rules
		for (String id : getProjRule().keySet()) {
			ProjectionRule prRule = getProjRuleFromId(id);
			// for(ProjectionRule prRule : prList){


			if(prRule.isLazy()){
				sb.append("lazy ");
			} else if(prRule.isForRegex()){
				sb.append("forRegex ");
			}

			sb.append("rule ");


			sb.append(prRule.getUIMAType() + " id:"+prRule.id+" {"+ls);


			sb.append("\tnodes = {"+ls);
			Map<String, PlaceholderStruct> placeholderMap = prRule.getPlaceholderMap();
			for (String placeholderKey : placeholderMap.keySet()) {
				PlaceholderStruct plStruct = placeholderMap.get(placeholderKey);
				//add the annotations, if any, for the current placeholder
				List<Annotation> annList = plStruct.getAnnotationList();
				for(Annotation annotation : annList){
					sb.append("\t\t"+getAnnotationAsString(annotation)+System.lineSeparator());
				}

				//add the placeholder definition
				sb.append("\t\t"+plStruct.getName()+" "+addRDFType(plStruct));
				if(plStruct.getFeaturePath() != null) {
					sb.append(" "+plStruct.getFeaturePath());
				}
				sb.append(" ."+ls);
			}

			sb.append("\t}"+ls);


			if(!prRule.getInsertGraphList().isEmpty()) {
				//check if there is the delete section, in this case, instead of the graph section put the insert section
				if (!prRule.getDeleteGraphList().isEmpty()) {
					sb.append("\tinsert = {" + "\n");
				} else {
					sb.append("\tgraph = {" + "\n");
				}
				Collection<GraphElement> geList = prRule.getInsertGraphList();
				for (GraphElement ge : geList) {
					if (ge instanceof OptionalGraphStruct) {
						sb.append(getOptionalGraphAsAString(2, ge, true) + "\n");
					} else {
						sb.append(getTripleAsAString(2, ge, true) + "\n");
					}

				}
				sb.append("\t}" + ls);
			}
			if(!prRule.getDeleteGraphList().isEmpty()) {
				//check if there is the delete section, in this case, instead of the graph section put the insert section
				sb.append("\tdelete = {" + "\n");
				Collection<GraphElement> geList = prRule.getDeleteGraphList();
				for (GraphElement ge : geList) {
					if (ge instanceof OptionalGraphStruct) {
						sb.append(getOptionalGraphAsAString(2, ge, true) + "\n");
					} else {
						sb.append(getTripleAsAString(2, ge, true) + "\n");
					}
				}
				sb.append("\t}" + ls);
			}
			//add the where part
			if(!prRule.getWhereList().isEmpty()){
				sb.append("\twhere = {\n");
				Collection<GraphElement> geList = prRule.getWhereList();
				for (GraphElement ge : geList) {
					if (ge instanceof OptionalGraphStruct) {
						sb.append(getOptionalGraphAsAString(2, ge, true) + "\n");
					} else {
						sb.append(getTripleAsAString(2, ge, true) + "\n");
					}
				}
				sb.append("\t}" + ls);
			}

			sb.append("}"+ls+ls);

		}


		return sb.toString();
		// return result;
	}

	private String addRDFType(PlaceholderStruct plStruct) {
		String text=plStruct.getRDFType();
		if(plStruct.getLiteralLang()!=null){
			text+="@"+plStruct.getLiteralLang();
		} else if(plStruct.getLiteralDatatype()!=null){
			String datatype = plStruct.getLiteralDatatype();
			if(!datatype.startsWith("<")){
				datatype = "<"+datatype+">";
			}
			text+="^^"+getQname(datatype);
		}
		List<ConverterMention> convList = plStruct.getConverterList();

		if(convList.size()==0 || (convList.size()==1 && convList.get(0).getURI().equals(PearlParserDescription.DEFAULT_CONVERTER_URI))){
			return text;
		}
		text+="(";
		boolean firstConv = true;
		for (ConverterMention conv : convList) {
			String convUri = "<"+conv.getURI()+">";
			String convQname = getQname(convUri);
			if(firstConv) {
				text += convQname;
				firstConv = false;
			} else {
				text+=", "+convQname;
			}
			if(conv.getAdditionalArguments().size()!=0){
				text+="(";
				boolean firstArg = true;
				for(ConverterArgumentExpression converterArgumentExpression : conv.getAdditionalArguments()){
					if(firstArg){
						firstArg = false;
					} else {
						text+=", ";
					}
					text+=processConverterArgumentExpression(converterArgumentExpression);


				}
				text+=")";
			}

		}
		text+=")";
		return text;
	}

	private String processConverterArgumentExpression(ConverterArgumentExpression converterArgumentExpression){
		String text="";
		if(converterArgumentExpression instanceof ConverterRDFLiteralArgument) {
			ConverterRDFLiteralArgument converterRDFLiteralArgument = (ConverterRDFLiteralArgument) converterArgumentExpression;
			Literal literal = converterRDFLiteralArgument.getLiteralValue();
			text+=NTriplesUtil.toNTriplesString(literal);
		} else if(converterArgumentExpression instanceof  ConverterMapArgument){
			ConverterMapArgument converterMapArgument = (ConverterMapArgument) converterArgumentExpression;
			Map<String, ConverterArgumentExpression> IdToCaeMap = converterMapArgument.getMap();
			text+="{";
			boolean firstEntry = true;
			for(String key: IdToCaeMap.keySet()){
				if(firstEntry){
					firstEntry = false;
				} else {
					text+=", ";
				}
				text+=key+" = "+processConverterArgumentExpression(IdToCaeMap.get(key));
			}

			text+="}";

		} else if(converterArgumentExpression instanceof  ConverterPlaceholderArgument){
			ConverterPlaceholderArgument converterPlaceholderArgument = (ConverterPlaceholderArgument) converterArgumentExpression;
			text+="$"+converterPlaceholderArgument.getPlaceholderId();

		} else if(converterArgumentExpression instanceof  ConverterRDFURIArgument){
			ConverterRDFURIArgument converterRDFURIArgument = (ConverterRDFURIArgument) converterArgumentExpression;
			IRI iri = converterRDFURIArgument.getURI();
			text+=getQname(NTriplesUtil.toNTriplesString(iri));
		}
		return text;
	}

	private String getQname(String iriString){
		if(!iriString.startsWith("<") && !iriString.endsWith(">")) {
			//it is not an iri, so just return it as it is
			return iriString;
		}
		//so the iriString starts with < and ends with >
		if(iriString.endsWith("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")){
			return "a";
		}

		//remove the starting < and ending >
		String iriNoPar = iriString.substring(1, iriString.length()-1);
		SimpleValueFactory simpleValueFactory = SimpleValueFactory.getInstance();
		IRI iri = simpleValueFactory.createIRI(iriNoPar);
		if(!iri.stringValue().equals(iriNoPar)){
			return iriString;
		}
		String namespace = iri.getNamespace();
		for(String prefix : prefixToNamespaceMap.keySet()){
			if(prefixToNamespaceMap.get(prefix).equals(namespace)){
				return prefix+":"+iri.getLocalName();
			}
		}
		return  iriString;
	}

	/**
	 * Return a String containing almost all the information stored in the current model. It is useful for
	 * debugging it
	 * 
	 * @return a String representing the current model
	 */
	public String getModelAsStringForDebug() {
		StringBuilder sb = new StringBuilder();

		// String result = "";
		// Map<String, Collection<ProjectionRule>> prMap = getMapProjectionRules();
		// System.out.println("%%%%%%%%% prMap.size() = "+prMap.size());
		// for(String uimatype : prMap.keySet()){
		// sb.append("UIMATYPE = "+uimatype+"\n");
		// Collection<ProjectionRule> prList = prMap.get(uimatype);

		for (String id : getProjRule().keySet()) {
			ProjectionRule prRule = getProjRuleFromId(id);
			// for(ProjectionRule prRule : prList){

			
			sb.append("rule id = " + prRule.getId());
			if(prRule.isLazy()){
				sb.append(" [lazy]");
			} else if(prRule.isForRegex()){
				sb.append(" [forRegex]");
			}
			
			sb.append("\n");
			sb.append("\tUIMATYPE = " + prRule.getUIMAType() + "\n");

			// get the dependencies and the associated metadata
			sb.append("\tdependencies:" + "\n");

			Map<String, Map<String, DependsOnInfo>> dependOnMap = prRule.getDependOnMap();
			for (String dependsOnKey : dependOnMap.keySet()) {
				sb.append("\t\tdependencyType : " + dependsOnKey + "\n");
				Map<String, DependsOnInfo> dependsOnInfoMap = dependOnMap.get(dependsOnKey);
				for (String dependsOnInfoKey : dependsOnInfoMap.keySet()) {
					DependsOnInfo dependOnInfo = dependsOnInfoMap.get(dependsOnInfoKey);
					String depOnAliasRuleId = dependOnInfo.getAliasRuleId();
					String depOnDepType = dependOnInfo.getDependsOnType();
					List<String> paramsList = dependOnInfo.getParamsList();
					sb.append("\t\t\tdepOnAltRuleId : " + depOnAliasRuleId + "\n");
					List<String> realIdList = prRule.getRealIdFromAliasId(depOnAliasRuleId);
					String realRuleIdString = "";
					for (String realRuleId : realIdList) {
						realRuleIdString += realRuleId + ", ";
					}
					sb.append("\t\t\trealId (from aliasId) : " + realRuleIdString + "\n");
					List<String> plchldList = prRule.getPlchldUsedFromAltId(depOnAliasRuleId);
					sb.append("\t\t\tused placeholder for this dependency : \n");
					for (String plchld : plchldList) {
						sb.append("\t\t\t\tplchld : " + plchld + "\n");
					}
					sb.append("\t\t\tdepOnDepType : " + depOnDepType + "\n");
					sb.append("\t\t\tparams : \n");
					for (String param : paramsList) {
						sb.append("\t\t\t\tparam : " + param + "\n");
					}
				}
			}

			// get the bindings
			Map<String, BindingStruct> bindingMap = prRule.getBindingsMap();
			sb.append("\tbindings:" + "\n");
			for (String bindingKey : bindingMap.keySet()) {
				BindingStruct bindingStruct = bindingMap.get(bindingKey);
				String name = bindingStruct.getName();
				String featPat = bindingStruct.getFeaturePath();
				String boundedRuleId = bindingStruct.getBoundRuleId();
				sb.append("\t\tbinding: " + bindingKey + "\n");
				sb.append("\t\t\tname= " + name + "\n");
				sb.append("\t\t\tfeatPat= " + featPat + "\n");
				sb.append("\t\t\tboundedRuleId= " + boundedRuleId + "\n");

			}
			
			// get the conditions
			List<ConditionStruct> conditionsList = prRule.getConditionStructList();
			sb.append("\tconditions:"+"\n");
			for(ConditionStruct conditionStruct : conditionsList){
				String featPath = conditionStruct.getFeatPath();
				String operator = conditionStruct.getCondOperator().name();
				List<String> valueList = conditionStruct.getValueList();
				sb.append("\t\t"+featPath);
				sb.append("\t"+operator);
				sb.append("\t{ ");
				boolean first = true;
				for(String value : valueList){
					if(!first){
						sb.append(", ");
					}
					first = false;
					sb.append(value);
				}
				sb.append("}\n");
			}
			

			// get the placeholders
			sb.append("\tplaceholders:" + "\n");
			Map<String, PlaceholderStruct> placeholderMap = prRule.getPlaceholderMap();
			for (String placeholderKey : placeholderMap.keySet()) {
				sb.append("\t\tplaceholder: " + placeholderKey + "\n");
				PlaceholderStruct plStruct = placeholderMap.get(placeholderKey);
				sb.append("\t\t\ttype= " + plStruct.getRDFType() + "\n");
				sb.append("\t\t\tconvertes: \n");
				List<ConverterMention> convList = plStruct.getConverterList();
				for (ConverterMention conv : convList) {
					sb.append("\t\t\t\tconverter= " + conv + "\n");
				}
				sb.append("\t\t\tfeaturePath= " + plStruct.getFeaturePath() + "\n");
			}

			sb.append("\tgraph:" + "\n");
			Collection<GraphElement> geList = prRule.getInsertGraphList();
			for (GraphElement ge : geList) {
				if (ge instanceof OptionalGraphStruct) {
					sb.append(getOptionalGraphAsAString(2, ge, false) + "\n");
				} else {
					sb.append(getTripleAsAString(2, ge, false) + "\n");
				}

			}
		}

		// }

		// get the regex part
		for (String regexId : idToRegexRuleMap.keySet()) {
			sb.append("REGEX id=" + regexId + "\n");
			RegexProjectionRule regexProjectionRule = idToRegexRuleMap.get(regexId);
			SingleRegexStruct singleRegexStruct = regexProjectionRule.getSingleRegexStruct();
			sb.append("\tInteranlId to RuleTd");
			Set<String> internalIdList = singleRegexStruct.getInternalId();
			for(String interlaId : internalIdList){
				sb.append("\n\t\t"+interlaId+" -> "+singleRegexStruct.geteRuleIdFromInternalId(interlaId));
			}
			sb.append("\n");
			sb.append(getFSAAsString(singleRegexStruct.getNfsa(), "NFSA") + "\n");
			sb.append(getFSAAsString(singleRegexStruct.getDfsa(), "DFSA") + "\n");
			Collection<GraphElement> graphElementCollection = regexProjectionRule.getGraphList();
			sb.append("\tgraph:\n");
			for (GraphElement graphElem : graphElementCollection) {
				if (graphElem instanceof OptionalGraphStruct) {
					sb.append(getOptionalGraphAsAString(2, graphElem, false) + "\n");
				} else {
					sb.append(getTripleAsAString(2, graphElem, false) + "\n");
				}
			}

		}

		return sb.toString();
		// return result;
	}

	private String getFSAAsString(FSA fsa, String title) {
		StringBuilder sb = new StringBuilder();

		sb.append("\t" + title + ":");

		sb.append("\n\t\tStates:");
		for (StateFSA state : fsa.getStateMap().values()) {
			sb.append("\n\t\t\t" + state.getStateId());
			if (state.isStartState()) {
				sb.append("\tstartState");
			}
			if (state.isEndState()) {
				sb.append("\tendState");
			}
			// sb.append("\t#transition = "+state.getNumberTransition());
			// sb.append("\ttransitionId = "+state.getAllTransitionId());
		}

		sb.append("\n\t\tTransitions:");
		for (String transitionId : fsa.getTransitionId()) {
			TransitionFSA transition = fsa.getTransition(transitionId);
			sb.append("\n\t\t\t" + transition.getStartingState().getStateId());
			sb.append(" --- ");
			sb.append(transition.getTransitionId());
			if (!transition.isEpsilonTransition()) {
				sb.append(", " + transition.getInternalId());
			} else {
				sb.append(", Epsilon");
			}
			if (transition.isMaxDistancePresent()) {
				sb.append(", " + transition.getMaxDistance());
			}
			sb.append(" --> ");
			sb.append(transition.getEndingState().getStateId());
		}

		return sb.toString();
	}

	private String getOptionalGraphAsAString(int indent, GraphElement inputGE, boolean printAnnotation) {
		String result = "";
		String indentString = "";
		for (int i = 0; i < indent; ++i) {
			indentString += "\t";
		}
		result += indentString + "OPTIONAL {\n";
		OptionalGraphStruct ogs = (OptionalGraphStruct) inputGE;
		Collection<GraphElement> geList = ogs.getOptionalTriples();
		for (GraphElement ge : geList) {
			if (ge instanceof OptionalGraphStruct)
				result += getOptionalGraphAsAString(indent + 1, ge, printAnnotation) + "\n";
			else
				// it is a triple
				result += getTripleAsAString(indent + 1, ge, printAnnotation) + "\n";
		}
		result += indentString + "}\n";
		return result;
	}

	private String getTripleAsAString(int indent, GraphElement inputGE, boolean printAnnotation) {
		String result = "";
		String indentString = "";
		for (int i = 0; i < indent; ++i) {
			indentString += "\t";
		}
		GraphStruct gs = (GraphStruct) inputGE;
		if(printAnnotation){
			List<Annotation> annList = gs.getAnnotationList();
			for(Annotation annotation : annList){
				result+=indentString+getAnnotationAsString(annotation)+System.lineSeparator();
			}
		}
		String subj = getQname(gs.getSubject().getValueAsString());
		String pred = getQname(gs.getPredicate().getValueAsString());
		String obj = getQname(gs.getObject().getValueAsString());
		result += indentString + subj + "  " + pred + "  " + obj+" .";
		return result;
	}

	private String getAnnotationAsString(Annotation annotation){
		String text = "@"+annotation.getName();
		if(annotation.getParamMap().size()>0) {
			Map<String, List<ParamValueInterface>> paramMap = annotation.getParamMap();
			text+=addParamValueInterfaceInText(paramMap);
		}

		return  text;
	}

	private String addParamValueInterfaceInText(Map<String, List<ParamValueInterface>> paramMap ){

		String text="(";
		boolean first = true;

		for (String key : paramMap.keySet()) {
			if(first){
				first = false;
			} else {
				text+=", ";
			}
			text+=key+"=";
			List<ParamValueInterface> valueList = paramMap.get(key);
			if(valueList.size()==1){
				if(valueList.get(0) instanceof ParamValueIri){
					text+=getQname(valueList.get(0).asString());
				} else {
					text += valueList.get(0).asString();
				}
			} else {
				text+="{";
				boolean firstValue=true;
				for(ParamValueInterface paramValueInterface : valueList){
					if (firstValue) {
						firstValue = false;
					} else {
						text+=", ";
					}
					if(paramValueInterface instanceof ParamValueIri){
						text+=getQname(paramValueInterface.asString());
					} else {
						text += paramValueInterface.asString();
					}
				}
				text+="}";
			}
		}
		text+=")";
		return text;
	}


	public Collection<RegexProjectionRule> getRegexesByTypename(String annotationTypeName) {
		List<RegexProjectionRule> regexProjectionRuleList = new ArrayList<RegexProjectionRule>();
		for(String idRegex : idToRegexRuleMap.keySet()){
			RegexProjectionRule regexProjectionRule = idToRegexRuleMap.get(idRegex);
			List<StateFSA> startStateList = 
					regexProjectionRule.getSingleRegexStruct().getDfsa().getStartStateList();
			for(StateFSA startState : startStateList){
				List<String> transitionInternalIdList = startState.getAllTransitionInternalId();
				//these id are internal to each regex, so they have to be converted to forRegex rule id
				// and then, by searching the selected forRegex rule, the UIMA type can be taken
				for(String internalId : transitionInternalIdList){
					String forRegexRuleUimaType = getUimaTypeFromInternalId(regexProjectionRule, internalId);
					if(forRegexRuleUimaType == null){
						//this should never happen
						continue;
					}
					if(annotationTypeName.compareTo(forRegexRuleUimaType) == 0){
						//the input annotation is the same as the one used in a start state in the current
						// regex, so the current regex is a candidate regex
						regexProjectionRuleList.add(regexProjectionRule);
					}
				}
			}
		}
		return regexProjectionRuleList;
	}

	public String getUimaTypeFromInternalId(RegexProjectionRule rpr, String internalId) {
		String ruleId = rpr.getSingleRegexStruct().geteRuleIdFromInternalId(internalId);
		ProjectionRule forRegexRule = getProjRuleFromId(ruleId);
		if(forRegexRule == null){
			//TODO this should never happen, a ruleId was used in a regex, but no rule has this id
			return null;
		}
		if(forRegexRule.isForRegex()){
			return forRegexRule.getUIMAType();
		}
		return null;
	}
	
}
