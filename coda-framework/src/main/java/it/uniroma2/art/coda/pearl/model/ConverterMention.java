package it.uniroma2.art.coda.pearl.model;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Objects;
import com.google.common.collect.Multimap;

import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.exception.ConverterException;

/**
 * Represents the mention of a converter (or a converter contract). A mention consists of the URI
 * identifying a converter (or a contract) and a possibly empty list of additional arguments.
 */
public class ConverterMention {

	private String uri;
	private List<ConverterArgumentExpression> additionalArguments;

	/**
	 * Constructs a converter mention with no additional argument.
	 * 
	 * @param uri
	 */
	public ConverterMention(String uri) {
		this(uri, new ArrayList<ConverterArgumentExpression>());
	}

	/**
	 * Constructs a converter mention including the provided arguments.
	 * 
	 * @param uri
	 * @param additionalArguments
	 */
	public ConverterMention(String uri, List<ConverterArgumentExpression> additionalArguments) {
		this.uri = uri;
		this.additionalArguments = additionalArguments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String text = "uri=" + uri + "(";
		boolean first = true;
		for (ConverterArgumentExpression converterArgumentExpression : additionalArguments) {
			if (!first) {
				text += ",";
			}
			first = false;
			text += converterArgumentExpression.toString();
		}
		text += ")";

		return text;
		// return Objects.toStringHelper(this).add("uri", uri).add("additionalArguments", additionalArguments)
		// .toString();
	}

	/**
	 * Returns the list of additional arguments
	 * 
	 * @return
	 */
	public List<ConverterArgumentExpression> getAdditionalArguments() {
		return additionalArguments;
	}

	/**
	 * Returns the URI identifying the converter (or converter contract)
	 * 
	 * @return
	 */
	public String getURI() {
		return uri;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(uri, additionalArguments);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != ConverterMention.class)
			return false;
		ConverterMention other = (ConverterMention) obj;
		return Objects.equal(uri, other.uri) && Objects.equal(additionalArguments, other.additionalArguments);
	}

	/**
	 * Instantiates this {@link ConverterMention} using the provided values for placeholders. An empty
	 * collection is returned, if the instantiation fails because of the use of a placeholder without any
	 * value.
	 * 
	 * @param placeholder2valueMap
	 * @return
	 * @throws ConverterException
	 */
	public List<ConverterMention> instatiateWithPlaceholderValues(
			Multimap<String, Object> placeholder2valueMap) throws ConverterException {
		if (additionalArguments.isEmpty())
			return Collections.singletonList(this);

		List<List<ConverterArgumentExpression>> multiConverterArgumentExpressions = new ArrayList<>();
		for (ConverterArgumentExpression arg : additionalArguments) {
			List<Object> explodedArgs = UIMACODAUtilities.evaluateConverterArgumentExpression(arg,
					placeholder2valueMap);

			if (explodedArgs.isEmpty()) {
				return Collections.emptyList();
			}

			List<ConverterArgumentExpression> explodedConvArgs = explodedArgs.stream()
					.map(obj -> new ConverterObjectArgument(arg.getArgumentType(), obj)).collect(toList());
			UIMACODAUtilities.appendValuesToMultiList(multiConverterArgumentExpressions, explodedConvArgs);
		}
		return multiConverterArgumentExpressions.stream().map(args -> new ConverterMention(uri, args))
				.collect(toList());
	}
}
