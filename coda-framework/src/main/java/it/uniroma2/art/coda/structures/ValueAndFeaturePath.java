package it.uniroma2.art.coda.structures;

public class ValueAndFeaturePath {

	private String value;
	private String featurePath;
	
	public ValueAndFeaturePath(String value, String featurePath) {
		super();
		this.value = value;
		this.featurePath = featurePath;
	}

	public String getValue() {
		return value;
	}

	public String getFeaturePath() {
		return featurePath;
	}
	
	
}
