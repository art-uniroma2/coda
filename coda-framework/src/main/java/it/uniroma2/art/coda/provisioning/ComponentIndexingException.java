package it.uniroma2.art.coda.provisioning;

public class ComponentIndexingException extends Exception {

	private static final long serialVersionUID = 4094429009681162655L;

	public ComponentIndexingException() {
	}

	public ComponentIndexingException(String message) {
		super(message);
	}

	public ComponentIndexingException(String message, Exception cause) {
		super(message, cause);
	}

}
