package it.uniroma2.art.coda.exception;

public class ConversionException extends ConverterException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4213628302660302887L;

	/**
     * @param e
     */
	public ConversionException(Exception e) {
		super(e);
	}
	
	/**
     * @param msg
     */
	public ConversionException(String msg) {
		super(msg);
	}


}
