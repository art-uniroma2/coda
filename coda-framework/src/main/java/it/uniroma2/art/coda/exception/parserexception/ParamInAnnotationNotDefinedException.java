package it.uniroma2.art.coda.exception.parserexception;

public class ParamInAnnotationNotDefinedException extends PRParserException {

	private String annName;
	private String ruleId;
	private String paramName;

	private static final long serialVersionUID = 1L;

	public ParamInAnnotationNotDefinedException(String annName, String ruleId, String paramName) {
		super();
		this.annName = annName;
		this.ruleId = ruleId;
		this.paramName = paramName;
	}

	public ParamInAnnotationNotDefinedException(Exception e, String prefixName, String ruleId, String paramName) {
		super(e);
		this.annName = prefixName;
		this.ruleId = ruleId;
		this.paramName = paramName;
	}
	
	public String getAnnName(){
		return annName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	public String getParamName() {
		return paramName;
	}

	@Override
	public String getErrorAsString() {
		return "param "+paramName+" in annotation "+ annName +" in rule "+ruleId+" is used but not defined";
	}
}
