package it.uniroma2.art.coda.pearl.model.graph;

import it.uniroma2.art.coda.pearl.model.ProjectionRule;

/**
 * This class is the single element containing a URI
 * @author Andrea Turbati
 *
 */
public class GraphSingleElemPropPath extends GraphSingleElement{
	private String completePath;


	/**
	 * @param completePath the complete Property Path URI contained in this element
	 * @param prRule the projection rule where this element belongs to
	 */
	public GraphSingleElemPropPath(String completePath, ProjectionRule prRule) {
		super(prRule);
		this.completePath = completePath;
	}
	

	@Override
	public String getValueAsString() {
		return completePath;
	}
}
