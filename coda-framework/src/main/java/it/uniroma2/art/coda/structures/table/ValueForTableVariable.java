package it.uniroma2.art.coda.structures.table;

import org.eclipse.rdf4j.model.Value;

public class ValueForTableVariable extends ValueForTable {

	private Value value;
	private String id;
	
	
	public ValueForTableVariable(String id, Value artNode) {
		this.value = artNode;
		this.id = id;
	}


	public Value getArtNode() {
		return value;
	}


	public String getId() {
		return id;
	}
	
	

}
