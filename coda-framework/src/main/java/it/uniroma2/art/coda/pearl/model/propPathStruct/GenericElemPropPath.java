package it.uniroma2.art.coda.pearl.model.propPathStruct;

public interface GenericElemPropPath {

    public String getValueAsString();
}
