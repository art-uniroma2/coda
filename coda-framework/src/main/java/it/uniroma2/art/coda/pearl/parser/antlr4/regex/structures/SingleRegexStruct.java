package it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SingleRegexStruct {

	private String id;
	
	private FSA nfsa; // non deterministic FSA
	private FSA dfsa; // deterministic FSA
	private Map<String, String>internaIdToRuleIdMap; // used to map internalId to CODARuleId
	private List<StateFSA> dfsaStateToAnalyze; //used in the creation of the DFSA from the NFSA, 
	// in all other situation this List shluld be empty
	
	private Map<String, List<StateFSA>> dfsaStateToNfsaMap;
	
	public SingleRegexStruct(String id) {
		this.id = id;
		nfsa = new FSA();
		dfsa = new FSA();
		
		internaIdToRuleIdMap = new HashMap<String, String>();
		dfsaStateToNfsaMap = new HashMap<String, List<StateFSA>>();
		dfsaStateToAnalyze = new ArrayList<StateFSA>();  
	}

	public String getId(){
		return id;
	}
	
	public FSA getNfsa() {
		return nfsa;
	}

	public FSA getDfsa() {
		return dfsa;
	}
	
	public Set<String> getInternalId(){
		return internaIdToRuleIdMap.keySet();
	}
	
	public String geteRuleIdFromInternalId(String internalId){
		return internaIdToRuleIdMap.get(internalId);
	}
	
	public void addInternIdToRuleId(String internalId, String ruleId){
		internaIdToRuleIdMap.put(internalId, ruleId);
	}
	
	/*public Map<String, String> getIinternaIdToRuleIdMap(){
		return internaIdToRuleIdMap;
	}*/


	/*
	 * This function convert a NFSA (with Epsylon transition and ) to a DFSA (no Epsylon transition and )
	 */
	public void convertNFSAtoDFSA(String PREFIX_NFSA_STATE, String PREFIX_DFSA_STATE, 
			String PREFIX_DFSA_TRANSITION){
		if(dfsa.getStartStateList().size() != 0){
			//the DFSA was already created, so do nothing, this should never happen
			return;
		}
		
		// a map containg the dfsa state list to avoid duplciates
		Map<String, StateFSA> dfsaStateMap = new HashMap<String, StateFSA>();
		
		// get the start state list of the NFSA
		List<StateFSA> startNdfaStateList = nfsa.getStartStateList();
		
		//do the Epsylon transition on these starting states. Each State reachable using just Epsylon 
		//transition(s) is a starting state for the DFSA
		for(StateFSA startNdfaState : startNdfaStateList){ 
			List<StateFSA> ndfaStartStateFromEpsylonList = doEpslylonClosureForState(startNdfaState);
			
			//add the current startNdfaState to the state list obtained doing the Epsylon closure
			ndfaStartStateFromEpsylonList = combineStates(startNdfaState, ndfaStartStateFromEpsylonList);
			
			//create the new state using the list of states
			StateFSA dfsaStartState = orderStateAndGenerateNewState(ndfaStartStateFromEpsylonList, PREFIX_NFSA_STATE, 
					PREFIX_DFSA_STATE, dfsaStateMap);
			//dfsaStartState.setIsStartState(true);
			dfsaStateToNfsaMap.put(dfsaStartState.getStateId(), ndfaStartStateFromEpsylonList);
			dfsaStateToAnalyze.add(dfsaStartState);
		}

		//now create the states for the DFSA
		createDFSAStates(PREFIX_NFSA_STATE, PREFIX_DFSA_STATE, PREFIX_DFSA_TRANSITION, dfsaStateMap);
		
		//iterate over all the states in dfsa to find the start and the end state to set them in dfsa structure
		//Map<String, StateFSA> dfsaStateMap = dfsa.getStateMap();
		dfsaStateMap.clear();
		dfsaStateMap = dfsa.getStateMap();
		for(String dfsaStateId : dfsaStateMap.keySet()){
			StateFSA dfsaState = dfsaStateMap.get(dfsaStateId);
			if(dfsaState.isEndState()){
				dfsa.addEndState(dfsaState);
			}
			if(dfsaState.isStartState()){
				dfsa.addStartState(dfsaState);
			}
		}
	}
	
	private void createDFSAStates(String PREFIX_NFSA_STATE, String PREFIX_DFSA_STATE, 
			String PREFIX_DFSA_TRANSITION, Map<String, StateFSA> dfsaStateMap){
		
		while(!dfsaStateToAnalyze.isEmpty()){
			StateFSA currentDfsaState = dfsaStateToAnalyze.get(0); // get the first state to analyze
			dfsaStateToAnalyze.remove(currentDfsaState);// remove the first state from the list
			
			//check for every possible transition which NFSA state is reachable (using also Epsylon*
			// after the named transition)
			
			String currentDfsaStateName = currentDfsaState.getStateId();
			List<StateFSA> nfsaStateList = dfsaStateToNfsaMap.get(currentDfsaStateName);
			
			//iterate over the list of all possible transition
			for(String transition : nfsa.getTransitionId()){
				if(nfsa.getTransition(transition).isEpsilonTransition()){
					// it is an epsylon transition, so to not consider it
					continue;
				}
				String internalId = nfsa.getTransition(transition).getInternalId();
				List<StateFSA> stateFromNFSAList = new ArrayList<StateFSA>();
				for(StateFSA nfsaState : nfsaStateList){
//					List<StateFSA> reachableNfsaStateList = nfsaState.getStateForTransition(internalId);
					StateFSA reachableNfsaState = nfsaState.getStateForTransition(internalId);
					if(reachableNfsaState == null){
						// using this transition, no state is reachable
						continue;
					}
//					for(StateFSA reachableNfsaState: reachableNfsaStateList){
						// do the Epsylon closure
						List<StateFSA>epsylonReachableNdfaStateList = doEpslylonClosureForState(reachableNfsaState);
						//add the reachableState and the epsylonReachableStateList to create a new state 
						epsylonReachableNdfaStateList = combineStates(reachableNfsaState, 
								epsylonReachableNdfaStateList); 
						stateFromNFSAList = combineStates(stateFromNFSAList, 
								epsylonReachableNdfaStateList); 
//					}
				}
				if(stateFromNFSAList.isEmpty()){
					//there were only Epsylon transitions from the state
					continue;
				}
				//create the new state using the list of states nfsaState
				StateFSA newStateDfsa = orderStateAndGenerateNewState(stateFromNFSAList, 
						PREFIX_NFSA_STATE, PREFIX_DFSA_STATE, dfsaStateMap);
				TransitionFSA transitionNFSA = nfsa.getTransition(transition);
				
				//check if there is already a transition with the same startState, endState and internalId
//				List<StateFSA> endDfsaEndStateList = currentDfsaState.getStateForTransition(internalId);
				StateFSA endDfsaState = currentDfsaState.getStateForTransition(internalId);
				boolean transitionAlreadyExist = false;
				if(endDfsaState != null){
//					for(StateFSA endDfsaState : endDfsaEndStateList){
						if(endDfsaState.getStateId().compareTo(newStateDfsa.getStateId()) == 0){
							//there is already this transition between these two state, so do not add it
							transitionAlreadyExist = true;
						}
//					}
				}
				if(transitionAlreadyExist){
					continue;
				}
				
				TransitionFSA transitionDFSA = createNewTransition(PREFIX_DFSA_TRANSITION, dfsa, 
						transitionNFSA.getInternalId(), transitionNFSA.getMaxDistance(), currentDfsaState, 
						newStateDfsa);
				
				//add the state and the transition to dfsa
				dfsa.addState(newStateDfsa);
				dfsa.addTransition(transitionDFSA);
				if(!isStateAlreadyPresent(newStateDfsa.getStateId())){
					dfsaStateToNfsaMap.put(newStateDfsa.getStateId(), stateFromNFSAList);
					dfsaStateToAnalyze.add(newStateDfsa);
				}
			}
		}
	}


	private boolean isStateAlreadyPresent(String dfsaStateId){
		return dfsaStateToNfsaMap.containsKey(dfsaStateId);
	}
	
	private StateFSA orderStateAndGenerateNewState(List<StateFSA> stateList, String PREFIX_NFSA_STATE, 
			String PREFIX_DFSA_STATE, Map<String, StateFSA> dfsaStateMap){
		boolean isStartState = false;
		boolean isEndState = false;
		StringBuilder newStateId = new StringBuilder();
		
		List<String> stateIdStringList = new ArrayList<String>();
		for(StateFSA state : stateList){
			stateIdStringList.add(state.getStateId());
			isStartState = isStartState || state.isStartState();
			isEndState = isEndState || state.isEndState();
		}
		
		//order the input list
		Collections.sort(stateIdStringList);
		
		for(String stateString : stateIdStringList){
			String partStateId = stateString.substring(PREFIX_NFSA_STATE.length())+"_";
			newStateId.append(partStateId);
		}
		
		String newStateIdString = newStateId.toString();
		newStateIdString = newStateIdString.substring(0, newStateIdString.length()-1);
		
		StateFSA dfsaState = null;
		//check if the there is already a state with this id in the DFSA state list
		if(dfsaStateMap.containsKey(newStateIdString)){
			dfsaState = dfsaStateMap.get(newStateIdString);
		} else{
			dfsaState = createNewState(PREFIX_DFSA_STATE, dfsa, newStateIdString);
			dfsaState.setIsEndState(isEndState);
			dfsaState.setIsStartState(isStartState);
			dfsaStateMap.put(newStateIdString, dfsaState);
		}
		return dfsaState;
	}
	
	private List<StateFSA> doEpslylonClosureForState(StateFSA fromState) {
		List<StateFSA> epsylonStateList = new ArrayList<StateFSA>();
		epsylonStateList.add(fromState);
		List<String> epsylonStateStringList = new ArrayList<String>();
		epsylonStateStringList.add(fromState.getStateId());
		doEpslylonClosureForState(fromState, epsylonStateList, epsylonStateStringList);
		return epsylonStateList;
	}
	
	
	private void doEpslylonClosureForState(StateFSA startState, List<StateFSA> epsylonStateList,
			List<String> epsylonStateStringList) {
		List<StateFSA> reachableEpsylonStateList = startState.getStateForEpsylonTransition();
		for(StateFSA reachableEpsylonState : reachableEpsylonStateList){
			if(!epsylonStateStringList.contains(reachableEpsylonState.getStateId())){
				//this state has not been previously added to the list, so add it and iterate on this state
				epsylonStateStringList.add(reachableEpsylonState.getStateId());
				epsylonStateList.add(reachableEpsylonState);
				doEpslylonClosureForState(reachableEpsylonState, epsylonStateList, epsylonStateStringList);
			}
		}
		
	}


	private List<StateFSA> combineStates(StateFSA reachableState, List<StateFSA> epsylonReachableStateList) {
		
		List <StateFSA> tempStateList = new ArrayList<StateFSA>();
		tempStateList.add(reachableState);
		return combineStates(tempStateList, epsylonReachableStateList);
		
	}
	
	private List<StateFSA> combineStates(List<StateFSA> stateList1, List<StateFSA> stateList2) {
		List<StateFSA> newStateList = new ArrayList<StateFSA>();
		List<String> newStateStringList = new ArrayList<String>();
		for(StateFSA state : stateList1){
			if(!newStateStringList.contains(state.getStateId())){
				newStateStringList.add(state.getStateId());
				newStateList.add(state);
			}
		}
		
		for(StateFSA state : stateList2){
			if(!newStateStringList.contains(state.getStateId())){
				newStateStringList.add(state.getStateId());
				newStateList.add(state);
			}
		}
		
		return newStateList;
	}
	
	
	public StateFSA createNewState(String prefix, FSA fsa) {
		Map<String, StateFSA> stateMap = fsa.getStateMap();
		String newLocalStateName = generateLocalStateName(stateMap);
		return createNewState(prefix, fsa, newLocalStateName);
	}
	
	public StateFSA createNewState(String prefix, FSA fsa, String localName){
		Map<String, StateFSA> stateMap = fsa.getStateMap();
		String newStateName = prefix+localName;
		StateFSA newState = new StateFSA(newStateName);
		stateMap.put(newStateName, newState);
		return newState;
	}
	
	private String generateLocalStateName(Map<String, StateFSA> stateMap) {
		String name;
		int stateListSize = stateMap.size();
		name = ""+stateListSize;
		return name;
	}
	
	public TransitionFSA createNewTransition(String prefix, FSA fsa,
			String internalId, int maxDistance, StateFSA startState, StateFSA endState) {
		String newTransitionName = generateTransitionName(prefix, fsa);
		TransitionFSA newTransition = new TransitionFSA(newTransitionName, internalId, maxDistance,
				startState, endState);
		fsa.addTransition(newTransition);
		return newTransition;
	}

	public TransitionFSA createNewEpsilonTransition(String prefix, FSA fsa,
			StateFSA startState, StateFSA endState) {
		String newTransitionName = generateTransitionName(prefix, fsa);
		TransitionFSA newTransition = new TransitionFSA(newTransitionName, startState, endState);
		fsa.addTransition(newTransition);
		return newTransition;
	}
	
	private String generateTransitionName(String prefix, FSA fsa) {
		String name;
		int transitionListSize = fsa.getTransitionId().size();
		name = prefix + (transitionListSize + 1);
		return name;
	}
	
}
