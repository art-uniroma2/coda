package it.uniroma2.art.coda.exception.parserexception;

public class NodeNotDefinedException extends PRParserException {

	private String plcName;
	private String ruleId;
	
	private static final long serialVersionUID = 1L;

	public NodeNotDefinedException(String plcName, String ruleId) {
		super();
		this.plcName = plcName;
		this.ruleId = ruleId;
	}

	public NodeNotDefinedException(Exception e, String plcName, String ruleId) {
		super(e);
		this.plcName = plcName;
		this.ruleId = ruleId;
	}
	
	public String getPlcName(){
		return plcName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	@Override
	public String getErrorAsString() {
		return "placeholder "+plcName+" is used but not defined in rule "+ ruleId;
	}
}
