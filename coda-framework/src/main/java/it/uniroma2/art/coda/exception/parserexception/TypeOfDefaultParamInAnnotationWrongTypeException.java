package it.uniroma2.art.coda.exception.parserexception;

public class TypeOfDefaultParamInAnnotationWrongTypeException extends PRParserException {

	private String annName;
	private String paramName;
	private  String typeExpected;


	private static final long serialVersionUID = 1L;

	public TypeOfDefaultParamInAnnotationWrongTypeException(String annName, String paramName, String typeExpected) {
		super();
		this.annName = annName;
		this.paramName = paramName;
		this.typeExpected = typeExpected;
	}

	public TypeOfDefaultParamInAnnotationWrongTypeException(Exception e, String prefixName, String paramName, String typeExpected) {
		super(e);
		this.annName = prefixName;
		this.paramName = paramName;
		this.typeExpected = typeExpected;
	}
	
	public String getAnnName(){
		return annName;
	}
	

	public String getParamName() {
		return paramName;
	}

	public String getTypeExpected() {
		return typeExpected;
	}

	@Override
	public String getErrorAsString() {
		return "the dafault param "+paramName+" in annotation "+ annName +" has a value not compliant with type "+typeExpected;
	}
}
