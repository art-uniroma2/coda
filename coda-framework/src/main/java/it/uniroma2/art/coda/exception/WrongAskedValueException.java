package it.uniroma2.art.coda.exception;

public class WrongAskedValueException extends Exception {

    public WrongAskedValueException(String message) {
        super(message);
    }
}
