package it.uniroma2.art.coda.provisioning;

import java.util.List;

/**
 * A converter description.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public interface ConverterDescription extends ConverterContractDescription {
	/**
	 * Returns the descriptions of the implemented contracts.
	 * 
	 * @return
	 */
	List<ConverterContractDescription> getImplementedContracts();
}
