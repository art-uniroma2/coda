package it.uniroma2.art.coda.interfaces.annotations.converters;

public enum RDFCapabilityType {
	node, uri, literal
}
