package it.uniroma2.art.coda.interfaces.annotations.converters;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Provides metadata about the use of feature paths for the invocation of converters. This annotation should
 * be applied to a method of a converter, i.e. produceURI(...) or produceLiteral(...)  
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface FeaturePathArgument {
	/**
	 * Indicates whether a feature path is required (default), optional or ignored.
	 * @return
	 */
	RequirementLevels requirementLevel() default RequirementLevels.REQUIRED;
}
