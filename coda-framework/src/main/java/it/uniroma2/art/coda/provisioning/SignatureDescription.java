package it.uniroma2.art.coda.provisioning;

import java.util.List;

import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;

/**
 * A signature description.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface SignatureDescription {
	/**
	 * Returns an indicator of whether the feature path argument is required, optional or ignored.
	 * 
	 * @return
	 */
	RequirementLevels getFeaturePathRequirementLevel();

	/**
	 * Returns the description of the parameters (excluding the ones that are implicit based on the nature of
	 * the signature, e.g. datatype, lang, value).
	 * 
	 * @return
	 */
	List<ParameterDescription> getParameterDescriptions();

	/**
	 * Returns a description of the return type
	 * @return
	 */
	TypeDescription getReturnTypeDescription();

	/**
	 * Tells whether this signature produces URIs.
	 * @return
	 */
	boolean isProducingURI();

	/**
	 * Tells wheter this signature produces literals.
	 * @return
	 */
	boolean isProducingLiteral();
}
