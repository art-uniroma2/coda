package it.uniroma2.art.coda.structures.table;

import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;

public class SingleTableValuePlaceholder extends SingleTableValue{

	PlaceholderStruct placeholderStruct;
	
	public SingleTableValuePlaceholder(String id, Value value, PlaceholderStruct placeholderStruct) {
		super(id, value);
		this.placeholderStruct = placeholderStruct;
	}

	public PlaceholderStruct getPlaceholderStruct() {
		return placeholderStruct;
	}

	
}
