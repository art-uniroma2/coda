package it.uniroma2.art.coda.interfaces;


import it.uniroma2.art.coda.structures.SelectedProjectionRules;

/**
 * @author Andrea Turbati
 */
public abstract class ProjectionRulesDeciderInterface extends CODAExtensionModuleInterface{

	public ProjectionRulesDeciderInterface(String id, String description) {
		super(id, description);
	}
	
	/**
	 * This method must be implemented by all the modules that implement this abstract class
	 * @return list of Hints
	 */
	public abstract SelectedProjectionRules invokeComponent();
}
