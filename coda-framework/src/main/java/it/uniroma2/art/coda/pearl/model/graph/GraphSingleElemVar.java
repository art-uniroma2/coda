package it.uniroma2.art.coda.pearl.model.graph;

import it.uniroma2.art.coda.pearl.model.ProjectionRule;

/**
 * This class is the single element of a triple containing a Variable
 * 
 * @author Andrea Turbati
 * 
 */
public class GraphSingleElemVar extends GraphSingleElement {

	private String varId;

	/**
	 * @param varId
	 *            the id of the variable contained in this element
	 * @param prRule
	 *            the projection rule where this element belongs to
	 */
	public GraphSingleElemVar(String varId, ProjectionRule prRule) {
		super(prRule);
		this.varId = varId;
	}

	/**
	 * Get the id of the variable contained in this element
	 * 
	 * @return the id of the variable contained in this element
	 */
	public String getVarId() {
		return varId;
	}

	@Override
	public String getValueAsString() {
		return getVarId();
	}

}
