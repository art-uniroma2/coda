package it.uniroma2.art.coda.exception;

public class ValueNotPresentDueToConfigurationException extends Exception {

    public ValueNotPresentDueToConfigurationException(String message) {
        super(message);
    }
}
