package it.uniroma2.art.coda.structures;

import java.util.List;

public class DependsOnInfo {

	private String dependsOnType;  // the type of the dependency
	private List<String> dependsOnRuleIdList; // the list of the other rule id
	private List<String> paramsList; // the list of params
	//private boolean hasAlteranativeId; // if it has an alternativeId
	private String usedAliasRuleId; //  the alias used in the rule 
	
	/*public DependsOnInfo(String dependsOnType, String dependsOnRuleId, List<String> valuesList) {
		initialize(dependsOnType, dependsOnRuleId, null, false, valuesList);
	}*/
	
	public DependsOnInfo(String dependsOnType, List<String> dependsOnRuleIdList, List<String> valuesList, 
			String alias) {
		initialize(dependsOnType, dependsOnRuleIdList, valuesList, alias);
	}

	
	private void initialize(String dependsOnType, List<String> dependsOnRuleIdList, List<String> paramsList,
			String alias){
		this.dependsOnType = dependsOnType;
		this.dependsOnRuleIdList = dependsOnRuleIdList;
		this.paramsList = paramsList;
		this.usedAliasRuleId = alias;
	}

	public String getDependsOnType() {
		return dependsOnType;
	}


	public List<String> getDependsOnRuleIdList() {
		return dependsOnRuleIdList;
	}


	public List<String> getParamsList() {
		return paramsList;
	}
	
	public String getAliasRuleId(){
		return usedAliasRuleId;
	}
	
}
