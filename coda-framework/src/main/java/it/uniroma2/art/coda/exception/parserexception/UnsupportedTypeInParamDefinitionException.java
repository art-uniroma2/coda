package it.uniroma2.art.coda.exception.parserexception;

public class UnsupportedTypeInParamDefinitionException extends PRParserException {

	private String annName;
	private String paramName;
	private String type;

	private static final long serialVersionUID = 1L;

	public UnsupportedTypeInParamDefinitionException(String annName, String paramName, String type) {
		super();
		this.annName = annName;
		this.paramName = paramName;
		this.type = type;
	}

	public UnsupportedTypeInParamDefinitionException(Exception e, String prefixName, String paramName, String type) {
		super(e);
		this.annName = prefixName;
		this.paramName = paramName;
		this.type = type;
	}
	
	public String getAnnName(){
		return annName;
	}
	

	public String getParamName() {
		return paramName;
	}

	public String getType() {
		return type;
	}

	@Override
	public String getErrorAsString() {
		return "param "+paramName+" in annotation "+ annName +" is defined with type "+type+" which is not supported";
	}
}
