package it.uniroma2.art.coda.interfaces.annotations.converters;

/**
 * The constants of this enumerated type allow to specify (in conjunction with the annotation
 * {@link FeaturePathArgument}), whether a feature path is required, optional or ignored for the purpose of
 * executing a converter.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public enum RequirementLevels {
	REQUIRED, OPTIONAL, IGNORED
}
