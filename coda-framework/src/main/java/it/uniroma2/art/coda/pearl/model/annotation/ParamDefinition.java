package it.uniroma2.art.coda.pearl.model.annotation;

import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;

public class ParamDefinition {
    String type;
    String name;
    ParamValueInterface defaultValue;
    boolean hasDefault;

    public ParamDefinition(String type, String name, ParamValueInterface defaultValue) {
        this.type = type;
        this.name = name;
        this.defaultValue = defaultValue;
        this.hasDefault = true;
    }

    public ParamDefinition(String type, String name) {
        this.type = type;
        this.name = name;
        this.hasDefault=false;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public ParamValueInterface getDefaultValue() {
        return defaultValue;
    }

    public boolean hasDefault() {
        return hasDefault;
    }
}
