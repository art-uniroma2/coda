package it.uniroma2.art.coda.provisioning.impl;

import it.uniroma2.art.coda.provisioning.TypeDescription;

public abstract class TypeDescriptionImpl implements TypeDescription {
	private final String name;
	
	public TypeDescriptionImpl(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}
}
