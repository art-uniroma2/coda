package it.uniroma2.art.coda.structures;

public class ValueOrUri {

	private String value;
	private String uri;
	private boolean isURI;
	
	public ValueOrUri(String valueOrURI, boolean isURI) {
		if(isURI){
			this.uri = valueOrURI;
			this.isURI = true;
			this.value = null;
		} else {
			this.value = valueOrURI;
			this.isURI = false;
			this.uri = null;
		}
	}

	public String getValue() {
		return value;
	}

	public String getUri() {
		return uri;
	}

	public boolean isURI() {
		return isURI;
	}
}
