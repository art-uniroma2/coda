package it.uniroma2.art.coda.pearl.model.propPathStruct;

public class PathEltElemPropPath implements GenericElemPropPath {
    private PathPrimaryElemPropPath pathPrimaryElemPropPath;
    private String regexSymbol;
    private boolean hasRegexSymbol;

    public PathEltElemPropPath(PathPrimaryElemPropPath pathPrimaryElemPropPath) {
        this.pathPrimaryElemPropPath = pathPrimaryElemPropPath;
        this.regexSymbol = null;
        this.hasRegexSymbol = false;
    }

    public PathEltElemPropPath(PathPrimaryElemPropPath pathPrimaryElemPropPath, String regexSymbol) {
        this.pathPrimaryElemPropPath = pathPrimaryElemPropPath;
        this.regexSymbol = regexSymbol;
        this.hasRegexSymbol = true;
    }

    public PathPrimaryElemPropPath getPathPrimaryElemPropPath() {
        return pathPrimaryElemPropPath;
    }

    public String getRegexSymbol() {
        return regexSymbol;
    }

    public boolean hasRegexSymbol() {
        return hasRegexSymbol;
    }

    @Override
    public String getValueAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(pathPrimaryElemPropPath.getValueAsString());
        if (hasRegexSymbol) {
            stringBuilder.append(regexSymbol);
        }
        stringBuilder.append(" ");

        return stringBuilder.toString();
    }
}
