package it.uniroma2.art.coda.exception.parserexception;

public class DuplicateRuleIdException extends PRParserException {

	private String ruleId;
	
	private static final long serialVersionUID = 1L;

	public DuplicateRuleIdException(String ruleId) {
		super();
		this.ruleId = ruleId;
	}

	public DuplicateRuleIdException(Exception e, String plcName, String ruleId) {
		super(e);
		this.ruleId = ruleId;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	@Override
	public String getErrorAsString() {
		return "There are at least two rules/regexes with the same id: "+ ruleId;
	}
}
