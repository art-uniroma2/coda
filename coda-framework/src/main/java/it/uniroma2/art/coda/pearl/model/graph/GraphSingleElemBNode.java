package it.uniroma2.art.coda.pearl.model.graph;

import it.uniroma2.art.coda.pearl.model.ProjectionRule;

/**
 * This class is the single element containing a blank node
 * @author Andrea Turbati
 *
 */
public class GraphSingleElemBNode extends GraphSingleElement{
	public String getBnodeIdentifier() {
		return bnodeIdentifier;
	}


	private String bnodeIdentifier;
	

	public GraphSingleElemBNode(String bnodeIdentifier, ProjectionRule prRule) {
		super(prRule);
		this.bnodeIdentifier = bnodeIdentifier;
	}


	@Override
	public String getValueAsString() {
		return "_:" + getBnodeIdentifier();
	}

}
