package it.uniroma2.art.coda.pearl.model;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;

import com.google.common.collect.Multimap;

import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.exception.ConverterException;

/**
 * A combination of a converter mention list and a desired node type.
 */
public class ProjectionOperator {
	public static enum NodeType {
		uri, literal
	}

	private final NodeType nodeType;
	private final Optional<String> languageHolder;
	private final Optional<String> datatypeHolder;
	private final List<ConverterMention> converterMentions;

	public ProjectionOperator(NodeType nodeType, String language, String datatype,
			List<ConverterMention> converterMentions) {
		this.nodeType = nodeType;
		this.languageHolder = Optional.ofNullable(language);
		this.datatypeHolder = Optional.ofNullable(datatype);
		this.converterMentions = converterMentions;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public Optional<String> getLanguage() {
		return languageHolder;
	}

	public Optional<String> getDatatype() {
		return datatypeHolder;
	}

	public List<ConverterMention> getConverterMentions() {
		return converterMentions;
	}

	/**
	 * Instantiates this {@link ProjectionOperator} using the provided values for placeholders. An empty
	 * collection is returned, if the instantiation fails because of the use of a placeholder without any
	 * value.
	 * 
	 * @param placeholder2valueMap
	 * @return
	 * @throws ConverterException
	 */
	public List<ProjectionOperator> instantiateWithPlaceholderValues(
			Multimap<String, Object> placeholder2valueMap) throws ConverterException {
		if (converterMentions.isEmpty())
			return Collections.singletonList(this);

		List<List<ConverterMention>> multiConverterMentions = new ArrayList<>();
		for (ConverterMention converterMention : converterMentions) {
			List<ConverterMention> explodedConverterMentions = converterMention
					.instatiateWithPlaceholderValues(placeholder2valueMap);

			if (explodedConverterMentions.isEmpty()) {
				return Collections.emptyList();
			}

			UIMACODAUtilities.appendValuesToMultiList(multiConverterMentions, explodedConverterMentions);
		}
		return multiConverterMentions.stream().map(convMentions -> new ProjectionOperator(nodeType,
				languageHolder.orElse(null), datatypeHolder.orElse(null), convMentions)).collect(toList());
	}

	@Override
	public int hashCode() {
		return Objects.hash(nodeType, languageHolder, datatypeHolder, converterMentions);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != ProjectionOperator.class)
			return false;

		ProjectionOperator other = (ProjectionOperator) obj;

		return Objects.equals(nodeType, other.nodeType)
				&& Objects.equals(languageHolder, other.languageHolder)
				&& Objects.equals(datatypeHolder, other.datatypeHolder)
				&& Objects.equals(converterMentions, other.converterMentions);
	}

	public static ProjectionOperator createLiteralProjection(String language,
			List<ConverterMention> converterMentions) {
		return new ProjectionOperator(NodeType.literal, language, null, converterMentions);
	}

	public static ProjectionOperator createLiteralProjection(IRI datatype,
			List<ConverterMention> converterMentions) {
		return new ProjectionOperator(NodeType.literal, null, datatype.stringValue(), converterMentions);
	}

	public static ProjectionOperator createURIProjection(List<ConverterMention> converterMentions) {
		return new ProjectionOperator(NodeType.uri, null, null, converterMentions);
	}

}
