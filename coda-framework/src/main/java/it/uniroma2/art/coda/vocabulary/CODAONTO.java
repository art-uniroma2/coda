package it.uniroma2.art.coda.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public class CODAONTO {
	public static final String NAMESPACE = "http://art.uniroma2.it/coda/ontology#";
	public static final Namespace NS = new SimpleNamespace("coda-ont", NAMESPACE);
	
	public static final IRI CONVERTER_CONTRACT;
	public static final IRI CONVERTER;
	
	public static final IRI DATATYPE_CAPABILITY;
	public static final IRI RDF_CAPABILITY;

	public static final IRI IMPLEMENTED_CONTRACT;

	static {
		SimpleValueFactory vf = SimpleValueFactory.getInstance();
		
		CONVERTER = vf.createIRI(NAMESPACE, "Converter");
		CONVERTER_CONTRACT = vf.createIRI(NAMESPACE, "ConverterContract");
		
		DATATYPE_CAPABILITY = vf.createIRI(NAMESPACE, "datatypeCapability");
		RDF_CAPABILITY = vf.createIRI(NAMESPACE, "rdfCapability");
		
		IMPLEMENTED_CONTRACT = vf.createIRI(NAMESPACE, "implementedContract");
	}
}
