package it.uniroma2.art.coda.pearl.model.propPathStruct;

public class PathEltOrInverseElemPropPath implements GenericElemPropPath {
    private PathEltElemPropPath pathEltElemPropPath;
    private boolean hasInverse;

    public PathEltOrInverseElemPropPath(PathEltElemPropPath pathEltElemPropPath, boolean hasInverse) {
        this.pathEltElemPropPath = pathEltElemPropPath;
        this.hasInverse = hasInverse;
    }

    public PathEltElemPropPath getPathEltElemPropPath() {
        return pathEltElemPropPath;
    }

    public boolean isHasInverse() {
        return hasInverse;
    }

    @Override
    public String getValueAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (hasInverse) {
            stringBuilder.append(" ^");
        }
        stringBuilder.append(pathEltElemPropPath.getValueAsString());
        stringBuilder.append(" ");

        return stringBuilder.toString();
    }
}
