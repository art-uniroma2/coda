package it.uniroma2.art.coda.pearl.model.graph;

import it.uniroma2.art.coda.pearl.model.ProjectionRule;

/**
 * This class is the single element containing a String as a value
 * 
 * @author Andrea Turbati
 * 
 */
public class GraphSingleElemLiteral extends GraphSingleElement {
	private String stringValue;

	/**
	 * @param stringValue
	 *            the value contained in this element
	 * @param prRule
	 *            the projection rule where this element belongs to
	 */
	public GraphSingleElemLiteral(String stringValue, ProjectionRule prRule) {
		super(prRule);
		this.stringValue = stringValue;
	}

	/**
	 * Get the value contained in this element
	 * 
	 * @return the value contained in this element (in the form "value" or "value"@lang or "value"^^iri)
	 */
	public String getStringValue() {
		return stringValue;
	}

	@Override
	public String getValueAsString() {
		return getStringValue();
	}

}
