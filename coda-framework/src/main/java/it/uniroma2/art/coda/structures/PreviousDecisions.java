package it.uniroma2.art.coda.structures;

import java.util.ArrayList;
import java.util.List;

/**
 * This Class contains the previous decision (suggestion and added triple) regarding the previous analyzed
 * annotation
 * 
 * @author Andrea Turbati
 * 
 */
public class PreviousDecisions {
	private int numPrevDecTot = 0;
	private List<PreviousDecisionSingleElement> prevDecSingElemList;
	private int maxPrevDec = -1; // -1 means no limit

	public PreviousDecisions() {
		prevDecSingElemList = new ArrayList<PreviousDecisionSingleElement>();
	}

	/**
	 * Set the maximum number of previous decision to be stored
	 * 
	 * @param maxPrevDev
	 *            the maximum number of previous decision to be stored
	 */
	public void setMaxPrevDev(int maxPrevDev) {
		this.maxPrevDec = maxPrevDev;
	}

	/**
	 * Clear all the previous decision stored up to now
	 */
	public void clearAllPrevDec() {
		prevDecSingElemList.clear();
	}

	/**
	 * Add a PreviousDecisionSingleElement to the list of previous decision
	 * 
	 * @param prevDecSingleElem
	 *            the PreviousDecisionSingleElement to be added to the list
	 */
	public void addPrevDecSingleElem(PreviousDecisionSingleElement prevDecSingleElem) {
		if (maxPrevDec == 0)
			return;
		prevDecSingElemList.add(prevDecSingleElem);
		++numPrevDecTot;
		if (maxPrevDec == -1)
			return;
		if (maxPrevDec < prevDecSingElemList.size())
			prevDecSingElemList.remove(0);
	}

	/**
	 * Get the list of previous decision
	 * 
	 * @return the list of previous decision
	 */
	public List<PreviousDecisionSingleElement> getPrevDecSingleElemList() {
		return prevDecSingElemList;
	}

	/**
	 * Get the number of decision stored in this instance
	 * 
	 * @return the number of decision stored
	 */
	public int getTotNumPrevDec() {
		return numPrevDecTot;
	}

}
