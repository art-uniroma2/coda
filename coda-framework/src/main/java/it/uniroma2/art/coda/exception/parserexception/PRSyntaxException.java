package it.uniroma2.art.coda.exception.parserexception;

public class PRSyntaxException extends PRParserException {

	private String msg;
	
	private static final long serialVersionUID = 1L;

	public PRSyntaxException(String msg) {
		super(new Exception(msg));
		this.msg = msg;
	}

	public PRSyntaxException(Exception e) {
		super(e);
		this.msg = null;
	}
	
	public PRSyntaxException(Exception e, String msg) {
		super(e);
		this.msg = msg;
	}
	
	@Override
	public String getErrorAsString() {
		if(msg!=null) {
			return msg;
		} else { 
			return this.getMessage();
		}
	}
}
