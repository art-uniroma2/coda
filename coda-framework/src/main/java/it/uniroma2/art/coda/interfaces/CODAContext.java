package it.uniroma2.art.coda.interfaces;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import org.apache.uima.jcas.tcas.Annotation;
import org.eclipse.rdf4j.repository.RepositoryConnection;

/**
 * This class holds the execution context which is made available to each converter.
 */
public class CODAContext {
	private RepositoryConnection connection;
	private Map<String, Properties> converter2PropertiesMap;
	private Annotation currentUIMAAnnotation;
	private String defaultNamespace;
	private it.uniroma2.art.coda.pearl.model.annotation.Annotation codaMemoizedAnnotation; // this can be null, if the current Nodes do not have the @Memoized annotation
	private PlaceholderStruct placeholderStruct = null;

	private UIMACODAUtilities.LexModelForSearch lexModelForSearch;

	/**
	 * Constructs an instance of <code>CODAContext</code> for the provided <code>model</code>.
	 * 
	 * @param connection
	 */
	public CODAContext(RepositoryConnection connection) {
		this(connection, new HashMap<String, Properties>());

		initialize();
	}

	/**
	 * Constructs an instance of <code>CODAContext</code> for the provided <code>connection</code>.
	 * 
	 * @param connection
	 * @param converter2PropertiesMap
	 */
	public CODAContext(RepositoryConnection connection, Map<String, Properties> converter2PropertiesMap) {
		this.connection = connection;
		this.converter2PropertiesMap = converter2PropertiesMap;
		this.codaMemoizedAnnotation = null;

		initialize();
	}

	private void initialize() {
		// initialize the lexModelForSearch, so that if the user does not set it via the setLexModelForSearch there is
		// already a possible lexModelForSearch
		lexModelForSearch = UIMACODAUtilities.LexModelForSearch.ALL_MODEL;
	}

	/**
	 * Returns the target Repository Connection.
	 * 
	 * @return
	 */
	public RepositoryConnection getRepositoryConnection() {
		return connection;
	}

	/**
	 * Returns the default namespace of the target Repository Connection or the overridden value
	 * 
	 * @return
	 */
	public String getDefaultNamespace() {
		if (defaultNamespace != null) {
			return defaultNamespace;
		} else {
			return connection.getNamespace("");
		}
	}

	/**
	 * Returns the properties associated with a converter, or <code>null<code> if they have not been
	 * specified.
	 * 
	 * @param converter
	 * @return
	 */
	public Properties getConverterProperties(String converter) {
		return converter2PropertiesMap.get(converter);
	}

	/**
	 * Returns the current UIMA Annotation
	 * 
	 * @return
	 */
	/*public Annotation getCurrentUIMAAnnotation() {
		return currentUIMAAnnotation;
	}*/

	/**
	 * Set the current UIMA Annotation
	 * 
	 * @param currentUIMAAnnotation
	 *            the current UIMA annotation
	 */
	/*public void setCurrentUIMAAnnotation(Annotation currentUIMAAnnotation) {
		this.currentUIMAAnnotation = currentUIMAAnnotation;
	}*/

	/**
	 * Override the default namespace of the connection
	 * @param ns
	 */
    public void setDefaultNamespaceOverride(String ns) {
		this.defaultNamespace = ns;
    }

	/**
	 * Set the current Memoized Annotation (if present)
	 *
	 * @param codaMemoizedAnnotation
	 *            the current Memoized Annotation, or null if not present
	 */
	public void setCodaMemoizedAnnotation(it.uniroma2.art.coda.pearl.model.annotation.Annotation codaMemoizedAnnotation) {
		this.codaMemoizedAnnotation = codaMemoizedAnnotation;
	}

	/**
	 * Get the current Memoized Annotation (null if not present)
	 */
	public it.uniroma2.art.coda.pearl.model.annotation.Annotation getCodaMemoizedAnnotation() {
		return codaMemoizedAnnotation;
	}

	/**
	 * Set the current placeholderStruct
	 * @param placeholderStruct
	 */
	public void setPlaceholderStruct(PlaceholderStruct placeholderStruct) {
		this.placeholderStruct = placeholderStruct;
	}

	/**
	 *  Get the current PlaceholderStruct (null if not set)
	 * @return
	 */
	public PlaceholderStruct getPlaceholderStruct() {
		return placeholderStruct;
	}

	/**
	 * Set the lexModelForSearch which could be used by the converters. Only values from
	 * <code>UIMACODAUtilities.LexModelForSearch</code> can be used, and if
	 * <code>UIMACODAUtilities.LexModelForSearch.CTX_MODEL</code> is passed, then the default value is set
	 * (which is <code>UIMACODAUtilities.LexModelForSearch.ALL_MODEL</code>)
	 * @param lexModelForSearch the lexModelForSearch to set
	 */
	public void setLexModelForSearch(UIMACODAUtilities.LexModelForSearch lexModelForSearch) {
		if (lexModelForSearch.equals(UIMACODAUtilities.LexModelForSearch.CTX_MODEL)) {
			// it is not possible to set CTX_MODEL as the lexModelForSearch in the CODAContext (it will be a
			// cycle definition) so call initialize()
			initialize();
		} else {
			this.lexModelForSearch = lexModelForSearch;
		}
	}

	public UIMACODAUtilities.LexModelForSearch getLexModelForSearch() {
		return lexModelForSearch;
	}
}
