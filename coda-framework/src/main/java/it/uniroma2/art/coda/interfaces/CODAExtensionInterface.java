package it.uniroma2.art.coda.interfaces;

import java.util.Map;

import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.pearl.model.ConverterMention;

/**
 * This abstract class is the general Extension used in CODA
 * @author Andrea Turbati
 *
 */
public abstract class CODAExtensionInterface{

	protected String id;
	
	protected String description;	
	
	public CODAExtensionInterface(String id, String description){
		this.id = id;
		this.description = description;
	
	}
	
	/**
	 * This method return the id of the module
	 * @return the id of the module
	 */
	public String getId(){
		return id;
	}
	
	/**
	 * Set the description of the module
	 * @param description the description of the module
	 */
	public void setDescription(String description){
		this.description = description;
	}
	
	/**
	 * Get the description of the module
	 * @return the description of the module
	 */
	public String getDescription(){
		return description;
	}
}
