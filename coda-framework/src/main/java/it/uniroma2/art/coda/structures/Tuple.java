package it.uniroma2.art.coda.structures;


import java.util.HashMap;
import java.util.Map;

import org.eclipse.rdf4j.model.Value;

/**
 * This class represents a single tuple of a result obtained from a SPARQL query
 * @author Andrea Turbati
 *
 */
public class Tuple {
	private Map<String, Value> varMap;
	
	public Tuple(){
		varMap = new HashMap<String, Value>();
	}
	
	/**
	 * Add a variable and its associated ontology resource
	 * @param varId the variable id
	 * @param artNode the ontology resource associated to the variable 
	 * @return true if the variable was not already present in the map, false otherwise
	 */
	public boolean addVarValue(String varId, Value artNode){
		if(varMap.containsKey(varId))
			return false;
		varMap.put(varId, artNode);
		return true;
	}
	
	/**
	 * Get the map containing the variable name and its associated ontology resource
	 * @return the map containing the variable name and its associated ontology resource
	 */
	public Map <String, Value> getTupleMap(){
		return varMap;
	}
}
