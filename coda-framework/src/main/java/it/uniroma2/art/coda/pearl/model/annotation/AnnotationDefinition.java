package it.uniroma2.art.coda.pearl.model.annotation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;


public class AnnotationDefinition {

	private String name; // without the starting with @
	private Map<String, MetaAnnotation> metaAnnotationMap; // this Map will contains the MetaAnnotationTarget, MetaAnnotationRetained and MetaAnnotationGeneric
	private Map<String, ParamDefinition> paramDefinitionMap; // the Map of ParamDefinition for this annotation
	
	private List<String> defaultValueForTargetList;
	private final boolean defaultValueForRetained = false;

	public AnnotationDefinition(String name, List <MetaAnnotation> metaAnnotationList) {
		this.name = name;
		metaAnnotationMap = new HashMap<>();
		//add the default valued for the target MetaAnnotation
		defaultValueForTargetList = new ArrayList<>();
		defaultValueForTargetList.add(PearlParserDescription.TARGET_TRIPLE);
		defaultValueForTargetList.add(PearlParserDescription.TARGET_NODE);
		if(metaAnnotationList != null){
			for(MetaAnnotation metaAnnotation : metaAnnotationList){
				if(!metaAnnotationMap.containsKey(metaAnnotation.getName())) {
					metaAnnotationMap.put(metaAnnotation.getName(), metaAnnotation);
				}
			}
		}
		paramDefinitionMap = new HashMap<>();
	}

	public String getName(){
		return name;
	}

	public MetaAnnotation getMetaAnnotation(String metaAnnotationName){
		return metaAnnotationMap.get(metaAnnotationName);
	}
	
	public List<MetaAnnotation> getMetaAnnotationList(){
		List<MetaAnnotation> metaAnnotationsList = new ArrayList<>();
		for(MetaAnnotation metaAnnotation : metaAnnotationMap.values()){
			metaAnnotationsList.add(metaAnnotation);
		}
		return metaAnnotationsList;
	}

	public boolean addParamDefinition(ParamDefinition paramDefinition){
		//check that the param does not already exist
		if(paramDefinitionMap.containsKey(paramDefinition.getName())){
			return false;
		}
		paramDefinitionMap.put(paramDefinition.getName(), paramDefinition);
		return true;
	}

	public ParamDefinition getParamDefinition(String paramName){
		return paramDefinitionMap.get(paramName);
	}

	public List<ParamDefinition> getParamDefinitionList(){
		List<ParamDefinition> paramDefinitionList = new ArrayList<>();
		for(ParamDefinition paramDefinition : paramDefinitionMap.values()){
			paramDefinitionList.add(paramDefinition);
		}
		return paramDefinitionList;
	}

	public boolean checkTargetValueCompatibility(String desiredContext){
		for(MetaAnnotation metaAnnotation : metaAnnotationMap.values()){
			//get the metaAnnotation @Target
			if(metaAnnotation instanceof  MetaAnnotationTarget) {
				MetaAnnotationTarget metaAnnotationTarget = (MetaAnnotationTarget) metaAnnotation;
				List<String> targetValueList = metaAnnotationTarget.getTargetList();
				for(String context : targetValueList){
					if(removeQuotes(context).toLowerCase().equals(removeQuotes(desiredContext).toLowerCase())){
						return true;
					}
				}
			}
		}
		return false;
	}

	public List<String> getTargetValues(){
		List<String> targetValueList = new ArrayList<>();
		for(MetaAnnotation metaAnnotation : metaAnnotationMap.values()){
			//get the metaAnnotation @Target
			if(metaAnnotation instanceof  MetaAnnotationTarget) {
				MetaAnnotationTarget metaAnnotationTarget = (MetaAnnotationTarget) metaAnnotation;
				targetValueList.addAll(metaAnnotationTarget.getTargetList());
			}
		}
		if(targetValueList.isEmpty()){
			//since no value regarding the TARGET_ANN_DECL metaannotation, use the default one
			targetValueList.addAll(defaultValueForTargetList);

		}
		return targetValueList;
	}
	
	public boolean checkTargetCompatibilityWithGraph(){
		return checkTargetValueCompatibility(PearlParserDescription.TARGET_SUBJECT) || checkTargetValueCompatibility(PearlParserDescription.TARGET_PREDICATE)
				|| checkTargetValueCompatibility(PearlParserDescription.TARGET_OBJECT)  || checkTargetValueCompatibility(PearlParserDescription.TARGET_TRIPLE) ;
	}
	
	public boolean checkTargetCompatibilityWithNodes(){
		return checkTargetValueCompatibility(PearlParserDescription.TARGET_NODE) || checkTargetValueCompatibility(PearlParserDescription.TARGET_CONVERTER)
				|| checkTargetValueCompatibility(PearlParserDescription.TARGET_FS) || checkTargetValueCompatibility(PearlParserDescription.TARGET_NODE_DEF) ;
	}

	public boolean checkTargetCompatibilityWithEntireRule() {
		return checkTargetValueCompatibility(PearlParserDescription.TARGET_ENTIRE_RULE);
	}
	
	/*public boolean checkTargetValueCompatibility(String desiredValue){
		for(MetaAnnotation metaAnnotation : metaAnnotationList){
			if(metaAnnotation.getName().equals("@Target")){
				List<String> paramValueList = metaAnnotation.getParamList("value");
				//paramValueList is empty or does not exist, then its default value is "triple"
				if(paramValueList == null || paramValueList.isEmpty() ){
					return removeQuotes(defaultValueForTarget).toLowerCase().equals(removeQuotes(desiredValue).toLowerCase());
				}
				
				for(String context : paramValueList){
					if(removeQuotes(context).toLowerCase().equals(removeQuotes(desiredValue).toLowerCase())){
						return true;
					}
				}
			}
		}
		return false;
	}*/
	
	private String removeQuotes(String inputString){
		String cleanString = inputString;
		if(cleanString.startsWith("\"") || cleanString.startsWith("\'")){
			cleanString = cleanString.substring(1);
		}
		if(cleanString.endsWith("\"") || cleanString.endsWith("\'")){
			cleanString = cleanString.substring(0, cleanString.length()-1);
		}
		return cleanString;
	}

	public boolean hasRetention() {
		
		for(MetaAnnotation metaAnnotation : metaAnnotationMap.values()){
			if(metaAnnotation instanceof  MetaAnnotationRetained){
				MetaAnnotationRetained metaAnnotationRetained = (MetaAnnotationRetained) metaAnnotation;
				return metaAnnotationRetained.hasRetained();
			}
		}
		return false;
	}
	
	public List<String> getDefaultValueForTarget(){
		return defaultValueForTargetList;
	}

	public boolean getDefaultValueForRetained() {
		return defaultValueForRetained;
	}
}
