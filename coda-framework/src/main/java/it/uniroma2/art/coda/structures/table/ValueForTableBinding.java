package it.uniroma2.art.coda.structures.table;

import java.util.Map;

import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.pearl.model.BindingStruct;

public class ValueForTableBinding extends ValueForTable {

	//This structure can contain more than a single value, this is because these values should not be divided
	// when put in the table. 
	// All these values belongs to the same BindingStructure, so just one Binding structure is stores, but
	
	private BindingStruct bindingStruct;
	private Map<String, Value> idToValueMap;
	
	public ValueForTableBinding(BindingStruct bindingStruct, Map<String, Value> idToARTNodeMap) {
		this.bindingStruct = bindingStruct;
		this.idToValueMap = idToARTNodeMap;
	}

	public BindingStruct getBindingStruct() {
		return bindingStruct;
	}
	
	public Map <String, Value>getIdToValueMap(){
		return idToValueMap;
	}
	
}
