package it.uniroma2.art.coda.pearl.model;

import java.util.Collection;

public class OptionalGraphStruct extends GraphElement {
	
	//private Collection<GraphStruct> optionalTriples;
	private Collection<GraphElement> optionalTriples;
	
	public OptionalGraphStruct(Collection<GraphElement> optionalTriples, ProjectionRule ownerRule) {
		this.optionalTriples = optionalTriples;
		this.ownerRule = ownerRule;
	}
	
	public Collection<GraphElement> getOptionalTriples() {
		return optionalTriples;
	}
	

}
