package it.uniroma2.art.coda.pearl.model;

abstract public class GraphElement {
	
	protected ProjectionRule ownerRule;
		
	public ProjectionRule getOwnerRule() {
		return ownerRule;
	}
	
	public boolean isOptionalGraphStruct(){
		return (this instanceof OptionalGraphStruct);
	}
	
	public OptionalGraphStruct asOptionalGraphStruct(){
		return (OptionalGraphStruct) this;
	}
	
	public boolean isGraphStruct(){
		return (this instanceof GraphStruct);
	}
	
	public GraphStruct asGraphStruct(){
		return (GraphStruct) this;
	}
}
