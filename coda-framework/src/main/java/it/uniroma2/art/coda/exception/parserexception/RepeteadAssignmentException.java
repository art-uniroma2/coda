package it.uniroma2.art.coda.exception.parserexception;

public class RepeteadAssignmentException extends PRParserException {

	private String plcName;
	private String ruleId;
	
	private static final long serialVersionUID = 1L;

	public RepeteadAssignmentException(String plcName, String ruleId) {
		super();
		this.plcName = plcName;
		this.ruleId = ruleId;
	}

	public RepeteadAssignmentException(Exception e, String plcName, String ruleId) {
		super(e);
		this.plcName = plcName;
		this.ruleId = ruleId;
	}
	
	public String getPlcName(){
		return plcName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	@Override
	public String getErrorAsString() {
		return "there are at least two placeholder named "+ plcName+" in the rule "+ruleId;
	}
}
