package it.uniroma2.art.coda.provisioning;

import java.util.Collection;

/**
 * A <i>Component Provider</i> provisions the PEARL execution engine with the converters necessary to support
 * the execution of a PEARL document. Currently, the provisioning interface only supports global policies, in
 * which a dependency is bound to a converter irrespectively of the context in which it is mentioned.
 * Therefore, it is possible for a component provider to cache previous resolutions, and always return the
 * same object for successive requests concenring the same URI.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public interface ComponentProvider {

	/**
	 * Activates this component provider so that lookup and other operations can be invoked.
	 */
	public void open() throws ComponentIndexingException;
	
	/**
	 * Looks up a suitable converter to satisfy the dependency expressed via the given URI.
	 * 
	 * @param dependencyURI
	 *            the URI identifies the required dependency
	 * @return a converter object to be bound to the dependency
	 * @throws ComponentProvisioningException
	 */
	public Object lookup(String dependencyURI)
			throws ComponentProvisioningException;

	/**
	 * Binds a dependency URI (which is assumed to denote a contract) to a given converter.
	 *  
	 * @param contractURI
	 * @param converterURI
	 */
	public void setGlobalContractBinding(String contractURI, String converterURI);
	
	/**
	 * Returns the descriptions of the currently known converter contracts.
	 * @return
	 */
	public Collection<ConverterContractDescription> listConverterContracts();

	/**
	 * Returns the description of the currently known converters.
	 * @return
	 */
	Collection<ConverterDescription> listConverters();

	/**
	 * Releases the resources associated with this component provider.
	 */
	public void close();

}
