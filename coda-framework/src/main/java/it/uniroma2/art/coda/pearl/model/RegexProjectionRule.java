package it.uniroma2.art.coda.pearl.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.uniroma2.art.coda.pearl.model.annotation.Annotation;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.SingleRegexStruct;

public class RegexProjectionRule extends BaseProjectionRule{

	private SingleRegexStruct singleRegexStruct;
	private Collection<GraphElement> graphList;

	private List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> annotationList;
	
	public RegexProjectionRule(String id, SingleRegexStruct singleRegexStruct,
			Collection<GraphElement> graphList, ProjectionRulesModel projectionRulesModel) {
		super(id, projectionRulesModel);
		this.id = id;
		this.singleRegexStruct = singleRegexStruct;
		this.graphList = graphList;
		this.annotationList = new ArrayList<>();
	}

	

	public SingleRegexStruct getSingleRegexStruct() {
		return singleRegexStruct;
	}

	public Collection<GraphElement> getGraphList() {
		return graphList;
	}

	public void setAnnotations(List<Annotation> annotationList){
		this.annotationList = annotationList;
	}

	public boolean hasAnnotations(){
		if (annotationList == null)
			return false;
		if (annotationList.isEmpty())
			return false;
		return true;
	}

	public List<it.uniroma2.art.coda.pearl.model.annotation.Annotation> getAnnotationList(){
		return annotationList;
	}
	
}
