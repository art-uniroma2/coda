package it.uniroma2.art.coda.exception.parserexception;

public class ConfidenceValueNotAcceptableException extends PRParserException {

	private String ruleId;
	private  double value;


	private static final long serialVersionUID = 1L;

	public ConfidenceValueNotAcceptableException(String ruleId, double value) {
		super();
		this.ruleId = ruleId;
		this.value = value;
	}

	public ConfidenceValueNotAcceptableException(Exception e, String ruleId, double value) {
		super(e);
		this.ruleId = ruleId;
		this.value = value;
	}
	
	public double getValue(){
		return value;
	}
	
	public String getRuleId() {
		return ruleId;
	}



	@Override
	public String getErrorAsString() {
		return "the confidence value  "+value+" in rule "+ruleId+" is not an acceptable value ( should be >0.0 and <1.0 )";
	}
}
