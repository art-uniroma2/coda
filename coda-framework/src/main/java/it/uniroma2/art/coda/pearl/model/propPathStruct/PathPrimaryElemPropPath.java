package it.uniroma2.art.coda.pearl.model.propPathStruct;

public class PathPrimaryElemPropPath implements GenericElemPropPath{

    //only one of the following has a value, the other are always null;
    private String uriOrAbbr = null; // the URI is already in the form <XYZ>
    private PathNegatedPropertySetElemPropPath pathNegatedPropertySetElemPropPath = null;
    private PathAlternativeElemPropPath pathAlternativeElemPropPath = null;

    public PathPrimaryElemPropPath(String uriOrAbbr) {
        this.uriOrAbbr = uriOrAbbr;
    }

    public PathPrimaryElemPropPath(PathNegatedPropertySetElemPropPath pathNegatedPropertySetElemPropPath) {
        this.pathNegatedPropertySetElemPropPath = pathNegatedPropertySetElemPropPath;
    }

    public PathPrimaryElemPropPath(PathAlternativeElemPropPath pathAlternativeElemPropPath) {
        this.pathAlternativeElemPropPath = pathAlternativeElemPropPath;
    }



    public PathAlternativeElemPropPath getPathAlternativeElemPropPath() {
        return pathAlternativeElemPropPath;
    }

    public PathNegatedPropertySetElemPropPath getPathNegatedPropertySetElemPropPath() {
        return pathNegatedPropertySetElemPropPath;
    }

    public String getUriOrAbbr() {
        return uriOrAbbr;
    }

    @Override
    public String getValueAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        //check which of the various element is not null, and return such element
        if (uriOrAbbr != null) {
            stringBuilder.append(" ").append(uriOrAbbr).append(" ");
        } else if (pathNegatedPropertySetElemPropPath != null) {
            stringBuilder.append(" !"+pathNegatedPropertySetElemPropPath.getValueAsString());
        } else { // pathAlternativeElemPropPath != null
            stringBuilder.append(" (").append(pathAlternativeElemPropPath.getValueAsString()).append(") ");
        }
        stringBuilder.append(" ");

        return stringBuilder.toString();
    }
}
