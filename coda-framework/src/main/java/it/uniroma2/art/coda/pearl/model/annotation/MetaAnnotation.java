package it.uniroma2.art.coda.pearl.model.annotation;


public abstract class MetaAnnotation {

	private String name;

	public MetaAnnotation(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
