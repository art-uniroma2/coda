package it.uniroma2.art.coda.exception.parserexception;

public class ConfidenceGenericErrorException extends PRParserException {

	private String ruleId;

	private static final long serialVersionUID = 1L;

	public ConfidenceGenericErrorException(String ruleId) {
		super();
		this.ruleId = ruleId;
	}

	public ConfidenceGenericErrorException(Exception e, String ruleId) {
		super(e);
		this.ruleId = ruleId;
	}
	
	public String getRuleId() {
		return ruleId;
	}



	@Override
	public String getErrorAsString() {
		return "there was a problem regarding the annotation confidence in the rule "+ruleId;
	}
}
