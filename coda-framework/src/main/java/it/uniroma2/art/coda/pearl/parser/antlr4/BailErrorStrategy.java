package it.uniroma2.art.coda.pearl.parser.antlr4;

import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.NoViableAltException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;

import it.uniroma2.art.coda.exception.parserexception.PRSyntaxRuntimeException;

public class BailErrorStrategy extends DefaultErrorStrategy {
	
	@Override
	public void recover(Parser recognizer, RecognitionException e) {
		if(e instanceof NoViableAltException) {
			NoViableAltException noViableAltException = (NoViableAltException) e ;
			Token token = noViableAltException.getOffendingToken();
			String text = "line "+token.getLine()+": no viable alternative at input '"+token.getText()+"'";
			throw new PRSyntaxRuntimeException(text);
		}
		throw new PRSyntaxRuntimeException(e.getMessage());
	}
	
	@Override
	public Token recoverInline(Parser recognizer) throws RecognitionException {
		Token token = recognizer.getCurrentToken();
		String text = "line "+token.getLine()+": error with input '"+token.getText()+"'";
		throw new PRSyntaxRuntimeException(text);
		//throw new PRSyntaxRuntimeException(new InputMismatchException(recognizer));
	}
	
	@Override
	public void sync(Parser recognizer) {}
	
}

