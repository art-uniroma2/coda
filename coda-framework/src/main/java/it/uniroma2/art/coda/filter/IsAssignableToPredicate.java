package it.uniroma2.art.coda.filter;

import com.google.common.base.Predicate;

public class IsAssignableToPredicate implements Predicate<Class<?>>{

	private Class<?> sourceClazz;
	
	public IsAssignableToPredicate(Class<?> sourceClazz) {
		this.sourceClazz = sourceClazz;
	}
	
	public static final IsAssignableToPredicate getFilter(Class<?> sourceClazz) {
		return new IsAssignableToPredicate(sourceClazz);
	}
	@Override
	public boolean apply(Class<?> input) {
		return sourceClazz.isAssignableFrom(input);
	}

}
