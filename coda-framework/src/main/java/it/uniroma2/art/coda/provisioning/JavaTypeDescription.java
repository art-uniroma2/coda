package it.uniroma2.art.coda.provisioning;

import java.lang.reflect.Type;

/**
 * A description of a Java type.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface JavaTypeDescription extends TypeDescription {
	/**
	 * Returns the corresponding Java {@link Type}
	 * @return
	 */
	Type getJavaType();
}
