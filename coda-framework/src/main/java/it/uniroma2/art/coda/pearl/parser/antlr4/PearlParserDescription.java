package it.uniroma2.art.coda.pearl.parser.antlr4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.coda.exception.parserexception.AnnotationNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.AnnotationTargetNotCompatibleException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceNotDoubleNumException;
import it.uniroma2.art.coda.exception.parserexception.MissingMandatoryParamInAnnotationException;
import it.uniroma2.art.coda.exception.parserexception.NoRuleInFileException;
import it.uniroma2.art.coda.exception.parserexception.PRGenericException;
import it.uniroma2.art.coda.exception.parserexception.ParamInAnnotationNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.PropPathNotAllowedException;
import it.uniroma2.art.coda.exception.parserexception.TypeOfDefaultParamInAnnotationWrongTypeException;
import it.uniroma2.art.coda.exception.parserexception.TypeOfParamInAnnotationWrongTypeException;
import it.uniroma2.art.coda.exception.parserexception.UnsupportedTypeInParamDefinitionException;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotationGeneric;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotationRetained;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotationTarget;
import it.uniroma2.art.coda.pearl.model.annotation.ParamDefinition;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueDouble;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInteger;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueIri;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueLiteral;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValuePlaceholder;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueString;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemPropPath;
import it.uniroma2.art.coda.pearl.model.propPathStruct.PathAlternativeElemPropPath;
import it.uniroma2.art.coda.pearl.model.propPathStruct.PathEltElemPropPath;
import it.uniroma2.art.coda.pearl.model.propPathStruct.PathEltOrInverseElemPropPath;
import it.uniroma2.art.coda.pearl.model.propPathStruct.PathNegatedPropertySetElemPropPath;
import it.uniroma2.art.coda.pearl.model.propPathStruct.PathOneInPropertySetElemPropPath;
import it.uniroma2.art.coda.pearl.model.propPathStruct.PathPrimaryElemPropPath;
import it.uniroma2.art.coda.pearl.model.propPathStruct.PathSequenceElemPropPath;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.exception.parserexception.DeleteAndGraphSectionException;
import it.uniroma2.art.coda.exception.parserexception.NodeNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.PRIOException;
import it.uniroma2.art.coda.exception.parserexception.PRParamToReplaceException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.exception.parserexception.PRSyntaxException;
import it.uniroma2.art.coda.exception.parserexception.PRSyntaxRuntimeException;
import it.uniroma2.art.coda.exception.parserexception.PrefixNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.RepeteadAssignmentException;
import it.uniroma2.art.coda.pearl.model.BindingStruct;
import it.uniroma2.art.coda.pearl.model.ConditionStruct;
import it.uniroma2.art.coda.pearl.model.ConverterArgumentExpression;
import it.uniroma2.art.coda.pearl.model.ConverterMapArgument;
import it.uniroma2.art.coda.pearl.model.ConverterMention;
import it.uniroma2.art.coda.pearl.model.ConverterPlaceholderArgument;
import it.uniroma2.art.coda.pearl.model.ConverterRDFLiteralArgument;
import it.uniroma2.art.coda.pearl.model.ConverterRDFURIArgument;
import it.uniroma2.art.coda.pearl.model.GraphElement;
import it.uniroma2.art.coda.pearl.model.GraphStruct;
import it.uniroma2.art.coda.pearl.model.OptionalGraphStruct;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import it.uniroma2.art.coda.pearl.model.ProjectionOperator;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.pearl.model.RegexProjectionRule;
import it.uniroma2.art.coda.pearl.model.ConditionStruct.Operator;
import it.uniroma2.art.coda.pearl.model.annotation.Annotation;
import it.uniroma2.art.coda.pearl.model.annotation.AnnotationDefinition;
import it.uniroma2.art.coda.pearl.model.annotation.AnnotationParam;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotation;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemBNode;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemLiteral;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemPlaceholder;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemUri;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElemVar;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElement;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.FSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.SingleRegexStruct;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.StateFSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.AbbrContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.AnnotationContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.AnnotationsDefinitionContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.BaseRuleContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.BindingDefContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.BindingsClauseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.BlankNodeContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ConditionClauseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ConditionDefContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ConverterAdditionalArgumentsContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ConverterArgumentExpressionContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ConverterMapArgumentContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ConverterPlaceholderArgumentContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ConvertersContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.DeleteClauseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.DependContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.DependParamContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.FeaturePathContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ForRegexRuleContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.GraphClauseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.GraphElementContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.GraphObjectContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.GraphPredicateContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.GraphSubjectContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.GraphTripleContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.IndividualConverterContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.InsertClauseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.IriContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.LazyRuleContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.LiteralContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.MapEntryContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.MetaAnnotationContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.NodeDefContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.NodesClauseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.OptionalGraphElementContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PearlUnitContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PlaceholderContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PrefixDeclContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ProjectionOperatorContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PrologueContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.RegexBaseElementContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.RegexBaseElementWithSymbolContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.RegexContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.RegexPatternContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.RegexSequenceElementContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.RegexWithOrContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.SingleAnnotationDefinitionContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.SingleParamContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.SingleParamOrListOfNameParamContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.SingleParamOrListOfParamContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.StandardRuleContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.StringContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.VarContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.WhereClauseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.ParamsDefinitionContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PropPathContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PathAlternativeContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PathSequenceContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PathEltOrInverseContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PathEltContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PathPrimaryContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PathNegatedPropertySetContext;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParser.PathOneInPropertySetContext;


import it.uniroma2.art.coda.structures.DependsOnInfo;

public class PearlParserDescription extends PearlBaseListener {

	private List<String> automaticTargetContextGraphList;
	private List<String> automaticRetentionList; //it should contain a subset of the previous List

	//list of annotation that can be used in the declaration of an annotation
	public static final String RETAINED_ANN_DECL = "Retained"; // it indicates if the annotation should be passed to the model or can be eliminated when generating the PEARL structure
	public static final String TARGET_ANN_DECL = "Target";

	//list of annotation that can be used when applying an annotation to a specific element (in the nodes section or in the graph one)
	public static final String CONFIDENCE_ANN = "Confidence";
	public static final String MEMOIZED_ANN = "Memoized";
	public static final String DEFAULT_NAMESPACE_ANN = "DefaultNamespace"; // This annotation overrides the default namespace in the CODA context
	public static final String TRIM_ANN = "Trim";
	public static final String REMOVE_DUP_SPACES_ANN = "RemoveDuplicateSpaces";
	public static final String LOWER_CASE_ANN = "LowerCase";
	public static final String UPPER_CASE_ANN = "UpperCase";
	public static final String REMOVE_PUNCTUATION_ANN = "RemovePunctuation";


	//parameter for the hard-coded defined annotations
	public static final String MEMOIZED_ANN_VALUE_PARAM_DEFAULT = "default";
	public static final String MEMOIZED_ANN_PARAM_IGNORECASE = "ignoreCase";
	public static final String MEMOIZED_ANN_PARAM_IGNORECASE_DEFAULT_VALUE = "true";
	public static final String TRIM_ANN_PARAM_IGNORECASE_DEFAULT_VALUE = "true";
	public static final String REMOVE_DUP_SPACES_ANN_PARAM_IGNORECASE_DEFAULT_VALUE = "true";
	public static final String LOWER_CASE_ANN_PARAM_IGNORECASE_DEFAULT_VALUE = "true";
	public static final String CAPITALIZE_ANN_PARAM_IGNORECASE_DEFAULT_VALUE = "true";
	public static final String REMOVE_PUNCTUATION_ANN_PARAM_IGNORECASE_DEFAULT_VALUE = "true";


	//list of parameters to be used inside the annotations
	public static final String TARGET_PARAM = "target";

	//name of the default value when no names is passed
	public static final String DEFAULT_NAME_FOR_PARAM = "value";

	//possible types for the params of an annotation
	public static final String TYPE_STRING = "string";
	public static final String TYPE_STRING_ARRAY = "string[]";
	public static final String TYPE_INT = "int";
	public static final String TYPE_INT_ARRAY = "int[]";
	public static final String TYPE_DOUBLE = "double";
	public static final String TYPE_DOUBLE_ARRAY = "double[]";
	public static final String TYPE_IRI = "iri";
	public static final String TYPE_IRI_ARRAY = "iri[]";
	public static final String TYPE_LITERAL = "literal";
	public static final String TYPE_LITERAL_ARRAY = "literal[]";



	//public static final String CONTEXT = "context"; // OLD
	//public static final String GRAPH = "graph"; // OLD
	//public static final String NODES = "nodes"; // OLD

	public static final String SECTION_GRAPH = "graph";
	public static final String SECTION_NODES = "nodes";
	public static final String SECTION_ENTIRE_RULE = "rule";

	//the target of the annotation TARGET_ANN
	//target related to the nodes sections
	public static final String TARGET_NODE = "node"; // the placeholder
	public static final String TARGET_CONVERTER = "converter"; // the type+converter
	public static final String TARGET_FS = "fs"; // the feature path
	public static final String TARGET_NODE_DEF = "node_def"; // all the node generation triple (placeholder, type+converter, feature path)
	//target related to the graph sections
	public static final String TARGET_SUBJECT = "subject"; // the subject of the triple
	public static final String TARGET_PREDICATE = "predicate"; // the predicate of the triple
	public static final String TARGET_OBJECT = "object"; //  the object of the triple
	public static final String TARGET_TRIPLE = "triple"; //  the entire triple
	//target related to the entire rule
	public static final String TARGET_ENTIRE_RULE = "entire_rule"; // the subject of the triple



	//public static Map<String, List<String>> defultAnnotationToTargetMap;
	//private List<String> defaulRetainedList; //it should contain a subset of the previous Map (its keys)
	
	
	private final String REGEX_SYMBOL_ZEROORONE = "?";
	private final String REGEX_SYMBOL_ONEORMORE = "+";
	private final String REGEX_SYMBOL_ZEROORMORE = "*";

	private final String REGEX_PREFIX_NFSA_STATE = "S_N_";
	private final String REGEX_PREFIX_NFSA_TRANSITION = "T_N_";

	private final String REGEX_PREFIX_DFSA_STATE = "S_D_";
	private final String REGEX_PREFIX_DFSA_TRANSITION = "T_D_";

	public static final String DEFAULT_CONVERTER_URI = "http://art.uniroma2.it/coda/contracts/default";
	//private static final String DEPENDSON = "dependsOn"; //It seems that it not used
	
	private static Logger logger = LoggerFactory.getLogger(PearlParserDescription.class);
	
	private Map<String, String> prefixMap;
	private ProjectionRulesModel prModel;

	private ProjectionRulesModel previousPrModel = null;

	public PearlParserDescription() {
		this.initialize();
	}

	public PearlParserDescription(ProjectionRulesModel previousPrModel){
		this.previousPrModel = previousPrModel;
		this.initialize();
	}

	public void initialize(){

		prefixMap = new HashMap<>();
		prModel  = new ProjectionRulesModel();

		// the Confidence Annotation definition
		AnnotationDefinition annotationDefinition = createConfidenceAnnotationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}

		// the Memoized Annotation definition
		annotationDefinition = createMemoizedAnnotationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}

		// the DefaultNamespace Annotation definition
		annotationDefinition = createDefaultNamespaceAnntationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}

		// the Trim Annotation definition
		annotationDefinition = createTrimAnntationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}

		// the RemoveDuplicateSpaces Annotation definition
		annotationDefinition = createRemoveDuplicateSpacesAnntationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}

		// the lowerCase Annotation definition
		annotationDefinition = createLowerCaseAnntationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}

		// the upperCase Annotation definition
		annotationDefinition = createUpperCaseAnntationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}

		// the remove punctuation Annotation definition
		annotationDefinition = createRemovePunctuationAnntationDef();
		if(annotationDefinition!=null){
			prModel.addAnnotationDefinition(annotationDefinition);
		}
	}

	private AnnotationDefinition createConfidenceAnnotationDef(){
		//create CONFIDENCE_ANN (if not already present), having target TARGET_TRIPLE, param DEFAULT_NAME_FOR_PARAM
		// and RETAINED_ANN_DECL = true
		if(prModel.isAnnotationPresent(CONFIDENCE_ANN)){
			//in the prModel there is already CONFIDENCE_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_TRIPLE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_TRIPLE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);
		//add the parameter DEFAULT_NAME_FOR_PARAM with type TYPE_DOUBLE
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_DOUBLE, DEFAULT_NAME_FOR_PARAM);

		AnnotationDefinition confidenceAnnotationDef = new AnnotationDefinition(CONFIDENCE_ANN, metaAnnotationList);
		confidenceAnnotationDef.addParamDefinition(paramDefinition);
		return confidenceAnnotationDef;
	}

	private AnnotationDefinition createMemoizedAnnotationDef(){
		//create MEMOIZED_ANN (if not already present), having target TARGET_NODE and RETAINED_ANN_DECL = true
		if(prModel.isAnnotationPresent(MEMOIZED_ANN)){
			//in the prModel there is already CONFIDENCE_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_NODE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_NODE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);

		AnnotationDefinition memoizedAnnotationDef = new AnnotationDefinition(MEMOIZED_ANN, metaAnnotationList);
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_STRING, DEFAULT_NAME_FOR_PARAM, new ParamValueString(MEMOIZED_ANN_VALUE_PARAM_DEFAULT));
		memoizedAnnotationDef.addParamDefinition(paramDefinition);
		paramDefinition = new ParamDefinition(TYPE_STRING, MEMOIZED_ANN_PARAM_IGNORECASE, new ParamValueString(MEMOIZED_ANN_PARAM_IGNORECASE_DEFAULT_VALUE));
		memoizedAnnotationDef.addParamDefinition(paramDefinition);
		return memoizedAnnotationDef;
	}

	private AnnotationDefinition createDefaultNamespaceAnntationDef() {
		//create DEFAULT_NAMESPACE_ANN (if not already present), having target TARGET_NODE and RETAINED_ANN_DECL = true
		if(prModel.isAnnotationPresent(DEFAULT_NAMESPACE_ANN)){
			//in the prModel there is already DEFAULT_NAMESPACE_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_NODE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_NODE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);

		AnnotationDefinition defNSAnnotationDef = new AnnotationDefinition(DEFAULT_NAMESPACE_ANN, metaAnnotationList);
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_IRI, DEFAULT_NAME_FOR_PARAM);
		defNSAnnotationDef.addParamDefinition(paramDefinition);
		return defNSAnnotationDef;
	}

	private AnnotationDefinition createTrimAnntationDef() {
		//create TRIM_ANN (if not already present), having target TARGET_NODE and TARGET_ENTIRE_RULE
		// and RETAINED_ANN_DECL = true
		if(prModel.isAnnotationPresent(TRIM_ANN)){
			//in the prModel there is already TRIM_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_NODE and TARGET_ENTIRE_RULE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_NODE));
		targetList.add(new ParamValueString(TARGET_ENTIRE_RULE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);

		AnnotationDefinition trimAnnotationDef = new AnnotationDefinition(TRIM_ANN, metaAnnotationList);
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_STRING, DEFAULT_NAME_FOR_PARAM, new ParamValueString(TRIM_ANN_PARAM_IGNORECASE_DEFAULT_VALUE));
		trimAnnotationDef.addParamDefinition(paramDefinition);
		return trimAnnotationDef;
	}

	private AnnotationDefinition createRemoveDuplicateSpacesAnntationDef() {
		//create REMOVE_DUP_SPACES_ANN (if not already present), having target TARGET_NODE and TARGET_ENTIRE_RULE
		// and RETAINED_ANN_DECL = true
		if(prModel.isAnnotationPresent(REMOVE_DUP_SPACES_ANN)){
			//in the prModel there is already REMOVE_DUP_SPACES_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_NODE and TARGET_ENTIRE_RULE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_NODE));
		targetList.add(new ParamValueString(TARGET_ENTIRE_RULE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);

		AnnotationDefinition remDupSpacesAnnotationDef = new AnnotationDefinition(REMOVE_DUP_SPACES_ANN, metaAnnotationList);
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_STRING, DEFAULT_NAME_FOR_PARAM, new ParamValueString(REMOVE_DUP_SPACES_ANN_PARAM_IGNORECASE_DEFAULT_VALUE));
		remDupSpacesAnnotationDef.addParamDefinition(paramDefinition);
		return remDupSpacesAnnotationDef;
	}

	private AnnotationDefinition createLowerCaseAnntationDef() {
		//create LOWER_CASE_ANN (if not already present), having target TARGET_NODE and RETAINED_ANN_DECL = true
		if (prModel.isAnnotationPresent(LOWER_CASE_ANN)) {
			//in the prModel there is already LOWER_CASE_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_NODE and TARGET_ENTIRE_RULE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_NODE));
		targetList.add(new ParamValueString(TARGET_ENTIRE_RULE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);

		AnnotationDefinition lowerCaseAnnotationDef = new AnnotationDefinition(LOWER_CASE_ANN, metaAnnotationList);
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_STRING, DEFAULT_NAME_FOR_PARAM, new ParamValueString(LOWER_CASE_ANN_PARAM_IGNORECASE_DEFAULT_VALUE));
		lowerCaseAnnotationDef.addParamDefinition(paramDefinition);
		return lowerCaseAnnotationDef;
	}

	private AnnotationDefinition createUpperCaseAnntationDef() {
		//create LOWER_CASE_ANN (if not already present), having target TARGET_NODE and RETAINED_ANN_DECL = true
		if (prModel.isAnnotationPresent(UPPER_CASE_ANN)) {
			//in the prModel there is already LOWER_CASE_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_NODE and TARGET_ENTIRE_RULE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_NODE));
		targetList.add(new ParamValueString(TARGET_ENTIRE_RULE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);

		AnnotationDefinition upperCaseAnnotationDef = new AnnotationDefinition(UPPER_CASE_ANN, metaAnnotationList);
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_STRING, DEFAULT_NAME_FOR_PARAM, new ParamValueString(CAPITALIZE_ANN_PARAM_IGNORECASE_DEFAULT_VALUE));
		upperCaseAnnotationDef.addParamDefinition(paramDefinition);
		return upperCaseAnnotationDef;
	}

	private AnnotationDefinition createRemovePunctuationAnntationDef() {
		//create REMOVE_PUNCTUATION_ANN (if not already present), having target TARGET_NODE and RETAINED_ANN_DECL = true
		if (prModel.isAnnotationPresent(REMOVE_PUNCTUATION_ANN)) {
			//in the prModel there is already REMOVE_PUNCTUATION_ANN, so no need to create it
			return null;
		}
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		//set RETAINED_ANN_DECL = true
		metaAnnotationList.add(new MetaAnnotationRetained(true));
		//set TARGET_ANN_DECL with TARGET_NODE and TARGET_ENTIRE_RULE
		List<ParamValueInterface>targetList = new ArrayList<>();
		targetList.add(new ParamValueString(TARGET_NODE));
		targetList.add(new ParamValueString(TARGET_ENTIRE_RULE));
		MetaAnnotationTarget metaAnnotationTarget = new MetaAnnotationTarget();
		metaAnnotationTarget.addTargetValueList(targetList);
		metaAnnotationList.add(metaAnnotationTarget);

		AnnotationDefinition removePunctuationAnnotationDef = new AnnotationDefinition(REMOVE_PUNCTUATION_ANN, metaAnnotationList);
		ParamDefinition paramDefinition = new ParamDefinition(TYPE_STRING, DEFAULT_NAME_FOR_PARAM, new ParamValueString(""));
		removePunctuationAnnotationDef.addParamDefinition(paramDefinition);
		//the meaning of the value passed to the annotation REMOVE_PUNCTUATION_ANN depends on the value itself:
		// - when no value is passed (so the value "" ) then the removePunctuation is applied
		// - when true/false is passed then the removePunctuation can be applied (with "true") or not (with "false")
		// - a different string is passed, (eg. ".?;") then removePunctuation is applied using those as symbols to be
		// 		removed and not the default one
		return removePunctuationAnnotationDef;
	}
	
	public ProjectionRulesModel parsePearlDocument(File prFile, boolean rulesShouldExists) throws PRParserException  {
		String fileContent;

		InputStream is;
		try {
			is = new FileInputStream(prFile);
			BufferedReader buf = new BufferedReader(new InputStreamReader(is, "utf-8"));
			String line = buf.readLine();
			StringBuilder sb = new StringBuilder();
			while(line != null){
			   sb.append(line).append("\n");
			   line = buf.readLine();
			}
			buf.close();
			fileContent = sb.toString();
			
		} catch (IOException e) {
			throw new PRIOException(e);
		}
		return parsePearlDocument(fileContent, rulesShouldExists);
	}
	
	public ProjectionRulesModel parsePearlDocument(InputStream is, boolean rulesShouldExists) throws PRParserException  {
		String fileContent;

		try {
			BufferedReader buf = new BufferedReader(new InputStreamReader(is, "utf-8"));
			String line = buf.readLine();
			StringBuilder sb = new StringBuilder();
			while(line != null){
			   sb.append(line).append("\n");
			   line = buf.readLine();
			}
			fileContent = sb.toString();
			
		} catch (IOException e) {
			throw new PRIOException(e);
		}
		return parsePearlDocument(fileContent, rulesShouldExists);
	}
	
	
	public ProjectionRulesModel parsePearlDocument(String pearlContent, boolean rulesShouldExists) throws PRParserException {

		// Get our lexer
		PearlLexer pearlLexer = new PearlLexer(CharStreams.fromString(pearlContent));
		// Get a list of matched tokens
		CommonTokenStream tokens = new CommonTokenStream(pearlLexer);
		// Pass the tokens to the parser
		PearlParser parser = new PearlParser(tokens);
		//set the error handler that does not try to recover from error, it just throw exception
		parser.setErrorHandler(new BailErrorStrategy());
		
		try {
			PearlUnitContext pearlUnitContext = parser.pearlUnit();
			// Walk it and attach our listener
			/*ParseTreeWalker walker = new ParseTreeWalker();
			walker.walk(this, pearlUnitContext);*/
			
			parsePearlUnit(pearlUnitContext, rulesShouldExists);
			
			//now return the PRModel which was generated
			return prModel;
		} catch (PRSyntaxRuntimeException e) {
			throw new PRSyntaxException(e.getMessage());
		}
		
		
	}
	
	
	// the main entry point for this class to parse the description (which is the main element)
	// it throws PRParserRuntimeException containing PRParserException in case of any exception
	// @Override
	//public void enterPearlUnit(PearlUnitContext ctx) {
	
	
	private void parsePearlUnit(PearlUnitContext ctx, boolean rulesShouldExists) throws PRParserException {
		//parse the prefixes
		parsePrologue(ctx.prologue());
		//parse the annotations, if present
		AnnotationsDefinitionContext annotationsDefinition = ctx.annotationsDefinition();
		if(annotationsDefinition!=null) {
			parseAnnotationsDefinition(annotationsDefinition);
		} 
		
		//the next part is composed in the following way:
		// (baseRule | regex)+
		// so by a list of baseRule and/or regex
		
		boolean oneRuleAtLeast = false;

		//parse a list of baseRule
		List<BaseRuleContext> baseRuleContextList = ctx.baseRule();
		if(baseRuleContextList != null && baseRuleContextList.size()>0) { // it could be null if only regexes are present
			oneRuleAtLeast = true;
			for(BaseRuleContext baseRuleContext : baseRuleContextList) {
				parseBaseRule(baseRuleContext);
			}
		}

		//parse a list of regexRule
		List<RegexContext> regexContextList = ctx.regex();
		if(regexContextList != null && regexContextList.size()>0) { // it could be null if only baseRule are present
			oneRuleAtLeast = true;
			for(RegexContext regexContext : regexContextList) {
				parseRegex(regexContext);
			}
		}

		if(!oneRuleAtLeast && rulesShouldExists) {
			throw new NoRuleInFileException();
		}
	}
	
	public ConverterMention parseIndividualConverter(Map<String, String> prefixMap, 
			Map<String, PlaceholderStruct> placeholderMapEIC, String ruleIdForIndividualConverter, 
			String pearlContent) throws PRParserException {
		
		//used the input prefixMap
		this.prefixMap = prefixMap;
		
		// Get our lexer
		PearlLexer pearlLexer = new PearlLexer(CharStreams.fromString(pearlContent));
		// Get a list of matched tokens
		CommonTokenStream tokens = new CommonTokenStream(pearlLexer);
		// Pass the tokens to the parser
		PearlParser parser = new PearlParser(tokens);
		//set the error handler that does not try to recover from error, it just throw exception
	    parser.setErrorHandler(new BailErrorStrategy());
		
	    try {
			IndividualConverterContext individualConverter = parser.individualConverter();
			
			// Walk it and attach our listener
			//ParseTreeWalker walker = new ParseTreeWalker();
			//walker.walk(this, individualConverter);
			
			return parseIndividualConverter(individualConverter, placeholderMapEIC, ruleIdForIndividualConverter);
	    } catch (PRSyntaxRuntimeException e) {
			throw new PRSyntaxException(e.getMessage());
		}
	}
	
	public ProjectionOperator parseProjectionOperator(Map<String, String> prefixMap, 
			Map<String, PlaceholderStruct> placeholderMap, String ruleIdForIndividualConverter, 
			String pearlContent) throws PRParserException {
	
		//used the input prefixMap
		this.prefixMap = prefixMap;
		
		// Get our lexer
		PearlLexer pearlLexer = new PearlLexer(CharStreams.fromString(pearlContent));
		// Get a list of matched tokens
		CommonTokenStream tokens = new CommonTokenStream(pearlLexer);
		// Pass the tokens to the parser
		PearlParser parser = new PearlParser(tokens);
		//set the error handler that does not try to recover from error, it just throw exception
	    parser.setErrorHandler(new BailErrorStrategy());
		
	    try {
			ProjectionOperatorContext projectionOperatorContext = parser.projectionOperator();
			
			return parseProjectionOperator(projectionOperatorContext, placeholderMap, ruleIdForIndividualConverter);
	    } catch (PRSyntaxRuntimeException e) {
			throw new PRSyntaxException(e.getMessage());
		}
	}
	
	/*PROLOGUE PART*/
	private void parsePrologue(PrologueContext prologue) {
		prefixMap.clear();
		List<PrefixDeclContext> prefixDeclList = prologue.prefixDecl();
		if(prefixDeclList!=null) {
			for(PrefixDeclContext prefixDeclContext : prefixDeclList) {
				parsePrefix(prefixDeclContext);
			}
			//all prefixes has been parsed, so add the map to the Projection Model
			prModel.setPrefixToNamespaceMap(prefixMap);
		} else {
			//TODO decide what to do
		}
	}


	private void parsePrefix(PrefixDeclContext prefixDeclContext) {
		String pname_ns = prefixDeclContext.PNAME_NS().getText();
		String iriref = prefixDeclContext.IRIREF().getText();
		prefixMap.put(pname_ns.substring(0, pname_ns.length()-1), iriref.substring(1, iriref.length()-1));
	}
	

	private void parseAnnotationsDefinition(AnnotationsDefinitionContext annotationsDefinition) throws PRParserException {
		
		for(SingleAnnotationDefinitionContext sadc : annotationsDefinition.singleAnnotationDefinition()){
			parseSingleAnnotationDefinition(sadc);
		}
	}

	private void parseSingleAnnotationDefinition(SingleAnnotationDefinitionContext sadc) throws PRParserException {
		List<MetaAnnotation> metaAnnotationList = new ArrayList<>();
		// first get the first element, which is the annotation name (it does not have @, so add it)
		String annotationName = sadc.annotationName.getText();
		//now get the meta annotations
		List<MetaAnnotationContext> metaAnnotationContextList = sadc.metaAnnotation();
		if(metaAnnotationContextList!=null) {
			for(MetaAnnotationContext metaAnnotationContext : metaAnnotationContextList) {
				metaAnnotationList.add(parseMetaAnnotation(metaAnnotationContext));
			}
		}
		AnnotationDefinition annotationDefinition = new AnnotationDefinition(annotationName,
				metaAnnotationList);

		//get the parameter definition (if present)
		List<ParamsDefinitionContext> paramsDefinitionContextList = sadc.paramsDefinition();
		if(paramsDefinitionContextList!=null ){
			for(ParamsDefinitionContext paramsDefinitionContext : paramsDefinitionContextList){
				annotationDefinition.addParamDefinition(parseParamDefinitionContext(annotationName, paramsDefinitionContext));
			}
		}
				
		prModel.addAnnotationDefinition(annotationDefinition);
	}

	private ParamDefinition parseParamDefinitionContext(String annName, ParamsDefinitionContext paramsDefinitionContext)
			throws PRParserException {
		String type = paramsDefinitionContext.paramType().getText();
		String name = paramsDefinitionContext.paramName.getText();
		if(paramsDefinitionContext.defaultValue()!=null){
			ParamValueInterface defaultValue = null;
			String ruleId = "annotation definition";

			//manage the IRI case (complete IRI or prefixName)
			if(paramsDefinitionContext.defaultValue().iri()!=null){
				String iriString = parseIri(paramsDefinitionContext.defaultValue().iri().getText(), ruleId, true, true);
				IRI iriValue = NTriplesUtil.parseURI(iriString, SimpleValueFactory.getInstance());
				defaultValue = new ParamValueIri(iriValue);
			}
			//literal case
			else if(paramsDefinitionContext.defaultValue().literal() != null){
				boolean isLiteral = true;
				String literalString = parseLiteral(paramsDefinitionContext.defaultValue().literal(), ruleId, true);
				//check if, instead of a literal it should be considered as a string
				//a string start and end with " or with ' , so it has no language nor type and the type of the param is String or String[]
				if(surroundedWithQuotation(literalString) &&
						(type.toLowerCase().equals(TYPE_STRING) || type.toLowerCase().equals(TYPE_STRING_ARRAY) )){
					isLiteral = false;
				}
				if(isLiteral) {
					Literal literalValue = NTriplesUtil.parseLiteral(literalString, SimpleValueFactory.getInstance());
					defaultValue = new ParamValueLiteral(literalValue);
				} else {
					//it is not a literal, so sanitize it
					String stringValue = removeQuotation(literalString, false);
					defaultValue = new ParamValueString(stringValue);
				}
			}
			//double case
			else if(paramsDefinitionContext.defaultValue().DOUBLE() != null) {
				double doubleValue = Double.parseDouble(paramsDefinitionContext.defaultValue().getText());
				defaultValue = new ParamValueDouble(doubleValue);
			}
			//integer case
			else if(paramsDefinitionContext.defaultValue().INTEGER() != null){
				int intValue = Integer.parseInt(paramsDefinitionContext.defaultValue().getText());
				defaultValue = new ParamValueInteger(intValue);
			}
			//string case
			else if(paramsDefinitionContext.defaultValue().valueStr != null){
				String value = paramsDefinitionContext.defaultValue().getText();
				defaultValue = new ParamValueString(value);

			}
			else {
				//this should never happen
			}
			//check tha the default value is consistent to the type of the parameter
			List<ParamValueInterface> valueList = new ArrayList<>();
			valueList.add(defaultValue);
			if(!checkParamTypeCompliant(annName, name, type, valueList)){
				throw new TypeOfDefaultParamInAnnotationWrongTypeException(annName, name, type);
			}

			return new ParamDefinition(type, name, defaultValue);
		} else {
			return new ParamDefinition(type, name);
		}
	}

	private MetaAnnotation parseMetaAnnotation(MetaAnnotationContext metaAnnotationContext) throws PRParserException {
		String metaAnnName = metaAnnotationContext.annotationName.getText().substring(1);

		//depending on the name of the MetaAnnotation, different class should be used
		MetaAnnotation metaAnnotation;
		if(metaAnnName.equals(TARGET_ANN_DECL)){
			metaAnnotation = new MetaAnnotationTarget();
		} else if(metaAnnName.equals(RETAINED_ANN_DECL)){
			metaAnnotation = new MetaAnnotationRetained(true);
		} else{
			metaAnnotation = new MetaAnnotationGeneric(metaAnnName);
		}
		SingleParamOrListOfNameParamContext singleParamOrListOfNameParamContext = metaAnnotationContext.singleParamOrListOfNameParam();

		if(singleParamOrListOfNameParamContext != null) {
			List<AnnotationParam> annotationParamList = parseSingleParamOrListOfNameParam(singleParamOrListOfNameParamContext, new HashMap<>(),
					"before rule(s) definition", null);
			for (AnnotationParam annotationParam : annotationParamList) {
				if(metaAnnotation instanceof  MetaAnnotationTarget){
					//since the MetaAnnotation is the TARGET_ANN_DECL, just get the value and ignore the name of the param
					MetaAnnotationTarget metaAnnotationTarget = (MetaAnnotationTarget) metaAnnotation;
					metaAnnotationTarget.addTargetValueList(annotationParam.getValueList());
				} else if(metaAnnotation instanceof  MetaAnnotationRetained){
					//TODO this should never happen, since the RETAINED_ANN_DECL should have no value
				} else {
					MetaAnnotationGeneric metaAnnotationGeneric = (MetaAnnotationGeneric) metaAnnotation;
					metaAnnotationGeneric.addNameValueList(annotationParam.getName(), annotationParam.getValueList());
				}
			}
		}

		return metaAnnotation;
	}

	private List<AnnotationParam> parseSingleParamOrListOfNameParam(
			SingleParamOrListOfNameParamContext singleParamOrListOfNameParamContext,
			Map<String, PlaceholderStruct> placeholderMap, String ruleId, AnnotationDefinition annDef) throws PRParserException {
		//String paramName;
		//when annDef is null, it means that there is no AnnotationDefinition, so it is dealing with a Meta Annotation
		String type = TYPE_STRING; // default value used for the parameter in the Meta Annotation

		List<AnnotationParam> annotationParamList = new ArrayList<>();

		SingleParamOrListOfParamContext singleP = singleParamOrListOfNameParamContext.singleP;
		if(singleP != null){
			//it is a single value, or a list of values, without the name=param for
			if(annDef != null){
				type = annDef.getParamDefinition(DEFAULT_NAME_FOR_PARAM).getType();
			}
			AnnotationParam annotationParam = new AnnotationParam(DEFAULT_NAME_FOR_PARAM, parseSingleParamOrListOfParam(singleP, placeholderMap, ruleId, type));
			annotationParamList.add(annotationParam);
		} else {
			//it is in the form name=param (it is possible to have multiple name=param and the names and params are in two separate lists)
			List<Token> nameList = singleParamOrListOfNameParamContext.names;
			List<SingleParamOrListOfParamContext> spOLopList = singleParamOrListOfNameParamContext.params;
			//parse each SingleParamOrListOfParam
			for(int i=0; i<nameList.size(); ++i){
				String paramName = nameList.get(i).getText();
				if(paramName.equals(TARGET_PARAM)){
					//the target param is not managed here, it has a special parse function (parseTargetParam)
					continue;
				}
				SingleParamOrListOfParamContext singleParamOrListOfParam = spOLopList.get(i);
				if(annDef != null) {
					if(annDef.getParamDefinition(paramName) == null){
						throw  new ParamInAnnotationNotDefinedException(annDef.getName(), ruleId, paramName);
					}
					type = annDef.getParamDefinition(paramName).getType();
				}
				AnnotationParam annotationParam = new AnnotationParam(paramName, parseSingleParamOrListOfParam(singleParamOrListOfParam, placeholderMap, ruleId, type));
				annotationParamList.add(annotationParam);
			}
		}
		return annotationParamList;
	}
	
	private List<ParamValueInterface> parseSingleParamOrListOfParam(
			SingleParamOrListOfParamContext singleParamOrListOfParamContext, 
			Map<String, PlaceholderStruct> placeholderMap, String ruleId, String type) throws PRParserException {
		List<ParamValueInterface> valueList = new ArrayList<>();
		for(SingleParamContext singleParamContext : singleParamOrListOfParamContext.singleParam()) {
			ParamValueInterface valueToAdd = null;
			//manage the IRI case (complete IRI or prefixName)
			if(singleParamContext.iri()!=null){
				String iriString = parseIri(singleParamContext.iri().getText(), ruleId, true, true);
				IRI iriValue = NTriplesUtil.parseURI(iriString, SimpleValueFactory.getInstance());
				valueToAdd = new ParamValueIri(iriValue);
			}
			//literal case
			else if(singleParamContext.literal() != null){
				boolean isLiteral = true;
				String literalString = parseLiteral(singleParamContext.literal(), ruleId, true);
				//check if, instead of a literal it should be considered as a string
				//a string start and end with " or with ' , so it has no language nor type and the type of the param is String or String[]
				if(surroundedWithQuotation(literalString) &&
						(type.toLowerCase().equals(TYPE_STRING) || type.toLowerCase().equals(TYPE_STRING_ARRAY) )){
					isLiteral = false;
				}
				if(isLiteral) {
					if(literalString.startsWith("'")){
						//since NTriplesUtil.parseLiteral accepts only literal in the form "text" and not in the form 'text' I replace all
						// ' with " when the literal starts with '
						literalString = literalString.replaceAll("'", "\"");
					}
					Literal literalValue = NTriplesUtil.parseLiteral(literalString, SimpleValueFactory.getInstance());
					valueToAdd = new ParamValueLiteral(literalValue);
				} else {
					//it is not a literal, so sanitize it
					String stringValue = removeQuotation(literalString, false);
					valueToAdd = new ParamValueString(stringValue);
				}
			}
			//double case
			else if(singleParamContext.DOUBLE() != null) {
				double doubleValue = Double.parseDouble(singleParamContext.getText());
				valueToAdd = new ParamValueDouble(doubleValue);
			}
			//integer case
			else if(singleParamContext.INTEGER() != null){
				int intValue = Integer.parseInt(singleParamContext.getText());
				valueToAdd = new ParamValueInteger(intValue);
			}
			//string case
			else if(singleParamContext.valueStr != null){
				String value = singleParamContext.getText();
				valueToAdd = new ParamValueString(value);
			}
			//placeholder case
			else if(singleParamContext.plch != null){
				String plch = singleParamContext.plch.getText().substring(1);
				//check that the placeholder is defined in the current Rule
				if(!placeholderMap.containsKey(plch)){
					throw  new NodeNotDefinedException(plch, ruleId);
				}
				valueToAdd = new ParamValuePlaceholder(plch);
			}
			else {
				throw new PRGenericException("There are some problems regarding the value "+singleParamContext.getText()+" of one of the " +
						"params of an annotation in rule "+ruleId);
				//this should never happen
			}

			valueList.add(valueToAdd);
		}
		return valueList;
	}

	
	
	/* BASERULE PART*/
	

	private void parseBaseRule(BaseRuleContext baseRuleContext) throws PRParserException {
		//a baseRule can be one of the following:
		// - standardRule
		// - lazyRule
		// - forRegexRule
		StandardRuleContext standardRuleContext = baseRuleContext.standardRule();
		LazyRuleContext lazyRuleContext = baseRuleContext.lazyRule();
		ForRegexRuleContext forRegexRuleContext = baseRuleContext.forRegexRule();

		//get the annotations, if present
		List<AnnotationContext> annotationContextList = baseRuleContext.annotation();

		if(standardRuleContext != null) {
			//it is a standard rule
			parseStandardRule(standardRuleContext, annotationContextList);
		} else if(lazyRuleContext != null) {
			//it is a lazy rule
			parseLazyRule(lazyRuleContext, annotationContextList);
		} else if(forRegexRuleContext != null) {
			//it is a regex rule
			parseForRegexRule(forRegexRuleContext, annotationContextList);
		}
	}
	
	

	private void parseStandardRule(StandardRuleContext standardRuleContext, List<AnnotationContext> annotationContextList)
			throws PRParserException {
		String ruleId = standardRuleContext.ruleId().getText().substring(3);

		ProjectionRule projectionRule = new ProjectionRule(ruleId, prModel);
		projectionRule.setLaziness(false);
		
		String uimaTypeName = standardRuleContext.uimaTypeName().getText();
		projectionRule.setUimaTypeName(uimaTypeName);
		
		List<DependContext> dependContextList = standardRuleContext.depend();
		if(dependContextList != null) {
			for(DependContext dependContext : dependContextList) {
				parseDepend(dependContext, projectionRule);
			}
		}
		ConditionClauseContext conditionClauseContext = standardRuleContext.conditionClause();
		if(conditionClauseContext != null) {
			parseConditionaClause(conditionClauseContext, projectionRule);
		}
		BindingsClauseContext bindingsClauseContext = standardRuleContext.bindingsClause();
		if(bindingsClauseContext != null) {
			parseBindingClause(bindingsClauseContext, projectionRule);
		}
		NodesClauseContext nodesClauseContext = standardRuleContext.nodesClause();
		if(nodesClauseContext != null) {
			parseNodesClause(nodesClauseContext, projectionRule);
		}
		InsertClauseContext insertClauseContext = standardRuleContext.insertClause();
		if(insertClauseContext != null) {
			parseInsertClause(insertClauseContext, projectionRule);
		}
		GraphClauseContext graphClauseContext = standardRuleContext.graphClause();
		if(graphClauseContext != null) {
			parseGraphClause(graphClauseContext, projectionRule);
		}
		DeleteClauseContext deleteClauseContext = standardRuleContext.deleteClause();
		if(deleteClauseContext != null) {
			parseDeleteClause(deleteClauseContext, projectionRule);
		}
		WhereClauseContext whereClauseContext = standardRuleContext.whereClause();
		if(whereClauseContext != null) {
			parseWhereClause(whereClauseContext, projectionRule);
		}
		
		// get all the placeholders used in this rule that are from other rules (use the alternative id,
		// if it is present)
		Collection<GraphElement> graphElementList = projectionRule.getInsertGraphList();
		for (GraphElement graphElement : graphElementList) {
			setDependsPlchldFromGraphElem(graphElement);
		}

		// resolve the mandatory placeholder: set all the placeholder which appear in at least one triple
		// in the graph section outside an OPTIONAL graph as mandatory
		projectionRule.resolveIsMandatoryCheck();

		//add the annotations, if present
		if(annotationContextList!=null) {
			// pass an empty HashMap instead of the projectionRule.getPlaceholderMap() since the annotations associated
			// to a rule SHOULD NOT use placeholders
			List<Annotation> annotationList = parseAnnotation(annotationContextList,
					new HashMap<>(), projectionRule.getId(), SECTION_ENTIRE_RULE);
			if (!annotationList.isEmpty()) {
				projectionRule.setAnnotations(annotationList);
			}
		}


		prModel.addProjectionRule(projectionRule);
	}

	
	
	private void parseDepend(DependContext dependContext, ProjectionRule projectionRule) {
		String dependencyType = dependContext.dependType.getText();
		
		List<String> depRuleIdsList = new ArrayList<>();
		
		// list of all the ruleid used
		List<Token> depRuleIdsTokenList = dependContext.depRuleIds;
		if(depRuleIdsTokenList != null) {
			for(Token depRuleId : depRuleIdsTokenList) {
				depRuleIdsList.add(depRuleId.getText());
			}
		}
		
		// list of all the parameters used
		List<String> paramsList = new ArrayList<>();
		List<DependParamContext> dependencyParamContextList = dependContext.params;
		if(dependencyParamContextList != null) {
			for(DependParamContext dependParamContext : dependencyParamContextList) {
				String name = dependParamContext.name.getText();
				String value = dependParamContext.value.getText();
				String singleParam = name+"="+value;
				paramsList.add(singleParam);
			}
		}

		// alias
		String alias = dependContext.depRuleIdAs.getText();
		
		DependsOnInfo dependsOnInfo = new DependsOnInfo(dependencyType, depRuleIdsList, paramsList, alias);
		projectionRule.addDependsOn(dependsOnInfo);
	}

	private void parseConditionaClause(ConditionClauseContext conditionClauseContext,
			ProjectionRule projectionRule) {
		List<ConditionDefContext> conditionDefContextList = conditionClauseContext.conditionDef();
		for(ConditionDefContext conditionDefContext : conditionDefContextList) {
			projectionRule.addCondition(parseConditionDef(conditionDefContext, projectionRule));
		}
		
	}

	private ConditionStruct parseConditionDef(ConditionDefContext conditionDefContext, 
			ProjectionRule projectionRule) {
		String featurePath = conditionDefContext.featurePath().getText();
		
		String condOperatorString = conditionDefContext.CONDITIONOPERATOR().getText();
		ConditionStruct.Operator condOperator = null;
		switch (condOperatorString) {
			case "IN":
			case "in":
				condOperator = Operator.IN;
				break;
			case "NOT IN":
			case "not in":
				condOperator = Operator.NOT_IN;
				break;
			default:
				break;
		}
		
		List<StringContext> valueList = conditionDefContext.values;
		List<String> valueStringList = new ArrayList<>();
		for(StringContext stringContent : valueList) {
			valueStringList.add(cleanValue(stringContent.getText()));
		}
		
		
		String uimaTypeAndFeat = projectionRule.getUIMAType() + ":" + featurePath;

		return new ConditionStruct(featurePath, uimaTypeAndFeat, condOperator, valueStringList);
	}
	
	private String cleanValue(String value) {
		String result = value;
		if (result.startsWith("\"") || result.startsWith("'")) {
			result = result.substring(1);
		}
		if (result.endsWith("\"") || result.endsWith("'")) {
			result = result.substring(0, result.length() - 1);
		}
		result = sanitizeString(result);

		return result;
	}

	private void parseBindingClause(BindingsClauseContext bindingsClauseContext,
			ProjectionRule projectionRule) {
		for( BindingDefContext bindingDefContext : bindingsClauseContext.bindingDef()) {
			String bindingId = bindingDefContext.bindingId.getText();
			String featurePath = bindingDefContext.featurePath().getText();
			String ruleId = bindingDefContext.bindingRuleId.getText();
			
			BindingStruct bindingStruct = new BindingStruct(bindingId, featurePath, ruleId, projectionRule);
			projectionRule.addBinding(bindingStruct);
		}
	}

	private void parseNodesClause(NodesClauseContext nodesClauseContext, ProjectionRule projectionRule) 
			throws PRParserException {
		for(NodeDefContext nodeDefContext : nodesClauseContext.nodeDef()) {
			//the name of the placeholder
			String nodeName = nodeDefContext.nodeName.getText();
			
			//the the type + the converters
			ProjectionOperatorContext projectionOperatorContext = nodeDefContext.projectionOperator();
			//get the type associated to the placeholder (uri or literal)
			if(projectionOperatorContext.rep_plc!=null) {
				//since the type is REP_PCL, throw PRParamToReplaceException
				throw new PRParamToReplaceException(projectionOperatorContext.rep_plc.getLine());
			}
			String type = projectionOperatorContext.type.getText();
			//get the  ProjectionOperator and parse all the info contained in it
			ProjectionOperator projectionOperator = parseProjectionOperator(projectionOperatorContext, 
					projectionRule.getPlaceholderMap(), type);
			List<ConverterMention> converterList = projectionOperator.getConverterMentions();
			String lang = projectionOperator.getLanguage().orElse(null);
			String datatype = projectionOperator.getDatatype().orElse(null);
			
			//get the feature path, if present
			FeaturePathContext featurePathContext = nodeDefContext.featurePath();
			
			PlaceholderStruct placeholder;
			
			boolean isDependsOn = false; // TODO find out the meaning of this thing, it seems that it is never used
			
			if(featurePathContext!=null) {
				if(featurePathContext.rep_plc!=null) {
					throw new PRParamToReplaceException(featurePathContext.rep_plc.getLine());
				}
				//featurePathContext.getText() should have the feature path, with the / and [], so no real
				// parsing is needed
				placeholder = new PlaceholderStruct(nodeName, type, converterList, featurePathContext.getText(),
						projectionRule, isDependsOn);
			} else {
				placeholder = new PlaceholderStruct(nodeName, type, converterList, projectionRule, isDependsOn);
			}
			
			//parse the annotations, if present
			List<AnnotationContext> annotationContextList = nodeDefContext.annotation();
			if(annotationContextList!=null) {
				List<Annotation> annotationList = parseAnnotation(annotationContextList, 
						projectionRule.getPlaceholderMap(), projectionRule.getId(), SECTION_NODES);
				if(annotationList!=null && !annotationList.isEmpty()) {
					//add the annotations to the placeholder
					placeholder.setAnnotations(annotationList);
				}
				
			}
			
			//add, if present, the language and the 
			if (lang != null) {
				placeholder.setLiteralLang(lang);
			} else if (datatype != null) {
				placeholder.setLiteralDatatype(datatype);
			}
			boolean added = projectionRule.addPlaceholder(placeholder);
			if(!added){
				//this mean that there already was a placeholder with that name
				throw new RepeteadAssignmentException(placeholder.getName(), projectionRule.getId());
			}
		}
		
	}

	private ProjectionOperator parseProjectionOperator(ProjectionOperatorContext projectionOperatorContext,
			Map<String, PlaceholderStruct> placeholderMap, String ruleId) throws PRParserException {
		String type = projectionOperatorContext.type.getText();
		String language = null;
		String datatype = null;
		ProjectionOperator.NodeType nodeType;
		if(type.equals(ProjectionOperator.NodeType.uri.toString())) {
			nodeType = ProjectionOperator.NodeType.uri;
		} else {
			nodeType = ProjectionOperator.NodeType.literal;
			//see if a language and datatype are specified
			if(projectionOperatorContext.iri() != null) {
				datatype = parseIri(projectionOperatorContext.iri().getText(), ruleId, false, false);
			}
			if(projectionOperatorContext.LANGTAGORANNOTATION() != null) {
				language = projectionOperatorContext.LANGTAGORANNOTATION().getText().substring(1);
			}
		}
		
		ConvertersContext converterContext = projectionOperatorContext.converters();
		List<ConverterMention> converterList;
		if(converterContext==null) {
			//the converter are specified so assume the existence of a default converter
			converterList = new ArrayList<>();
			converterList.add(new ConverterMention(DEFAULT_CONVERTER_URI));
			
		} else {
			//the converter are specified
			converterList = parseConverter(converterContext, placeholderMap, ruleId);
		}
		
		return new ProjectionOperator(nodeType, language, datatype, converterList);
	}

	private List<ConverterMention> parseConverter(ConvertersContext converterContext, 
			Map<String, PlaceholderStruct> placeholderMap, String ruleId) throws PRParserException {
		List<ConverterMention> converterList = new ArrayList<>(converterContext.individualConverter().size());
		for(IndividualConverterContext individualConverterContext : converterContext.individualConverter()) {
			ConverterMention converterMention = parseIndividualConverter(individualConverterContext, placeholderMap, 
					ruleId);
			converterList.add(converterMention);
		}
		return converterList;
	}
	
	private ConverterMention parseIndividualConverter(IndividualConverterContext individualConverterContext,
			Map<String, PlaceholderStruct> placeholderMap, String ruleId) throws PRParserException {
		List<ConverterArgumentExpression> converterAdditionalArguments = new ArrayList<>();
		if(individualConverterContext.rep_plc!=null) {
			throw new PRParamToReplaceException(individualConverterContext.rep_plc.getLine());
		}
		String iri = parseIri(individualConverterContext.iri().getText(), ruleId, false, false);
		ConverterAdditionalArgumentsContext converterAdditionalArgumentsContext
				= individualConverterContext.converterAdditionalArguments();
		if(converterAdditionalArgumentsContext!=null) {
			for(ConverterArgumentExpressionContext converterArgumentExpressionContext 
					: converterAdditionalArgumentsContext.converterArgumentExpression()) {
				converterAdditionalArguments.add(parseConverterArgumentExpression(converterArgumentExpressionContext,
						placeholderMap, ruleId));
			}
		}
		return new ConverterMention(iri, converterAdditionalArguments);
	}

	private ConverterArgumentExpression parseConverterArgumentExpression(
			ConverterArgumentExpressionContext converterArgumentExpressionContext, 
			Map<String, PlaceholderStruct> placeholderMap, String ruleId) throws PRParserException {
		IriContext iriContext = converterArgumentExpressionContext.iri();
		LiteralContext literalContext = converterArgumentExpressionContext.literal();
		ConverterPlaceholderArgumentContext converterPlaceholderArgumentContext = 
				converterArgumentExpressionContext.converterPlaceholderArgument();
		ConverterMapArgumentContext converterMapArgumentContext = converterArgumentExpressionContext.converterMapArgument();
		
		//now check which one of these one is not null, to understand which type of arguments expression is
		if(iriContext != null) {
			return new ConverterRDFURIArgument(parseIri(iriContext.getText(), ruleId, false, false));
		} else if(literalContext != null) {
			return new ConverterRDFLiteralArgument(parseLiteral(literalContext, ruleId, true));
		} else if(converterPlaceholderArgumentContext != null) {
			return parseConverterPlaceholderArgument(converterPlaceholderArgumentContext, placeholderMap, ruleId);
		} else if(converterMapArgumentContext != null) {
			return parseConverterMapArgumentContext(converterMapArgumentContext, placeholderMap, ruleId);
		} else {
			throw new IllegalArgumentException(
					"This should never happen. Unknown type of converter argument expression: "
							+ converterArgumentExpressionContext.getText());
		}
		
	}

	private ConverterArgumentExpression parseConverterPlaceholderArgument(
			ConverterPlaceholderArgumentContext converterPlaceholderArgumentContext, 
			Map<String, PlaceholderStruct> placeholderMap, String ruleId) throws NodeNotDefinedException {
		
		String placeholderId = converterPlaceholderArgumentContext.VAR2().getText().substring(1); // remove the starting $ 
		PlaceholderStruct phStruct = placeholderMap.get(placeholderId);
		
		if (phStruct == null) {
			throw new NodeNotDefinedException(placeholderId, ruleId);
		}
				
		String rdfType = phStruct.getRDFType();
		
		Class<? extends Value> clazz = rdfType.equals("uri") ? IRI.class : Literal.class;
		
		return new ConverterPlaceholderArgument(placeholderId, clazz);
	}

	private ConverterArgumentExpression parseConverterMapArgumentContext(
			ConverterMapArgumentContext converterMapArgumentContext, 
			Map<String, PlaceholderStruct> placeholderMap, String ruleId) throws PRParserException {
		Map<String, ConverterArgumentExpression> map = new HashMap<String, ConverterArgumentExpression>();

		for(MapEntryContext mapEntryContext : converterMapArgumentContext.mapEntry()) {
			String mapKey = mapEntryContext.JAVA_IDENTIFIER().getText();
			ConverterArgumentExpression valueExpr = null;
			// currently, the only type of supported maps are Map<String, String>
			//check the type of the value contained in the mapEntryContext, to decide which function to call
			if(mapEntryContext.iri()!=null) {
				//it is an IRI
				valueExpr = new ConverterRDFURIArgument(parseIri(mapEntryContext.iri().getText(), ruleId, 
						false, false));
			} else if(mapEntryContext.literal()!=null) {
				//it is a literal
				valueExpr = new ConverterRDFLiteralArgument(parseLiteral(mapEntryContext.literal(), ruleId, true));
			} else if(mapEntryContext.converterPlaceholderArgument() != null) {
				//it is a placeholder
				valueExpr = parseConverterPlaceholderArgument(
						mapEntryContext.converterPlaceholderArgument(), placeholderMap, ruleId);
			} else {
				//TODO this should never happen
			}
			map.put(mapKey, valueExpr);
		}
		return new ConverterMapArgument(map);
	}

	private List<Annotation> parseAnnotation(List<AnnotationContext> annotationContextList,
			Map<String, PlaceholderStruct> placeholderMap, String ruleId, String locationOfAnnotation) throws PRParserException {
		List<Annotation> annotationsList = new ArrayList<>();
		for(AnnotationContext annotationContext : annotationContextList) {
			String annotationName = annotationContext.annotationName.getText().substring(1);
			AnnotationDefinition annotationDefinition = prModel.getAnnotationDefinition(annotationName);
			if(annotationDefinition == null && previousPrModel != null) {
				//in the current ProjectionRuleModel, this annotation is not declared, search if it was declared in a previous parsed PEARL file
				// if the previousPrModel is not null
				annotationDefinition = previousPrModel.getAnnotationDefinition(annotationName);
			}
			// check if this annotation was declared
			if (annotationDefinition == null) {
				// the annotation is not defined, so do not put it into the model
				logger.error("the annotation " + annotationName + " is used but not defined");
				throw new AnnotationNotDefinedException(annotationName, ruleId);
			}
			// check the target compatibility with current section. This means that the annotation definition
			// should have the Target meta annotation with the parameter compliant with the location of the annotation
			if(locationOfAnnotation.equals(SECTION_NODES)){
				if(!annotationDefinition.checkTargetCompatibilityWithNodes()){
					// this annotation is not compatible with the placeholder section
					logger.error("the annotation " + annotationName + " is not compatible with the 'node' role");
					throw new AnnotationTargetNotCompatibleException(annotationName, ruleId, SECTION_NODES);
				}
			} else if(locationOfAnnotation.equals(SECTION_GRAPH)){
				if(!annotationDefinition.checkTargetCompatibilityWithGraph()) {
					// this annotation is not compatible with the triple pattern
					logger.error("the annotation " + annotationName + " is not compatible with the 'graph' role");
					throw new AnnotationTargetNotCompatibleException(annotationName, ruleId, SECTION_GRAPH);
				}

			} else if (locationOfAnnotation.equals(SECTION_ENTIRE_RULE)) {
				if(!annotationDefinition.checkTargetCompatibilityWithEntireRule()) {
					// this annotation is not compatible with the entire rule
					logger.error("the annotation " + annotationName + " is not compatible with the 'entire rule' role");
					throw new AnnotationTargetNotCompatibleException(annotationName, ruleId, SECTION_ENTIRE_RULE);
				}
			} else {
				//TODO this should never happen
				continue;
			}
			// the annotation has the right target (at least regarding the section where it is used), so create it and (if all other checks are passed) add it to the model
			Annotation annotation = new Annotation(annotationName, annotationDefinition);

			// now see if there are any parameters to add to the annotation itself
			SingleParamOrListOfNameParamContext singleParamOrListOfNameParamContext = annotationContext.singleParamOrListOfNameParam();
			//add the target param(s), if any
			List<String> targetList=null;
			if(singleParamOrListOfNameParamContext!=null) {
				targetList = parseTargetParam(singleParamOrListOfNameParamContext, placeholderMap, ruleId);
			}
			if(targetList!= null && targetList.size()!=0){
				for(String targetValue : targetList) {
					annotation.addTarget(targetValue);
				}
			}
			if(singleParamOrListOfNameParamContext!=null) {
				List<AnnotationParam> annotationParamList = parseSingleParamOrListOfNameParam(singleParamOrListOfNameParamContext, placeholderMap, ruleId, annotationDefinition);
				for (AnnotationParam annotationParam : annotationParamList) {
					//verify that the used parameter is in the list of possible parameter for the specified annotation (via it annotation definition) and that the value(s)
					// associated to such name is compatible with the value type specified in the annotation definition
					ParamDefinition paramDefinition = annotationDefinition.getParamDefinition(annotationParam.getName());
					if(paramDefinition == null){
						//the used parameter in this annotation has not been defined in the Annotation Definition, so it cannot be used
						throw new ParamInAnnotationNotDefinedException(annotationName, ruleId, annotationParam.getName());
					} else {
						//now check that the type of its value(s) is(are) compliant with the type specified in the Annotation Definition
						if(checkParamTypeCompliant(annotationName, annotationParam.getName(), paramDefinition.getType(), annotationParam.getValueList())) {
							annotation.addParams(annotationParam.getName(), annotationParam.getValueList());
						} else {
							if(annotationName.equals(CONFIDENCE_ANN) && annotationParam.getValueList().size()==1){
								throw new ConfidenceNotDoubleNumException(ruleId, annotationParam.getValueList().get(0).toString(),
										annotationParam.getValueList().get(0).getType());
							} else {
								logger.error("there is a problem in the annotation " + annotationName + " with the parameter " + annotationParam.getName());
								throw new TypeOfParamInAnnotationWrongTypeException(annotationName, ruleId, annotationParam.getName(), paramDefinition.getType());
							}
						}
					}
				}
			}
			//check that all the mandatory parameters defined in the Annotation Definition (the parameters with no default value)
			//are present in the annotation itself
			for(ParamDefinition paramDefinition : annotation.getAnnotationDefinition().getParamDefinitionList()) {
				if(annotation.getParamValueList(paramDefinition.getName()) == null && !paramDefinition.hasDefault()){
					//the parameter is not present in the annotation, but it was defined in the Annotation Definition with no default
					throw new MissingMandatoryParamInAnnotationException(annotationName, ruleId, paramDefinition.getName());
				}
			}

			// check the target_value compatibility with what it is stated in the annotation itself
			// get the target from the annotation, if no value is present or specified, then assume the
			// default value, so add such default value(s) as the target of the annotation
			List<String> paramTargetList = annotation.getTargetList();
			//boolean compatible = false;
			if (paramTargetList.isEmpty()) {
				// the annotation does not have the target value, so add the default value
				paramTargetList.addAll(annotationDefinition.getTargetValues());
			}
			//check that ALL the target of the annotation are compatible with the one specified in the annotationDefinition
			for(String paramTarget : paramTargetList){
				if(!annotationDefinition.checkTargetValueCompatibility(paramTarget)){
					throw new AnnotationTargetNotCompatibleException(annotation.getName(), ruleId, paramTarget);
				}
			}
			//if all target were compatible, then add the annotation
			annotationsList.add(annotation);
		}
		return annotationsList;
	}

	private List<String> parseTargetParam(SingleParamOrListOfNameParamContext singleParamOrListOfNameParamContext,
			Map<String, PlaceholderStruct> placeholderMap, String ruleId) throws PRParserException {
		List<String> targetValueAsStringList = new ArrayList<>();
		List<ParamValueInterface> targetValueList = new ArrayList<>();
		List<Token> nameList = singleParamOrListOfNameParamContext.names;
		//check if there is a least one parameter with name, since the target parameter has always a name
		if(nameList!=null && nameList.size()>0){
			//check if there is the parameter target
			for(int i=0; i<nameList.size(); ++i) {
				String name = nameList.get(i).getText();
				if (name.equals(TARGET_PARAM)) {
					//this is the target param
					SingleParamOrListOfParamContext singleParamOrListOfParamContext
							= singleParamOrListOfNameParamContext.params.get(i);
					targetValueList= parseSingleParamOrListOfParam(singleParamOrListOfParamContext, placeholderMap, ruleId, null);
				}
			}
		}
		//get the values from targetValueList and trasform them as single String
		for(ParamValueInterface pvi : targetValueList){
			targetValueAsStringList.add(pvi.toString());
		}
		return targetValueAsStringList;
	}

	static public boolean checkParamTypeCompliant(String annName, String paramName, String type, List<ParamValueInterface> valueList)
			throws UnsupportedTypeInParamDefinitionException {
		//if the ParamValueInterface is an instance of ParamValuePlaceholder, then they are considered compliant
		if(type.toLowerCase().equals(TYPE_STRING)){
			if(valueList.size()==1 && (valueList.get(0) instanceof ParamValueString || valueList.get(0) instanceof ParamValuePlaceholder)){
				return true;
			} else {
				return false;
			}
		} else if (type.toLowerCase().equals(TYPE_STRING_ARRAY)){
			for(ParamValueInterface pri : valueList){
				if(!(pri instanceof  ParamValueString || pri instanceof ParamValuePlaceholder)){
					return false;
				}
			}
			return true;
		} else if (type.toLowerCase().equals(TYPE_INT)){
			if(valueList.size()==1 && (valueList.get(0) instanceof ParamValueInteger || valueList.get(0) instanceof ParamValuePlaceholder)){
				return true;
			} else {
				return false;
			}
		} else if (type.toLowerCase().equals(TYPE_INT_ARRAY)){
			for(ParamValueInterface pri : valueList){
				if(!(pri instanceof  ParamValueInteger || pri instanceof ParamValuePlaceholder)){
					return false;
				}
			}
			return true;
		} else if (type.toLowerCase().equals(TYPE_DOUBLE)){
			if(valueList.size()==1 && (valueList.get(0) instanceof ParamValueDouble || valueList.get(0) instanceof ParamValuePlaceholder)){
				return true;
			} else {
				return false;
			}
		} else if (type.toLowerCase().equals(TYPE_DOUBLE_ARRAY)){
			for(ParamValueInterface pri : valueList){
				if(!(pri instanceof  ParamValueDouble || pri instanceof ParamValuePlaceholder)){
					return false;
				}
			}
			return true;
		} else if (type.toLowerCase().equals(TYPE_IRI)){
			if(valueList.size()==1 && (valueList.get(0) instanceof ParamValueIri || valueList.get(0) instanceof ParamValuePlaceholder)){
				return true;
			} else {
				return false;
			}
		} else if (type.toLowerCase().equals(TYPE_IRI_ARRAY)){
			for(ParamValueInterface pri : valueList){
				if(!(pri instanceof  ParamValueIri || pri  instanceof ParamValuePlaceholder)){
					return false;
				}
			}
			return true;
		} else if (type.toLowerCase().equals(TYPE_LITERAL)){
			if(valueList.size()==1 && (valueList.get(0) instanceof ParamValueLiteral || valueList.get(0) instanceof ParamValuePlaceholder)){
				return true;
			} else {
				return false;
			}
		} else if (type.toLowerCase().equals(TYPE_LITERAL_ARRAY)){
			for(ParamValueInterface pri : valueList){
				if(!(pri instanceof  ParamValueLiteral || pri instanceof ParamValuePlaceholder)){
					return false;
				}
			}
			return true;
		} else {
			throw  new UnsupportedTypeInParamDefinitionException(annName, paramName, type);
		}
	}

	static public boolean isPlchd(String value){
		return (value.startsWith("$") && !value.contains(" ")) ? true : false;
	}

	static public String removeQuotation(String text, boolean removeOnlySingleQuot){
		if(text.startsWith("\"") && text.endsWith("\"") && !removeOnlySingleQuot){
			text = text.substring(1, text.length()-1);
		}
		else if(text.startsWith("'") && text.endsWith("'") ){
			text = text.substring(1, text.length()-1);
		}
		return text;
	}

	private boolean surroundedWithQuotation(String text){
		if(text.startsWith("\"") && text.endsWith("\"") ){
			return true;
		}
		else if(text.startsWith("'") && text.endsWith("'") ){
			return true;
		}
		return false;
	}

	private void parseInsertClause(InsertClauseContext insertClauseContext, ProjectionRule projectionRule) 
			throws PRParserException {
		List<GraphElementContext> graphElementContextList = insertClauseContext.graph().graphElement();
		for(GraphElementContext graphElementConcext : graphElementContextList) {
			projectionRule.addElementToInsertGraph(parseGraphElement(graphElementConcext, projectionRule, false), false);
		}
		
	}

	private void parseGraphClause(GraphClauseContext graphClauseContext, ProjectionRule projectionRule) 
			throws PRParserException {
		List<GraphElementContext> graphElementContextList = graphClauseContext.graph().graphElement();
		for(GraphElementContext graphElementContext : graphElementContextList) {
			projectionRule.addElementToInsertGraph(parseGraphElement(graphElementContext, projectionRule, false), true);
		}
	}

	private GraphElement parseGraphElement(GraphElementContext graphElementContext,
										   ProjectionRule projectionRule, boolean propPathAllowed) throws PRParserException {
		GraphElement ge;
		OptionalGraphElementContext optionalGraphElementContext = graphElementContext.optionalGraphElement();
		if(optionalGraphElementContext != null) {
			//it is optionalGraphElement, which can contain more triples
			ge = parseOptionalGraphElement(optionalGraphElementContext, projectionRule, propPathAllowed);
		} else {
			List<AnnotationContext> annotationContextList = graphElementContext.annotation();
			List<Annotation> annotationList = parseAnnotation(annotationContextList, projectionRule.getPlaceholderMap(),
					projectionRule.getId(), SECTION_GRAPH);
			GraphTripleContext graphTripleContext = graphElementContext.graphTriple();
			ge = parseGraphTriple(graphTripleContext, projectionRule, annotationList, propPathAllowed);
		}
		return ge;
		
	}

	private GraphElement parseOptionalGraphElement(OptionalGraphElementContext optionalGraphElementContext,
			ProjectionRule projectionRule, boolean propPathAllowed) throws PRParserException {
		List<GraphElement> graphElements = new ArrayList<>();
		for(GraphElementContext graphElementContext : optionalGraphElementContext.graphElement()) {
			GraphElement ge = parseGraphElement(graphElementContext, projectionRule, propPathAllowed);
			graphElements.add(ge);
		}
		return new OptionalGraphStruct(graphElements, projectionRule);
	}

	private GraphElement parseGraphTriple(GraphTripleContext graphTripleContext,
			ProjectionRule projectionRule, List<Annotation> annotationList, boolean propPathAllowed)
			throws PRParserException {
		if(graphTripleContext.graphSubject().rep_plc!=null) {
			throw new PRParamToReplaceException(graphTripleContext.graphSubject().rep_plc.getLine());
		} else if (graphTripleContext.graphPredicate().rep_plc!=null) {
			throw new PRParamToReplaceException(graphTripleContext.graphPredicate().rep_plc.getLine());
		} else if (graphTripleContext.graphObject().rep_plc!=null) {
			throw new PRParamToReplaceException(graphTripleContext.graphObject().rep_plc.getLine());
		} 
		
		GraphSingleElement subj = parseGraphSingleElementSubj(graphTripleContext.graphSubject(), projectionRule);
		GraphSingleElement pred = parseGraphSingleElementPred(graphTripleContext.graphPredicate(), projectionRule, propPathAllowed);
		GraphSingleElement obj = parseGraphSingleElementObj(graphTripleContext.graphObject(), projectionRule);
		
		// If there are some annotations related to the triple.
		if (!(annotationList == null || annotationList.isEmpty())) {
			// Clone of the List. It can be done better.
			List<Annotation> listAnnotationsClone = new ArrayList<Annotation>(annotationList);
			return new GraphStruct(subj, pred, obj, projectionRule, listAnnotationsClone);
		}
		return new GraphStruct(subj, pred, obj, projectionRule);
	}

	private GraphSingleElement parseGraphSingleElementSubj(GraphSubjectContext graphSubject,
			ProjectionRule projectionRule) throws PRParserException {
		return parseGraphSingleElement(graphSubject.var(), graphSubject.iri(), graphSubject.blankNode(), 
				graphSubject.placeholder(), null, null, null, false,
				projectionRule);
	}
	
	private GraphSingleElement parseGraphSingleElementPred(GraphPredicateContext graphPredicateContext,
			ProjectionRule projectionRule, boolean propPathAllowed) throws PRParserException {
		return parseGraphSingleElement(graphPredicateContext.var(), graphPredicateContext.iri(), null,
				graphPredicateContext.placeholder(), graphPredicateContext.abbr(), null,
				graphPredicateContext.propPath(), propPathAllowed, projectionRule);
	}
	
	private GraphSingleElement parseGraphSingleElementObj(GraphObjectContext graphObjectContext,
			ProjectionRule projectionRule) throws PRParserException {
		return parseGraphSingleElement(graphObjectContext.var(), graphObjectContext.iri(), 
				graphObjectContext.blankNode(), graphObjectContext.placeholder(), null, graphObjectContext.literal(),
				null, false, projectionRule);
	}
	
	private GraphSingleElement parseGraphSingleElement(VarContext var, IriContext iriContext,
			   BlankNodeContext blankNodeContext, PlaceholderContext placeholderContext,
			   AbbrContext abbrContext, LiteralContext literalContext, PropPathContext propPathContext,
			   boolean propPathAllowed, ProjectionRule projectionRule)
					throws PRParserException {
		//check which one is not null
		if(var!= null) {
			return new GraphSingleElemVar(parseVariable(var), projectionRule);
		} else if(iriContext != null) {
			return new GraphSingleElemUri(parseIri(iriContext.getText(), projectionRule.getId(), true, false),
					projectionRule);
		} else if(blankNodeContext != null) {
			return new GraphSingleElemBNode(parseBlankNode(blankNodeContext), projectionRule);
		} else if(placeholderContext != null) {
			return parsePlaceholder(placeholderContext, projectionRule);
		} else if(abbrContext != null) {
			return new GraphSingleElemUri(resolveAbbreviation(abbrContext), projectionRule);
		} else if(propPathContext!=null) {
			//if there is a propPathContext but propPathAllowed is false, then there is a problem, since a propPath is
			// not allowed
			if (!propPathAllowed) {
				// the propPath cannot be used in this case, so throw an exception
				throw new PropPathNotAllowedException(projectionRule.getId());
			}
			//the propPath can be used, so parse it
			return parsePropPath(propPathContext, projectionRule);
		} else { // if(literalContext != null) {
			return new GraphSingleElemLiteral(parseLiteral(literalContext, projectionRule.getId(), false), projectionRule);
		}
	}



	private void parseDeleteClause(DeleteClauseContext deleteClauseContext, ProjectionRule projectionRule) 
			throws PRParserException {
		if(projectionRule.isGraphSection()){
			throw new DeleteAndGraphSectionException(projectionRule.getId());
		}
		List<GraphElementContext> graphElementContextList = deleteClauseContext.graph().graphElement();
		for(GraphElementContext graphElementConcext : graphElementContextList) {
			projectionRule.addElementToDeleteGraph(parseGraphElement(graphElementConcext, projectionRule, false));
		}
		
	}

	private void parseWhereClause(WhereClauseContext whereClauseContext, ProjectionRule projectionRule) 
			throws PRParserException {
		List<GraphElementContext> graphElementContextList = whereClauseContext.graph().graphElement();
		for(GraphElementContext graphElementConcext : graphElementContextList) {
			projectionRule.addElementToWhere(parseGraphElement(graphElementConcext, projectionRule, true));
		}
		
	}
	
	
	private void parseLazyRule(LazyRuleContext lazyRuleContext, List<AnnotationContext> annotationContextList) throws PRParserException {
		String ruleId = lazyRuleContext.ruleId().getText().substring(3);
		ProjectionRule projectionRule = new ProjectionRule(ruleId, prModel);
		projectionRule.setLaziness(true);
		
		String uimaTypeName = lazyRuleContext.uimaTypeName().getText();
		projectionRule.setUimaTypeName(uimaTypeName);
		NodesClauseContext nodesClauseContext = lazyRuleContext.nodesClause();
		parseNodesClause(nodesClauseContext, projectionRule);

		//add the annotations, if present
		if(annotationContextList!=null) {
			// pass an empty HashMap instead of the projectionRule.getPlaceholderMap() since the annotations associated
			// to a rule SHOULD NOT use placeholders

			List<Annotation> annotationList = parseAnnotation(annotationContextList,
					new HashMap<>(), projectionRule.getId(), SECTION_ENTIRE_RULE);
			if (!annotationList.isEmpty()) {
				projectionRule.setAnnotations(annotationList);
			}
		}

		prModel.addProjectionRule(projectionRule);
	}

	private void parseForRegexRule(ForRegexRuleContext forRegexRuleContext, List<AnnotationContext> annotationContextList) throws PRParserException {
		String ruleId = forRegexRuleContext.ruleId().getText().substring(3);
		ProjectionRule projectionRule = new ProjectionRule(ruleId, prModel);
		projectionRule.setIfForRegex(true);
		
		String uimaTypeName = forRegexRuleContext.uimaTypeName().getText();
		projectionRule.setUimaTypeName(uimaTypeName);
		
		ConditionClauseContext conditionClauseContext = forRegexRuleContext.conditionClause();
		if(conditionClauseContext != null) {
			parseConditionaClause(conditionClauseContext, projectionRule);
		}
		
		NodesClauseContext nodesClauseContext = forRegexRuleContext.nodesClause();
		parseNodesClause(nodesClauseContext, projectionRule);

		//add the annotations, if present
		if(annotationContextList!=null) {
			// pass an empty HashMap instead of the projectionRule.getPlaceholderMap() since the annotations associated
			// to a rule SHOULD NOT use placeholders

			List<Annotation> annotationList = parseAnnotation(annotationContextList,
					new HashMap<>(), projectionRule.getId(), SECTION_ENTIRE_RULE);
			if (!annotationList.isEmpty()) {
				projectionRule.setAnnotations(annotationList);
			}
		}
	
		prModel.addProjectionRule(projectionRule);
	}
	

	private void parseRegex(RegexContext regexContext) throws PRParserException {
		String ruleId = regexContext.ruleId().getText().substring(3);
		
		// get the regex (which has to be converted first in a NFSA e and then in a DFSA)
		RegexPatternContext regexPatternContext = regexContext.regexPattern();
		SingleRegexStruct singleRegexStruct = parseRegexToFSA(regexPatternContext, ruleId);
		
		// the projection rule is not really used, but it is needed by parseGraph, so just create a fake one 
		//with the right id
		ProjectionRule projectionRule = new ProjectionRule(ruleId, prModel);
		GraphClauseContext graphClauseContext = regexContext.graphClause();
		if(graphClauseContext != null) {
			parseGraphClause(graphClauseContext, projectionRule);
		}
		Collection<GraphElement> graphList = projectionRule.getInsertGraphList();
		
		// use all these information to construct the RegexProjectionRule
		RegexProjectionRule regexProjectionRule = new RegexProjectionRule(ruleId, singleRegexStruct, graphList, prModel);

		// add the regex structure to the prModel
		prModel.addRegex(regexProjectionRule);
	}

	private SingleRegexStruct parseRegexToFSA(RegexPatternContext regexPatternContext, String ruleId) {

		SingleRegexStruct singleRegexStruct = new SingleRegexStruct(ruleId);
		
		// first create a base FSA for the NFSA
		FSA nfsa = singleRegexStruct.getNfsa();
		// create the start state
		StateFSA startState = singleRegexStruct.createNewState(REGEX_PREFIX_NFSA_STATE, nfsa);
		startState.setIsStartState(true);
		
		// add this state to the FSA
		singleRegexStruct.getNfsa().addStartState(startState);
		
		List<StateFSA> endStateList = new ArrayList<>();
		// set the endStateList to contain the startState for the first iteration
		endStateList.add(startState);
		
		// now start parsing the tree containing the complete regex
		StateFSA currentStartState = startState;
		currentStartState = parseRegexOrElem(regexPatternContext.regexWithOr(), currentStartState,
				singleRegexStruct);
		
		// set the currentStartState as the end state of the FSA
		currentStartState.setIsEndState(true);
		singleRegexStruct.getNfsa().addEndState(currentStartState);
		// StateFSA endState = new StateFSA("S_I_last", true, false);

		// convert the nfsa to dfsa
		singleRegexStruct.convertNFSAtoDFSA(REGEX_PREFIX_NFSA_STATE, REGEX_PREFIX_DFSA_STATE,
				REGEX_PREFIX_DFSA_TRANSITION);
		
		return singleRegexStruct;
	}

	private void setDependsPlchldFromGraphElem(GraphElement graphElem) {
		if (graphElem instanceof OptionalGraphStruct) {
			OptionalGraphStruct optionalGraphStruct = (OptionalGraphStruct) graphElem;
			for (GraphElement graphElemFromOptional : optionalGraphStruct.getOptionalTriples()) {
				setDependsPlchldFromGraphElem(graphElemFromOptional);
			}
		} else {
			GraphSingleElemPlaceholder tempGSEP;
			GraphStruct gs = (GraphStruct) graphElem;
			GraphSingleElement subjGSE = gs.getSubject();
			if (subjGSE instanceof GraphSingleElemPlaceholder) {
				tempGSEP = (GraphSingleElemPlaceholder) subjGSE;
				if (tempGSEP.isFromDependsOnRule()) {
					String altDepRuleId = tempGSEP.getName().split(CODACore.PLACEHOLDER_DEPENDS_REGEX)[0];
					String depRulePlchld = tempGSEP.getName().split(CODACore.PLACEHOLDER_DEPENDS_REGEX)[1];
					graphElem.getOwnerRule().addPlchldUsedWithAltId(altDepRuleId, depRulePlchld);
				}
			}
			GraphSingleElement predGSE = gs.getPredicate();
			if (predGSE instanceof GraphSingleElemPlaceholder) {
				tempGSEP = (GraphSingleElemPlaceholder) predGSE;
				if (tempGSEP.isFromDependsOnRule()) {
					String altDepRuleId = tempGSEP.getName().split(CODACore.PLACEHOLDER_DEPENDS_REGEX)[0];
					String depRulePlchld = tempGSEP.getName().split(CODACore.PLACEHOLDER_DEPENDS_REGEX)[1];
					graphElem.getOwnerRule().addPlchldUsedWithAltId(altDepRuleId, depRulePlchld);
				}
			}
			GraphSingleElement objGSE = gs.getObject();
			if (objGSE instanceof GraphSingleElemPlaceholder) {
				tempGSEP = (GraphSingleElemPlaceholder) objGSE;
				if (tempGSEP.isFromDependsOnRule()) {
					String altDepRuleId = tempGSEP.getName().split(CODACore.PLACEHOLDER_DEPENDS_REGEX)[0];
					String depRulePlchld = tempGSEP.getName().split(CODACore.PLACEHOLDER_DEPENDS_REGEX)[1];
					graphElem.getOwnerRule().addPlchldUsedWithAltId(altDepRuleId, depRulePlchld);
				}
			}
		}
	}
	
	private String parseIri(String iriOrPrefixName, String ruleId, boolean savePrefix, boolean returnNTformat) throws PRParserException {
		if(iriOrPrefixName.startsWith("<")) {
			//it is an iri
			if(returnNTformat){
				return iriOrPrefixName;
			} else {
				return iriOrPrefixName.substring(1, iriOrPrefixName.length() - 1);
			}
		} else if(iriOrPrefixName.contains(":")) {
			//it is a prefixName
			String[] prefixNameArray = iriOrPrefixName.split(":");
			String namespace = prefixMap.get(prefixNameArray[0]);
			if(namespace==null) {
				throw new PrefixNotDefinedException(prefixNameArray[0], ruleId);
			}
			if(savePrefix) {
				prModel.addUsedPrefix(prefixNameArray[0]);
			}
			String iriNoNT = namespace + prefixNameArray[1];
			if(returnNTformat){
				return "<"+iriNoNT+">";
			} else {
				return iriNoNT;
			}
		} else {
				//TODO this should never happen, decide what to do
			return "";
		}
	}
	
	private String parseBlankNode(BlankNodeContext blankNodeContext) {
		return blankNodeContext.getText().substring(2); // strip leading _:
	}
	
	private String resolveAbbreviation(AbbrContext abbrContext) {
		String text = abbrContext.getText();
		if (text.equals("a")) {
			return "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
		}

		throw new IllegalArgumentException("Unknown abbreviation");
	}
	
	private String parseVariable(VarContext var) {
		return var.getText();
	}
	
	private GraphSingleElemPlaceholder parsePlaceholder(PlaceholderContext placeholderContext, 
			ProjectionRule projectionRule) throws PRParserException {
		GraphSingleElemPlaceholder graphSingleElemPlaceholder;
		
		String name = placeholderContext.getText();
		if(placeholderContext.separator != null) {
			//it is in the one of the for 
			//	VAR2 separator='.' JAVA_IDENTIFIER
			//	VAR2 separator='..' JAVA_IDENTIFIER
			if(placeholderContext.separator.getText().equals(".")) {
				graphSingleElemPlaceholder = new GraphSingleElemPlaceholder(name, projectionRule, false, true);
				return graphSingleElemPlaceholder;
			} else { // placeholderContext.separator.getText().equals("..")
				graphSingleElemPlaceholder = new GraphSingleElemPlaceholder(name, projectionRule, true, false);
				return graphSingleElemPlaceholder;
			}
		} else {
			//it is a simple and plain placeholder
			//check if in the current projection rule the placeholder is defined (in the node section)
			if(projectionRule.getPlaceholderMap().get(name.substring(1)) == null){
				throw new NodeNotDefinedException(name.substring(1), projectionRule.getId());
			}
			graphSingleElemPlaceholder = new GraphSingleElemPlaceholder(name, projectionRule, false, false);
			return graphSingleElemPlaceholder;
		}
		
		/* OLD VERSION, NOW I SHOULD HAVE THE NAME DIRECTLY 
		String var2 = placeholderContext.VAR2().getText();
		String name;
		if(placeholderContext.separator != null) {
			//it is in the one of the for 
			//	VAR2 separator='.' JAVA_IDENTIFIER
			//	VAR2 separator='..' JAVA_IDENTIFIER
			String finalPart = placeholderContext.JAVA_IDENTIFIER().getText();
			if(placeholderContext.separator.getText().equals(".")) {
				name = var2 + "." + finalPart;
				graphSingleElemPlaceholder = new GraphSingleElemPlaceholder(name, projectionRule, false, true);
				return graphSingleElemPlaceholder;
			} else { // placeholderContext.separator.getText().equals("..")
				name = var2 + ".." + finalPart;
				graphSingleElemPlaceholder = new GraphSingleElemPlaceholder(name, projectionRule, true, false);
				return graphSingleElemPlaceholder;
			}
		}
		else {
			//it is a simple and plain placeholder
			//check if in the current projection rule the placeholder is defined (in the node section)
			if(projectionRule.getPlaceholderMap().get(name.substring(1)) == null){
				throw new NodeNotDefinedException(name.substring(1), projectionRule.getId());
			}
			name = var2;
			graphSingleElemPlaceholder = new GraphSingleElemPlaceholder(name, projectionRule, false, false);
			return graphSingleElemPlaceholder;
		}
		*/
	}

	private GraphSingleElemPropPath parsePropPath(PropPathContext propPathContext, ProjectionRule projectionRule)
			throws PRParserException {
		GraphSingleElemPropPath graphSingleElemPropPath;

		PathAlternativeElemPropPath pathAlternativeElemPropPath = parsePathAlternative(propPathContext.pathAlternative(),
				projectionRule);
		String completePath = sanitazeCompletePath(pathAlternativeElemPropPath.getValueAsString());

		graphSingleElemPropPath = new GraphSingleElemPropPath(completePath, projectionRule);

		return graphSingleElemPropPath;
	}

	private String sanitazeCompletePath(String completePath) {
		return completePath.replaceAll("  +", " ").trim();
	}

	private PathAlternativeElemPropPath parsePathAlternative(PathAlternativeContext pathAlternativeContext,
															 ProjectionRule projectionRule) throws PRParserException {
		PathAlternativeElemPropPath pathAlternativeElemPropPath = new PathAlternativeElemPropPath();
		for (PathSequenceContext pathSequenceContext : pathAlternativeContext.pathSequence()) {
			pathAlternativeElemPropPath.addPathSequenceElemPropPath(parsePathSequence(pathSequenceContext, projectionRule));
		}
		return pathAlternativeElemPropPath;
	}

	private PathSequenceElemPropPath parsePathSequence(PathSequenceContext pathSequenceContext, ProjectionRule projectionRule) throws PRParserException {
		PathSequenceElemPropPath pathSequenceElemPropPath = new PathSequenceElemPropPath();
		for (PathEltOrInverseContext pathEltOrInverseContext : pathSequenceContext.pathEltOrInverse()) {
			PathEltOrInverseElemPropPath pathEltOrInverseElemPropPath = parsePathEltOrInverse(pathEltOrInverseContext,
					projectionRule);
			pathSequenceElemPropPath.addPathEltOrInverseElemPropPath(pathEltOrInverseElemPropPath);
		}

		return pathSequenceElemPropPath;
	}

	private PathEltOrInverseElemPropPath parsePathEltOrInverse(PathEltOrInverseContext pathEltOrInverseContext,
															   ProjectionRule projectionRule) throws PRParserException {
		PathEltOrInverseElemPropPath pathEltOrInverseElemPropPath;
		//check if it has the starting '^'
		String completeText = pathEltOrInverseContext.getText();
		boolean hasInverse = completeText.startsWith("^");
		pathEltOrInverseElemPropPath = new PathEltOrInverseElemPropPath(
				parsePartElt(pathEltOrInverseContext.pathElt(), projectionRule), hasInverse);
		return pathEltOrInverseElemPropPath;
	}

	private PathEltElemPropPath parsePartElt(PathEltContext pathEltContext, ProjectionRule projectionRule) throws PRParserException {
		PathEltElemPropPath pathEltElemPropPath;
		pathEltContext.REGEX_SYMBOL();

		// first of all, take the regex symbol (?, *, +), if present
		String symbol = null;
		TerminalNode regexSymbol = pathEltContext.REGEX_SYMBOL();
		if(regexSymbol != null) {
			symbol = regexSymbol.getText();
		}
		if (symbol != null) {
			pathEltElemPropPath = new PathEltElemPropPath(parsePathPrimary(pathEltContext.pathPrimary(), projectionRule), symbol);
		} else {
			//no regex symbol
			pathEltElemPropPath = new PathEltElemPropPath(parsePathPrimary(pathEltContext.pathPrimary(), projectionRule));
		}
		return pathEltElemPropPath;
	}

	private PathPrimaryElemPropPath parsePathPrimary(PathPrimaryContext pathPrimaryContext, ProjectionRule projectionRule)
			throws PRParserException {
		PathPrimaryElemPropPath pathPrimaryElemPropPath;
		//check which element is inside the pathPrimaryContext
		if (pathPrimaryContext.getText().equals("a")) {
			// it is the 'a' case
			pathPrimaryElemPropPath = new PathPrimaryElemPropPath("a");
		} else if (pathPrimaryContext.iri()!=null) {
			// it is the iri case, so try to resolve such iri
			pathPrimaryElemPropPath = new PathPrimaryElemPropPath(parseIri(pathPrimaryContext.iri().getText(),
					projectionRule.getId(), true, true ));
		} else if (pathPrimaryContext.pathNegatedPropertySet()!=null) {
			// it is the '!' pathNegatedPropertySet case
			pathPrimaryElemPropPath = new PathPrimaryElemPropPath(
					parsePathNegatedPropertySet(pathPrimaryContext.pathNegatedPropertySet(), projectionRule));
		} else { // pathPrimaryContext.pathAlternative() != null
			pathPrimaryElemPropPath = new PathPrimaryElemPropPath(parsePathAlternative(
					pathPrimaryContext.pathAlternative(), projectionRule));
		}

		return pathPrimaryElemPropPath;
	}

	private PathNegatedPropertySetElemPropPath parsePathNegatedPropertySet(PathNegatedPropertySetContext pathNegatedPropertySetContext,
																		   ProjectionRule projectionRule) throws PRParserException {
		PathNegatedPropertySetElemPropPath pathNegatedPropertySetElemPropPath = new PathNegatedPropertySetElemPropPath();

		for (PathOneInPropertySetContext pathOneInPropertySetContext : pathNegatedPropertySetContext.pathOneInPropertySet()) {
			PathOneInPropertySetElemPropPath pathOneInPropertySetElemPropPath =
					parsePathOneInPropertySet(pathOneInPropertySetContext, projectionRule);
			pathNegatedPropertySetElemPropPath.addPathOneInPropertySetElemPropPath(pathOneInPropertySetElemPropPath);
		}

		return pathNegatedPropertySetElemPropPath;
	}

	private PathOneInPropertySetElemPropPath parsePathOneInPropertySet(PathOneInPropertySetContext pathOneInPropertySetContext,
																	   ProjectionRule projectionRule) throws PRParserException {
		PathOneInPropertySetElemPropPath pathOneInPropertySetElemPropPath;
		// check which of the following case is:  iri | 'a' | '^' ( iri | 'a' )

		//check if it starts with '^'
		String completeText = pathOneInPropertySetContext.getText();
		boolean hasInverse = completeText.startsWith("^");
		//check if it has iri (if not, then it is the 'a' case)
		if (pathOneInPropertySetContext.iri()!=null) {
			// it has the iri
			pathOneInPropertySetElemPropPath = new PathOneInPropertySetElemPropPath(
					parseIri(pathOneInPropertySetContext.iri().getText(), projectionRule.getId(), true, true ),
					hasInverse);
		} else {
			// since it has no iri, then it is the case 'a' (with or without the starting '^')
			pathOneInPropertySetElemPropPath = new PathOneInPropertySetElemPropPath("a",hasInverse);
		}
		return pathOneInPropertySetElemPropPath;
	}


	private String parseLiteral(LiteralContext literalContext, String ruleId, boolean returnNTformat)
			throws PRParserException {
		String literal;
		literal = sanitizeString(literalContext.string().getText());
		// now check if there is a langtag or a datatype (or neither one)
		if(literalContext.LANGTAGORANNOTATION() != null) {
			literal+=literalContext.LANGTAGORANNOTATION();
		} else if(literalContext.iri() != null) {
			literal+="^^"+parseIri(literalContext.iri().getText(), ruleId, true, returnNTformat);
		}

		return literal;
	}

	/*************** private functions to support the parsing *******************/

	private String sanitizeString(String text) {
		// this function is used to sanitize the value coming from the "string" element of the grammar, mainly to deal
		// with characters '\' and its use as an escape characters
		text = text.replaceAll("\\\\\\\\", "\\\\");

		return text;
	}
	
	/*************** private functions to parse the regex part ******************/
	
	private StateFSA parseRegexOrElem(RegexWithOrContext regexWithOrContext, StateFSA startState, 
			SingleRegexStruct singleRegexStruct) {

		FSA nfsa = singleRegexStruct.getNfsa();
		StateFSA endState;
		// check if there are more than a single child, because it can happen that the first
		// AST_REGEX_OR has just one child
		
		if (regexWithOrContext.regexSequenceElement().size() == 1) {
			// there is jus one child
			endState = parseRegexSequenceElem(regexWithOrContext.regexSequenceElement().get(0), startState, 
					singleRegexStruct);
		
		} else { // there are more than a single child
			// startState : A
			// A -T1-> B
			// B -Epsylon-> C
			// A -T2-> D
			// D -Epsylon-> C
			// ...
			// endState : C
			endState = singleRegexStruct.createNewState(REGEX_PREFIX_NFSA_STATE, nfsa);
		
			//for (int i = 0; i < regexWithOrContext.getChildCount(); ++i) {
			for(RegexSequenceElementContext regexSequenceElementContext : 
					regexWithOrContext.regexSequenceElement()) {
		
				// add the transition between startState and internalState1 and (X -T1-> B)
				StateFSA internalState = parseRegexSequenceElem(regexSequenceElementContext, startState,
						singleRegexStruct);
		
				// now add the Epsilon transition between internalState1 and the endState (X -Epsylon-> C)
				singleRegexStruct.createNewEpsilonTransition(REGEX_PREFIX_NFSA_TRANSITION, nfsa,
						internalState, endState);
			}
		}
		
		return endState;
	}
		
	private StateFSA parseRegexSequenceElem(RegexSequenceElementContext regexSequenceElementContext, 
			StateFSA startState, SingleRegexStruct singleRegexStruct)  {
		
		// startState : A
		// A -T1-> B
		// B -T2-> C
		// ...
		// (N-1) -Epsylon-> N
		// endState : N
		
		StateFSA lastStartState = startState;
		StateFSA lastEndState = null;
		//for (int i = 0; i < regexSequenceElementContext.getChildCount(); ++i) {
		for(RegexBaseElementWithSymbolContext regexBaseElementWithSymbolContext : 
				regexSequenceElementContext.regexBaseElementWithSymbol()) {
			// add the transition between currenstStateState and currenstEndState
			lastEndState = parseRegexBaseElem(regexBaseElementWithSymbolContext, lastStartState, 
					singleRegexStruct);
			lastStartState = lastEndState;
		}
		
		return lastEndState;
	}
	
	private StateFSA parseRegexBaseElem(RegexBaseElementWithSymbolContext regexBaseElementWithSymbolContext, 
			StateFSA startState, SingleRegexStruct singleRegexStruct)  {
		
		StateFSA endState = null;
		// first of all, take the regex symbol (?, *, +), if present
		String symbol = null;
		TerminalNode regexSymbol = regexBaseElementWithSymbolContext.REGEX_SYMBOL();
		if(regexSymbol != null) {
			symbol = regexSymbol.getText();
		} 
		RegexBaseElementContext regexBaseElementContext = regexBaseElementWithSymbolContext.regexBaseElement();
		if(symbol == null) {
			endState = parseRegexOneElem(regexBaseElementContext, startState, singleRegexStruct);
		} else {
			switch (symbol) {
			case REGEX_SYMBOL_ZEROORONE:
				endState = parseRegexZeroOrOneElem(regexBaseElementContext, startState, singleRegexStruct);
				break;
			case REGEX_SYMBOL_ZEROORMORE:
				endState = parseRegexZeroOrMoreElem(regexBaseElementContext, startState, singleRegexStruct);
				break;
			case REGEX_SYMBOL_ONEORMORE:
				endState = parseRegexOneOrMoreElem(regexBaseElementContext, startState, singleRegexStruct);
				break;
			}
		}
		return endState;
	}
		
	private StateFSA parseRegexOneElem(RegexBaseElementContext regexBaseElementContext, StateFSA startState, 
			SingleRegexStruct singleRegexStruct) {
		
		// startState : A
		// A -T-> B
		// endState : B
		
		StateFSA endState;
		
		if (regexBaseElementContext.regexRuleId !=null) {
			//it is really just a just a base regex
			String ruleId = regexBaseElementContext.regexRuleId.getText();
			String internalId = regexBaseElementContext.internalId.getText();
			int maxDistance = 0;
			if (regexBaseElementContext.maxDist != null) {
				maxDistance = Integer.parseInt(regexBaseElementContext.maxDist.getText());
			}
			singleRegexStruct.addInternIdToRuleId(internalId, ruleId);
			FSA nfsa = singleRegexStruct.getNfsa();
			// create the end state
			endState = singleRegexStruct.createNewState(REGEX_PREFIX_NFSA_STATE, nfsa);
			// create a new transition from startState to endState (A -T-> B)
			singleRegexStruct.createNewTransition(REGEX_PREFIX_NFSA_TRANSITION, nfsa, internalId, maxDistance,
					startState, endState);
		} else {
			//it is a regexWithOr
			endState = parseRegexOrElem(regexBaseElementContext.regexWithOr(), startState, singleRegexStruct);
		}
		
		return endState;
	}
	
	private StateFSA parseRegexZeroOrOneElem(RegexBaseElementContext regexBaseElementContext, StateFSA startState,
			SingleRegexStruct singleRegexStruct)  {
		
		FSA nfsa = singleRegexStruct.getNfsa();
		
		// startState : A
		// A -T-> B
		// A -Epsylon-> B
		// endState : B
		
		StateFSA endState;
		// add the transition between startState and endState (A -T-> B)
		if (regexBaseElementContext.regexRuleId !=null) {
			//it is a just a base regex
			endState = parseRegexOneElem(regexBaseElementContext, startState, singleRegexStruct);
		} else {
			endState = parseRegexOrElem(regexBaseElementContext.regexWithOr(), startState, singleRegexStruct);
		}
		
		// now add the Epsilon transition between the startState and the endState (A -Epsylon-> B)
		singleRegexStruct
				.createNewEpsilonTransition(REGEX_PREFIX_NFSA_TRANSITION, nfsa, startState, endState);
		
		return endState;
	}
		
	private StateFSA parseRegexZeroOrMoreElem(RegexBaseElementContext regexBaseElementContext, StateFSA startState,
			SingleRegexStruct singleRegexStruct)  {
		
		FSA nfsa = singleRegexStruct.getNfsa();
		
		// startState : A
		// A -Epsylon-> B
		// B -T-> C
		// C -Epsylon-> B
		// endState : B
		
		// add the Epsilon transition between startState and the endState (A -Epsylon-> B)
		StateFSA endState = singleRegexStruct.createNewState(REGEX_PREFIX_NFSA_STATE, nfsa);
		singleRegexStruct
				.createNewEpsilonTransition(REGEX_PREFIX_NFSA_TRANSITION, nfsa, startState, endState);
		
		// now add the transition between endState and internalState and (B -T-> C)
		StateFSA internalState;
		if (regexBaseElementContext.regexRuleId !=null) {
			internalState = parseRegexOneElem(regexBaseElementContext, endState, singleRegexStruct);
		} else {
			internalState = parseRegexOrElem(regexBaseElementContext.regexWithOr(), endState, singleRegexStruct);
		}
		// now add the Epsilon transition between the internalState and the endState (C -Epsylon-> B)
		singleRegexStruct.createNewEpsilonTransition(REGEX_PREFIX_NFSA_TRANSITION, nfsa, internalState,
				endState);
		
		return endState;
	}
	
	private StateFSA parseRegexOneOrMoreElem(RegexBaseElementContext regexBaseElementContext, StateFSA startState,
			SingleRegexStruct singleRegexStruct) {
		
		FSA nfsa = singleRegexStruct.getNfsa();
		
		// startState : A
		// A -T-> B
		// B -T-> C
		// C -Epsylon-> B
		// endState : B
		
		StateFSA endState;
		// add the transition between startState and endState (A -T-> B)
		if (regexBaseElementContext.regexRuleId !=null) {
			endState = parseRegexOneElem(regexBaseElementContext, startState, singleRegexStruct);
		} else {
			endState = parseRegexOrElem(regexBaseElementContext.regexWithOr(), startState, singleRegexStruct);
		}
		
		StateFSA internalState;
		// now add the same transition between the endState and the internalState (B -T-> C)
		if (regexBaseElementContext.regexRuleId !=null) {
			internalState = parseRegexOneElem(regexBaseElementContext, endState, singleRegexStruct);
		} else {
			internalState = parseRegexOrElem(regexBaseElementContext.regexWithOr(), endState, singleRegexStruct);
		}
		
		// now add the Epsilon transition between the internalState and the endState (C -Epsylon-> B)
		singleRegexStruct.createNewEpsilonTransition(REGEX_PREFIX_NFSA_TRANSITION, nfsa, internalState,
				endState);
		
		return endState;
	}
	
	
	
	/*******************************************************/
	//TEST to delete when everything is working 
	public static void main(String[] args) {
		File inputFile = new File("regexOr3Elements.pr");
		System.out.println("inputFile = "+inputFile.getAbsolutePath());
		
		PearlParserDescription parserDescription = new PearlParserDescription();
		ProjectionRulesModel projectionRulesModel;
		try {
			projectionRulesModel = parserDescription.parsePearlDocument(inputFile, true);
			
			System.out.println(projectionRulesModel.getModelAsStringForDebug());
		} catch (PRParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
	}
}
