package it.uniroma2.art.coda.structures.table;

import org.eclipse.rdf4j.model.Value;

public class SingleTableValueVariable extends SingleTableValue {

	public SingleTableValueVariable(String id, Value value) {
		super(id, value);
	}

}
