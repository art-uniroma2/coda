package it.uniroma2.art.coda.structures;

import org.eclipse.rdf4j.model.Value;

/**
 * This class can contain an ARTNode or a String
 * 
 * @author Andrea Turbati
 * 
 */
public class ValueOrString {
	private Value value;
	private String string;

	/**
	 * @param value
	 *            the Value that this instance will contain
	 */
	public ValueOrString(Value value) {
		this.value = value;
		string = null;
	}

	/**
	 * @param string
	 *            the string this instance will contain
	 */
	public ValueOrString(String string) {
		value = null;
		this.string = string;
	}

	/**
	 * check if this instance contains a Value
	 * 
	 * @return true if it contains a Value, false otherwise (it contains a String)
	 */
	public boolean isValue() {
		return (value != null);
	}
	
	/**
	 * check if this instance contains a String
	 * 
	 * @return true if it contains a String, false otherwise (it contains a Value)
	 */
	public boolean isString() {
		return (string != null);
	}

	/**
	 * Get the Value. Should be used only if {@link #isValue()} returns true
	 * 
	 * @return the Value or null if it contains a String
	 */
	public Value getValue() {
		return value;
	}

	/**
	 * Get the String contained in this instance. Should be used only if {@link #isString()} returns true
	 * 
	 * @return the String or null if it contains a Value
	 */
	public String getString() {
		return string;
	}

}
