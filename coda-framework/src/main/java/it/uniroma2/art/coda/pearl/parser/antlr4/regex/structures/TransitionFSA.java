package it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures;

public class TransitionFSA {

	private boolean isEpsilonTransition; // true is it is a en epsylon transition, false otherwise
	
	private String transitionId; // is a unique value inside the FSA (e.g. T_I_1 or T_I_42)
	
	private String internalId;	// is a value associated to a specific UIMA type name, two transition could 
							  	// have the same internalId
	
	private int maxDistance;
	private boolean isMaxDistPresent;
	
	private StateFSA startingState;
	
	private StateFSA endingState;

	private final int NO_MAX_DISTANCE = 0;
	
	public TransitionFSA(String transitionId, StateFSA startingState, StateFSA endingState) {
		initialize(true, transitionId, null, NO_MAX_DISTANCE, startingState, endingState);
	}
	
	public TransitionFSA(String transitionId, String internalId, int maxDistance,
			StateFSA staringState, StateFSA endingState) {
		initialize(false, transitionId, internalId, maxDistance, staringState, endingState);
	}
	
	
	private void initialize(boolean isEpsilonTransition, String transitionId, String internalId, int maxDistance,
			StateFSA startingState, StateFSA endingState){
		this.isEpsilonTransition = isEpsilonTransition;
		this.transitionId = transitionId;
		this.internalId = internalId;
		if(maxDistance != NO_MAX_DISTANCE){
			isMaxDistPresent = true;
		} else {
			isMaxDistPresent = false;
		}
		this.maxDistance = maxDistance;
		this.startingState = startingState;
		this.endingState = endingState;
		//add the transition from the startingState to the endingState
		if(isEpsilonTransition){
			startingState.addEpsylonTransition(endingState);
		} else{
			startingState.addTransitionToState(this, endingState);
		}
	}

	public boolean isEpsilonTransition() {
		return isEpsilonTransition;
	}

	public String getTransitionId() {
		return transitionId;
	}

	public String getInternalId() {
		return internalId;
	}

	public int getMaxDistance() {
		return maxDistance;
	}
	
	public boolean isMaxDistancePresent(){
		return isMaxDistPresent;
	}

	public StateFSA getStartingState() {
		return startingState;
	}

	public StateFSA getEndingState() {
		return endingState;
	}
	
	
	
}
