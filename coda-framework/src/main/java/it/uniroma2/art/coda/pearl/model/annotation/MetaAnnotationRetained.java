package it.uniroma2.art.coda.pearl.model.annotation;


import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;

public class MetaAnnotationRetained extends MetaAnnotation {

	boolean hasRetained;

	public MetaAnnotationRetained(boolean hasRetained) {
		super(PearlParserDescription.RETAINED_ANN_DECL);
		this.hasRetained = hasRetained;
	}

	public boolean hasRetained(){
		return hasRetained;
	}
}
