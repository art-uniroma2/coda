package it.uniroma2.art.coda.depends;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.provisioning.ComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.structures.DependsOnInfo;
import it.uniroma2.art.coda.structures.table.ValuesFromAnAnnotation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

public class DependsBetween extends DependsAbstactClass {

	public DependsBetween() {
		super("between");
	}

	@Override
	public List<ValuesFromAnAnnotation> execute(DependsOnInfo dependsOnInfo, Annotation annotation, JCas jcas,
			ProjectionRulesModel projectionRulesModel, ProjectionRule projRule, ComponentProvider cp,
			CODAContext ctx) throws ComponentProvisioningException, ConverterException, DependencyException {
		boolean random = false;
		// analyze the parameters passed for this dependency
		List<String> paramsList = dependsOnInfo.getParamsList();
		for (String param : paramsList) {
			if (param.startsWith(PARAM_RANDOM)) {
				// this parameter represent the desired for a random annotation or not, check its 
				//associated value
				if(param.contains("t")){
					random = true;
				}
			}
		}

		List<ValuesFromAnAnnotation> valuesFromAnAnnotationList = new ArrayList<ValuesFromAnAnnotation>();
		// get the id of the other rule (this dependency should have just one rule id)
		List<String> depRuleIdList = dependsOnInfo.getDependsOnRuleIdList();
		if(depRuleIdList.size() != 1){
			throw new DependencyException("The dependency between should have just one depencency rule id, " +
					"istead it has "+depRuleIdList.size()+" ruleId(s)");
		}
		String depRuleId = depRuleIdList.get(0);
		// get the type of the annotation that can be used with that rule
		ProjectionRule depProjRule = projectionRulesModel.getProjRuleFromId(depRuleId);
		if (depProjRule == null) {
			// no projection rule exists with this particular id, this should never happen
			return valuesFromAnAnnotationList;
		}

		FSIterator<Annotation> iter = jcas.getAnnotationIndex().iterator();
		List<Annotation> candidateAnnotaionList = new ArrayList<Annotation>();
		while (iter.hasNext()) {
			Annotation otherAnnotation = iter.next();
			String otherAnnName = otherAnnotation.getType().getName();
			boolean discard = false;
			if (depProjRule.getUIMAType().compareTo(otherAnnName) == 0) {
				// found a compatible annotation, now see if this is the right one
				// first check if it is before the current annotation
				if ((otherAnnotation.getBegin() < annotation.getBegin())
						|| (otherAnnotation.getEnd() > annotation.getEnd())) {
					// the otherAnnotation begins before the selected annotation begins or ends after the
					// selected annotation end, so it cannot be a good candidate
					discard = true;
				}
				if (otherAnnotation.getBegin() > annotation.getEnd()) {
					// the otherAnnotation starts after the current annotation ends, since they are ordered,
					// all the next annotations cannot be a good candidate, so stop searching
					break;
				}
			} else {
				discard = true;
			}
			if (!discard) {
				candidateAnnotaionList.add(otherAnnotation);
			}
		}

		if (candidateAnnotaionList.size() == 0) {
			// no candidate annotation was found, return
			return valuesFromAnAnnotationList;
		}

		// now take all the placeholder used inside the current rule which are defined in the candidate
		// Annotation
		if (random) {
			// pick just one random annotation
			Random randomObject = new Random();
			int selPos = randomObject.nextInt(candidateAnnotaionList.size());  // TODO check

			Annotation candidateAnnotation = candidateAnnotaionList.get(selPos);
			candidateAnnotaionList.clear();
			candidateAnnotaionList.add(candidateAnnotation);
		}
		for (int i = 0; i < candidateAnnotaionList.size(); ++i) {
			valuesFromAnAnnotationList.add(getValuesFromAnAnnotation(dependsOnInfo, projRule, depProjRule,
					candidateAnnotaionList.get(i), ctx, cp));
			
		}

		return valuesFromAnAnnotationList;
	}

}
