package it.uniroma2.art.coda.exception.parserexception;

public class PrefixNotDefinedException extends PRParserException {

	private String prefixName;
	private String ruleId;
	
	private static final long serialVersionUID = 1L;

	public PrefixNotDefinedException(String prefixName, String ruleId) {
		super();
		this.prefixName = prefixName;
		this.ruleId = ruleId;
	}

	public PrefixNotDefinedException(Exception e, String prefixName, String ruleId) {
		super(e);
		this.prefixName = prefixName;
		this.ruleId = ruleId;
	}
	
	public String getPrefixName(){
		return prefixName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	@Override
	public String getErrorAsString() {
		return "prefix "+prefixName+" is used but not defined in rule "+ruleId;
	}
}
