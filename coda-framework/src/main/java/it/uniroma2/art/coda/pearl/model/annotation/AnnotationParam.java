package it.uniroma2.art.coda.pearl.model.annotation;

import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;

import java.util.List;

public class AnnotationParam {
	private String name;
	private List<ParamValueInterface> valueList;
	
	public AnnotationParam(String name, List<ParamValueInterface> valueList) {
		this.name = name;
		this.valueList = valueList;
	}

	public String getName() {
		return name;
	}

	public List<ParamValueInterface> getValueList() {
		return valueList;
	}
	
}
