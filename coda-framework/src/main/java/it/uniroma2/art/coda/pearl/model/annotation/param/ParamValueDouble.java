package it.uniroma2.art.coda.pearl.model.annotation.param;

public class ParamValueDouble implements  ParamValueInterface{

    private double number;

    public ParamValueDouble(double number) {
        this.number = number;
    }

    public double getNumber() {
        return number;
    }


    @Override
    public String toString() {
        return Double.toString(number);
    }

    @Override
    public String asString() {
        return Double.toString(number);
    }

    @Override
    public String getType() {
        return "double";
    }
}
