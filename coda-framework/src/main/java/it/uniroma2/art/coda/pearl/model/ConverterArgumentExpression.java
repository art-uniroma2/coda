package it.uniroma2.art.coda.pearl.model;

/**
 * Abstract base class for the representation of an argument provided in a {@link ConverterMention}.
 */
public abstract class ConverterArgumentExpression {
	/**
	 * Returns the Java type associated with the argument.
	 * @return
	 */
	public abstract Class<?> getArgumentType();

	/**
	 * Tells whether this argument is a constant value or an expression the value of which may be affected by
	 * the state of the execution environment (typically the values assigned to placeholders).
	 * @return
	 */
	public abstract boolean isConstant();
	
	public abstract String toString();
	
	public abstract Object getGroundObject();
}
