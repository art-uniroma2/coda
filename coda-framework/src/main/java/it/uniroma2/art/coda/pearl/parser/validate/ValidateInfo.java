package it.uniroma2.art.coda.pearl.parser.validate;

import java.util.HashMap;
import java.util.Map;

public class ValidateInfo {
	Map<String, SingleInfo> ruleIdInfoMap;

	public ValidateInfo() {
		this.ruleIdInfoMap = new HashMap<String, SingleInfo>();
	}
	
	public void addRuleIdAndInfo(String ruleId, SingleInfo info){
		ruleIdInfoMap.put(ruleId, info);
	}
	
	public Map<String, SingleInfo> getRuleIdInfoMap(){
		return ruleIdInfoMap;
	}
	
	public SingleInfo getInfoFromRuleId(String ruleId){
		return ruleIdInfoMap.get(ruleId);
	}
	
}


