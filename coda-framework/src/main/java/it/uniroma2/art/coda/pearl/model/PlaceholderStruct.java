package it.uniroma2.art.coda.pearl.model;

import it.uniroma2.art.coda.pearl.model.annotation.Annotation;

import java.util.ArrayList;
import java.util.List;

public class PlaceholderStruct {

	private String name; // the id for a particular type and feature
	private String rdfType;
	private List<ConverterMention> converterList;
	private String featurePath;
	private ProjectionRule ownerRule;
	private String literalLang;
	private String literalDatatype;
	private boolean isDependsOn;
	private boolean hasFeaturePath;
	private boolean isMandatoryInGraphSection;
	private List<Annotation> annotationList;
	
	// placeholder taken from the nodes part of the projection rule
	public PlaceholderStruct(String name, String rdfType, List<ConverterMention> converterList, String featurePath,
			ProjectionRule ownerRule, boolean isDependsOn) {
		initialize(name, rdfType, converterList, featurePath, ownerRule, isDependsOn);
	}
	
	public PlaceholderStruct(String name, String rdfType, List<ConverterMention> converterList, 
			ProjectionRule ownerRule, boolean isDependsOn) {
		initialize(name, rdfType, converterList, ownerRule, isDependsOn);
	}

	private void initialize(String name, String type, List<ConverterMention> converterList, String featurePath,
			ProjectionRule ownerRule, boolean isDependsOn) {
		this.name = name;
		this.rdfType = type;
		this.converterList = converterList;
		this.featurePath = featurePath;
		this.ownerRule = ownerRule;
		this.isDependsOn = isDependsOn;
		this.hasFeaturePath = true;
		this.isMandatoryInGraphSection = false;
		this.annotationList = new ArrayList<>();
	}
	
	private void initialize(String name, String type, List<ConverterMention> converterList,
			ProjectionRule ownerRule, boolean isDependsOn) {
		this.name = name;
		this.rdfType = type;
		this.converterList = converterList;
		this.featurePath = null;
		this.ownerRule = ownerRule;
		this.isDependsOn = isDependsOn;
		this.hasFeaturePath = false;
		this.isMandatoryInGraphSection = false;
		this.annotationList = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public String getRDFType() {
		return rdfType;
	}

	public List<ConverterMention> getConverterList() {
		return converterList;
	}

	public String getFeaturePath() {
		return featurePath;
	}

	public boolean hasFeaturePath() {
		return hasFeaturePath;
	}
	
	public ProjectionRule getOwnerRule() {
		return ownerRule;
	}

	public void setLiteralLang(String lang) {
		this.literalLang = lang;
	}
	
	public void setLiteralDatatype(String datatype) {
		this.literalDatatype = datatype;
	}

	public String getLiteralLang() {
		return literalLang;
	}

	public String getLiteralDatatype() {
		return literalDatatype;
	}	
	
	public boolean isDependsOn(){
		return isDependsOn;
	}
	
	public void setIsMandatoryInGraphSection(boolean isMandatoryInGraphSection){
		this.isMandatoryInGraphSection = isMandatoryInGraphSection;
	}
	
	public boolean isMandatoryInGraphSection(){
		return isMandatoryInGraphSection;
	}
	
	public void setAnnotations(List<Annotation> annotationList){
		this.annotationList = annotationList;
	}
	
	public boolean hasAnnotations(){
		if (annotationList == null)
			return false;
		if (annotationList.isEmpty())
			return false;
		return true;
	}
	
	public List<Annotation> getAnnotationList(){
		return annotationList;
	}
}
