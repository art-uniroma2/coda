package it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StateFSA {

	private boolean isStartState;
	private boolean isEndState;
	private String stateId;
	
	//private Map <TransitionFSA, List<StateFSA>>transitionStateMap; // it is a non deterministic one
	
	// the key is the internalId (not the uima type nor the transitionId)
	private Map<String, StateFSA> transitionStateMap;
	//private Map <String, List<StateFSA>>transitionStateMap; //TODO check the need for the List in the Map
	private List<StateFSA> transitionEpsylonStateList; 
	
	public StateFSA(String name) {
		initialize(name, false, false);
	}
	
	/*public StateFSA(String name, boolean isStartState, boolean isEndState){
		initialize(name, isStartState, isEndState);
	}*/
	
	private void initialize(String name, boolean isStartState, boolean isEndState){
		this.stateId = name;
		setIsStartState(isStartState);
		setIsEndState(isEndState);
		
		transitionStateMap = new HashMap<String, StateFSA>();
//		transitionStateMap = new HashMap<String, List<StateFSA>>();
		transitionEpsylonStateList = new ArrayList<StateFSA>();
	}
	
	public String getStateId(){
		return stateId;
	}
	
	public void setIsStartState(boolean isStartState){
		this.isStartState = isStartState;
	}
	
	public void setIsEndState(boolean isEndState){
		this.isEndState = isEndState;
	}
	
	public boolean isStartState(){
		return isStartState;
	}
	
	public boolean isEndState(){
		return isEndState;
	}
	
	public void addTransitionToState(TransitionFSA transitionFSA, StateFSA stateFSA){
		String transitionInternalId = transitionFSA.getInternalId();
		transitionStateMap.put(transitionInternalId, stateFSA);
		
		
		
//		if(!transitionStateMap.containsKey(transitionInternalId)){
//			transitionStateMap.put(transitionInternalId, new ArrayList<StateFSA>());
//		}
//		transitionStateMap.get(transitionInternalId).add(stateFSA);
	}
	
	public StateFSA getStateForTransition(String transitionId){
		return transitionStateMap.get(transitionId);
	}
	
	
	public void addEpsylonTransition(StateFSA stateFSA){
		transitionEpsylonStateList.add(stateFSA);
	}
	
	public List<StateFSA> getStateForEpsylonTransition(){
		return transitionEpsylonStateList;
	}
	
	public int getNumberTransition(){
		return transitionStateMap.size() + transitionEpsylonStateList.size();
	}
	
	public List<String> getAllTransitionInternalId(){
		List<String> transitionList = new ArrayList<String>();
		for(String id : transitionStateMap.keySet()){
			transitionList.add(id);
		}
		return transitionList;
	}
}
