package it.uniroma2.art.coda.pearl.model;


/**
 * A proxy for an external component. 
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface ComponentProxy {
	/**
	 * Returns the contract for the proxy. It is expected that the contract name
	 * is a URI
	 * 
	 * @return the contract name
	 */
	String getContractName();

	/**
	 * Returns the Java interfaces that the binding is expected to implement.
	 * 
	 * @return the implemented interface
	 */
	Class<?> getInterface();

	/**
	 * Binds a proxy a Java object (assumed to implement the interface returned
	 * by {@link #getInterface()}.
	 * 
	 * @param binding
	 *            a binding for the proxy
	 */
	void bindTo(Object binding);

	/**
	 * Gets the binding, if any; otherwise, null is returned.
	 * 
	 * @return the binding
	 */
	Object getBinding();
}
