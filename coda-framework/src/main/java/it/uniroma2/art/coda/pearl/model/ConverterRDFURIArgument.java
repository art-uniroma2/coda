package it.uniroma2.art.coda.pearl.model;

import java.util.Objects;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

/**
 * A URI used as an additional argument in the context of a {@link ConverterMention}. It is always considered
 * constant (i.e. {@link #isConstant()} == <code>true</code>).
 */
public class ConverterRDFURIArgument extends ConverterArgumentExpression {

	private IRI uriValue;

	/**
	 * Constructs a URI argument.
	 * 
	 * @param uriSpec
	 */
	public ConverterRDFURIArgument(String uriSpec) {
		this.uriValue = SimpleValueFactory.getInstance().createIRI(uriSpec);
	}

	public IRI getURI() {
		return uriValue;
	}

	public Class<IRI> getArgumentType() {
		return IRI.class;
	}

	@Override
	public boolean isConstant() {
		return true;
	}

	@Override
	public String toString() {
		return NTriplesUtil.toNTriplesString(uriValue);
	}
	
	@Override
	public Object getGroundObject() {
		return getURI();
	}

	@Override
	public int hashCode() {
		return uriValue.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != this.getClass())
			return false;
		ConverterRDFURIArgument other = (ConverterRDFURIArgument) obj;
		return Objects.equals(uriValue, other.uriValue);
	}
}
