package it.uniroma2.art.coda.interfaces;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import it.uniroma2.art.coda.core.CODACore;
//import it.uniroma2.art.coda.exception.parserexception.AntlrParserRegexException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.pearl.model.ConverterMention;
import it.uniroma2.art.coda.pearl.model.ProjectionOperator;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;

/**
 * @author Andrea Turbati
 */

public abstract class ParserPR extends CODAExtensionInterface {
	protected CODACore codaCore = null;
	protected ProjectionRulesModel previousPrModel;


	public ParserPR(String id, String description) {
		super(id, description);
	}

	public ParserPR(String id, String description, ProjectionRulesModel previousPrModel) {
		super(id, description);
		this.previousPrModel = previousPrModel;
	}

	public void initialize(CODACore codaCore) {
		this.codaCore = codaCore;
	}

	protected PearlParserDescription createPearlParserDescription(){
		if(previousPrModel!=null){
			return new PearlParserDescription(previousPrModel);
		}
		return new PearlParserDescription();
	}

	abstract public ProjectionRulesModel parsePearlDocument(File prFile, boolean rulesShouldExists)
			throws PRParserException;

	abstract public ProjectionRulesModel parsePearlDocument(InputStream prInputStream, boolean rulesShouldExists)
			throws PRParserException;

	abstract public ConverterMention parseConverterMention(String textualConverterMention,
			Map<String, String> prefixMapping) throws PRParserException;
	
	abstract public ProjectionOperator parseProjectionOperator(String textualProjectionOperator,
			Map<String, String> prefixMapping) throws PRParserException;
	
	// abstract public boolean checkCondition(IfElseStruct ifElse,
	// Map<PlaceholderStruct, String> placeholderAliasToValueMap, Annotation
	// ann);
}
