package it.uniroma2.art.coda.provisioning.impl;

import java.lang.reflect.Type;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.coda.provisioning.JavaTypeDescription;

public class JavaTypeDescriptionImpl extends TypeDescriptionImpl implements JavaTypeDescription{

	private Type javaType;

	public JavaTypeDescriptionImpl(Type javaType) {
		super(javaType.getTypeName());
		this.javaType = javaType;
	}

	@Override
	public Type getJavaType() {
		return javaType;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("name", getName()).add("javaType", javaType).toString();
	}

}
