package it.uniroma2.art.coda.pearl.model.annotation.param;

public class ParamValueInteger implements  ParamValueInterface {
    private int number;

    public ParamValueInteger(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return Integer.toString(number);
    }

    @Override
    public String asString() {
        return Integer.toString(number);
    }

    @Override
    public String getType() {
        return "int";
    }
}
