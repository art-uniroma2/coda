package it.uniroma2.art.coda.depends;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.provisioning.ComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.structures.DependsOnInfo;
import it.uniroma2.art.coda.structures.StringOrFeatureStruct;
import it.uniroma2.art.coda.structures.table.ValueForTable;
import it.uniroma2.art.coda.structures.table.ValueForTablePlaceholder;
import it.uniroma2.art.coda.structures.table.ValuesFromAnAnnotation;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.eclipse.rdf4j.model.Value;

public abstract class DependsAbstactClass {

	protected final String PARAM_MAX = "M=";
	protected final String PARAM_RANDOM = "R=";
	protected final String PARAM_NUMBER = "N=";
	
	protected String depName;

	public DependsAbstactClass(String depName) {
		this.depName = depName;
	}

	public String getName() {
		return depName;
	}

	public abstract List<ValuesFromAnAnnotation> execute(DependsOnInfo dependsOnInfo, Annotation annotation,
			JCas jcas, ProjectionRulesModel projectionRulesModel, ProjectionRule projRule,
			ComponentProvider cp, CODAContext ctx) throws ComponentProvisioningException, 
			ConverterException, DependencyException;

	protected ValuesFromAnAnnotation getValuesFromAnAnnotation(DependsOnInfo dependsOnInfo,
			ProjectionRule projRule, ProjectionRule depProjRule, Annotation candidateAnnotaion,
			CODAContext ctx, ComponentProvider cp) throws ComponentProvisioningException, ConverterException {

		ValuesFromAnAnnotation valuesFromAnAnnotation = new ValuesFromAnAnnotation(
				dependsOnInfo.getAliasRuleId(), dependsOnInfo.getDependsOnType());

		String otherRuleAliasId = dependsOnInfo.getAliasRuleId();

		List<String> plchldUsedFromAltId = projRule.getPlchldUsedFromAltId(otherRuleAliasId);

		for (String plchldName : plchldUsedFromAltId) {
			PlaceholderStruct depPlchld = depProjRule.getPlaceholderMap().get(plchldName);
			if (depPlchld == null) {
				// in the depRule there is no placeholder with the specified , this should never happen
				continue;
			}
			String featPath = depProjRule.getUIMAType() + ":" + depPlchld.getFeaturePath();
			List<StringOrFeatureStruct> valuesOfFeatureFromFeatPath = UIMACODAUtilities.getValuesOfFeatureFromFeatPath(
					candidateAnnotaion, featPath);

			String completeDepPlchld = CODACore.PLACEHOLDER_SYMBOL + dependsOnInfo.getAliasRuleId()
					+ CODACore.PLACEHOLDER_DEPENDS_SYMBOL + plchldName;

			//List<ARTNode> artNodeList = new ArrayList<ARTNode>();
			List<ValueForTable> vfpList = new ArrayList<ValueForTable>();
			if(depPlchld.hasFeaturePath()){
				for (StringOrFeatureStruct valueFromUIMA : valuesOfFeatureFromFeatPath) {
					Value value = UIMACODAUtilities.getRDFValueFromUIMAValue(depPlchld, valueFromUIMA, cp,
							ctx);
					//artNodeList.add(artNode);
					ValueForTablePlaceholder valueForTablePlaceholder = new ValueForTablePlaceholder(depPlchld,
							completeDepPlchld, value);
					vfpList.add(valueForTablePlaceholder);
	
				}
				valuesFromAnAnnotation.addSingleValueListToList(vfpList);
			} else{
				Value value = UIMACODAUtilities.getRDFValueFromUIMAValue(depPlchld, cp, ctx);
				ValueForTablePlaceholder valueForTablePlaceholder = new ValueForTablePlaceholder(depPlchld,
						completeDepPlchld, value);
				vfpList.add(valueForTablePlaceholder);
				valuesFromAnAnnotation.addSingleValueListToList(vfpList);
			}
		}
		return valuesFromAnAnnotation;
	}
}
