package it.uniroma2.art.coda.pearl.model.propPathStruct;

import java.util.ArrayList;
import java.util.List;

public class PathAlternativeElemPropPath implements GenericElemPropPath {

    private List<PathSequenceElemPropPath> pathSequenceElemPropPathArrayList = new ArrayList<>();


    public PathAlternativeElemPropPath() {
    }


    public void addPathSequenceElemPropPath(PathSequenceElemPropPath pathSequenceElemPropPath) {
        pathSequenceElemPropPathArrayList.add(pathSequenceElemPropPath);
    }

    public List<PathSequenceElemPropPath> getPathSequenceElemPropPathArrayList() {
        return pathSequenceElemPropPathArrayList;
    }

    @Override
    public String getValueAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" ( ");
        boolean first = true;
        for (PathSequenceElemPropPath pathSequenceElemPropPath : pathSequenceElemPropPathArrayList) {
            if (!first) {
                stringBuilder.append(" | ");
            }
            first = false;
            stringBuilder.append(pathSequenceElemPropPath.getValueAsString());
        }
        stringBuilder.append(" ) ");

        return stringBuilder.toString();
    }
}
