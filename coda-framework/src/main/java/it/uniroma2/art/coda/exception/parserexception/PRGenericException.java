package it.uniroma2.art.coda.exception.parserexception;

public class PRGenericException extends PRParserException {

	private String msg;
	
	private static final long serialVersionUID = 1L;

	public PRGenericException(String msg) {
		super();
		this.msg = msg;
	}

	public PRGenericException(Exception e) {
		super(e);
		this.msg = null;
	}
	
	public PRGenericException(Exception e, String msg) {
		super(e);
		this.msg = msg;
	}
	
	@Override
	public String getErrorAsString() {
		if(msg!=null) {
			return msg;
		} else { 
			return this.getMessage();
		}
	}
}
