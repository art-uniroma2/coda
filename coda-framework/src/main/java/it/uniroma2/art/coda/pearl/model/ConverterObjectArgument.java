package it.uniroma2.art.coda.pearl.model;

import java.util.Objects;

/**
 * A Java Object used as an argument in the context of {@link ConverterMention}. It is always considered
 * constant (i.e. {@link #isConstant()} == <code>true</code>).
 */
public class ConverterObjectArgument extends ConverterArgumentExpression {

	private Class<?> clazz;
	private Object arg;

	public ConverterObjectArgument(Class<?> clazz, Object arg) {
		this.clazz = clazz;
		this.arg = arg;
	}

	@Override
	public Object getGroundObject() {
		return arg;
	}

	@Override
	public int hashCode() {
		return arg.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != this.getClass())
			return false;
		ConverterObjectArgument other = (ConverterObjectArgument) obj;
		return Objects.equals(clazz, other.clazz) && Objects.equals(arg, other.arg);
	}

	@Override
	public Class<?> getArgumentType() {
		return clazz;
	}

	@Override
	public boolean isConstant() {
		return true;
	}

	@Override
	public String toString() {
		return arg.toString();
	}
}
