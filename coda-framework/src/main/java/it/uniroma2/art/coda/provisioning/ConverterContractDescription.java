package it.uniroma2.art.coda.provisioning;

import java.util.Collection;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;

import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;

/**
 * A contract description.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public interface ConverterContractDescription {
	/**
	 * Return the URI identifying this contract.
	 * 
	 * @return
	 */
	String getContractURI();

	/**
	 * Returns the human-readable name of this contract.
	 * 
	 * @return
	 */
	String getContractName();

	/**
	 * Returns a description of this contract.
	 * 
	 * @return
	 */
	String getContractDescription();

	/**
	 * Returns the nature of the returned RDF term. See {@link RDFCapabilityType} for the allowed values and
	 * their meaning.
	 * 
	 * @return
	 */
	RDFCapabilityType getRDFCapability();

	/**
	 * Returns the datatype that this contract can handle.
	 * 
	 * @return
	 */
	Set<IRI> getDatatypes();

	/**
	 * Returns the descriptions of the available signatures.
	 * 
	 * @return
	 */
	Collection<SignatureDescription> getSignatureDescriptions();

	/**
	 * Returns an RDF representation of this description.
	 * 
	 * @param model
	 */
	void toRDF(Model model);
}
