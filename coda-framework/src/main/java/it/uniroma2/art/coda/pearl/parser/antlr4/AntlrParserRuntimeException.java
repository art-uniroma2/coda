package it.uniroma2.art.coda.pearl.parser.antlr4;

public class AntlrParserRuntimeException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String msg;
	
	public AntlrParserRuntimeException(String msg) {
		this.msg = msg;
	}
	
	public String getMsg(){
		return msg;
	}
}
