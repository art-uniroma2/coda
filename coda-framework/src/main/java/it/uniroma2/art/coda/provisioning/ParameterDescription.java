package it.uniroma2.art.coda.provisioning;

/**
 * A parameter description.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public interface ParameterDescription {
	/**
	 * Returns the name of this parameter.
	 * @return
	 */
	String getName();
	
	/**
	 * Returns an HTML description of this parameter.
	 * @return
	 */
	String getHTMLDescription();
	
	/**
	 * Returns a description of the type of this parameter.
	 * @return
	 */
	TypeDescription getTypeDescription();
	
}
