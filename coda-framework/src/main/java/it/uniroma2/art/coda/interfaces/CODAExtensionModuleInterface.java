package it.uniroma2.art.coda.interfaces;

import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.structures.CasAnnotationStructure;
import it.uniroma2.art.coda.structures.InterComponentObject;
import it.uniroma2.art.coda.structures.PreviousDecisions;

/**
 * This class represent the generic interface for all the five modules
 * 
 * @author Andrea Turbati
 */
public abstract class CODAExtensionModuleInterface extends CODAExtensionInterface {

	protected CasAnnotationStructure casAnn;

	RepositoryConnection connection;
	
	
	protected int maxAnswers;

	protected CODACore codaCore;

	protected PreviousDecisions prevDecision;

	public CODAExtensionModuleInterface(String id, String description) {
		super(id, description);
	}

	/**
	 * Set the structure containing the UIMA Annotation to be analyzed
	 * 
	 * @param casAnn
	 *            the structure containing the UIMA Annotation to be analyzed
	 */
	public void setCasAnnotation(CasAnnotationStructure casAnn) {
		this.casAnn = casAnn;
	}

	/**
	 * Set the Repository Connection representing the ontology
	 * 
	 * @param connection
	 *            the Repository Connection that will be set
	 */
	public void setRDFModel(RepositoryConnection connection) {
		this.connection = connection;
	}

	/**
	 * Set the CODACore in which this modules is used
	 * 
	 * @param codaCore
	 *            the CODACore that will be set
	 */
	public void setCODACore(CODACore codaCore) {
		this.codaCore = codaCore;
	}

	/**
	 * Set the maximum number of hints the module should return
	 * 
	 * @param maxNumAnswer the maximum number of answer
	 */
	public void setMaxNumAnswer(int maxNumAnswer) {
		this.maxAnswers = maxNumAnswer;
	}

	/**
	 * Set the previous action List done by the external tool on the ontology
	 * 
	 * @param prevDecision
	 *            the list of previous action done by the external tool on the ontology
	 */
	public void setPrevAnnProj(PreviousDecisions prevDecision) {
		this.prevDecision = prevDecision;
	}

	/**
	 * This method must be implemented by all the modules that implement the interface for the single module
	 * @return s list of Hints
	 */
	public abstract InterComponentObject invokeComponent();

}
