package it.uniroma2.art.coda.interfaces.annotations.converters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The kind of RDF terms produces by a converter.
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RDFCapability {
	RDFCapabilityType value() default RDFCapabilityType.node;
}
