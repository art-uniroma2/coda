package it.uniroma2.art.coda.structures.table;

import it.uniroma2.art.coda.pearl.model.BindingStruct;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.rdf4j.model.Value;

public class TablePlaceholdersBindingsVariables {

	private List<Map<String, SingleTableValue>> placeholderBindingList;
	private List<Map<String, SingleTableValue>> placeholderBindingVariablesList;

	public TablePlaceholdersBindingsVariables() {
		placeholderBindingList = new ArrayList<Map<String, SingleTableValue>>();
		placeholderBindingVariablesList = new ArrayList<Map<String, SingleTableValue>>();
	}

	public void constructPlaceholderBindingMap(List<List<ValuesFromAnAnnotation>> valuesFromAnnotationListList) {
		
		
		
		List<List<Map<String, SingleTableValue>>> tablesList = 
				new ArrayList<List<Map<String,SingleTableValue>>>();
		// first iterate until ValuesFromAnAnnotation are reached to create several tables, each one of them 
		// dealing with a particular rule (and maybe more instances of annotation, but using just one rule)
		for(int i=0; i<valuesFromAnnotationListList.size(); ++i ){
			List<ValuesFromAnAnnotation> annlist = valuesFromAnnotationListList.get(i);
			List<Map<String, SingleTableValue>> vfaaConcatList = new ArrayList<Map<String,SingleTableValue>>();
			for(int k=0; k<annlist.size(); ++k){
				ValuesFromAnAnnotation valuesFromAnAnnotation = annlist.get(k);
				
				// all the table constructed from constructTableFromVFAA are concatenated and not JOIN because
				// are derived from annotation using the same rule (for example in a specific dependency which
				// point to several annotation using the same rule)
				// In this way a bigger table is created, with more row but with the same number of column
				List<Map<String, SingleTableValue>> tableFromVFAA = constructTableFromVFAA(valuesFromAnAnnotation);
					
				vfaaConcatList.addAll(tableFromVFAA);
			}
			//now add the table just created to a table list
			tablesList.add(vfaaConcatList);
		}
		
		//now iterate on every table ( List<Map<String, SingleTableValue>> ) stored in tablesList to do 
		// a JOIN between them
		
		for(int i=0; i<tablesList.size(); ++i){
			List<Map<String, SingleTableValue>> singleTable = tablesList.get(i);
			if(placeholderBindingList.isEmpty()){
				placeholderBindingList.addAll(singleTable);
			} else {
				placeholderBindingList = doJoinBetweenTable(placeholderBindingList, singleTable);
			}
		}
		
	}


	/**
	 * Given a ValuesFromAnAnnotation it return a List of Table row (Map<String, SingleTableValue>)
	 */
	private List<Map<String, SingleTableValue>> constructTableFromVFAA( ValuesFromAnAnnotation vfaa) {
		//construct the empty (for the moment) result table, each row is <Map<String, SingleTableValue>
		List<Map<String, SingleTableValue>> idToSingleTableValueMapList = 
				new ArrayList<Map<String, SingleTableValue>>();
		//get the List of List of value associated to each placehodler/binding
		List<List<ValueForTable>> singleTableValueListList = vfaa.getSingleTableValueListList();
		
		Map<String, SingleTableValue> idToSingleTableValueMap = new HashMap<String, SingleTableValue>();
		recursiveMapConstruct(singleTableValueListList, 0, idToSingleTableValueMap, idToSingleTableValueMapList);
		
		return idToSingleTableValueMapList;
	}
	
	
	private void recursiveMapConstruct(List<List<ValueForTable>> valueForTableListList, int index,
			Map<String, SingleTableValue> idToSingleTableValueMap,
			List<Map<String, SingleTableValue>> idToSingleTableValueMapList) {

		
		if(valueForTableListList.size() <= index){ 
			return;
		}
		
		List<ValueForTable> valueForTableList = valueForTableListList.get(index);

		for (ValueForTable valueForTable : valueForTableList) {
			// First of all clone the input map and work on this clone
			Map<String, SingleTableValue> idToSingleTableValueClonedMap = new HashMap<String, SingleTableValue>();
			for (String key : idToSingleTableValueMap.keySet()){
				idToSingleTableValueClonedMap.put(key, idToSingleTableValueMap.get(key));
			}
			
			// Depenending of the type of currentElement perform a different action
			addVFT(valueForTable, idToSingleTableValueClonedMap);

			if (valueForTableListList.size() == index + 1) {
				// this is the last element of the valueForTableList, so store the
				// idToSingleTableValueClonedMap
				idToSingleTableValueMapList.add(idToSingleTableValueClonedMap);
			} else {
				recursiveMapConstruct(valueForTableListList, index + 1, idToSingleTableValueClonedMap,
						idToSingleTableValueMapList);
			}
		}
	}
	
	private void addVFT(ValueForTable valueForTable, Map<String, SingleTableValue> idToSingleTableValueMap){
		if (valueForTable instanceof ValueForTablePlaceholder) {
			ValueForTablePlaceholder currentElementPlaceholder = (ValueForTablePlaceholder) valueForTable;
			String id = currentElementPlaceholder.getId();
			Value value = currentElementPlaceholder.getArtNode();
			PlaceholderStruct placeholderStruct = currentElementPlaceholder.getPlaceholderStruct();
			SingleTableValuePlaceholder singleTableValuePlaceholder = new SingleTableValuePlaceholder(
					id, value, placeholderStruct);
			idToSingleTableValueMap.put(id, singleTableValuePlaceholder);
		} else if (valueForTable instanceof ValueForTableBinding) {
			ValueForTableBinding currentElementBinding = (ValueForTableBinding) valueForTable;
			BindingStruct bindingStruct = currentElementBinding.getBindingStruct();
			Map<String, Value> idToARTNodeMap = currentElementBinding.getIdToValueMap();
			for (String bindId : idToARTNodeMap.keySet()) {
				idToARTNodeMap.get(bindId);
				Value artNode = idToARTNodeMap.get(bindId);
				SingleTableValueBinding singleTableValueBinding = new SingleTableValueBinding(bindId,
						artNode, bindingStruct);
				idToSingleTableValueMap.put(bindId, singleTableValueBinding);
			}
		}
	}
	
	private List<Map<String, SingleTableValue>> doJoinBetweenTable(
			List<Map<String, SingleTableValue>> firstTable,
			List<Map<String, SingleTableValue>> secondTable) {
		List<Map<String, SingleTableValue>> resultTable = new ArrayList<Map<String,SingleTableValue>>();
		for(int firstTableIndex=0; firstTableIndex<firstTable.size(); ++firstTableIndex ){
			for(int secondTableIndex=0; secondTableIndex<secondTable.size(); ++secondTableIndex){
				
				Map<String, SingleTableValue> firstTableRow = firstTable.get(firstTableIndex);
				Map<String, SingleTableValue> secondTableRow = secondTable.get(secondTableIndex);
				//create the Row for the new Table
				Map<String, SingleTableValue> newTableRow = new HashMap<String, SingleTableValue>();
				//clone the firstTableRow in the newTableRow
				newTableRow.putAll(firstTableRow);
				// add the secondTableRow to newTableRow
				newTableRow.putAll(secondTableRow);
				// now newTableRow = firstTableRow + secondTableRow, so added this new row to the new table
				resultTable.add(newTableRow);
			}
		}
		
		
		return resultTable;
	}


	
	
	
	
	public void addVariableValueToPlaceholderBindingVariableMap(
			Collection<SingleTableValue> singleTableValuePlaceholderBindingList,
			List<List<SingleTableValueVariable>> valueVariableList) {

		Map<String, SingleTableValue> idToSingleTableValueMap = new HashMap<String, SingleTableValue>();
		// first of all add to the map the value corresponding to the placeholder and the binding
		for (SingleTableValue singleTableValue : singleTableValuePlaceholderBindingList) {
			idToSingleTableValueMap.put(singleTableValue.getId(), singleTableValue);
		}

		if (valueVariableList == null || valueVariableList.isEmpty()) {
			placeholderBindingVariablesList.add(idToSingleTableValueMap);
		} else {
			// now add the variable associated values
			recursiveSecondMapConstruct(valueVariableList, 0, idToSingleTableValueMap);
		}
	}

	private void recursiveSecondMapConstruct(List<List<SingleTableValueVariable>> valueVariableList,
			int index, Map<String, SingleTableValue> idToSingleTableValueMap) {

		List<SingleTableValueVariable> currentElementList = valueVariableList.get(index);

		for (SingleTableValueVariable singleTableVariable : currentElementList) {
			// First of all clone the input map and word on this clone
			Map<String, SingleTableValue> idToSingleTableValueClonedMap = new HashMap<String, SingleTableValue>();
			for (String key : idToSingleTableValueMap.keySet())
				idToSingleTableValueClonedMap.put(key, idToSingleTableValueMap.get(key));

			idToSingleTableValueClonedMap.put(singleTableVariable.getId(), singleTableVariable);

			if (valueVariableList.size() == index + 1) {
				// this is the last element of the valueForTableList, so store the
				// idToSingleTableValueClonedMap
				placeholderBindingVariablesList.add(idToSingleTableValueClonedMap);
			} else {
				recursiveSecondMapConstruct(valueVariableList, index + 1, idToSingleTableValueClonedMap);
			}
		}
	}

	public Iterator<Map<String, SingleTableValue>> getIteratorForPlaceholderBindingMap() {
		return placeholderBindingList.iterator();
	}

	public Iterator<Map<String, SingleTableValue>> getIteratorForPlaceholderBindingVariableMap() {
		return placeholderBindingVariablesList.iterator();
	}

	public void printPlaceholderBindingTable() {
		System.out.println("PRINTING TABLE");
		boolean firstRow = true;
		for (Map<String, SingleTableValue> row : placeholderBindingList) {
			if (firstRow) {
				firstRow = false;
				for (String id : row.keySet()) {
					System.out.print("\t\t" + id + "\t\t||");
				}
				System.out.println();
			}
			System.out.println();
			for (String id : row.keySet()) {
				System.out.print(row.get(id).toString() + " || ");
			}

		}
	}

	public void printPlaceholderBindingVariableTable() {
		for (Map<String, SingleTableValue> row : placeholderBindingVariablesList) {
			boolean firstRow = true;
			if (firstRow) {
				firstRow = false;
				for (String id : row.keySet()) {
					System.out.print(id + "\t\t");
				}
				System.out.println();
			}
			System.out.println();
			for (String id : row.keySet()) {
				System.out.print(row.get(id).toString() + " || ");
			}
		}
	}
}
