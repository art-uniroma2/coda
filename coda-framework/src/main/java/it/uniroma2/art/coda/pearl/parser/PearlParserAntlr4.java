package it.uniroma2.art.coda.pearl.parser;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.interfaces.ParserPR;
import it.uniroma2.art.coda.pearl.model.ConverterMention;
import it.uniroma2.art.coda.pearl.model.ProjectionOperator;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;

public class PearlParserAntlr4 extends ParserPR {

	public PearlParserAntlr4(String id, String description) {
		super(id, description);
	}

	public PearlParserAntlr4(String id, String description, ProjectionRulesModel prevProjRulesModel) {
		super(id, description, prevProjRulesModel);
	}

	@Override
	public ProjectionRulesModel parsePearlDocument(File prFile, boolean rulesShouldExists) throws PRParserException {
		PearlParserDescription parserDescription = createPearlParserDescription();
		return parserDescription.parsePearlDocument(prFile, rulesShouldExists);
	}

	@Override
	public ProjectionRulesModel parsePearlDocument(InputStream prInputStream, boolean rulesShouldExists) throws PRParserException {
		PearlParserDescription parserDescription = createPearlParserDescription();
		return parserDescription.parsePearlDocument(prInputStream, rulesShouldExists);
	}

	@Override
	public ConverterMention parseConverterMention(String textualConverterMention,
			Map<String, String> prefixMapping) throws PRParserException {
		PearlParserDescription parserDescription = new PearlParserDescription();
		return parserDescription.parseIndividualConverter(prefixMapping, Collections.emptyMap(), 
				"<out of any rule>", textualConverterMention);
	}

	@Override
	public ProjectionOperator parseProjectionOperator(String textualProjectionOperator,
			Map<String, String> prefixMapping) throws PRParserException {
		PearlParserDescription parserDescription = new PearlParserDescription();
		return parserDescription.parseProjectionOperator(prefixMapping, Collections.emptyMap(), 
				"<out of any rule>", textualProjectionOperator);
	}

}
