package it.uniroma2.art.coda.exception.parserexception;

public class NoRuleInFileException extends PRParserException {

	private static final long serialVersionUID = 1L;

	public NoRuleInFileException() {
		super();
	}

	public NoRuleInFileException(Exception e) {
		super(e);
	}
	
	@Override
	public String getErrorAsString() {
		return "The PEARL file has no rules";
	}
}
