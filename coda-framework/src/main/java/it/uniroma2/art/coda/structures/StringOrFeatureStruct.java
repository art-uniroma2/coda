package it.uniroma2.art.coda.structures;

import org.apache.uima.cas.FeatureStructure;

public class StringOrFeatureStruct {

	private String valueString;
	private boolean hasStringValue;
	private FeatureStructure featStruct;
	private boolean hasFeatStructValue;
	
	public StringOrFeatureStruct(String value) {
		initialize(value, true, null, false);
	}
	
	public StringOrFeatureStruct(FeatureStructure value) {
		initialize(null,  false, value, true);
	}
	
	private void initialize(String value, boolean hasString, FeatureStructure valueFS, 
			boolean hasFS){
		valueString = value;
		hasStringValue = hasString;
		featStruct = valueFS;
		hasFeatStructValue = hasFS;
	}
	
	public String getStringValue(){
		return valueString;
	}
	
	public boolean hasStringValue(){
		return hasStringValue;
	}
	
	public FeatureStructure getFeatureStruct(){
		return featStruct;
	}
	
	public boolean hasFeatureStruct(){
		return hasFeatStructValue;
	}
	
	
}
