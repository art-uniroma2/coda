package it.uniroma2.art.coda.interfaces.annotations.converters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Description {
	String value();
}
