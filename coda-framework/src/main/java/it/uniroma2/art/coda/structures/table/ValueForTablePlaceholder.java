package it.uniroma2.art.coda.structures.table;

import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;

public class ValueForTablePlaceholder extends ValueForTable {

	private Value artNode;
	private String id;
	private PlaceholderStruct placeholderStruct;
	
	public ValueForTablePlaceholder(PlaceholderStruct placeholderStruct, String id, Value value) {
		this.placeholderStruct = placeholderStruct;
		this.id = id;
		this.artNode = value;
	}

	public Value getArtNode() {
		return artNode;
	}

	public String getId() {
		return id;
	}

	public PlaceholderStruct getPlaceholderStruct() {
		return placeholderStruct;
	}
	
	

}
