package it.uniroma2.art.coda.structures.table;

import org.eclipse.rdf4j.model.Value;

public class SingleTableValue {

	private String id;
	private Value value;
	
	public SingleTableValue(String id, Value artNode) {
		super();
		this.id = id;
		this.value = artNode;
	}

	public String getId() {
		return id;
	}

	public Value getArtNode() {
		return value;
	}
	
	@Override
	public String toString(){
		return id + " -> "+ value.stringValue();
	}
	
	
	
}
