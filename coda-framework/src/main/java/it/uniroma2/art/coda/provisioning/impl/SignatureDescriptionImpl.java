package it.uniroma2.art.coda.provisioning.impl;

import java.util.List;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import it.uniroma2.art.coda.provisioning.ParameterDescription;
import it.uniroma2.art.coda.provisioning.SignatureDescription;
import it.uniroma2.art.coda.provisioning.TypeDescription;

public class SignatureDescriptionImpl implements SignatureDescription {

	private final List<ParameterDescription> parameterDescriptions;
	private final RequirementLevels featurePathRequirementLevel;
	private final boolean producingURI;
	private final TypeDescription returnTypeDescription;

	public SignatureDescriptionImpl(boolean producingURI, TypeDescription returnTypeDescription,
			List<ParameterDescription> parameterDescriptions, RequirementLevels featurePathRequirementLevel) {
		this.producingURI = producingURI;
		this.returnTypeDescription = returnTypeDescription;
		this.featurePathRequirementLevel = featurePathRequirementLevel;
		this.parameterDescriptions = parameterDescriptions;
	}

	@Override
	public RequirementLevels getFeaturePathRequirementLevel() {
		return featurePathRequirementLevel;
	}

	@Override
	public List<ParameterDescription> getParameterDescriptions() {
		return parameterDescriptions;
	}

	@Override
	public TypeDescription getReturnTypeDescription() {
		return returnTypeDescription;
	}

	@Override
	public boolean isProducingURI() {
		return producingURI;
	}

	@Override
	public boolean isProducingLiteral() {
		return !producingURI;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).addValue(isProducingURI() ? "produceURI" : "produceLiteral")
				.add("featurePathRequirementLevel", featurePathRequirementLevel)
				.add("parameterDescriptions", parameterDescriptions).toString();
	}

}
