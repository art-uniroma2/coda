package it.uniroma2.art.coda.exception.parserexception;

public class AnnotationNotDefinedException extends PRParserException {

	private String annName;
	private String ruleId;

	private static final long serialVersionUID = 1L;

	public AnnotationNotDefinedException(String annName, String ruleId) {
		super();
		this.annName = annName;
		this.ruleId = ruleId;
	}

	public AnnotationNotDefinedException(Exception e, String prefixName, String ruleId) {
		super(e);
		this.annName = prefixName;
		this.ruleId = ruleId;
	}
	
	public String getAnnName(){
		return annName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	@Override
	public String getErrorAsString() {
		return "annotation "+ annName +" is used but not defined in rule "+ruleId;
	}
}
