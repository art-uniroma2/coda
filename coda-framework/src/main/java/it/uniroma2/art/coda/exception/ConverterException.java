package it.uniroma2.art.coda.exception;

public class ConverterException extends Exception {
    

	/**
	 * 
	 */
	private static final long serialVersionUID = 6356051432263640825L;

	/**
	 * @param msg
	 */
	public ConverterException(String msg) {
        super(msg);
    }
    
    /**
     * @param e
     */
    public ConverterException(Exception e) {
        super(e);
    }

}
