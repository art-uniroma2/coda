package it.uniroma2.art.coda.pearl.model.propPathStruct;

public class PathOneInPropertySetElemPropPath implements GenericElemPropPath {

    private String uriOrAbbr;
    private boolean hasInverse;

    public PathOneInPropertySetElemPropPath(String uriOrAbbr, boolean hasInverse) {
        this.uriOrAbbr = uriOrAbbr;
        this.hasInverse = hasInverse;
    }

    public String getUriOrAbbr() {
        return uriOrAbbr;
    }

    public boolean isHasInverse() {
        return hasInverse;
    }

    @Override
    public String getValueAsString() {
        StringBuilder stringBuilder = new StringBuilder();

        if (hasInverse) {
            stringBuilder.append(" ^");
        }
        stringBuilder.append(uriOrAbbr);
        stringBuilder.append(" ");

        return stringBuilder.toString();
    }
}
