package it.uniroma2.art.coda.pearl.model;

import java.util.ArrayList;
import java.util.List;

import it.uniroma2.art.coda.pearl.model.annotation.Annotation;
import it.uniroma2.art.coda.pearl.model.graph.GraphSingleElement;

/**
 * This class represent the graph element (the triple, not the single element inside it)
 * 
 * @author Andrea Turbati
 * 
 */
public class GraphStruct extends GraphElement {

	private GraphSingleElement subject;
	private GraphSingleElement predicate;
	private GraphSingleElement object;

	// List of annotations belonging to the triple
	private List<Annotation> annotationList;

	public GraphStruct(GraphSingleElement subject,
			GraphSingleElement predicate, GraphSingleElement object,
			ProjectionRule ownerRule) {
		initialize(subject, predicate, object, ownerRule);
		annotationList = new ArrayList<Annotation>();
	}

	// Constructor with listAnnotations
	public GraphStruct(GraphSingleElement subject,
			GraphSingleElement predicate, GraphSingleElement object,
			ProjectionRule ownerRule,
			List<Annotation> listAnnotations) {
		initialize(subject, predicate, object, ownerRule);
		this.annotationList = listAnnotations;
	}

	private void initialize(GraphSingleElement subject,
			GraphSingleElement predicate, GraphSingleElement object,
			ProjectionRule ownerRule) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
		this.ownerRule = ownerRule;
//		if (subj.getPRRule() == pred.getPRRule() && pred.getPRRule() == obj.getPRRule()) {
//			prRule = subj.getPRRule();
//			issueWithPRRule = false;
//		} else {
//			prRule = null;
//			issueWithPRRule = true;
//		}
	}
	
	public GraphSingleElement getSubject() {
		return subject;
	}

	public GraphSingleElement getPredicate() {
		return predicate;
	}

	public GraphSingleElement getObject() {
		return object;
	}

	public List<Annotation> getAnnotationList() {
		return annotationList;
	}

}
