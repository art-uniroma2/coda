package it.uniroma2.art.coda.exception.parserexception;

public class PropPathNotAllowedException extends PRParserException {

	private String ruleId;

	private static final long serialVersionUID = 1L;

	public PropPathNotAllowedException(String ruleId) {
		super();
		this.ruleId = ruleId;
	}

	public PropPathNotAllowedException(Exception e, String ruleId) {
		super(e);
		this.ruleId = ruleId;
	}
	

	public String getRuleId() {
		return ruleId;
	}

	@Override
	public String getErrorAsString() {
		return "a Property Path was found in the rule "+ ruleId+" in a position where it cannot be used";
	}
}
