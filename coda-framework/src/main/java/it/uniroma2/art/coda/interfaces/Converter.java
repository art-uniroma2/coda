package it.uniroma2.art.coda.interfaces;


import it.uniroma2.art.coda.provisioning.ComponentIndexingException;
import org.eclipse.rdf4j.model.IRI;
import org.pf4j.ExtensionPoint;

import java.lang.reflect.Field;

/**
 * The primary purpose of a <i>converter</i> is to transform the value of a UIMA feature path into an RDF node
 * (either literal or URI) that can be used for the instantiation of new triples.
 * <p>
 * Converters are always invoked in the context of the generation of a node inside the <code>nodes</code>
 * clause; nonetheless, the returned value is not required to be an RDF term. In such cases when the returned
 * object is not suitable for the assignment to the placeholder, it is assumed that the converter is combined
 * with other converters, thus forming a chain of converters that ultimately produces the node value for the
 * placeholder.
 * </p>
 * <p>
 * This is a tagging interface that does not define any operation. Indeed, its purpose is to mark objects as
 * being CODA converters. The expectations on the functionality of a converter can be documented through the
 * definition of a subinterface that established the <i>contract</i> for a family of functionally equivalent
 * converters.
 * </p>
 * <p>
 * A converter should be registered in the <i>OSGi service registry</i> under the generic interface
 * {@link Converter} with the following properties:
 * <ul>
 * <li>the URI of the specific converter (via property {@link #OSGI_SERVICE_PROPERTY_CONVERTER})</li>
 * <li>an array of implemented contracts (via property {@link #OSGI_SERVICE_PROPERTY_CONTRACT_INTERFACE})</li>
 * </ul>
 * </p>
 * <p>
 * The URI identifying a contract should be advertised inside the corresponding interface via a string
 * constant named <code>CONTRACT_URI</code>.
 * </p>
 * <p>
 * A converter should implement one or more interfaces establishing the contracts it is obliged to. Then, it
 * is possible to invoke the operations implemented by a converter that adhere to some conventions. The
 * expectations on the signature of an operation depend on whether the converter will be used to generate a
 * {@link IRI} or a {@link Literal}.
 * </p>
 * <p>
 * In the context of a {@link IRI} generation, the expected signature is as follows:
 * </p>
 * <p>
 * 
 * <pre>
 * Q produceURI(CODAContext ctx, T value [, S_i param_i]*) [throws ConverterException];
 * </pre>
 * 
 * </p>
 * <p>
 * where <code>T</code> is type of the first parameter, the value of which depends on the position of the
 * converter inside the chain associated with a placeholder definition in the <code>nodes</code> section.
 * </p>
 * <ul>
 * <li>The first converter is always provided with the value of a UIMA feature path, and thus T should be
 * <code>java.lang.String</code>. Even if a converter does not use that information, and thus can be invoked
 * with a <code>null</code> feature path, the argument will nonetheless a <code>null</code> string.</li>
 * <li>
 * Otherwise, the value is the object returned by the previous converter in the chain, and thus T should be a
 * type assignable from it.</li>
 * </ul>
 * <p>
 * Concerning <code>Q</code>, we can say that:
 * </p>
 * <ul>
 * <li>if it should be assignable to <code>IRI</code>, if the converter is intended to be used as
 * last component of a chain</li>
 * <li>otherwise, it can be anything</li>
 * </ul>
 * </p>
 * <p>
 * In addition to the parameter <code>value</code> the signature may include an arbitrary number of additional
 * parameters. Currently, the PEARL language only supports strings, various types of ARTNodes and maps, whose
 * keys are Strings and values are either strings or nodes.
 * </p>
 * 
 * <p>
 * In the context of a {@link Literal} generation, the expected signature is as follows:
 * </p>
 * <p>
 * 
 * <pre>
 * Q produceLiteral(CODAContext ctx, String datatype, String lang, T value [, S_i param_i]*) [throws ConverterException];
 * </pre>
 * 
 * </p>
 * <p>
 * where the parameter <code>value</code> has exactly the same characteristics as in the case above concerning
 * URIs. An analogous discourse applied to the return value, which allows or not the use of the converter at
 * the end of a chain, depending on whether or not it is assignable to Literal. The additional parameters
 * <code>lang</code> and <code>datatype</code> hold, respectively, the language tag and the datatype type,
 * statically specified in a node declaration.
 * </p>
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public interface Converter extends ExtensionPoint {
	String STATIC_FIELD_CONTRACT_URI = "CONTRACT_URI";
	String STATIC_FIELD_CONVERTER_URI = "CONVERTER_URI";

	default String getConverterURI() {
		String converterURI;
		try {
			Field f = this.getClass().getField(Converter.STATIC_FIELD_CONVERTER_URI);
			converterURI = (String) f.get(null);
		} catch (IllegalAccessException | NoSuchFieldException | SecurityException e) {
			throw new RuntimeException("Could not access static field telling the converter URI",
					e);
		}
		return converterURI;

	}
}
