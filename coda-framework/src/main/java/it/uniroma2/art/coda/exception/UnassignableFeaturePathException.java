package it.uniroma2.art.coda.exception;

public class UnassignableFeaturePathException extends Exception {

	private String plcName;
	private String ruleId;
	private String featPath;
	private String type;
	
	private static final long serialVersionUID = 1L;

	public UnassignableFeaturePathException(String msg, String plcName, String ruleId, String featPath, 
			String type) {
		super(msg);
		this.plcName = plcName;
		this.ruleId = ruleId;
		this.featPath = featPath;
		this.type = type;
	}

	public UnassignableFeaturePathException(Exception e, String plcName, String ruleId, String featPath,
			String type) {
		super(e);
		this.plcName = plcName;
		this.ruleId = ruleId;
		this.featPath = featPath;
		this.type = type;
	}
	
	public String getPlcName(){
		return plcName;
	}
	
	public String getRuleId() {
		return ruleId;
	}
	
	public String getFeatPath(){
		return featPath;
	}
	
	public String getUimaType(){
		return type;
	}
}
