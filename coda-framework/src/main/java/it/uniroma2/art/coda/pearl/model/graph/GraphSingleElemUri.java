package it.uniroma2.art.coda.pearl.model.graph;

import it.uniroma2.art.coda.pearl.model.ProjectionRule;

/**
 * This class is the single element containing a URI
 * @author Andrea Turbati
 *
 */
public class GraphSingleElemUri extends GraphSingleElement{
	private String uri;
	
	
	/**
	 * @param uri the URI contained in this element
	 * @param prRule the projection rule where this element belongs to
	 */
	public GraphSingleElemUri(String uri, ProjectionRule prRule) {
		super(prRule);
		this.uri = uri;
	}
	
	/**
	 * Get the URI contained in this element
	 * @return the URI contained in this element
	 */
	public String getURI(){
		return uri;
	}

	@Override
	public String getValueAsString() {
		return "<"+getURI()+">";
	}
}
