package it.uniroma2.art.coda.exception;

import it.uniroma2.art.coda.exception.parserexception.PRParserException;

public class PlaceholderValueWrongTypeForParamInAnnotationException extends PRParserException {

	private String annName;
	private String ruleId;
	private String paramName;
	private  String typeExpected;
	private String plchName;
	private String plchValue;


	private static final long serialVersionUID = 1L;

	public PlaceholderValueWrongTypeForParamInAnnotationException(String annName, String ruleId, String paramName, String typeExpected,
			String plchName, String plchValue) {
		super();
		this.annName = annName;
		this.ruleId = ruleId;
		this.paramName = paramName;
		this.typeExpected = typeExpected;
		this.plchName = plchName;
		this.plchValue = plchValue;
	}

	public PlaceholderValueWrongTypeForParamInAnnotationException(Exception e, String prefixName, String ruleId, String paramName, String typeExpected,
			String plchName, String plchValue) {
		super(e);
		this.annName = prefixName;
		this.ruleId = ruleId;
		this.paramName = paramName;
		this.typeExpected = typeExpected;
		this.plchName = plchName;
		this.plchValue = plchValue;
	}
	
	public String getAnnName(){
		return annName;
	}
	
	public String getRuleId() {
		return ruleId;
	}

	public String getParamName() {
		return paramName;
	}

	public String getTypeExpected() {
		return typeExpected;
	}

	public String getPlchName() {
		return plchName;
	}

	public String getPlchValue() {
		return plchValue;
	}

	@Override
	public String getErrorAsString() {
		return "param "+paramName+" in annotation "+ annName +" in rule "+ruleId+" is used but not defined";
	}
}
