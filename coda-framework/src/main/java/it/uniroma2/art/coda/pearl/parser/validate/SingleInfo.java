package it.uniroma2.art.coda.pearl.parser.validate;

public class SingleInfo {

	private String info;
	private boolean hasWarning;
	
	public SingleInfo(String info, boolean hasWarning) {
		this.info = info;
		this.hasWarning = hasWarning;
	}

	public String getInfo() {
		return info;
	}

	public boolean hasWarning() {
		return hasWarning;
	}
	
	
	
}
