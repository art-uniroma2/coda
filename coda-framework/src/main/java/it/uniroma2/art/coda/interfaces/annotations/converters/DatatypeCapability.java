package it.uniroma2.art.coda.interfaces.annotations.converters;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * If a converter produces  typedLiteral, literal or node (see {@link RDFCapability}, it should be possible to
 * optionally specify one or more accepted datatype.
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DatatypeCapability {
	String[] value();
}
