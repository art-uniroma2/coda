package it.uniroma2.art.coda.structures;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains a list of tuple obtained from executing a SPARQL query
 * 
 * @author Andrea Turbati
 * 
 */
public class WhereResult {
	private List<Tuple> tupleList;

	public WhereResult() {
		tupleList = new ArrayList<Tuple>();
	}

	/**
	 * Add a tuple to the list
	 * 
	 * @param tuple
	 *            the tuple to be added to the list
	 */
	public void addTuple(Tuple tuple) {
		tupleList.add(tuple);
	}

	/**
	 * Get the list of tuples
	 * 
	 * @return the list of tuples
	 */
	public List<Tuple> getTupleList() {
		return tupleList;
	}
}
