package it.uniroma2.art.coda.pearl.model;

import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.structures.StringOrFeatureStruct;

import java.util.List;

import org.apache.uima.jcas.tcas.Annotation;

public class ConditionStruct {

	public enum Operator {
		IN, NOT_IN
	}
	
	private String featPath;
	private String uimaTypeAndFeat;
	private Operator condOperator;
	//the value in the condition (in the IN or NOT_IN list)
	private List<String> valueList;
	
	public ConditionStruct(String featPath, String uimaTypeAndFeat, Operator condOperator, 
			List<String> valueList) {
		this.featPath = featPath;
		this.uimaTypeAndFeat = uimaTypeAndFeat;
		this.condOperator = condOperator;
		this.valueList = valueList;
	}

	public String getUimaTypeAndFeat(){
		return uimaTypeAndFeat;
	}
	
	public String getFeatPath() {
		return featPath;
	}

	public Operator getCondOperator() {
		return condOperator;
	}

	public List<String> getValueList() {
		return valueList;
	}
	
	public boolean evaluteCondition(Annotation ann){
		
		if(valueList == null){
			return false;
		}
		
		List<StringOrFeatureStruct> listOfStringOrFeat = 
				UIMACODAUtilities.getValuesOfFeatureFromFeatPath(ann, uimaTypeAndFeat);
		//at the moment, just one value should be returned, so if the featurePath return more than one
		// value, the evaluation return false
		if(listOfStringOrFeat.size()!=1){
			return false;
		}
		StringOrFeatureStruct stringOrFeat = listOfStringOrFeat.get(0);
		// only string value are considered
		if(stringOrFeat.hasFeatureStruct()){
			return false;
		}
		String valueFromFeat = listOfStringOrFeat.get(0).getStringValue();
		
		if(condOperator.equals(Operator.IN)){
			return valueList.contains(valueFromFeat);
		} else if(condOperator.equals(Operator.NOT_IN)){
			return !valueList.contains(valueFromFeat);
		} else // this should never happen
			return false;
	}
	

}
