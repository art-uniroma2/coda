package it.uniroma2.art.coda.structures;

import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.rdf4j.model.Value;

/**
 * This class contains the final suggestion from CODA: the triple that should be added to the ontology
 * 
 * @author Andrea Turbati
 * 
 */
public class CodaFinalSuggestions extends InterComponentObject {

	// the list of triple the external program should add to the ontology
	List<CODATriple> artInsertTripleList;
	// the list of triple the external program should delete from the ontology
	List<CODATriple> artDeleteTripleList;
	// a map containing for each projection rules used the placeholder and the associated Value
	Map<ProjectionRule, Map<PlaceholderStruct, List<? extends Value>>> projRuleToPlacehoderToValueMap;

	//Map<ProjectionRule, Boolean> projRuleSucceded;

	public CodaFinalSuggestions() {
		artInsertTripleList = new ArrayList<CODATriple>();
		artDeleteTripleList = new ArrayList<CODATriple>();
		projRuleToPlacehoderToValueMap = new HashMap<ProjectionRule, Map<PlaceholderStruct, List<? extends Value>>>();
		//projRuleSucceded = new HashMap<ProjectionRule, Boolean>();
	}

	/**
	 * Add a triple to the list of triple that should be added to the ontology
	 * 
	 * @param codaTriple
	 *            the triple that should be added to the ontology
	 * @return true if the triple was not already present in the list, false oherwise
	 */
	public boolean addToInsertTriple(CODATriple codaTriple) {
		if (artInsertTripleList.contains(codaTriple))
			return false;
		artInsertTripleList.add(codaTriple);
		return true;
	}
	
	/**
	 * Add a triple to the list of triple that should be deleted from the ontology
	 * 
	 * @param codaTriple
	 *            the triple that should be removed from the ontology
	 * @return true if the triple was not already present in the list, false oherwise
	 */
	public boolean addToADeleteTriple(CODATriple codaTriple) {
		if (artDeleteTripleList.contains(codaTriple))
			return false;
		artDeleteTripleList.add(codaTriple);
		return true;
	}

	/**
	 * Get the list of the  Triples to be deleted
	 * 
	 * @return the list of triples to be deleted
	 */
	public List<CODATriple> getDeleteTriplesList() {
		return artDeleteTripleList;
	}
	
	/**
	 * Get the list of the Triples to be inserted
	 * 
	 * @return the list of triples to be inserted
	 */
	public List<CODATriple> getAddTriplesList() {
		return artInsertTripleList;
	}

	/**
	 * Add a list of value regarding a placeholder for a particular projection rule
	 * 
	 * @param projRule
	 *            the projection rule to which the placeholder belongs to
	 * @param placeholder
	 *            the placeholder to which the list of values belongs to
	 * @param artNodeList
	 *            the list of values for the palceholder
	 */
	public void addPlaceholderToValue(ProjectionRule projRule, PlaceholderStruct placeholder,
			List<? extends Value> artNodeList) {
		if (projRuleToPlacehoderToValueMap.get(projRule) == null)
			projRuleToPlacehoderToValueMap.put(projRule,
					new HashMap<PlaceholderStruct, List<? extends Value>>());
		projRuleToPlacehoderToValueMap.get(projRule).put(placeholder, artNodeList);
	}

	/**
	 * Get the list of values for a particular placeholder
	 * 
	 * @param placeholder
	 *            the placeholder to which the returned list belongs to
	 * @return the list of values associated to the particular placeholder
	 */
	//public List<? extends Value> getValueFromPlaceholder(PlaceholderStruct placeholder) {
	//	ProjectionRule projRule = placeholder.getProjRule();
	//	Map<PlaceholderStruct, List<? extends Value>> projRuleLabelMap = projRuleToPlacehoderToValueMap
	//			.get(projRule);
	//	if (projRuleLabelMap == null)
	//		return null;
	//	return projRuleLabelMap.get(placeholder);
	//}

	/**
	 * Set the success/unsuccess for a particular projection rule
	 * 
	 * @param projRule
	 *            the projection rule that succeeded or unsucceeded
	 * @param succeded
	 *            true if the projection rule succeeded, false otherwise
	 */
	//public void addProjRuleSucceed(ProjectionRule projRule, boolean succeded) {
	//	projRuleSucceded.put(projRule, new Boolean(succeded));
	//}

	/**
	 * Get the success/unsuccess for a particular projection rule
	 * 
	 * @param projRule
	 *            the projection rule we want to check
	 * @return true if the projection rule succeeded, false otherwise
	 */
	//public Boolean getSuccededFromProjRule(ProjectionRule projRule) {
	//	return projRuleSucceded.get(projRule).booleanValue();
	//}
}
