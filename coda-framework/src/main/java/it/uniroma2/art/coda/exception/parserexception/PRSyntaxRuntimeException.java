package it.uniroma2.art.coda.exception.parserexception;

public class PRSyntaxRuntimeException extends RuntimeException {
	
	String message;
	
	private static final long serialVersionUID = 1L;


	public PRSyntaxRuntimeException(RuntimeException e) {
		super(e);
		message = e.getMessage();
	}
	
	public PRSyntaxRuntimeException(String text) {
		super(new RuntimeException(text));
		message = text;
	}
	
	public String getMessage() {
		return message;
	}
}
