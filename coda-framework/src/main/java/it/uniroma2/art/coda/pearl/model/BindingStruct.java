package it.uniroma2.art.coda.pearl.model;

public class BindingStruct {
	private String name;
	private String featurePath;
	private String boundRuleId;
	
	private ProjectionRule ownerRule;

	public BindingStruct(String name, String featurePath, String boundRuleId, ProjectionRule ownerRule) {
		initialize(name, featurePath, boundRuleId, ownerRule);
	}

	private void initialize(String name, String featurePath, String boundRuleId, ProjectionRule ownerRule) {
		this.name = name;
		this.featurePath = featurePath;
		this.boundRuleId = boundRuleId;
		this.ownerRule = ownerRule;
	}

	public String getName() {
		return name;
	}

	public String getFeaturePath() {
		return featurePath;
	}

	public String getBoundRuleId() {
		return boundRuleId;
	}

	public ProjectionRule getOwnerRule() {
		return ownerRule;
	}
}
