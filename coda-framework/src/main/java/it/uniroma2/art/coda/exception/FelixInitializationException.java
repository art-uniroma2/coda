package it.uniroma2.art.coda.exception;

/**
 * This class represent a generic Exception regarding the Felix extension Management Part
 * 
 * @author Andrea Turbati
 *
 */
public class FelixInitializationException extends Exception {
    
    private static final long serialVersionUID = -2462163028127570429L;

	/**
	 * @param msg
	 */
	public FelixInitializationException(String msg) {
        super(msg);
    }
    
    /**
     * 
     * @param e
     */
    public FelixInitializationException(Exception e) {
        super(e);
    }

}
