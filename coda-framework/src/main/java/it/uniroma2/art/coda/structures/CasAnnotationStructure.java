package it.uniroma2.art.coda.structures;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

/**
 * This class contains the JCas and the annotation
 * 
 * @author Andrea Turbati
 * 
 */
public class CasAnnotationStructure {
	private JCas jcas;
	private Annotation ann;

	/**
	 * @param jcas
	 *            the JCas contained in this instance
	 * @param ann
	 *            the Annotation contained in this instance
	 */
	public CasAnnotationStructure(JCas jcas, Annotation ann) {
		super();
		this.jcas = jcas;
		this.ann = ann;
	}

	/**
	 * Get the JCas
	 * 
	 * @return the JCas
	 */
	public JCas getJcas() {
		return jcas;
	}

	/**
	 * Get the Annotation
	 * 
	 * @return the annotation
	 */
	public Annotation getAnnotation() {
		return ann;
	}
}
