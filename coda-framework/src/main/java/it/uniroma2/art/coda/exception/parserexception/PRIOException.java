package it.uniroma2.art.coda.exception.parserexception;

import java.io.IOException;

public class PRIOException extends PRParserException {

	private IOException ieException = null;
	
	private static final long serialVersionUID = 1L;

	public PRIOException(IOException e) {
		super();
		ieException = e;
	}

	public PRIOException(Exception e, String prefixName, String ruleId) {
		super(e);
	}
	
	@Override
	public String getErrorAsString() {
		//TODO try to return a better message
		return ieException.getMessage();
	}
}
