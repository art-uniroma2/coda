package it.uniroma2.art.coda.util;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.repository.RepositoryConnection;

public class OntoUtils {

	public static boolean existsResource(RepositoryConnection conncetion, Resource res, Resource ...contexts ){
		
		boolean hasSubjNoInf = conncetion.hasStatement(res, null, null, false, contexts);
		boolean hasSubjInf = conncetion.hasStatement(res, null, null, true, contexts);
		if(hasSubjNoInf || hasSubjInf){
			return true;
		}
		
		if(res instanceof IRI){
			boolean hasPredNoInf = conncetion.hasStatement(null, (IRI) res, null, false, contexts);
			boolean hasPredInf = conncetion.hasStatement(null, (IRI) res, null, true, contexts);
			if(hasPredNoInf || hasPredInf){
				return true;
			}
		}
		
		boolean hasObjNoInf = conncetion.hasStatement(null, null, res, false, contexts);
		boolean hasObjInf = conncetion.hasStatement(null, null, res, true, contexts);
		if(hasObjNoInf || hasObjInf){
			return true;
		}
		
		//the resource is not already present, so return false
		return false;
	}
	
	public static Literal createLiteral(String literal, Map<String, String> prefixToNSMap){
		String partiallyPrunedLiteral = literal.substring(1);
		if (partiallyPrunedLiteral.contains("\"^^")) {
			String[] literalPieces = partiallyPrunedLiteral.split("\"\\^\\^");
			String datatypeURI;
			if (literalPieces[1].startsWith("<")) {
				datatypeURI = literalPieces[1].substring(1, literalPieces[1].length() - 1);
			} else{
				datatypeURI = prefixToNSMap.get(literalPieces[1].substring(0, literalPieces[1].length()));
			}
			return SimpleValueFactory.getInstance().createLiteral(literalPieces[0], 
					SimpleValueFactory.getInstance().createIRI(datatypeURI));
		}

		if (partiallyPrunedLiteral.contains("\"@")) {
			int lastIndexOfAt = partiallyPrunedLiteral.lastIndexOf("\"@");
			return SimpleValueFactory.getInstance().createLiteral(partiallyPrunedLiteral.substring(0, lastIndexOfAt),
					partiallyPrunedLiteral.substring(lastIndexOfAt + 2));
		}

		return SimpleValueFactory.getInstance()
				.createLiteral(partiallyPrunedLiteral.substring(0, partiallyPrunedLiteral.length() - 1));
	}
	
	public static boolean areTwoOntologyEquals(RepositoryConnection connectionOne, 
			RepositoryConnection connectionTwo){
		
		Set<Statement> firstSet = new HashSet<Statement>();
		Set<Statement> secondoSet = new HashSet<Statement>();
		
		//the two ontology have different size, so they are different
		if(firstSet.size() != secondoSet.size()){
			return false;
		}
		
		//TODO test if this works
		for(Statement statementOne : firstSet){
			if(!secondoSet.contains(statementOne)){
				return false;
			}
		}
		
		return true;
		
		
	}
}
