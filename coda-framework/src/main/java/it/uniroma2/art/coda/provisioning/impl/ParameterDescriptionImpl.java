package it.uniroma2.art.coda.provisioning.impl;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.coda.provisioning.ParameterDescription;
import it.uniroma2.art.coda.provisioning.TypeDescription;

public class ParameterDescriptionImpl implements ParameterDescription {

	private final String name;
	private final String htmlDescription;
	private final TypeDescription typeDescription;

	public ParameterDescriptionImpl(String name, String htmlDescription, TypeDescription typeDescription) {
		this.name = name;
		this.htmlDescription = htmlDescription;
		this.typeDescription = typeDescription;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getHTMLDescription() {
		return htmlDescription;
	}

	@Override
	public TypeDescription getTypeDescription() {
		return typeDescription;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("name", name).add("htmlDescription", htmlDescription)
				.add("typeDescription", typeDescription).toString();
	}

}
