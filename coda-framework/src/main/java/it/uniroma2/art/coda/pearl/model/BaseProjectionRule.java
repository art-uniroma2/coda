package it.uniroma2.art.coda.pearl.model;

public abstract class BaseProjectionRule {
	protected String id;
	private ProjectionRulesModel projectionRulesModel;
	
	public BaseProjectionRule(String id, ProjectionRulesModel projectionRulesModel) {
		this.id = id;
		this.projectionRulesModel = projectionRulesModel;
	}
	
	public String getId() {
		return id;
	}

	public ProjectionRulesModel getProjectionRulesModel() {
		return projectionRulesModel;
	}
}
