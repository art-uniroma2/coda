package it.uniroma2.art.coda.provisioning;

/**
 * An exception related to component provisioning.
 */
public class ComponentProvisioningException extends Exception {

	private static final long serialVersionUID = 7192168503167874644L;

	public ComponentProvisioningException() {
		super();
	}

	public ComponentProvisioningException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ComponentProvisioningException(String message, Throwable cause) {
		super(message, cause);
	}

	public ComponentProvisioningException(String message) {
		super(message);
	}

	public ComponentProvisioningException(Throwable cause) {
		super(cause);
	}

}
