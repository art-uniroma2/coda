package it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FSA {
	private List <StateFSA>startStateList;
	private List <StateFSA>endStateList;
	
	// the key is the id of the state
	private Map<String, StateFSA> stateMap;
	//the key is the id of the transition (not the internalId used to refer to a particular ruleId)
	private Map<String, TransitionFSA> transitionMap;

	private Map<String, TransitionFSA> internalIdToTransitionMap;
	
	public FSA() {
//		initialize(new ArrayList<StateFSA>(), new ArrayList<StateFSA>());
		initialize();
	}
	
	//private void initialize(List <StateFSA>inputStartStateList, List <StateFSA>inputEndStateList){
	private void initialize(){
		startStateList = new ArrayList<StateFSA>();
		endStateList = new ArrayList<StateFSA>();
		
		stateMap = new HashMap<String, StateFSA>();
		transitionMap = new HashMap<String, TransitionFSA>();
		internalIdToTransitionMap = new HashMap<>();
		
//		startStateList.addAll(inputStartStateList);
//		endStateList.addAll(inputEndStateList);
	}
	
	
	public void addStartState(StateFSA startState){
		startStateList.add(startState);
		addState(startState);
	}
	
	public void addEndState(StateFSA endState){
		endStateList.add(endState);
		addState(endState);
	}
	
	public List<StateFSA> getStartStateList(){
		return startStateList;
	}
	
	public List<StateFSA> getEndStateList(){
		return endStateList;
	}
	
	public void addState(StateFSA state) {
		stateMap.put(state.getStateId(), state);
	}
	
	public void addTransition(TransitionFSA transition){
		transitionMap.put(transition.getTransitionId(), transition);
		internalIdToTransitionMap.put(transition.getInternalId(), transition);
	}
	
	public Map<String, StateFSA> getStateMap(){
		return stateMap;
	}
	
	/*public Map<String, TransitionFSA> getTransitionMap(){
		return transitionMap;
	}*/
	
	public Collection<String> getTransitionId(){
		return transitionMap.keySet();
	}
	
	public TransitionFSA getTransition(String transitionId){
		return transitionMap.get(transitionId);
	}
	
	public TransitionFSA getTransitionFromInternalId(String internalId){
		return internalIdToTransitionMap.get(internalId);
	}
	
}
