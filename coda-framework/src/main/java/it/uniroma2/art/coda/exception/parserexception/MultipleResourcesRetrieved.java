package it.uniroma2.art.coda.exception.parserexception;

import it.uniroma2.art.coda.exception.ConverterException;

public class MultipleResourcesRetrieved extends ConverterException {

    private static final long serialVersionUID = 6351051432239740725L;

    private String propertyPath;
    private String inputValue;

    public MultipleResourcesRetrieved(String propertyPath, String inputValue) {
        super("Multiple resources where retrieve using the PropertyPath  "+propertyPath+" and value "+ inputValue);
        this.propertyPath = propertyPath;
        this.inputValue = inputValue;
    }

    public String getPropertyPath() {
        return propertyPath;
    }

    public String getInputValue() {
        return inputValue;
    }
}
