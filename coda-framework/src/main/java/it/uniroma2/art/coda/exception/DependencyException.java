package it.uniroma2.art.coda.exception;

public class DependencyException extends Exception {
    

	/**
	 * 
	 */
	private static final long serialVersionUID = 6356051432263640825L;

	/**
	 * @param msg
	 */
	public DependencyException(String msg) {
        super(msg);
    }
    
    /**
     * @param e
     */
    public DependencyException(Exception e) {
        super(e);
    }

}
