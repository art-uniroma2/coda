package it.uniroma2.art.coda.pearl.model.annotation.param;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class ParamValueIri implements  ParamValueInterface {
    private IRI iri;

    public ParamValueIri(IRI iri) {
        this.iri = iri;
    }

    public IRI getIri() {
        return iri;
    }

    @Override
    public String toString() {
        return NTriplesUtil.toNTriplesString(iri);
    }

    @Override
    public String asString() {
        return NTriplesUtil.toNTriplesString(iri);
    }

    @Override
    public String getType() {
        return "iri";
    }
}
