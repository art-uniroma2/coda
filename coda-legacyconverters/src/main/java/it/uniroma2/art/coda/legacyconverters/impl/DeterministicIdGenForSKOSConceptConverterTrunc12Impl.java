package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractDeterministicIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.DeterministicIdGenForSKOSConceptConverterTrunc12;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class DeterministicIdGenForSKOSConceptConverterTrunc12Impl extends AbstractDeterministicIdGenID
		implements DeterministicIdGenForSKOSConceptConverterTrunc12 {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "detGen-ConceptId-trunc12";

	public DeterministicIdGenForSKOSConceptConverterTrunc12Impl() {
		super("c_", 12);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
