package it.uniroma2.art.coda.legacyconverters.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;

public interface RandomIdGenForSKOSConceptConverter extends Converter {
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "randGen-ConceptId";

	IRI produceURI(CODAContext ctx, String value);
}
