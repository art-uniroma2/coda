package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractRandomIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.RandomIdGenForSKOSXLLabelConverterTrunc8;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class RandomIdGenForSKOSXLLabelConverterTrunc8Impl extends AbstractRandomIdGenID implements
		RandomIdGenForSKOSXLLabelConverterTrunc8 {
	
	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "randGen-XLabelId-trunc8";

	public RandomIdGenForSKOSXLLabelConverterTrunc8Impl() {
		super("xl_", 8);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
