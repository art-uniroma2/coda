package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractRandomIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.RandomIdGenForSKOSConceptConverter;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class RandomIdGenForSKOSConceptConverterImpl extends AbstractRandomIdGenID implements
		RandomIdGenForSKOSConceptConverter {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "randGen-ConceptId";

	public RandomIdGenForSKOSConceptConverterImpl() {
		super("c_", 16);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
