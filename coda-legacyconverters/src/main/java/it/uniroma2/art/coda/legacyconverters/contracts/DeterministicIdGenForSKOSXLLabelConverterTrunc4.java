package it.uniroma2.art.coda.legacyconverters.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;

public interface DeterministicIdGenForSKOSXLLabelConverterTrunc4 extends
		DeterministicIdGenForSKOSXLLabelConverter {
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "detGen-XLabelId-trunc4";
}
