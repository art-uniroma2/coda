package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractRandomIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.RandomIdGenForSKOSXLLabelConverterTrunc12;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class RandomIdGenForSKOSXLLabelConverterTrunc12Impl extends AbstractRandomIdGenID implements
		RandomIdGenForSKOSXLLabelConverterTrunc12 {
	
	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "randGen-XLabelId-trunc12";

	public RandomIdGenForSKOSXLLabelConverterTrunc12Impl() {
		super("xl_", 12);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
