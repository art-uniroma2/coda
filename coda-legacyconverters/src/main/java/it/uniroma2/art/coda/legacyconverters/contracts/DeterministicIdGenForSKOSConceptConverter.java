package it.uniroma2.art.coda.legacyconverters.contracts;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;

public interface DeterministicIdGenForSKOSConceptConverter extends Converter {
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "detGen-ConceptId";

	IRI produceURI(CODAContext ctx, String value);
}
