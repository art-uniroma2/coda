package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractRandomIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.RandomIdGenForSKOSConceptConverterTrunc8;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class RandomIdGenForSKOSConceptConverterTrunc8Impl extends AbstractRandomIdGenID implements
		RandomIdGenForSKOSConceptConverterTrunc8 {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "randGen-ConceptId-trunc8";

	public RandomIdGenForSKOSConceptConverterTrunc8Impl() {
		super("c_", 8);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
