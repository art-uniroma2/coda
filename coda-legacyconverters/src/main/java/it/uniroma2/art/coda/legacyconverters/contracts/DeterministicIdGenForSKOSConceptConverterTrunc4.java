package it.uniroma2.art.coda.legacyconverters.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;

public interface DeterministicIdGenForSKOSConceptConverterTrunc4 extends
		DeterministicIdGenForSKOSConceptConverter {
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "detGen-ConceptId-trunc4";
}
