package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractDeterministicIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.DeterministicIdGenForSKOSConceptConverterTrunc8;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class DeterministicIdGenForSKOSConceptConverterTrunc8Impl extends AbstractDeterministicIdGenID
		implements DeterministicIdGenForSKOSConceptConverterTrunc8 {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "detGen-ConceptId-trunc8";
	
	public DeterministicIdGenForSKOSConceptConverterTrunc8Impl() {
		super("c_", 8);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
