package it.uniroma2.art.coda.legacyconverters.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;

public interface RandomIdGenForSKOSXLLabelConverterTrunc8 extends RandomIdGenForSKOSXLLabelConverter {
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "randGen-XLabelId-trunc8";
}
