package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractDeterministicIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.DeterministicIdGenForSKOSXLLabelConverter;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class DeterministicIdGenForSKOSXLLabelConverterImpl extends AbstractDeterministicIdGenID implements
		DeterministicIdGenForSKOSXLLabelConverter {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "detGen-XLabelId";

	public DeterministicIdGenForSKOSXLLabelConverterImpl() {
		super("xl_", 16);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
