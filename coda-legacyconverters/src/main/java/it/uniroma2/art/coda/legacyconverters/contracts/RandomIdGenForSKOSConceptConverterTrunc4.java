package it.uniroma2.art.coda.legacyconverters.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;

public interface RandomIdGenForSKOSConceptConverterTrunc4 extends RandomIdGenForSKOSConceptConverter{
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "randGen-ConceptId-trunc4";
}