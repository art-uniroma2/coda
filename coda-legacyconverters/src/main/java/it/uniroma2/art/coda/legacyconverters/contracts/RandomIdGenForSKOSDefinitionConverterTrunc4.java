package it.uniroma2.art.coda.legacyconverters.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;

public interface RandomIdGenForSKOSDefinitionConverterTrunc4 extends RandomIdGenForSKOSDefinitionConverter {
	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "randGen-DefinitionId-trunc4";
}
