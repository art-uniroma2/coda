package it.uniroma2.art.coda.legacyconverters.impl;

import it.uniroma2.art.coda.converters.commons.AbstractDeterministicIdGenID;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.legacyconverters.contracts.DeterministicIdGenForSKOSXLLabelConverterTrunc4;
import org.pf4j.Extension;

@Extension(points = { Converter.class })
public class DeterministicIdGenForSKOSXLLabelConverterTrunc4Impl extends AbstractDeterministicIdGenID
		implements DeterministicIdGenForSKOSXLLabelConverterTrunc4 {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI + "detGen-XLabelId-trunc4";

	public DeterministicIdGenForSKOSXLLabelConverterTrunc4Impl() {
		super("xl_", 4);
	}

	// @Override
	// public ARTNode produceURI(CODAContext ctx, String value) {
	// ... Inherited from base class ...
	// }

}
