package it.uniroma2.art.coda.test.contracts;

import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.text.AnnotationFS;
import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;

public interface CompleteNameContract extends Converter {
	public static final String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI
			+ "completeName";

	IRI produceURI(CODAContext ctx, FeatureStructure fs);
	IRI produceURI(CODAContext ctx, String value);
}
