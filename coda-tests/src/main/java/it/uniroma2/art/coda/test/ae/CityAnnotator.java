package it.uniroma2.art.coda.test.ae;

//import it.uniroma2.art.coda.test.ae.type.City;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;

public class CityAnnotator extends JCasAnnotator_ImplBase {

	public static final String CITY_ARRAY = "cityArray";
	@ConfigurationParameter(name = CITY_ARRAY)
	private String [] cityArray;

	public static final String CITY_ANNOTATION_TYPE_STRING = "cityAnnotationType";
	@ConfigurationParameter(name = CITY_ANNOTATION_TYPE_STRING)
	private String cityAnnotationType;

	public static final String NAME_STRING = "name";
	@ConfigurationParameter(name = NAME_STRING)
	private String name;

	public static final String WORD_STRING = "word";
	@ConfigurationParameter(name = WORD_STRING)
	private String word;

	public static final String CONFIDENCE_DOUBLE = "confidence";
	@ConfigurationParameter(name = CONFIDENCE_DOUBLE)
	private String confidence;

	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

		TypeSystem ts = aJCas.getTypeSystem();
		Type type = ts.getType(cityAnnotationType);

		Pattern patter = Pattern.compile("\\b([A-Z]?[a-z]+)\\b");
		
		
		String text = aJCas.getDocumentText();
		
		Matcher matcher = patter.matcher(text);
		
		if(cityArray == null || cityArray.length==0){
			// the array is null or has no element, so there is no reason to execute the matcher
			return;
		}
		
		int count = 0;
		while(matcher.find(count)){
			
			String textFound = matcher.group();
			for(int i=0; i<cityArray.length; ++i){
				String currentCity = cityArray[i].toLowerCase();
				if(currentCity.compareToIgnoreCase(textFound) == 0){
					//create the CityAnnotation
					AnnotationFS ann = aJCas.getCas().createAnnotation(type, matcher.start(), matcher.end());

					// create the various features and add the value
					Feature nameFeature = type.getFeatureByBaseName(name);
					ann.setStringValue(nameFeature, textFound);
					Feature wordFeature = type.getFeatureByBaseName(word);
					ann.setStringValue(wordFeature, textFound);
					Feature confidenceFeature = type.getFeatureByBaseName(confidence);
					ann.setDoubleValue(confidenceFeature, 0.7);

					// add the annotation to the index
					aJCas.addFsToIndexes(ann);

					//OLD way to create the annotation
					/*
					City cityAnnotation = new City(aJCas);
					cityAnnotation.setBegin(matcher.start());
					cityAnnotation.setEnd(matcher.end());
					cityAnnotation.setName(textFound);
					cityAnnotation.setWord(textFound);
					cityAnnotation.setConfidence(0.7);// for now add a fixed number for the confidence feature
					cityAnnotation.addToIndexes();
					*/

					break; // the city has been found, so skip to the next word
				}
			}
			
			count = matcher.end();
		}

	}

}
