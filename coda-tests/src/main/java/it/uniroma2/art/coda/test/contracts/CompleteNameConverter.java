package it.uniroma2.art.coda.test.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.apache.uima.cas.FeatureStructure;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.pf4j.Extension;

@Extension(points = {Converter.class})
public class CompleteNameConverter implements CompleteNameContract {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "completeName";

	@Override
	public IRI produceURI(CODAContext ctx, FeatureStructure fs) {
		// the value passed is not a simple String, but it is a feature structure

		//String firstName = value.getFirstName();
		//String lastName = value.getLastName();
		//return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace() + firstName + lastName);

		String firstName = fs.getFeatureValueAsString(fs.getType().getFeatureByBaseName("firstName"));
		String lastName = fs.getFeatureValueAsString(fs.getType().getFeatureByBaseName("lastName"));
		return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace() + firstName + lastName);
	}

	@Override
	public IRI produceURI(CODAContext ctx, String value) {
		return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace() + value);
	}


}
