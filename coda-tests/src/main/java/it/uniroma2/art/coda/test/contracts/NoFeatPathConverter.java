package it.uniroma2.art.coda.test.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.pf4j.Extension;

@Extension(points = {Converter.class})
public class NoFeatPathConverter implements NoFeatPathContract {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "noFeatPath";


	@Override
	public IRI produceURI(CODAContext ctx, String value) {
		String time = new java.util.Date().getTime()+"";
		return SimpleValueFactory.getInstance().createIRI(ctx.getDefaultNamespace() + time);
	}

	@Override
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) {
		String time = new java.util.Date().getTime()+"";
		if (datatype != null) {
			return SimpleValueFactory.getInstance().createLiteral(time, 
					SimpleValueFactory.getInstance().createIRI(datatype));
		} else if (lang != null) {
			return SimpleValueFactory.getInstance().createLiteral(time, lang);
		} else {
			return SimpleValueFactory.getInstance().createLiteral(time);
		}
	}


}
