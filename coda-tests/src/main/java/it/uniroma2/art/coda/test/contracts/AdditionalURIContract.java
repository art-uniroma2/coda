package it.uniroma2.art.coda.test.contracts;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;

public interface AdditionalURIContract extends Converter {
	public final String CONTRACT_URI = "http://art.uniroma2.it/coda/contracts/additionalURI";
	
	public IRI produceURI(CODAContext codaContext, String value, IRI additionalURI);
}
