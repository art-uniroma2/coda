package it.uniroma2.art.coda.test.ae;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;

public class MultiWordAnnotator extends JCasAnnotator_ImplBase {

	public static final String NUMBEROFWORD_INT = "numberOfWord";
	@ConfigurationParameter(name = NUMBEROFWORD_INT)
	private int numberOfWord;

	public static final String MULTI_WORD_ANNOTATION_TYPE_STRING = "multiwordAnnotationType";
	@ConfigurationParameter(name = MULTI_WORD_ANNOTATION_TYPE_STRING)
	private String multiwordAnnotationType;

	public static final String FIRST_WORD_STRING = "firstWord";
	@ConfigurationParameter(name = FIRST_WORD_STRING)
	private String firstWord;

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		TypeSystem ts = aJCas.getTypeSystem();
		Type type = ts.getType(multiwordAnnotationType);

		String singleWordRegex = "([A-Z]?[a-z]+)";
		
		String multiWord = "";

		//create a REGEX concatenating singleWordRegex and "\\s" depending on the value of numberOfWord
		for(int i=0; i<numberOfWord; ++i){
			if(i>0){
				multiWord += "\\s";
			}
			multiWord += singleWordRegex;
		}
		multiWord = "\\b"+multiWord+"\\b";
		
		
		Pattern patter = Pattern.compile(multiWord);
		
		String text = aJCas.getDocumentText();
		
		Matcher matcher = patter.matcher(text);
		
		int count = 0;
		while(matcher.find(count)){

			//create the MultiWordAnnotation
			AnnotationFS ann = aJCas.getCas().createAnnotation(type, matcher.start(), matcher.end());

			// create the various features and add the value
			Feature wordFeature = type.getFeatureByBaseName(firstWord);
			ann.setStringValue(wordFeature, matcher.group().split(" ")[0]);

			// add the annotation to the index
			aJCas.addFsToIndexes(ann);

			//OLD way to create the annotation
			/*
			MultiWord multiWordAnn = new MultiWord(aJCas);
			multiWordAnn.setBegin(matcher.start());
			multiWordAnn.setEnd(matcher.end());
			multiWordAnn.setFirstWord(matcher.group().split(" ")[0]);
			multiWordAnn.addToIndexes();
			*/

			count = matcher.end();
		}

	}

}
