package it.uniroma2.art.coda.test.contracts;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

public interface NoFeatPathContract extends Converter {
	public static final String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI
			+ "noFeatPath";

	IRI produceURI(CODAContext ctx, String value);
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value);
}
