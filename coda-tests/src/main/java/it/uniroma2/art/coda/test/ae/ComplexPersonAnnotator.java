package it.uniroma2.art.coda.test.ae;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;

public class ComplexPersonAnnotator extends JCasAnnotator_ImplBase {

	public static final String PERSON_ARRAY = "personArray";
	@ConfigurationParameter(name = PERSON_ARRAY)
	private String [] personArray;

	public static final String COMPLETE_NAME_ANNOTATION_TYPE_STRING = "completenameAnnotationType";
	@ConfigurationParameter(name = COMPLETE_NAME_ANNOTATION_TYPE_STRING)
	private String completenameAnnotationType;

	public static final String FIRST_NAME_STRING = "firstName";
	@ConfigurationParameter(name = FIRST_NAME_STRING)
	private String firstName;

	public static final String LAST_NAME_STRING = "lastName";
	@ConfigurationParameter(name = LAST_NAME_STRING)
	private String lastName;

	public static final String COMPLEX_PERSON_ANNOTATION_TYPE_STRING = "complexpersonAnnotationType";
	@ConfigurationParameter(name = COMPLEX_PERSON_ANNOTATION_TYPE_STRING)
	private String complexpersonAnnotationType;

	public static final String NAME_COMPLETENAME = "name";
	@ConfigurationParameter(name = NAME_COMPLETENAME)
	private String name;


	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

		TypeSystem ts = aJCas.getTypeSystem();
		Type completenameType = ts.getType(completenameAnnotationType);
		Type complexpersonType = ts.getType(complexpersonAnnotationType);

		Pattern patter = Pattern.compile("\\b([A-Z]?[a-z]+)\\s([A-Z]?[a-z]+)\\b");
		
		
		String text = aJCas.getDocumentText();
		
		Matcher matcher = patter.matcher(text);
		
		if(personArray == null || personArray.length==0){
			// the array is null or has no element, so there is no reason to execute the matcher
			return;
		}
		
		int count = 0;
		int pos = 0;
		//while(matcher.find(count)){
		while(pos<text.length() && matcher.find(pos)){
			String textFound = matcher.group();
			for(int i=0; i<personArray.length; ++i){
				String currentPerson = personArray[i].toLowerCase();
				if(currentPerson.compareToIgnoreCase(textFound) == 0){

					//create the CompleteNameAnnotation
					//FeatureStructure completeNameFS = aJCas.getCas().createFS(completenameType);
					AnnotationFS completeNameFS = aJCas.getCas().createAnnotation(completenameType, matcher.start(), matcher.end());

					// create the various features and add the value
					Feature firstnameFeature = completenameType.getFeatureByBaseName(firstName);
					completeNameFS.setStringValue(firstnameFeature, matcher.group(1));
					Feature lastnameFeature = completenameType.getFeatureByBaseName(lastName);
					completeNameFS.setStringValue(lastnameFeature, matcher.group(2));

					//create the ComplexPersonAnnotation
					AnnotationFS complexPersonAnn = aJCas.getCas().createAnnotation(complexpersonType, matcher.start(), matcher.end());

					// create the various features and add the value
					Feature nameFeature = complexpersonType.getFeatureByBaseName(name);
					complexPersonAnn.setFeatureValue(nameFeature, completeNameFS);

					// add the annotation to the index
					aJCas.addFsToIndexes(complexPersonAnn);

					/*
					ComplexPerson complexPerson = new ComplexPerson(aJCas);
					complexPerson.setBegin(matcher.start());
					complexPerson.setEnd(matcher.end());

					CompleteName completeName = new CompleteName(aJCas);
					completeName.setFirstName(matcher.group(1));
					completeName.setLastName(matcher.group(2));
					complexPerson.setName(completeName);
					
					complexPerson.addToIndexes();
					 */
					
					pos = matcher.start() + matcher.group(1).length()+1; 
					
					break; // the person has been found, so skip to the next word
					//continue;
				}
			}
			pos += matcher.group(1).length()+1;
			
			
			count = count+matcher.group(1).length()+1;
		}

	}

}
