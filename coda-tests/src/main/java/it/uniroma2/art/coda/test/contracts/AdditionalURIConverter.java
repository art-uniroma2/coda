package it.uniroma2.art.coda.test.contracts;

import it.uniroma2.art.coda.interfaces.Converter;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.coda.interfaces.CODAContext;
import org.pf4j.Extension;

@Extension(points = {Converter.class})
public class AdditionalURIConverter implements AdditionalURIContract {
	public static final String CONVERTER_URI = "http://art.uniroma2.it/coda/converters/additionalURI";

	public static final IRI constantURI = SimpleValueFactory.getInstance().createIRI("http://constant.it");
	
	@Override
	public IRI produceURI(CODAContext codaContext, String value, IRI additionalURI) {
		return constantURI;
	}
	
}
