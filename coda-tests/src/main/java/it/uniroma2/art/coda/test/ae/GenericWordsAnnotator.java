package it.uniroma2.art.coda.test.ae;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenericWordsAnnotator extends JCasAnnotator_ImplBase {

	public static final String WORDS_ARRAY = "wordsArray";
	@ConfigurationParameter(name = WORDS_ARRAY)
	private String [] wordsArray;

	public static final String MULTI_WORD_ANNOTATION_TYPE_STRING = "multiwordAnnotationType";
	@ConfigurationParameter(name = MULTI_WORD_ANNOTATION_TYPE_STRING)
	private String multiwordAnnotationType;

	public static final String FIRST_WORD_STRING = "firstWord";
	@ConfigurationParameter(name = FIRST_WORD_STRING)
	private String firstWord;

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		TypeSystem ts = aJCas.getTypeSystem();
		Type type = ts.getType(multiwordAnnotationType);

		if(wordsArray == null || wordsArray.length==0){
			// the array is null or has no element, so there is no reason to execute the matcher
			return;
		}

		//iterate over the wordsArray, create a pattern for each word and search it in the aJCas.getDocumentText()
		for (String words : wordsArray) {
			Pattern patter = Pattern.compile(escapeForRegex(words));
			String text = aJCas.getDocumentText();
			Matcher matcher = patter.matcher(text);

			int count = 0;
			while(matcher.find(count)){
				String textFound = matcher.group();

				//create the MultiWordAnnotation
				AnnotationFS ann = aJCas.getCas().createAnnotation(type, matcher.start(), matcher.end());

				// create the various features and add the value
				Feature wordFeature = type.getFeatureByBaseName(firstWord);
				// in theory the FirstWord should contain only the first word, but in this case it contains
				// ALL the found words
				ann.setStringValue(wordFeature, textFound);

				// add the annotation to the index
				aJCas.addFsToIndexes(ann);

				//OLD way to create the annotation
				/*
				MultiWord multiWord = new MultiWord(aJCas);
				// in theory the FirstWord should contain only the first word, but in this case it contains
				// ALL the found words
				multiWord.setFirstWord(textFound);
				multiWord.setBegin(matcher.start());
				multiWord.setEnd(matcher.end());
				multiWord.addToIndexes();
				 */

				count = matcher.end();
			}
		}
	}

	private String escapeForRegex(String text) {
		return Pattern.quote(text);
	}

}
