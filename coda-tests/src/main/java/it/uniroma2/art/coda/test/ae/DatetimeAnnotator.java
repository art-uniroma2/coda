package it.uniroma2.art.coda.test.ae;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;

public class DatetimeAnnotator extends JCasAnnotator_ImplBase {

	public static final String DATE_ANNOTATION_TYPE_STRING = "dateAnnotationType";
	@ConfigurationParameter(name = DATE_ANNOTATION_TYPE_STRING)
	private String dateAnnotationType;

	public static final String TIME_ANNOTATION_TYPE_STRING = "plantAnnotationType";
	@ConfigurationParameter(name = TIME_ANNOTATION_TYPE_STRING)
	private String timeAnnotationType;

	public static final String DATE_TIME_ANNOTATION_TYPE_STRING = "datetimeAnnotationType";
	@ConfigurationParameter(name = DATE_TIME_ANNOTATION_TYPE_STRING)
	private String datetimeAnnotationType;

	public static final String VALUE_STRING = "value";
	@ConfigurationParameter(name = VALUE_STRING)
	private String value;

	private static List<String> DATE_PATTERNS = new ArrayList<String>();
	static {
		DATE_PATTERNS.add("\\d{4}-\\d{1,2}-\\d{1,2}"); //yyyy-MM-dd
		DATE_PATTERNS.add("\\d{4}\\s\\d{1,2}\\s\\d{1,2}"); //yyyy MM dd
		DATE_PATTERNS.add("\\d{4}/\\d{1,2}/\\d{1,2}"); //yyyy/MM/dd
		DATE_PATTERNS.add("\\d{4}\\.\\d{1,2}\\.\\d{1,2}"); //yyyy.MM.dd
		
		DATE_PATTERNS.add("\\d{1,2}-\\d{1,2}-\\d{4}"); //dd-MM-yyyy
		DATE_PATTERNS.add("\\d{1,2}\\s\\d{1,2}\\s\\d{4}"); //dd MM yyyy
		DATE_PATTERNS.add("\\d{1,2}/\\d{1,2}/\\d{4}"); //dd/MM/yyyy
		DATE_PATTERNS.add("\\d{1,2}\\.\\d{1,2}\\.\\d{4}"); //dd.MM.yyyy
		
		DATE_PATTERNS.add("\\d{4}\\s[a-z]{3,}\\s\\d{1,2}"); //yyyy MMM dd (month as text)
		DATE_PATTERNS.add("\\d{1,2}\\s[a-z]{3,}\\s\\d{4}"); //dd MMM yyyy (month as text)
	}
	
	private static List<String> TIME_PATTERNS = new ArrayList<String>();
	static {
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}"); //H:m
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}(\\+|\\-)\\d{2}:\\d{2}"); //H:mXXX
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}\\s(AM|PM)");	//H:m a
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}\\s(AM|PM)\\s(\\+|\\-)\\d{2}:\\d{2}"); //H:m a XXX
		
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}"); //H:m:s
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}\\s(AM|PM)"); //H:m:s a (a is AM/PM)
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}\\.\\d{1,3}"); //H:m:s.SSS
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}\\.\\d{1,3}\\s(AM|PM)"); //H:m:s.SSS
		
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}(\\+|\\-)\\d{2}:\\d{2}"); //H:m:sXXX (XXX is offset)
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}\\s(AM|PM)\\s(\\+|\\-)\\d{2}:\\d{2}"); //H:m:s a XXX
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}\\.\\d{1,3}(\\+|\\-)\\d{2}:\\d{2}"); //H:m:s.SSSXXX
		TIME_PATTERNS.add("\\d{1,2}:\\d{2}:\\d{2}\\.\\d{1,3}\\s(AM|PM)\\s(\\+|\\-)\\d{2}:\\d{2}"); //H:m:s.SSS a XXX
	}
	
	private static List<String> DATETIME_PATTERNS = new ArrayList<String>();
	static {
		DATETIME_PATTERNS.add("\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}"); //yyyy-MM-dd'T'HH:mm:ss
		DATETIME_PATTERNS.add("\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}(\\+|\\-)\\d{2}:\\d{2}"); //yyyy-MM-dd'T'HH:mm:ssXXX
	}
	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		
		String text = aJCas.getDocumentText();
		
		processForDate(text, aJCas);
		processForTime(text, aJCas);
		processForDatetime(text, aJCas);
		
	}
	
	private void processForDate(String text, JCas aJCas) {
		TypeSystem ts = aJCas.getTypeSystem();
		Type type = ts.getType(dateAnnotationType);
		for (int i = 0; i < DATE_PATTERNS.size(); i++) {
			Pattern p = Pattern.compile(DATE_PATTERNS.get(i));
			Matcher matcher = p.matcher(text);
			int count = 0;
			while (matcher.find(count)) {
				String textFound = matcher.group();

				//create the DateAnnotation
				AnnotationFS ann = aJCas.getCas().createAnnotation(type, matcher.start(), matcher.end());

				// create the various features and add the value
				Feature nameFeature = type.getFeatureByBaseName(value);
				ann.setStringValue(nameFeature, textFound);

				// add the annotation to the index
				aJCas.addFsToIndexes(ann);

				count = matcher.end();

				//OLD way to create the annotation
				/*
				Date date = new Date(aJCas);
				date.setBegin(matcher.start());
				date.setEnd(matcher.end());
				date.setValue(textFound);
//				System.out.println(textFound + " annotated as date");
				date.addToIndexes();
				count = matcher.end();
				*/
			}
		}
	}
	
	private void processForTime(String text, JCas aJCas) {
		TypeSystem ts = aJCas.getTypeSystem();
		Type type = ts.getType(timeAnnotationType);
		for (int i = 0; i < TIME_PATTERNS.size(); i++) {
			Pattern p = Pattern.compile(TIME_PATTERNS.get(i));
			Matcher matcher = p.matcher(text);
			int count = 0;
			while (matcher.find(count)) {
				String textFound = matcher.group();

				//create the TimeAnnotation
				AnnotationFS ann = aJCas.getCas().createAnnotation(type, matcher.start(), matcher.end());

				// create the various features and add the value
				Feature nameFeature = type.getFeatureByBaseName(value);
				ann.setStringValue(nameFeature, textFound);

				// add the annotation to the index
				aJCas.addFsToIndexes(ann);

				count = matcher.end();

				//OLD way to create the annotation
				/*
				Time time = new Time(aJCas);
				time.setBegin(matcher.start());
				time.setEnd(matcher.end());
				time.setValue(textFound);
//				System.out.println(textFound + " annotated as time");
				time.addToIndexes();
				count = matcher.end();
				*/
			}
		}
	}
	
	private void processForDatetime(String text, JCas aJCas) {
		TypeSystem ts = aJCas.getTypeSystem();
		Type type = ts.getType(datetimeAnnotationType);
		for (int i = 0; i < DATETIME_PATTERNS.size(); i++) {
			Pattern p = Pattern.compile(DATETIME_PATTERNS.get(i));
			Matcher matcher = p.matcher(text);
			int count = 0;
			while (matcher.find(count)) {
				String textFound = matcher.group();

				//create the DatetimeAnnotation
				AnnotationFS ann = aJCas.getCas().createAnnotation(type, matcher.start(), matcher.end());

				// create the various features and add the value
				Feature nameFeature = type.getFeatureByBaseName(value);
				ann.setStringValue(nameFeature, textFound);

				// add the annotation to the index
				aJCas.addFsToIndexes(ann);

				count = matcher.end();

				//OLD way to create the annotation
				/*
				Datetime datetime = new Datetime(aJCas);
				datetime.setBegin(matcher.start());
				datetime.setEnd(matcher.end());
				datetime.setValue(textFound);
//				System.out.println(textFound + " annotated as datetime");
				datetime.addToIndexes();
				count = matcher.end();
				 */
			}
		}
	}
		
}
