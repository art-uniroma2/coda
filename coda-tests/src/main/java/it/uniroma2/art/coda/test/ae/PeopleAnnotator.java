package it.uniroma2.art.coda.test.ae;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.EmptyFSList;
import org.apache.uima.jcas.cas.NonEmptyFSList;
import org.apache.uima.jcas.cas.TOP;

public class PeopleAnnotator extends JCasAnnotator_ImplBase  {

	public static final String PERSON_ARRAY = "personArray";
	@ConfigurationParameter(name = PERSON_ARRAY)
	private String [] personArray;

	public static final String PERSON_ANNOTATION_TYPE_STRING = "personAnnotationType";
	@ConfigurationParameter(name = PERSON_ANNOTATION_TYPE_STRING)
	private String personAnnotationType;

	public static final String NAME_STRING = "name";
	@ConfigurationParameter(name = NAME_STRING)
	private String name;

	public static final String WORD_STRING = "word";
	@ConfigurationParameter(name = WORD_STRING)
	private String word;

	public static final String PEOPLE_ANNOTATION_TYPE_STRING = "peopleAnnotationType";
	@ConfigurationParameter(name = PEOPLE_ANNOTATION_TYPE_STRING)
	private String peopleAnnotationType;

	public static final String PERSON_LIST_FSLIST = "personList";
	@ConfigurationParameter(name = PERSON_LIST_FSLIST)
	private String personList;

	public static final String NUMBER_INTEGER = "number";
	@ConfigurationParameter(name = NUMBER_INTEGER)
	private String number;
	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		TypeSystem ts = aJCas.getTypeSystem();
		Type personType = ts.getType(personAnnotationType);
		Type peopleType = ts.getType(peopleAnnotationType);

		//List<Person> personJavaList = new ArrayList<Person>(); // OLD
		List<AnnotationFS> personJavaList = new ArrayList<>();

		Pattern patter = Pattern.compile("\\b([A-Z]?[a-z]+)\\b");
		
		
		String text = aJCas.getDocumentText();
		
		Matcher matcher = patter.matcher(text);
		
		if(personArray == null || personArray.length==0){
			// the array is null or has no element, so there is no reason to execute the matcher
			return;
		}
		
		int count = 0;
		while(matcher.find(count)){
			
			String textFound = matcher.group();
			for(int i=0; i<personArray.length; ++i){
				String currentPerson = personArray[i].toLowerCase();
				if(currentPerson.compareToIgnoreCase(textFound) == 0){


					//create a Person annotation, but do NOT add it to the UIMA index, just add it to personJavaList
					AnnotationFS ann = aJCas.getCas().createAnnotation(personType, matcher.start(), matcher.end());

					// create the various features and add the value
					if (!("anonymous".equalsIgnoreCase(textFound))) {
						Feature nameFeature = personType.getFeatureByBaseName(name);
						ann.setStringValue(nameFeature, textFound);
					}
					Feature wordFeature = personType.getFeatureByBaseName(word);
					ann.setStringValue(wordFeature, textFound);

					personJavaList.add(ann);

					//OLD way to create the annotation
					/*
					Person personAnnotation = new Person(aJCas);
					personAnnotation.setBegin(matcher.start());
					personAnnotation.setEnd(matcher.end());
					personAnnotation.setName(textFound);
					personAnnotation.setWord(textFound);
					//personAnnotation.addToIndexes();
					personJavaList.add(personAnnotation);
					*/

					break; // the person has been found, so skip to the next word
				}
			}
			
			count = matcher.end();
		}
		
		//now see if at least there is a PersonAnnoation (found in the previous loop)
		//People people = null; // OLD
		int peopleBegin = -1;
		int peopleEnd = -1;

		NonEmptyFSList finalFSList = new NonEmptyFSList(aJCas);
		finalFSList.setTail(new EmptyFSList(aJCas));
		
		for(int i=personJavaList.size()-1; i>=0; --i){
			AnnotationFS personAnn = personJavaList.get(i);
			//Person person = personJavaList.get(i);
			if(peopleBegin==-1  || peopleBegin>personAnn.getBegin() ){
				peopleBegin = personAnn.getBegin();
			}
			if(peopleEnd == -1 || peopleEnd<personAnn.getEnd()){
				peopleEnd = personAnn.getEnd();
			}
			
			finalFSList.setHead((TOP) personAnn);
			
			if(i>0){
				//not the last iteration
				NonEmptyFSList tempFSList = new NonEmptyFSList(aJCas);
				tempFSList.setTail(finalFSList);
				finalFSList = tempFSList;
				//finalFSList.setTail(finalFSList);
			}
			
		}
		if(personJavaList.size()>0){
			//since at least one PersonAnnotation was found, add the PeopleAnnotation to the UIMA index
			AnnotationFS peopleAnn = aJCas.getCas().createAnnotation(peopleType, peopleBegin, peopleEnd);
			Feature numberFeature = peopleType.getFeatureByBaseName(number);
			peopleAnn.setIntValue(numberFeature, personJavaList.size());
			Feature personListFeature = peopleType.getFeatureByBaseName(personList);
			peopleAnn.setFeatureValue(personListFeature, finalFSList);

			// add the annotation to the index
			aJCas.addFsToIndexes(peopleAnn);
		}

	}

}
