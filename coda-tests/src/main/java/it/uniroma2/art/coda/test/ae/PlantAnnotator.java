package it.uniroma2.art.coda.test.ae;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;

public class PlantAnnotator extends JCasAnnotator_ImplBase {

	public static final String PLANT_ARRAY = "plantArray";
	@ConfigurationParameter(name = PLANT_ARRAY)
	private String [] plantArray;

	public static final String PLANT_ANNOTATION_TYPE_STRING = "plantAnnotationType";
	@ConfigurationParameter(name = PLANT_ANNOTATION_TYPE_STRING)
	private String plantAnnotationType;

	public static final String NAME_STRING = "name";
	@ConfigurationParameter(name = NAME_STRING)
	private String name;

	public static final String WORD_STRING = "word";
	@ConfigurationParameter(name = WORD_STRING)
	private String word;
	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		TypeSystem ts = aJCas.getTypeSystem();
		Type type = ts.getType(plantAnnotationType);

		Pattern patter = Pattern.compile("\\b([A-Z]?[a-z]+)\\b");

		String text = aJCas.getDocumentText();
		
		Matcher matcher = patter.matcher(text);
		
		if(plantArray == null || plantArray.length==0){
			// the array is null or has no element, so there is no reason to execute the matcher
			return;
		}
		
		int count = 0;
		while(matcher.find(count)){
			
			String textFound = matcher.group();
			for(int i=0; i<plantArray.length; ++i){
				String currentPerson = plantArray[i].toLowerCase();
				if(currentPerson.compareToIgnoreCase(textFound) == 0){

					//create the PlantAnnotation
					AnnotationFS ann = aJCas.getCas().createAnnotation(type, matcher.start(), matcher.end());

					// create the various features and add the value
					Feature nameFeature = type.getFeatureByBaseName(name);
					ann.setStringValue(nameFeature, textFound);
					Feature wordFeature = type.getFeatureByBaseName(word);
					ann.setStringValue(wordFeature, textFound);

					// add the annotation to the index
					aJCas.addFsToIndexes(ann);

					//OLD way to create the annotation
					/*
					Plant plantAnnotation = new Plant(aJCas);
					plantAnnotation.setBegin(matcher.start());
					plantAnnotation.setEnd(matcher.end());
					plantAnnotation.setName(textFound);
					plantAnnotation.setWord(textFound);
					plantAnnotation.addToIndexes();
					 */

					break; // the plant has been found, so skip to the next word
				}
			}
			
			count = matcher.end();
		}

	}

}
