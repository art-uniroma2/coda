package it.uniroma2.art.coda.junittest;

import static org.junit.Assert.assertTrue;

import java.io.File;

import com.google.common.io.Closer;
import org.apache.commons.io.FileUtils;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.dkpro.core.io.text.TextReader;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.AssumptionViolatedException;
import org.junit.Test;

import it.uniroma2.art.coda.ae.CODAAnalysisEngine;
import it.uniroma2.art.coda.test.ae.PersonAnnotator;
import it.uniroma2.art.coda.util.OntoUtils;

public class AnalysisEngineTest {

	private static final String PLUGINS_DIRECTORY = "plugins";

	private static final File BASE_TEST_DIRECTORY = new File("target/ae-test");

	private static final String BASE_URI = "http://example.org/";
	private static final String OUTPUT_FILE_PATH = "output.ttl";

	@Test
	public void aeUimafitTest() throws Exception {
		if (!new File(PLUGINS_DIRECTORY).isDirectory()) {
			throw new AssumptionViolatedException("Missing folder containing the plugins that provide the converters: " + PLUGINS_DIRECTORY);
		}
		BASE_TEST_DIRECTORY.mkdirs();
		FileUtils.cleanDirectory(BASE_TEST_DIRECTORY);

		File pluginsDirectory = new File(BASE_TEST_DIRECTORY, PLUGINS_DIRECTORY);
		pluginsDirectory.mkdirs();

		File outputFile = new File(BASE_TEST_DIRECTORY, OUTPUT_FILE_PATH);

		String[] personArray = { "Andrea", "Armando", "Manuel", "Anonymous" };

		CollectionReaderDescription readerDesc = CollectionReaderFactory.createReaderDescription(
				TextReader.class, TextReader.PARAM_PATH,
				"src/test/resources/it/uniroma2/art/coda/junittest/ae/", TextReader.PARAM_PATTERNS,
				"[+]input_*.txt");

		AnalysisEngineDescription personAEDesc = AnalysisEngineFactory
				.createEngineDescription(PersonAnnotator.class, PersonAnnotator.PERSON_ARRAY, personArray);

		AnalysisEngineDescription codaAEDesc = AnalysisEngineFactory.createEngineDescription(
				CODAAnalysisEngine.class, CODAAnalysisEngine.PARAM_PLUGINS_ROOTS, pluginsDirectory,
				CODAAnalysisEngine.PARAM_BASE_URI, BASE_URI,
				CODAAnalysisEngine.PARAM_PROJECTION_DOCUMENT,
				"src/test/resources/it/uniroma2/art/coda/junittest/ae/testAE.pr",
				CODAAnalysisEngine.PARAM_OUTPUT_FILE, outputFile);

		SimplePipeline.runPipeline(readerDesc, personAEDesc, codaAEDesc);

		try (Closer closer = Closer.create()) {
			Repository actualRep = new SailRepository(new MemoryStore());
			closer.register(actualRep::shutDown);
			actualRep.init();

			Repository exptedRep = new SailRepository(new MemoryStore());
			closer.register(exptedRep::shutDown);
			actualRep.init();

			try (RepositoryConnection actualConn = actualRep.getConnection();
				 RepositoryConnection exptedConn = actualRep.getConnection()) {


				actualConn.add(outputFile, BASE_URI, RDFFormat.TURTLE);

				exptedConn.add(AnalysisEngineTest.class
								.getResource("/it/uniroma2/art/coda/junittest/ae/expectedOutput.ttl"),
						BASE_URI, RDFFormat.TURTLE);


				assertTrue(OntoUtils.areTwoOntologyEquals(actualConn, exptedConn));
			}
		}
		
	}

}