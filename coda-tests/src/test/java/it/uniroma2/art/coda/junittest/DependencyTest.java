package it.uniroma2.art.coda.junittest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;

import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.junit.Test;

/*this junit should test the following dependency:
 last (maxDist)
 next (maxDist)
 between (random)
 previous (maxDist, maxNumberOfAnn, random)
 following (maxDist, maxNumberOfAnn, random)
 */

public class DependencyTest extends AbstractTest {

	/*****************************************************
	 * TEST DEPENDENCY LAST
	 ******************************************************/

	// test the dependency type last
	@Test
	public void lastDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLast.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple)); // now the model should contain the triple
	}

	// test the dependency type last
	@Test
	public void lastDependency2() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLast2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple)); // now the model should contain the triple
	}

	// test the dependency type last with a too small value for maxDist
	@Test
	public void lastMax1Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLastMax1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple)); // the model should not contain the triple
	}

	// test the dependency type last with a big enough value for the maxDist
	@Test
	public void lastMax2Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLastMax2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple)); // the model should contain the triple
	}

	/*****************************************************
	 * TEST DEPENDENCY LASTONEOF
	 ******************************************************/

	// test the dependency type lastOneOf
	@Test
	public void lastOneOfDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLastOneOf.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and so does Manuel Fiorelli";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple

	}

	// test the dependency type lastOneOf with a too small value for maxDist
	@Test
	public void lastOneOfMax1Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLastOneOfMax1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and so does Manuel Fiorelli";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
	}

	// test the dependency type lastOneOf with a big enough value for the maxDist to take one annotation
	@Test
	public void lastOneOfMax2Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLastOneOfMax2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and so does Manuel Fiorelli";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type lastOneOf with a big enough value for the maxDist to take both annotation
	// (but just the last one will be used according the semantics of this dependency)
	@Test
	public void lastOneOfMax3Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlLastOneOfMax3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/ManuelFiorelli> <http://art.uniroma2.it#linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and so does Manuel Fiorelli";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	/*****************************************************
	 * TEST DEPENDENCY NEXT
	 ******************************************************/

	// test the dependency type next
	@Test
	public void nextDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlNext.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple)); // now the model should contain the triple
	}

	// test the dependency type next with a too small value for maxDist
	@Test
	public void nextMax1Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlNextMax1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple)); // the model should not contain the triple
	}

	// test the dependency type next with a big enough value for maxDist
	@Test
	public void nextMax2Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlNextMax2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple)); // the model should contain the triple
	}

	/*****************************************************
	 * TEST DEPENDENCY BETWEEN
	 ******************************************************/

	// test the dependency type between
	@Test
	public void betweenDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlBetween.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/linkedTo> ?firstWord";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/linkedTo> ?firstWord";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type between
	@Test
	public void betweenRandomDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlBetweenRandom.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/linkedTo> ?firstWord";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/linkedTo> ?firstWord";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Maybe Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		boolean rdfTriple1Present = executeAskQuery(rdfTriple1);
		boolean rdfTriple2Present = executeAskQuery(rdfTriple2);

		assertFalse(rdfTriple1Present && rdfTriple2Present); // the model should not contain both triples
		assertTrue(rdfTriple1Present || rdfTriple2Present); // the model should contain one of the triples
	}

	/*****************************************************
	 * TEST DEPENDENCY PREVIOUS
	 ******************************************************/

	// test the dependency type previous
	@Test
	public void previousDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlPrevious.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type previous with a too small value for maxDist
	@Test
	public void previousMax1Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlPreviousMax1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
	}

	// test the dependency type previous with a value for maxDist big enough to reach just one of the two
	// previous annotations
	@Test
	public void previousMax2Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlPreviousMax2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type previous with a value for number of annotation set to 1
	@Test
	public void previousNumber1Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlPreviousNumber1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type previous with a value for maxDist big enough to reach for the two
	// previous annotations, but with a value for number of annotation set to 1
	@Test
	public void previousNumber2Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlPreviousNumber2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type previous with a value for maxDist big enough to reach for just one
	// previous annotation, but with a value for number of annotation set to 2
	@Test
	public void previousNumber3Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlPreviousNumber3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type previous with the random value set to true
	@Test
	public void previousRandomDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlPreviousRandom.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		boolean rdfTriple1Present = executeAskQuery(rdfTriple1);
		boolean rdfTriple2Present = executeAskQuery(rdfTriple2);

		assertFalse(rdfTriple1Present && rdfTriple2Present); // the model should not contain both triples
		assertTrue(rdfTriple1Present || rdfTriple2Present); // the model should contain one of the two triples
	}

	/*****************************************************
	 * TEST DEPENDENCY FOLLOWING
	 ******************************************************/

	// test the dependency type following
	@Test
	public void followingDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlFollowing.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and in Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	// test the dependency type following with a too small value for maxDist
	@Test
	public void followingMax1Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlFollowingMax1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and in Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
	}

	// test the dependency type following with a value for maxDist big enough to reach just one of the two
	// following annotations
	@Test
	public void followingMax2Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlFollowingMax2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and in Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the dependency type following with a value for number of annotation set to 1
	@Test
	public void followingNumber1Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlFollowingNumber1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and in Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the dependency type following with a value for maxDist big enough to reach for the two
	// following annotations, but with a value for number of annotation set to 1
	@Test
	public void followingNumber2Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlFollowingNumber2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and in Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the dependency type following with a value for maxDist big enough to reach for just one
	// following annotation, but with a value for number of annotation set to 2
	@Test
	public void followingNumber3Dependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlFollowingNumber3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and in Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the dependency type following with the random value set to true
	@Test
	public void followingRandomDependency() throws  UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, 
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/dependency/pearlFollowingRandom.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it#livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome and in Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		boolean rdfTriple1Present = executeAskQuery(rdfTriple1);
		boolean rdfTriple2Present = executeAskQuery(rdfTriple2);

		assertFalse(rdfTriple1Present && rdfTriple2Present); // the model should not contain both triples
		assertTrue(rdfTriple1Present || rdfTriple2Present); // the model should contain one of the two triples
	}

}
