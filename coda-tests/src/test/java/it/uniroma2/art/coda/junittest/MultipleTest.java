package it.uniroma2.art.coda.junittest;

import java.nio.file.NotDirectoryException;

import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.DuplicateRuleIdException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;

public class MultipleTest extends AbstractTest {

	/**
	 * @throws NotDirectoryException ***************************************************
	 * TEST BASE CODA
	 * 
	 * @throws RDFModelNotSetException
	 * @throws UnassignableFeaturePathException 
	 * @throws ProjectionRuleModelNotSet 
	 * @throws  
	 ******************************************************/

	// test the base multiple file with isRecursive set to true
	@Test
	public void multitpleRight1TrueTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, NotDirectoryException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String projRuleDir = "src/test/resources/it/uniroma2/art/coda/junittest/multiple/right1";
		addProjectionRules(projRuleDir, true);

		ProjectionRulesModel prModel = codaCore.getProjRuleModel();
		
		ProjectionRule pr1 = prModel.getProjRuleFromId("city");
		ProjectionRule pr2 = prModel.getProjRuleFromId("person");
		assertTrue(pr1 != null);
		assertTrue(pr2 != null);
		
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}
	
	
	// test the base multiple file with isRecursive set to false
	@Test
	public void multitpleRight1FalseTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, NotDirectoryException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String projRuleDir = "src/test/resources/it/uniroma2/art/coda/junittest/multiple/right1";
		addProjectionRules(projRuleDir, false);
		
		ProjectionRulesModel prModel = codaCore.getProjRuleModel();
		
		ProjectionRule pr1 = prModel.getProjRuleFromId("city");
		ProjectionRule pr2 = prModel.getProjRuleFromId("person");
		assertTrue(pr1 != null);
		assertTrue(pr2 == null);
		
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}
	
	// test the base multiple file with isRecursive set to false
		@Test
		public void multitpleRight2FalseTest()
				throws UIMAException, PRParserException, ComponentProvisioningException,
				ConverterException, DependencyException, RDFModelNotSetException,
				ProjectionRuleModelNotSet, UnassignableFeaturePathException, NotDirectoryException, ValueNotPresentDueToConfigurationException {

			// set the projection rules
			String projRuleDir = "src/test/resources/it/uniroma2/art/coda/junittest/multiple/right2";
			addProjectionRules(projRuleDir, false);
			
			ProjectionRulesModel prModel = codaCore.getProjRuleModel();
			
			ProjectionRule pr1 = prModel.getProjRuleFromId("city");
			ProjectionRule pr2 = prModel.getProjRuleFromId("person");
			assertTrue(pr1 != null);
			assertTrue(pr2 != null);
			
			String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
					+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
			String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
					+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

			assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
			assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

			// create and execute the UIMA AE
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);

			// pass the jcas to CODA and execute to populate/enrich the ontology
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

			// System.out.println("************ PROJECTION RULE MODEL
			// ************\n"+codaCore.getProjRuleModel().getModelAsString());

			assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
			assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		}

	// test the multiple files generating an exception
	@Test
	public void multitpleWrongTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, NotDirectoryException  {

		// set the projection rules
		String projRuleDir = "src/test/resources/it/uniroma2/art/coda/junittest/multiple/wrong";
		try {
			addProjectionRules(projRuleDir, false);
			assertTrue(false);
		} catch (DuplicateRuleIdException e) {
			assertTrue(true);
		}
	}

	

}
