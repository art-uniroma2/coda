package it.uniroma2.art.coda.junittest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.junit.Test;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.structures.CODATriple;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;

public class VariousTest extends AbstractTest {



	// test the base PEARL structure
	@Test
	public void retrivePRFromTypeTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			ProjectionRuleModelNotSet, UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/various/pearlVariousRetrievePR.pr";
		setProjectionRule(pearlFileName);

		String uimaType1 = "it.uniroma2.art.coda.test.ae.type.City";
		String uimaType2 = "it.uniroma2.art.coda.test.ae.type.Person";
		String uimaType3 = "it.uniroma2.art.coda.test.ae.type.Animal";
		
		
		ProjectionRulesModel prModel = codaCore.getProjRuleModel();

		
		Collection<ProjectionRule> prList1 = prModel.getStandardProjectionRulesByTypeName(uimaType1);
		Collection<ProjectionRule> prList2 = prModel.getStandardProjectionRulesByTypeName(uimaType2);
		Collection<ProjectionRule> prList3 = prModel.getStandardProjectionRulesByTypeName(uimaType3);
		
		
		assertTrue(prList1.size()==3);
		assertTrue(prList2.size()==1);
		assertTrue(prList3.size()==0);
		
	}

	@Test
	public void applyRulesToSpecificAnnotation()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {
		
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/various/pearlApplyRulesToSpecificAnn.pr";
		setProjectionRule(pearlFileName);

		String uimaType = "it.uniroma2.art.coda.test.ae.type.Person";
		
		String text = "Andrea lives in Rome";
		
		ProjectionRulesModel prModel = codaCore.getProjRuleModel();
		
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Male>";
		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Female>";

		
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		
		Collection<ProjectionRule> prList = prModel.getStandardProjectionRulesByTypeName(uimaType);
		
		JCas jcas = createAndExecuteAE(text);
		
		//get the annotation of the desired type
		Annotation desiredAnn = null;
		for(Annotation ann : jcas.getAnnotationIndex()){
			if(ann.getType().getName().equals(uimaType)){
				//found the right annotation
				desiredAnn = ann;
			}
		}
		if (desiredAnn==null) {
			//this should never happen, so fail this test
			assertTrue(false);
		}
		
		List<SuggOntologyCoda> suggOntCodaList = new ArrayList<SuggOntologyCoda>();
		
		for(ProjectionRule pr : prList) {
			SuggOntologyCoda suggOntCoda = new SuggOntologyCoda(desiredAnn, false);
			codaCore.executeProjectionRule(pr, desiredAnn, suggOntCoda, false, false);
			suggOntCodaList.add(suggOntCoda);
		}
		
		//now iterate over all the suggestions and take and add all the suggested triples
		for(SuggOntologyCoda suggOntCoda : suggOntCodaList) {
			List<CODATriple> tripleList = suggOntCoda.getAllInsertARTTriple();
			for(CODATriple codaTriple : tripleList){
				connection.add(SimpleValueFactory.getInstance().createStatement(codaTriple.getSubject(),
						codaTriple.getPredicate(), codaTriple.getObject()));
			}
		}
		
		assertTrue(executeAskQuery(rdfTriple1)); 
		assertTrue(executeAskQuery(rdfTriple2)); 
		assertFalse(executeAskQuery(rdfTriple3)); 
		
	}
	
}
