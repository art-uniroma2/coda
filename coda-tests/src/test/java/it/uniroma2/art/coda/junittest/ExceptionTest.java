package it.uniroma2.art.coda.junittest;

import static it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription.SECTION_GRAPH;
import static it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription.SECTION_NODES;
import static org.junit.Assert.assertTrue;

import it.uniroma2.art.coda.exception.PlaceholderValueWrongTypeForParamInAnnotationException;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.AnnotationNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.AnnotationTargetNotCompatibleException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceNotDoubleNumException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceValueNotAcceptableException;
import it.uniroma2.art.coda.exception.parserexception.MissingMandatoryParamInAnnotationException;
import it.uniroma2.art.coda.exception.parserexception.NoRuleInFileException;
import it.uniroma2.art.coda.exception.parserexception.ParamInAnnotationNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.TypeOfDefaultParamInAnnotationWrongTypeException;
import it.uniroma2.art.coda.exception.parserexception.TypeOfParamInAnnotationWrongTypeException;
import it.uniroma2.art.coda.exception.parserexception.UnsupportedTypeInParamDefinitionException;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.junit.Ignore;
import org.junit.Test;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.DeleteAndGraphSectionException;
import it.uniroma2.art.coda.exception.parserexception.DuplicateRuleIdException;
import it.uniroma2.art.coda.exception.parserexception.NodeNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.PRParamToReplaceException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.exception.parserexception.PRSyntaxException;
import it.uniroma2.art.coda.exception.parserexception.PrefixNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.RepeteadAssignmentException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;

import java.util.List;

public class ExceptionTest extends AbstractTest {

	/*****************************************************
	 * TEST BASE CODA
	 * 
	 * @throws RDFModelNotSetException
	 * @throws NodeNotDefinedException
	 * @throws PrefixNotDefinedException
	 ******************************************************/

	// test the exception PRSyntaxException
	@Test
	public void parlFileNotWorkingExceptionTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/pearlNotWorkingException.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (PRSyntaxException e) {
			assertTrue(true);
			String text = "line 16: no viable alternative at input '}'";
			assertTrue(e.getErrorAsString().contains(text));
		}
	}

	
	
	// test the exception RepeteadAssignmentException, a placeholder is defined two times
	@Test
	public void repeteadAssignmentExceptionTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/pearlRepeteadAssignmentException.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
			// should not arrive at this point
		} catch (RepeteadAssignmentException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getPlcName().equals("cityName"));
		}
	}

	@Test
	public void nodeNotDefinedExceptionTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/nodeNotDefinedException.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (NodeNotDefinedException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getPlcName().equals("cityName2"));
		}
	}

	@Test
	public void prefixNotDefinedExceptionTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/prefixNotDefinedException.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (PrefixNotDefinedException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getPrefixName().equals("fake"));
		}
	}

	@Test
	public void prefixNotDefinedExceptionTest2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/prefixNotDefinedException2.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (PrefixNotDefinedException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("firstRegex"));
			assertTrue(e.getPrefixName().equals("fake"));
		}
	}

	@Test
	public void unassignableFeaturePathExceptionTest()
			throws UIMAException, PRParserException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/unassignableFeaturePathExceptionSingle.pr";
		setProjectionRule(pearlFileName);

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch (UnassignableFeaturePathException e) {
			assertTrue(true);
			assertTrue(e.getPlcName().equals("error"));
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getFeatPath().equals("name2"));
		}
	}

	@Test
	public void unassignableFeaturePathExceptionTest2()
			throws UIMAException, PRParserException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/unassignableFeaturePathExceptionMultiple1.pr";
		setProjectionRule(pearlFileName);

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch (UnassignableFeaturePathException e) {
			System.out.println(e.getMessage());
			assertTrue(true);
			assertTrue(e.getUimaType().equals("it.uniroma2.art.coda.test.ae.type.ComplexPerson"));
			assertTrue(e.getPlcName().equals("error"));
			assertTrue(e.getRuleId().equals("completeName"));
			assertTrue(e.getFeatPath().equals("name/fake"));
		}
	}

	@Test
	public void unassignableFeaturePathExceptionTest3()
			throws UIMAException, PRParserException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/unassignableFeaturePathExceptionMultiple2.pr";
		setProjectionRule(pearlFileName);

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch (UnassignableFeaturePathException e) {
			assertTrue(true);
			assertTrue(e.getUimaType().equals("it.uniroma2.art.coda.test.ae.type.ComplexPerson"));
			assertTrue(e.getPlcName().equals("error"));
			assertTrue(e.getRuleId().equals("completeName"));
			assertTrue(e.getFeatPath().equals("name/firstName/fake"));
		}
	}

	@Test
	public void unassignableFeaturePathExceptionListTest1()
			throws UIMAException, PRParserException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/unassignableFeaturePathExceptionList1.pr";
		setProjectionRule(pearlFileName);

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

			assertTrue(true);
		} catch (UnassignableFeaturePathException e) {
			assertTrue(false);
		}
	}

	@Test
	@Ignore // this test is disables since, at the moment, there is no way to check if a feature path,
	// contaning a list, is correct or not
	public void unassignableFeaturePathExceptionListTest2()
			throws UIMAException, PRParserException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/unassignableFeaturePathExceptionList2.pr";
		setProjectionRule(pearlFileName);

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

			assertTrue(true);
		} catch (UnassignableFeaturePathException e) {
			assertTrue(false);
			assertTrue(e.getUimaType().equals("it.uniroma2.art.coda.test.ae.type.People"));
			assertTrue(e.getPlcName().equals("error"));
			assertTrue(e.getRuleId().equals("people"));
			assertTrue(e.getFeatPath().equals("personList/name2"));
		}
	}

	// test the exception DuplicateRuleIdException, two simple rule with the same id
	@Test
	public void duplicateRuleIdExceptionTest1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/DuplicateRuleIdException1.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (DuplicateRuleIdException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
		}
	}

	// test the exception DuplicateRuleIdException, a simple rule and a for regexRule with the same id
	@Test
	public void duplicateRuleIdExceptionTest2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/DuplicateRuleIdException2.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (DuplicateRuleIdException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city1"));
		}
	}

	// test the exception DuplicateRuleIdException, a simple rule and a for regex with the same id
	@Test
	public void duplicateRuleIdExceptionTest3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/DuplicateRuleIdException3.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (DuplicateRuleIdException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("errorId"));
		}
	}

	// test the exception DuplicateRuleIdException, two regexes with the same id
	@Test
	public void duplicateRuleIdExceptionTest4() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/DuplicateRuleIdException4.pr";
		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (DuplicateRuleIdException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("errorId"));
		}
	}

	// test the exception DeleteAndGraphSectionException, in a rule there are both the graph and the delete
	// section
	@Test
	public void deleteAndGraphSectionExceptionTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/deleteAndGraphSectionException.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (DeleteAndGraphSectionException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("errorId"));
		}
	}
	
	// test the use of a placeholder in an annotation, but such placeholder was not defined in the nodese section,
	// so a NodeNotDefinedException should be thrown
	@Test
	public void baseConfidencePlchldAnnotationNotDefinedTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/pearlConfidencePlchldAnnotationNotDef.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (NodeNotDefinedException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("errorId"));
		}
	}
	
	@Test
	public void sheet2RDFPlaceholderTest1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/sheet2rdf_1.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (PRParamToReplaceException e) {
			assertTrue(true);
			assertTrue(e.getLine()==18);
		}
	}
	
	@Test
	public void sheet2RDFPlaceholderTest2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/sheet2rdf_2.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (PRParamToReplaceException e) {
			assertTrue(true);
			assertTrue(e.getLine()==18);
		}
	}
	
	@Test
	public void sheet2RDFPlaceholderTest3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/sheet2rdf_3.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (PRParamToReplaceException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void sheet2RDFPlaceholderTest4() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/sheet2rdf_4.pr";

		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch (PRParamToReplaceException e) {
			assertTrue(true);
			assertTrue(e.getLine()==22);
		}
	}

	@Test
	public void annotationUsedNotDefinedTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/annotationUsedNotDefined.pr";
		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch(AnnotationNotDefinedException e) {
			assertTrue(true);
			assertTrue(e.getAnnName().equals("isClassWrong"));
			assertTrue(e.getRuleId().equals("city"));
		}
	}

	@Test
	public void noRuleInfileTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/noRulesInFileException.pr";
		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch(NoRuleInFileException e) {
			assertTrue(true);
		}
	}

	@Test
	public void noRuleInfileOkTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/noRulesInFileException.pr";
		try {
			//use the addProjectionRule and not the setProjectionRule, since with the add it is possible to not have any rule
			addProjectionRule(pearlFileName);
			assertTrue(true);
		} catch(NoRuleInFileException e) {
			// should not arrive at this point
			assertTrue(false);
		}
	}

	@Test
	public void paramInAnnotationUsedNotDefinedTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/paramInAnnotationUsedNotDefined.pr";
		try {
			setProjectionRule(pearlFileName);
			assertTrue(false);
		} catch(ParamInAnnotationNotDefinedException e) {
			// should not arrive at this point
			assertTrue(true);
			assertTrue(e.getParamName().equals("max2"));
			assertTrue(e.getAnnName().equals("isClass"));
			assertTrue(e.getRuleId().equals("city"));
		}
	}

	@Test
	public void paramInAnnotationWithUnsupportedTypeTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/paramInAnnotationWithUnsupportedType.pr";
		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch(UnsupportedTypeInParamDefinitionException e) {
			assertTrue(true);
			assertTrue(e.getAnnName().equals("isClass"));
			assertTrue(e.getParamName().equals("max"));
			assertTrue(e.getType().equals("int2"));
		}
	}

	@Test
	public void paramInAnnotationWithWrongTypeTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/paramInAnnotationWithWrongType.pr";
		try {
			setProjectionRule(pearlFileName);
			// should not arrive at this point
			assertTrue(false);
		} catch(TypeOfParamInAnnotationWrongTypeException e) {
			assertTrue(true);
			assertTrue(e.getParamName().equals("max"));
			assertTrue(e.getAnnName().equals("isClass"));
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getTypeExpected().equals("int"));
		}
	}

	@Test
	public void paramInAnnotationWithWrongType2Test()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/paramInAnnotationWithWrongType2.pr";
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			setProjectionRule(pearlFileName);
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch (PlaceholderValueWrongTypeForParamInAnnotationException e){
			assertTrue(true);
			assertTrue(e.getParamName().equals("max"));
			assertTrue(e.getAnnName().equals("isClass"));
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getTypeExpected().equals("int"));
			assertTrue(e.getPlchName().equals("$nameLit"));
			assertTrue(e.getPlchValue().equals("Rome"));

		}
	}

	@Test
	public void confidenceException1Test()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/confidenceAnnotationException1.pr";
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			setProjectionRule(pearlFileName);
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch(ConfidenceNotDoubleNumException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getValue().contains("error"));
		} /*catch(TypeOfParamInAnnotationWrongTypeException e) {
			System.out.println(e.getErrorAsString());
		}*/
	}

	@Test
	public void confidenceException2Test()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/confidenceAnnotationException2.pr";
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			setProjectionRule(pearlFileName);
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch(ConfidenceNotDoubleNumException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
			System.out.println("e.getValue() = "+e.getValue());
			assertTrue(e.getValue().contains("Rome"));
		}
	}

	@Test
	public void confidenceException3Test()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet, ValueNotPresentDueToConfigurationException,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/confidenceAnnotationException3.pr";
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			setProjectionRule(pearlFileName);
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch(ConfidenceValueNotAcceptableException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getValue() == 2.5);
		}
	}

	@Test
	public void missingMandatoryParamInAnnotationTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/missingMandatoryParamInAnnotation.pr";
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			setProjectionRule(pearlFileName);
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);
			// pass the jcas to CODA and execute to populate/enrich the ontology
			//List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch(MissingMandatoryParamInAnnotationException e) {
			assertTrue(true);
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getParamName().equals("text"));
			assertTrue(e.getAnnName().equals("isClass"));
		}
	}

	@Test
	public void defaultValueInParamNotCompliantWithTypeTest()throws PRParserException, UIMAException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
			UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/defaultValueInParamNotCompliantWithType.pr";
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			setProjectionRule(pearlFileName);
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);
			// pass the jcas to CODA and execute to populate/enrich the ontology
			//List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			// should not arrive at this point
			assertTrue(false);
		} catch(TypeOfDefaultParamInAnnotationWrongTypeException e) {
			assertTrue(true);
			assertTrue(e.getParamName().equals("max"));
			assertTrue(e.getAnnName().equals("isClass"));
		}
	}

	//copied form AnnotationTest.java
	@Test
	public void nodeAnnotationTestWrongTarget() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/metaAnnotationTestWrongTarget.pr";
		try {
			setProjectionRule(pearlFileName);
			assertTrue(false);
		} catch (AnnotationTargetNotCompatibleException e){
			assertTrue(e.getAnnName().equals("isClass"));
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getTargetNotCompatible().equals("object"));
		}
	}

	@Test
	public void annotationTestWrongTarget1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/annotationTestWrongTarget1.pr";
		try {
			setProjectionRule(pearlFileName);
			assertTrue(false);
		} catch (AnnotationTargetNotCompatibleException e){
			assertTrue(e.getAnnName().equals("isClass"));
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getTargetNotCompatible().equals(SECTION_NODES));
		}
	}

	@Test
	public void annotationTestWrongTarget2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/exception/annotationTestWrongTarget2.pr";
		try {
			setProjectionRule(pearlFileName);
			assertTrue(false);
		} catch (AnnotationTargetNotCompatibleException e){
			assertTrue(e.getAnnName().equals("plcAnn"));
			assertTrue(e.getRuleId().equals("city"));
			assertTrue(e.getTargetNotCompatible().equals(SECTION_GRAPH));
		}
	}


}
