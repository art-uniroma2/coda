package it.uniroma2.art.coda.junittest;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.exception.parserexception.PropPathNotAllowedException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.Test;

import static org.junit.Assert.*;

public class WhereTest extends AbstractTest {


	// test the where mechanism (and founding the existing triple)
	@Test
	public void baseWhereFound()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWhere1.pr";
		setProjectionRule(pearlFileName);

		String pastaURI = "http://art.uniroma2.it/Pasta";
		String foodURI = "http://art.uniroma2.it/Food";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURI
				+ ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository
		
		IRI subj = SimpleValueFactory.getInstance().createIRI(pastaURI);
		IRI obj = SimpleValueFactory.getInstance().createIRI(foodURI);
		
		connection.add(SimpleValueFactory.getInstance().createStatement(subj, RDF.TYPE, obj));

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the where mechanism when using a Literal (with lang) (and founding the existing triple)
	@Test
	public void baseWhereLiteralLangFound()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWhereLiteral1.pr";
		setProjectionRule(pearlFileName);


		String personA = "http://art.uniroma2.it/PersonA";
		String nameProp = "http://art.uniroma2.it/name";
		String andreaLitValue = "Andrea";
		String andreaLitLang = "it";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/knows> <"+personA+">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository
		IRI subj = SimpleValueFactory.getInstance().createIRI(personA);
		IRI pred = SimpleValueFactory.getInstance().createIRI(nameProp);
		Literal objLit = SimpleValueFactory.getInstance().createLiteral(andreaLitValue, andreaLitLang);
		connection.add(SimpleValueFactory.getInstance().createStatement(subj, pred, objLit));

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the where mechanism when using a Literal (with datatype) (and founding the existing triple)
	@Test
	public void baseWhereLiteralDatatypeFound()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWhereDatatype1.pr";
		setProjectionRule(pearlFileName);


		String personA = "http://art.uniroma2.it/PersonA";
		String nameProp = "http://art.uniroma2.it/name";
		String andreaLitValue = "Andrea";
		String andreaLitDatatype = "http://www.w3.org/2001/XMLSchema#string";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/knows> <"+personA+">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository
		IRI subj = SimpleValueFactory.getInstance().createIRI(personA);
		IRI pred = SimpleValueFactory.getInstance().createIRI(nameProp);
		Literal objLit = SimpleValueFactory.getInstance().createLiteral(andreaLitValue, SimpleValueFactory.getInstance().createIRI(andreaLitDatatype));
		connection.add(SimpleValueFactory.getInstance().createStatement(subj, pred, objLit));

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the where mechanism (and NOT founding the existing triple)
	@Test
	public void baseWhereNotFound()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWhere1.pr";
		setProjectionRule(pearlFileName);

		String pastaURI = "http://art.uniroma2.it/Pasta";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURI
				+ ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model should contain the triple
	}

	// test the where mechanism (and founding the existing triple)
	@Test
	public void baseWhereWithPlchFound()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePlch1.pr";
		setProjectionRule(pearlFileName);

		String pastaURI = "http://art.uniroma2.it/Pasta";
		String foodURI = "http://art.uniroma2.it/Food";
		String livesInURI = "http://art.uniroma2.it/livesIn";
		String romeURI = "http://art.uniroma2.it/Rome";
		String andreaURI = "http://art.uniroma2.it/Andrea";
		String rdfTriple1 = "<"+andreaURI+"> " + "<http://art.uniroma2.it/eats> <" + pastaURI
				+ ">";
		String rdfTriple2 = "<"+pastaURI+"> " + "<http://art.uniroma2.it/isIn> <" + romeURI
				+ ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI subj1 = SimpleValueFactory.getInstance().createIRI(pastaURI);
		IRI obj1 = SimpleValueFactory.getInstance().createIRI(foodURI);

		IRI subj2 = SimpleValueFactory.getInstance().createIRI(andreaURI);
		IRI pred2 = SimpleValueFactory.getInstance().createIRI(livesInURI);
		IRI obj2 = SimpleValueFactory.getInstance().createIRI(romeURI);

		connection.add(SimpleValueFactory.getInstance().createStatement(subj1, RDF.TYPE, obj1));
		connection.add(SimpleValueFactory.getInstance().createStatement(subj2, pred2, obj2));

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple

	}

	// test the where mechanism (and founding the existing triple)
	@Test
	public void baseWhereFound2SameId()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWhere2.pr";
		setProjectionRule(pearlFileName);

		String personURI = "http://art.uniroma2.it/Person";
		String manuelURI = "http://art.uniroma2.it/Manuel";
		String andreaURI = "http://art.uniroma2.it/Andrea";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/knows> <"
				+ manuelURI + ">";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/knows> <"
				+ andreaURI + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository
		IRI subj = SimpleValueFactory.getInstance().createIRI(manuelURI);
		IRI obj = SimpleValueFactory.getInstance().createIRI(personURI);
		connection.add(SimpleValueFactory.getInstance().createStatement(subj, RDF.TYPE, obj));

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model not should contain the triple
	}

	// test the where mechanism (and NOT founding the existing triple)
	@Test
	public void baseWhereNotFound2SameId()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWhere2.pr";
		setProjectionRule(pearlFileName);

		String manuelURI = "http://art.uniroma2.it/Manuel";
		String andreaURI = "http://art.uniroma2.it/Andrea";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/knows> <"
				+ manuelURI + ">";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/knows> <"
				+ andreaURI + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model not should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
	}

	@Test
	public void baseWherePropPath1Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPath1.pr";
		setProjectionRule(pearlFileName);


		String prop1UriString = "http://art.uniroma2.it/prop1";
		String prop2UriString = "http://art.uniroma2.it/prop2";
		String temp1NodeUriString = "http://art.uniroma2.it/temp1";
		String pastaURIString = "http://art.uniroma2.it/Pasta";


		String foodURI = "http://art.uniroma2.it/Food";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURIString + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI prop1Uri = SimpleValueFactory.getInstance().createIRI(prop1UriString);
		IRI prop2Uri = SimpleValueFactory.getInstance().createIRI(prop2UriString);
		IRI temp1NodeUri = SimpleValueFactory.getInstance().createIRI(temp1NodeUriString);
		IRI pastaUri = SimpleValueFactory.getInstance().createIRI(pastaURIString);
		Literal pastaItLiteral = SimpleValueFactory.getInstance().createLiteral("pasta", "it");

		connection.add(SimpleValueFactory.getInstance().createStatement(pastaUri, prop1Uri, temp1NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp1NodeUri, prop2Uri, pastaItLiteral));


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		// (because in WHERE the my:prop1/my:prop2 was used)
	}

	@Test
	public void baseWherePropPath2Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPath2.pr";
		setProjectionRule(pearlFileName);


		String prop1UriString = "http://art.uniroma2.it/prop1";
		String prop2UriString = "http://art.uniroma2.it/prop2";
		String temp1NodeUriString = "http://art.uniroma2.it/temp1";
		String pastaURIString = "http://art.uniroma2.it/Pasta";


		String foodURI = "http://art.uniroma2.it/Food";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURIString + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI prop1Uri = SimpleValueFactory.getInstance().createIRI(prop1UriString);
		IRI prop2Uri = SimpleValueFactory.getInstance().createIRI(prop2UriString);
		IRI temp1NodeUri = SimpleValueFactory.getInstance().createIRI(temp1NodeUriString);
		IRI pastaUri = SimpleValueFactory.getInstance().createIRI(pastaURIString);
		Literal pastaItLiteral = SimpleValueFactory.getInstance().createLiteral("pasta", "it");

		connection.add(SimpleValueFactory.getInstance().createStatement(pastaUri, prop1Uri, temp1NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp1NodeUri, prop2Uri, pastaItLiteral));


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		// (because in WHERE the my:prop3|my:prop1/my:prop2+ was used)
	}

	@Test
	public void baseWherePropPath3Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPath3.pr";
		setProjectionRule(pearlFileName);


		String prop1UriString = "http://art.uniroma2.it/prop1";
		String prop2UriString = "http://art.uniroma2.it/prop2";
		String temp1NodeUriString = "http://art.uniroma2.it/temp1";
		String temp2NodeUriString = "http://art.uniroma2.it/temp2";
		String temp3NodeUriString = "http://art.uniroma2.it/temp3";
		String pastaURIString = "http://art.uniroma2.it/Pasta";


		String foodURI = "http://art.uniroma2.it/Food";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURIString + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI prop1Uri = SimpleValueFactory.getInstance().createIRI(prop1UriString);
		IRI prop2Uri = SimpleValueFactory.getInstance().createIRI(prop2UriString);
		IRI temp1NodeUri = SimpleValueFactory.getInstance().createIRI(temp1NodeUriString);
		IRI temp2NodeUri = SimpleValueFactory.getInstance().createIRI(temp2NodeUriString);
		IRI temp3NodeUri = SimpleValueFactory.getInstance().createIRI(temp3NodeUriString);
		IRI pastaUri = SimpleValueFactory.getInstance().createIRI(pastaURIString);
		Literal pastaItLiteral = SimpleValueFactory.getInstance().createLiteral("pasta", "it");

		connection.add(SimpleValueFactory.getInstance().createStatement(pastaUri, prop1Uri, temp1NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp1NodeUri, prop2Uri, temp2NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp2NodeUri, prop2Uri, temp3NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp3NodeUri, prop2Uri, pastaItLiteral));


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // the model still does not contain the triple
		// (because in WHERE the my:prop1/my:prop2 was used)
	}

	@Test
	public void baseWherePropPath4Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPath4.pr";
		setProjectionRule(pearlFileName);


		String prop1UriString = "http://art.uniroma2.it/prop1";
		String prop2UriString = "http://art.uniroma2.it/prop2";
		String temp1NodeUriString = "http://art.uniroma2.it/temp1";
		String temp2NodeUriString = "http://art.uniroma2.it/temp2";
		String temp3NodeUriString = "http://art.uniroma2.it/temp3";
		String pastaURIString = "http://art.uniroma2.it/Pasta";


		String foodURI = "http://art.uniroma2.it/Food";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURIString + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI prop1Uri = SimpleValueFactory.getInstance().createIRI(prop1UriString);
		IRI prop2Uri = SimpleValueFactory.getInstance().createIRI(prop2UriString);
		IRI temp1NodeUri = SimpleValueFactory.getInstance().createIRI(temp1NodeUriString);
		IRI temp2NodeUri = SimpleValueFactory.getInstance().createIRI(temp2NodeUriString);
		IRI temp3NodeUri = SimpleValueFactory.getInstance().createIRI(temp3NodeUriString);
		IRI pastaUri = SimpleValueFactory.getInstance().createIRI(pastaURIString);
		Literal pastaItLiteral = SimpleValueFactory.getInstance().createLiteral("pasta", "it");

		connection.add(SimpleValueFactory.getInstance().createStatement(pastaUri, prop1Uri, temp1NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp1NodeUri, prop2Uri, temp2NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp2NodeUri, prop2Uri, temp3NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp3NodeUri, prop2Uri, pastaItLiteral));


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model still should contain the triple
		// (because in WHERE the my:prop1/my:prop2+ was used)
	}

	@Test
	public void baseWherePropPath5Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPath5.pr";
		setProjectionRule(pearlFileName);


		String prop1UriString = "http://art.uniroma2.it/prop1";
		String prop2UriString = "http://art.uniroma2.it/prop2";
		String temp1NodeUriString = "http://art.uniroma2.it/temp1";
		String pastaURIString = "http://art.uniroma2.it/Pasta";
		String foodURIString = "http://art.uniroma2.it/Food";

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURIString + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI prop1Uri = SimpleValueFactory.getInstance().createIRI(prop1UriString);
		IRI prop2Uri = SimpleValueFactory.getInstance().createIRI(prop2UriString);
		IRI temp1NodeUri = SimpleValueFactory.getInstance().createIRI(temp1NodeUriString);
		IRI pastaUri = SimpleValueFactory.getInstance().createIRI(pastaURIString);
		IRI foodURI = SimpleValueFactory.getInstance().createIRI(foodURIString);

		connection.add(SimpleValueFactory.getInstance().createStatement(pastaUri, prop1Uri, temp1NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp1NodeUri, prop2Uri, foodURI));


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		// (because in WHERE the ^my:prop2/^my:prop1  was used)
	}

	@Test
	public void baseWherePropPath6Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPath6.pr";
		setProjectionRule(pearlFileName);


		String prop1UriString = "http://art.uniroma2.it/prop1";
		String prop2UriString = "http://art.uniroma2.it/prop2";
		String temp1NodeUriString = "http://art.uniroma2.it/temp1";
		String pastaURIString = "http://art.uniroma2.it/Pasta";


		String foodURI = "http://art.uniroma2.it/Food";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pastaURIString + ">";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI prop1Uri = SimpleValueFactory.getInstance().createIRI(prop1UriString);
		IRI prop2Uri = SimpleValueFactory.getInstance().createIRI(prop2UriString);
		IRI temp1NodeUri = SimpleValueFactory.getInstance().createIRI(temp1NodeUriString);
		IRI pastaUri = SimpleValueFactory.getInstance().createIRI(pastaURIString);
		Literal pastaItLiteral = SimpleValueFactory.getInstance().createLiteral("pasta", "it");

		connection.add(SimpleValueFactory.getInstance().createStatement(pastaUri, prop1Uri, temp1NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp1NodeUri, prop2Uri, pastaItLiteral));


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		// (because in WHERE the !my:prop3/my:prop2 was used)
	}

	@Test
	public void baseWherePropPath7Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPath6.pr";
		setProjectionRule(pearlFileName);


		String prop1UriString = "http://art.uniroma2.it/prop1";
		String prop2UriString = "http://art.uniroma2.it/prop2";
		String temp1NodeUriString = "http://art.uniroma2.it/temp1";
		String temp2NodeUriString = "http://art.uniroma2.it/temp2";
		String pasta1URIString = "http://art.uniroma2.it/Pasta1";
		String pasta2URIString = "http://art.uniroma2.it/Pasta2";


		String foodURI = "http://art.uniroma2.it/Food";
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pasta1URIString + ">";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/eats> <" + pasta2URIString + ">";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// add manually a triple in the repository

		IRI prop1Uri = SimpleValueFactory.getInstance().createIRI(prop1UriString);
		IRI prop2Uri = SimpleValueFactory.getInstance().createIRI(prop2UriString);
		IRI temp1NodeUri = SimpleValueFactory.getInstance().createIRI(temp1NodeUriString);
		IRI temp2NodeUri = SimpleValueFactory.getInstance().createIRI(temp2NodeUriString);
		IRI pasta1Uri = SimpleValueFactory.getInstance().createIRI(pasta1URIString);
		IRI pasta2Uri = SimpleValueFactory.getInstance().createIRI(pasta2URIString);
		Literal pastaItLiteral = SimpleValueFactory.getInstance().createLiteral("pasta", "it");

		connection.add(SimpleValueFactory.getInstance().createStatement(pasta1Uri, prop1Uri, temp1NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(pasta2Uri, prop2Uri, temp2NodeUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp1NodeUri, prop2Uri, pastaItLiteral));
		connection.add(SimpleValueFactory.getInstance().createStatement(temp2NodeUri, prop2Uri, pastaItLiteral));


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should contain the triple
		// (because in WHERE the !my:prop3/my:prop2 was used)
	}


	@Test
	public void baseWherePropPathExceptionTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWherePropPathException.pr";
		try {
			setProjectionRule(pearlFileName);
			fail();
		} catch (PRParserException e) {
			assertTrue(e.getErrorAsString().contains("rule person"));
			assertTrue(e instanceof PropPathNotAllowedException);

		}


	}
}
