package it.uniroma2.art.coda.junittest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.junit.Test;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;

public class InsertDelete extends AbstractTest {

	/*****************************************************
	 * TEST BASE CODA
	 * 
	 * @throws RDFModelNotSetException
	 * @throws UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException
	 * @throws ProjectionRuleModelNotSet 
	 * @throws  
	 ******************************************************/

	// test the base PEARL structure
	@Test
	public void baseTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/insertDelete/insDelBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/TempPerson>";
		String rdfTriple4 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/TempCity>";

		addTriple(rdfTriple3);
		addTriple(rdfTriple4);
				
		
		assertFalse(executeAskQuery(rdfTriple1)); 
		assertFalse(executeAskQuery(rdfTriple2)); 
		assertTrue(executeAskQuery(rdfTriple3));
		assertTrue(executeAskQuery(rdfTriple4));
		
		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); 
		assertTrue(executeAskQuery(rdfTriple2)); 
		assertFalse(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4));
	}
	
	@Test
	public void baseTest2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/insertDelete/insDelBase2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		String rdfTriple3 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/TempPerson>";
		String rdfTriple4 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/TempCity>";

		addTriple(rdfTriple3);
		addTriple(rdfTriple4);
				
		
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3));
		assertTrue(executeAskQuery(rdfTriple4));
		
		// create and execute the UIMA AE
		String text = "Anonymous lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertFalse(executeAskQuery(rdfTriple1)); 
		assertTrue(executeAskQuery(rdfTriple2)); 
		assertTrue (executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4));
	}
	
	@Test
	public void insDelOptionalTest1()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/insertDelete/insDelOptional1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		String rdfTriple3 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/TempPerson>";
		String rdfTriple4 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/TempCity>";

		addTriple(rdfTriple3);
		addTriple(rdfTriple4);
				
		
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3));
		assertTrue(executeAskQuery(rdfTriple4));
		
		// create and execute the UIMA AE
		String text = "Anonymous lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); 
		assertTrue(executeAskQuery(rdfTriple2)); 
		assertTrue (executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4));
	}
	
	@Test
	public void insDelOptionalTest2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/insertDelete/insDelOptional2.pr";
		setProjectionRule(pearlFileName);

		
		String rdfTriple1 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/PersonTemp>";
		String rdfTriple2 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person2>";
		String rdfTriple4 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person3>";
		String rdfTriple5 = "<http://art.uniroma2.it/Anonymous> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person4>";
		

		addTriple(rdfTriple1);
				
		
		assertTrue(executeAskQuery(rdfTriple1)); 
		assertFalse(executeAskQuery(rdfTriple2)); 
		assertFalse(executeAskQuery(rdfTriple3)); 
		assertFalse(executeAskQuery(rdfTriple4)); 
		assertFalse(executeAskQuery(rdfTriple5)); 
		
		// create and execute the UIMA AE
		String text = "Anonymous lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertFalse(executeAskQuery(rdfTriple1)); 
		assertTrue(executeAskQuery(rdfTriple2)); 
		assertTrue(executeAskQuery(rdfTriple3)); 
		assertTrue(executeAskQuery(rdfTriple4)); 
		assertFalse(executeAskQuery(rdfTriple5)); 
	}


}
