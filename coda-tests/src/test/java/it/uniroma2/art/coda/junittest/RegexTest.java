package it.uniroma2.art.coda.junittest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;

import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.junit.Test;

public class RegexTest extends AbstractTest {

	// test the regex with a normal rule before
	@Test
	public void regexBaseRule() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexBaseRule.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);
		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a normal rule before
	@Test
	public void regexBaseRuleWithConditionIn1()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexBaseRuleWithConditionIn1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);
		// pass the jcas to CODA and execute to populate/enrich the ontology

		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	public void regexBaseRuleWithConditionIn2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexBaseRuleWithConditionIn2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);
		// pass the jcas to CODA and execute to populate/enrich the ontology

		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a normal rule before
	@Test
	public void regexBaseRuleWithConditionIn3()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexBaseRuleWithConditionIn3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);
		// pass the jcas to CODA and execute to populate/enrich the ontology

		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	@Test
	public void regexBaseRuleWithConditionIn4()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexBaseRuleWithConditionIn4.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple4 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Milan>";

		String rdfTriple5 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn2> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple6 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn2> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple7 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn2> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple8 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn2> "
				+ "<http://art.uniroma2.it/Milan>";

		
		
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple5)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple6)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple7)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple8)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando do not live in Milan, they live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);
		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // now the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // now the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple5)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple6)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple7)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple8)); // now the model should contain the triple
		
	}

	// test the regex with a normal rule before
	@Test
	public void regexWithNormalRuleBefore() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexWithNormalRuleBefore.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the regex with a normal rule after
	@Test
	public void regexWithNormalRuleAfter() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexWithNormalRuleAfter.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the regex with a normal rule before and after
	@Test
	public void regexWithNormalRuleBeforeAndAfter()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexWithNormalRuleBeforeAndAfter.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person1>";

		String rdfTriple4 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person2>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	// test the regex with a + and with no max distance
	@Test
	public void regexNoDistDependency() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexNoDist.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a + and with no max distance
	@Test
	public void regexNoDistDependencyOptional()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexNoDist_Optional.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a + and with no max distance
	@Test
	public void regexNoDistDependencyOptional2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexNoDist_Optional.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in a city";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a + and with no max distance
	@Test
	public void regexConditionInDependency() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexConditionIn.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a + and with no max distance
	@Test
	public void regexConditionNotInDependency()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexConditionNotIn.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a + and with a max distance that enables to use both persons
	@Test
	public void regexDistDependency1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexDist1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea, one of the developers of CODA, and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a + and with a max distance which enables to use just one person
	@Test
	public void regexDistDependency2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexDist2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea, one of the developers of CODA, and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex using the same pr file as the previous one, but this time use a City which is not
	// recognized by the annotator
	@Test
	public void regexDistDependency3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexDist2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea, one of the developers of CODA, and Armando do not live in New York";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with a + and with a max distance between the two person too small to consider both
	// person in the same regex, but a max distance with city which enable the possibility to consider one
	// person at a given time
	@Test
	public void regexDistDependency4() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexDist3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/livesIn> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea, one of the developers of CODA, and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the regex with 2 annotations in an OR
	@Test
	public void regexOrSimple1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Milan> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple3 = "<http://art.uniroma2.it/Milan> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "In Milan Andrea lives in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2));
		assertFalse(executeAskQuery(rdfTriple3));

	}

	// test the regex with 2 annotations in an OR
	@Test
	public void regexOrSimple2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Florence> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple2 = "<http://art.uniroma2.it/Milan> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Florence>";
		String rdfTriple3 = "<http://art.uniroma2.it/Milan> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Milan is far from Florence and from Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2));
		assertFalse(executeAskQuery(rdfTriple3));

	}

	// test the regex with 2 annotations in an OR
	@Test
	public void regexOrSimple3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Rome> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Rome is in Italy";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple

	}

	// test the regex with 2 annotations in an OR
	@Test
	public void regexOrSimple4() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea is in Italy";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple

	}

	// test the regex with 2 annotations in an OR
	@Test
	public void regexOrSimple5() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/rose>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea is near a rose";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple

	}

	// test the regex with 2 annotations in an OR
	@Test
	public void regexOrSimple6() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> "
				+ "<http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea is in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple

	}

	// test the regex with 3 annotations in an OR
	@Test
	public void regexOr3Elements1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr3Elements.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple3 = "<http://art.uniroma2.it/Lorenzetti> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";

		ArrayList<Boolean> booleanList = new ArrayList<Boolean>();

		booleanList.add(executeAskQuery(rdfTriple1));
		booleanList.add(executeAskQuery(rdfTriple2));
		booleanList.add(executeAskQuery(rdfTriple3));

		assertTrue(numberOfTrueValues(booleanList) == 0);
		assertTrue(numberOfFalseValues(booleanList) == 3);

		// create and execute the UIMA AE
		String text = "Maybe a cat test regex in Rome, Tiziano Lorenzetti or Armando are far from Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		booleanList.clear();
		booleanList.add(executeAskQuery(rdfTriple1));
		booleanList.add(executeAskQuery(rdfTriple2));
		booleanList.add(executeAskQuery(rdfTriple3));

		assertTrue(numberOfTrueValues(booleanList) == 1);
		assertTrue(numberOfFalseValues(booleanList) == 2);
	}

	// test the regex with 3 annotations in an OR
	@Test
	public void regexOr3Elements2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr3Elements.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple3 = "<http://art.uniroma2.it/Lorenzetti> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";

		ArrayList<Boolean> booleanList = new ArrayList<Boolean>();

		booleanList.add(executeAskQuery(rdfTriple1));
		booleanList.add(executeAskQuery(rdfTriple2));
		booleanList.add(executeAskQuery(rdfTriple3));

		assertTrue(numberOfTrueValues(booleanList) == 0);
		assertTrue(numberOfFalseValues(booleanList) == 3);

		// create and execute the UIMA AE
		String text = "Maybe a cat test regex Armando, Tiziano Lorenzetti or Rome are far from Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		booleanList.clear();
		booleanList.add(executeAskQuery(rdfTriple1));
		booleanList.add(executeAskQuery(rdfTriple2));
		booleanList.add(executeAskQuery(rdfTriple3));

		assertTrue(numberOfTrueValues(booleanList) == 1);
		assertTrue(numberOfFalseValues(booleanList) == 2);

	}

	// test the regex with 3 annotations in an OR
	@Test
	public void regexOr3Elements3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOr3Elements.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Armando> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple3 = "<http://art.uniroma2.it/Lorenzetti> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";

		ArrayList<Boolean> booleanList = new ArrayList<Boolean>();

		booleanList.add(executeAskQuery(rdfTriple1));
		booleanList.add(executeAskQuery(rdfTriple2));
		booleanList.add(executeAskQuery(rdfTriple3));

		assertTrue(numberOfTrueValues(booleanList) == 0);
		assertTrue(numberOfFalseValues(booleanList) == 3);

		// create and execute the UIMA AE
		String text = "Maybe a cat test regex Tiziano Lorenzetti, Armando or Florence are far from Milan";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		booleanList.clear();
		booleanList.add(executeAskQuery(rdfTriple1));
		booleanList.add(executeAskQuery(rdfTriple2));
		booleanList.add(executeAskQuery(rdfTriple3));

		assertTrue(numberOfTrueValues(booleanList) == 1);
		assertTrue(numberOfFalseValues(booleanList) == 2);
	}

	// test the regex with zeroOrOne ( ? )
	@Test
	public void regexZeroOne1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexZeroOne.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat belongs to Andrea in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2));

	}

	// test the regex with zeroOrOne ( ? )
	@Test
	public void regexZeroOne2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexZeroOne.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2));

	}

	// test the regex with oneOrMore ( + )
	@Test
	public void regexOneOrMore1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOneOrMore.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps in Rome while Andrea reads";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));

	}

	// test the regex with oneOrMore ( + )
	@Test
	public void regexOneOrMore2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOneOrMore.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple3 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple4 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Florence>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		// String text = "the cat sleeps in Rome, in Milan and in Florence while Andrea reads";
		String text = "the cat sleeps in Florence, in Milan and in Rome while Andrea reads";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertTrue(executeAskQuery(rdfTriple4));

	}

	// test the regex with oneOrMore ( + )
	@Test
	public void regexOneOrMore3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOneOrMore.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps while Andrea reads";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1));

	}

	// test the regex with oneOrMore ( + )
	@Test
	public void regexOneOrMore4() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOneOrMore2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps in Rome while Andrea reads";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));

	}

	// test the regex with oneOrMore ( + )
	@Test
	public void regexOneOrMore5() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOneOrMore2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Armando>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps in Rome while Andrea reads and in Milan Armando studies";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));

	}

	// test the regex with oneOrMore ( + )
	@Test
	public void regexOneOrMore6() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOneOrMore2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps and Andrea reads in Rome";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1));

	}

	// test the regex with oneOrMore ( + )
	@Test
	public void regexOneOrMore7() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexOneOrMore2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Armando>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps, Andrea reads in Rome and Armando studies in Milan";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));

	}

	// test the regex with oneOrMore ( * )
	@Test
	public void regexZeroOrMore1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexZeroOrMore.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps in Rome while Andrea reads";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));

	}

	// test the regex with oneOrMore ( * )
	@Test
	public void regexZeroOrMore2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexZeroOrMore.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Rome>";
		String rdfTriple3 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Milan>";
		String rdfTriple4 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Florence>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps in Rome, in Milan and in Florence while Andrea reads";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertTrue(executeAskQuery(rdfTriple4));

	}

	// test the regex with oneOrMore ( * )
	@Test
	public void regexZeroOrMore3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexZeroOrMore.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/cat> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "the cat sleeps while Andrea reads";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));

	}

	// test the regex with all the symbols (? + * ) and with or , and no maxDist
	@Test
	public void regexComplex1() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexComplex.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/cat>";
		String rdfTriple2 = "<http://art.uniroma2.it/Milan> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/cat>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Rome Andrea Manuel Armando Milan rose Andrea daisy Florence Manuel cat";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
	}

	// test the regex with all the symbols (? + * ) and with or , and with maxDist
	@Test
	public void regexComplex2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexComplexWithDist.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/cat>";
		String rdfTriple2 = "<http://art.uniroma2.it/Milan> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/cat>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Rome Andrea Manuel Armando Milan rose Andrea daisy Florence Manuel cat";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertFalse(executeAskQuery(rdfTriple2));
	}

	// test the regex with all the symbols (+ * ) and with or
	@Test
	public void regexComplex3() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexComplex2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Manuel>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/daisy>";
		String rdfTriple4 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Armando>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Rome Andrea Manuel daisy Milan Armando";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertTrue(executeAskQuery(rdfTriple4));
	}

	// test the regex with all the symbols (+ * ) and with or
	@Test
	public void regexComplex4() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/regex/regexComplex2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Andrea>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Manuel>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/daisy>";
		String rdfTriple4 = "<http://art.uniroma2.it/Rome> <http://art.uniroma2.it/linkedTo> "
				+ "<http://art.uniroma2.it/Armando>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Rome Andrea daisy Manuel rose Milan Armando";
		// String text = "Andrea live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertFalse(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertTrue(executeAskQuery(rdfTriple4));
	}

}
