package it.uniroma2.art.coda.junittest;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.uima.UIMAException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.junit.Test;

import it.uniroma2.art.coda.converters.contracts.RandomIdGenerator;
import it.uniroma2.art.coda.converters.impl.TemplateBasedRandomIdGenerator;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.provisioning.ConverterContractDescription;
import it.uniroma2.art.coda.provisioning.ConverterDescription;
import it.uniroma2.art.coda.provisioning.JavaTypeDescription;
import it.uniroma2.art.coda.provisioning.SignatureDescription;

public class ListableConvertersTest extends AbstractTest {

	@SuppressWarnings("unused")
	private static Map<String, Value> unusedField1;

	public static Type getType_Map_String_ARTNode() {
		try {
			return ListableConvertersTest.class.getDeclaredField("unusedField1").getGenericType();
		} catch (NoSuchFieldException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void contractListingTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException {

		// Gets all converter contracts
		Collection<ConverterContractDescription> converterContracts = codaCore.listConverterContracts();

		// Gets the contract coda:randIdGen
		Collection<ConverterContractDescription> randIdGenContractDescriptions = converterContracts.stream()
				.filter(aContract -> aContract.getContractURI().equals(RandomIdGenerator.CONTRACT_URI))
				.collect(Collectors.toList());

		assertThat(randIdGenContractDescriptions.size(), equalTo(1));

		ConverterContractDescription randIdGenContractDescription = randIdGenContractDescriptions.iterator()
				.next();

		// Checks the contract URI, RDF capability and datatypes
		assertEquals(RandomIdGenerator.CONTRACT_URI, randIdGenContractDescription.getContractURI());
		assertThat(randIdGenContractDescription.getDatatypes(), empty());
		assertThat(randIdGenContractDescription.getRDFCapability(), equalTo(RDFCapabilityType.uri));

		// Gets the signatures of coda:randIdGen
		Collection<SignatureDescription> randIdGenSignatureDescriptions = randIdGenContractDescription
				.getSignatureDescriptions();

		assertThat(randIdGenSignatureDescriptions, hasSize(3));

		{
			//// gets the signature
			//// it.uniroma2.art.coda.converters.contracts.RandomIdGenerator.produceURI(CODAContext, String,
			//// String,
			//// Map<String, ARTNode>)
			SignatureDescription randIdGenArtiy2 = randIdGenSignatureDescriptions.stream()
					.filter(sig -> sig.getParameterDescriptions().size() == 2).findAny().get();

			// Checks the requirement level of the feature path
			assertThat(randIdGenArtiy2.getFeaturePathRequirementLevel(), equalTo(RequirementLevels.IGNORED));

			// Checks that return type is IRI
			assertThat(randIdGenArtiy2.getReturnTypeDescription(), is(instanceOf(JavaTypeDescription.class)));
			assertThat((JavaTypeDescription) randIdGenArtiy2.getReturnTypeDescription(),
					both(hasProperty("name", is(IRI.class.getName())))
							.and(hasProperty("javaType", equalTo(IRI.class))));

			// Checks that the first parameter is a String called xRole
			assertThat(randIdGenArtiy2.getParameterDescriptions().get(0),
					both(hasProperty("name", equalTo("xRole"))).and(
							hasProperty("typeDescription", both(is(instanceOf(JavaTypeDescription.class)))
									.and(hasProperty("javaType", equalTo(String.class))))));

			// Checks that the first parameter is a Map<String, ARTNode> called args
			assertThat(randIdGenArtiy2.getParameterDescriptions().get(1),
					both(hasProperty("name", equalTo("args"))).and(
							hasProperty("typeDescription", both(is(instanceOf(JavaTypeDescription.class)))
									.and(hasProperty("javaType", equalTo(getType_Map_String_ARTNode()))))));
		}
		{
			//// gets the signature
			//// it.uniroma2.art.coda.converters.contracts.RandomIdGenerator.produceURI(CODAContext, String,
			//// String)
			SignatureDescription randIdGenArtiy1 = randIdGenSignatureDescriptions.stream()
					.filter(sig -> sig.getParameterDescriptions().size() == 1).findAny().get();

			// Checks the requirement level of the feature path
			assertThat(randIdGenArtiy1.getFeaturePathRequirementLevel(), equalTo(RequirementLevels.IGNORED));

			// Checks that return type is IRI
			assertThat(randIdGenArtiy1.getReturnTypeDescription(), is(instanceOf(JavaTypeDescription.class)));
			assertThat((JavaTypeDescription) randIdGenArtiy1.getReturnTypeDescription(),
					both(hasProperty("name", is(IRI.class.getName())))
							.and(hasProperty("javaType", equalTo(IRI.class))));

			// Checks that the first parameter is a String called xRole
			assertThat(randIdGenArtiy1.getParameterDescriptions().get(0),
					both(hasProperty("name", equalTo("xRole"))).and(
							hasProperty("typeDescription", both(is(instanceOf(JavaTypeDescription.class)))
									.and(hasProperty("javaType", equalTo(String.class))))));
		}
		
		{
			//// gets the signature
			//// it.uniroma2.art.coda.converters.contracts.RandomIdGenerator.produceURI(CODAContext, String)
			SignatureDescription randIdGenArtiy1 = randIdGenSignatureDescriptions.stream()
					.filter(sig -> sig.getParameterDescriptions().size() == 0).findAny().get();

			// Checks the requirement level of the feature path
			assertThat(randIdGenArtiy1.getFeaturePathRequirementLevel(), equalTo(RequirementLevels.IGNORED));

			// Checks that return type is IRI
			assertThat(randIdGenArtiy1.getReturnTypeDescription(), is(instanceOf(JavaTypeDescription.class)));
			assertThat((JavaTypeDescription) randIdGenArtiy1.getReturnTypeDescription(),
					both(hasProperty("name", is(IRI.class.getName())))
							.and(hasProperty("javaType", equalTo(IRI.class))));
		}

	}
	
	@Test
	public void converterListingTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException {

		// Gets all converter contracts
		Collection<ConverterDescription> converters = codaCore.listConverters();

		// Gets the contract coda:randIdGen
		Collection<ConverterDescription> templateBasedRandomIdGenerator = converters.stream()
				.filter(aConverter -> aConverter.getContractURI().equals(TemplateBasedRandomIdGenerator.CONVERTER_URI))
				.collect(Collectors.toList());

		assertThat(templateBasedRandomIdGenerator.size(), equalTo(1));

		ConverterDescription templateBasedRandomIdGeneratorDescription = templateBasedRandomIdGenerator.iterator()
				.next();


		// Gets the signatures of coda:randIdGen
		Collection<SignatureDescription> templateBasedRandomIdGeneratorSignatureDescriptions = templateBasedRandomIdGeneratorDescription
				.getSignatureDescriptions();

		assertThat(templateBasedRandomIdGeneratorSignatureDescriptions, hasSize(3));

		{
			//// gets the signature
			//// it.uniroma2.art.coda.converters.contracts.RandomIdGenerator.produceURI(CODAContext, String,
			//// String,
			//// Map<String, ARTNode>)
			SignatureDescription randIdGenArtiy2 = templateBasedRandomIdGeneratorSignatureDescriptions.stream()
					.filter(sig -> sig.getParameterDescriptions().size() == 2).findAny().get();

			// Checks the requirement level of the feature path
			assertThat(randIdGenArtiy2.getFeaturePathRequirementLevel(), equalTo(RequirementLevels.IGNORED));

			// Checks that return type is IRI
			assertThat(randIdGenArtiy2.getReturnTypeDescription(), is(instanceOf(JavaTypeDescription.class)));
			assertThat((JavaTypeDescription) randIdGenArtiy2.getReturnTypeDescription(),
					both(hasProperty("name", is(IRI.class.getName())))
							.and(hasProperty("javaType", equalTo(IRI.class))));

			// Checks that the first parameter is a String called xRole
			assertThat(randIdGenArtiy2.getParameterDescriptions().get(0),
					both(hasProperty("name", equalTo("xRole"))).and(
							hasProperty("typeDescription", both(is(instanceOf(JavaTypeDescription.class)))
									.and(hasProperty("javaType", equalTo(String.class))))));

			// Checks that the first parameter is a Map<String, ARTNode> called args
			assertThat(randIdGenArtiy2.getParameterDescriptions().get(1),
					both(hasProperty("name", equalTo("args"))).and(
							hasProperty("typeDescription", both(is(instanceOf(JavaTypeDescription.class)))
									.and(hasProperty("javaType", equalTo(getType_Map_String_ARTNode()))))));
		}
		{
			//// gets the signature
			//// it.uniroma2.art.coda.converters.contracts.RandomIdGenerator.produceURI(CODAContext, String,
			//// String)
			SignatureDescription randIdGenArtiy1 = templateBasedRandomIdGeneratorSignatureDescriptions.stream()
					.filter(sig -> sig.getParameterDescriptions().size() == 1).findAny().get();

			// Checks the requirement level of the feature path
			assertThat(randIdGenArtiy1.getFeaturePathRequirementLevel(), equalTo(RequirementLevels.IGNORED));

			// Checks that return type is IRI
			assertThat(randIdGenArtiy1.getReturnTypeDescription(), is(instanceOf(JavaTypeDescription.class)));
			assertThat((JavaTypeDescription) randIdGenArtiy1.getReturnTypeDescription(),
					both(hasProperty("name", is(IRI.class.getName())))
							.and(hasProperty("javaType", equalTo(IRI.class))));

			// Checks that the first parameter is a String called xRole
			assertThat(randIdGenArtiy1.getParameterDescriptions().get(0),
					both(hasProperty("name", equalTo("xRole"))).and(
							hasProperty("typeDescription", both(is(instanceOf(JavaTypeDescription.class)))
									.and(hasProperty("javaType", equalTo(String.class))))));
		}
		
		{
			//// gets the signature
			//// it.uniroma2.art.coda.converters.contracts.RandomIdGenerator.produceURI(CODAContext, String)
			SignatureDescription randIdGenArtiy1 = templateBasedRandomIdGeneratorSignatureDescriptions.stream()
					.filter(sig -> sig.getParameterDescriptions().size() == 0).findAny().get();

			// Checks the requirement level of the feature path
			assertThat(randIdGenArtiy1.getFeaturePathRequirementLevel(), equalTo(RequirementLevels.IGNORED));

			// Checks that return type is IRI
			assertThat(randIdGenArtiy1.getReturnTypeDescription(), is(instanceOf(JavaTypeDescription.class)));
			assertThat((JavaTypeDescription) randIdGenArtiy1.getReturnTypeDescription(),
					both(hasProperty("name", is(IRI.class.getName())))
							.and(hasProperty("javaType", equalTo(IRI.class))));
		}	}

}
