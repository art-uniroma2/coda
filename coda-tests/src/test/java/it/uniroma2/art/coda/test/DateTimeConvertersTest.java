package it.uniroma2.art.coda.test;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.Literal;

import it.uniroma2.art.coda.converters.commons.DateTimeUtils;
import it.uniroma2.art.coda.converters.contracts.DateConverter;
import it.uniroma2.art.coda.converters.contracts.DatetimeConverter;
import it.uniroma2.art.coda.converters.contracts.TimeConverter;
import it.uniroma2.art.coda.converters.impl.DateConverterImpl;
import it.uniroma2.art.coda.converters.impl.DatetimeConverterImpl;
import it.uniroma2.art.coda.converters.impl.TimeConverterImpl;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;

public class DateTimeConvertersTest {

	public static void main(String[] args) {
		
		CODAContext ctx = new CODAContext(null, null);
		
		DateConverter dateConverter = new DateConverterImpl();
		TimeConverter timeConverter = new TimeConverterImpl();
		DatetimeConverter datetimeConverter = new DatetimeConverterImpl();
		
		try {
			
			//========= DateConverter =========
			List<String> dateTestValues = new ArrayList<String>();
			dateTestValues.add(null);
			dateTestValues.add("2016-07-15");
			dateTestValues.add("2016 07 15");
			dateTestValues.add("2016 7 15");
			dateTestValues.add("2016.07.15");
			dateTestValues.add("15.7.2016");
			dateTestValues.add("7 15 2016");
			dateTestValues.add("7 5 2016");
			dateTestValues.add("15/7/2016");
			dateTestValues.add("15/07/2016");
			dateTestValues.add("15-07-2016");
			dateTestValues.add("15 luglio 2016");
			dateTestValues.add("15 gen 2016");
			dateTestValues.add("2016 July 15");
			dateTestValues.add("2016 Jul 15");
			dateTestValues.add("2016 julio 15");
			dateTestValues.add("22 july 2016");
			dateTestValues.add("2016 三月 15");
			dateTestValues.add("2016-07-15T16:08:01");
			
			for (int i = 0; i < dateTestValues.size(); i++) {
				String value = dateTestValues.get(i);
				System.out.println("Input: " + value);
				Literal conversionResult = dateConverter.produceLiteral(ctx, null, null, value);
				System.out.println("Output: " + conversionResult);
				System.out.println();
			}
			
			//========= TimeConverter =========
			List<String> timeTestValues = new ArrayList<String>();
			timeTestValues.add(null);
			timeTestValues.add("16:13:00");
			timeTestValues.add("16:13");
			timeTestValues.add("16:13:00.000");
			timeTestValues.add("16.13.00");
			timeTestValues.add("4:13 PM");
			timeTestValues.add("4:13 AM");
			timeTestValues.add("4.1 PM");
			timeTestValues.add("4.01 PM");
			timeTestValues.add("2016-07-15T16:08:01");
			timeTestValues.add("16:13:00Z");
			timeTestValues.add("16:13+01:00");
			timeTestValues.add("16:13:00.000-01:00");
			timeTestValues.add("16.13.00+01:00");
			timeTestValues.add("4:13 PM +01:00");
			timeTestValues.add("4.13 PM +01:00");
			timeTestValues.add("4.13 PM +0100");
			timeTestValues.add("4.13 PM +01");
			
			for (int i = 0; i < timeTestValues.size(); i++) {
				String value = timeTestValues.get(i);
				System.out.println("Input: " + value);
				Literal conversionResult = timeConverter.produceLiteral(ctx, null, null, value);
				System.out.println("Output: " + conversionResult);
				conversionResult = timeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_REUSE);
				System.out.println("Output with offset REUSE: " + conversionResult);
				conversionResult = timeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_UNDEFINED);
				System.out.println("Output with offset UNDEFINED: " + conversionResult);
				conversionResult = timeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_Z);
				System.out.println("Output with offset Z: " + conversionResult);
				conversionResult = timeConverter.produceLiteral(ctx, null, null, value, "+03:00");
				System.out.println("Output with offset +03:00: " + conversionResult);
				if (value != null) {
					conversionResult = timeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_REUSE, "+03:00");
					System.out.println("Output with offset REUSE and additional offset +03:00: " + conversionResult);
					conversionResult = timeConverter.produceLiteral(ctx, null, null, value, "-01:00", "+03:00");
					System.out.println("Output with offset -01:00 and additional offset  +03:00: " + conversionResult);
				}
				System.out.println();
			}
			
			//========= DatetimeConverter =========
			List<String> datetimeTestValues = new ArrayList<String>();
			datetimeTestValues.add(null);
			datetimeTestValues.add("2016-07-20T16:13:00");
			datetimeTestValues.add("2016-7-20T16:13:00");
			datetimeTestValues.add("2016-7-20T16:13:00+01:00");
			datetimeTestValues.add("2016-7-2T16:13:00+01");
			datetimeTestValues.add("2016-7-2T16:13:00-0100");
			datetimeTestValues.add("2016-7-2T16:13:00+01:00:00");
			
			for (int i = 0; i < datetimeTestValues.size(); i++) {
				String value = datetimeTestValues.get(i);
				System.out.println("Input: " + value);
				Literal conversionResult = datetimeConverter.produceLiteral(ctx, null, null, value);
				System.out.println("Output: " + conversionResult);
				conversionResult = datetimeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_REUSE);
				System.out.println("Output with offset REUSE: " + conversionResult);
				conversionResult = datetimeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_UNDEFINED);
				System.out.println("Output with offset UNDEFINED: " + conversionResult);
				conversionResult = datetimeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_Z);
				System.out.println("Output with offset Z: " + conversionResult);
				conversionResult = datetimeConverter.produceLiteral(ctx, null, null, value, "+03:00");
				System.out.println("Output with offset +03:00: " + conversionResult);
				if (value != null) {
					conversionResult = datetimeConverter.produceLiteral(ctx, null, null, value, DateTimeUtils.OFFSET_PARAM_REUSE, "+03:00");
					System.out.println("Output with offset REUSE and additional offset +03:00: " + conversionResult);
					conversionResult = datetimeConverter.produceLiteral(ctx, null, null, value, "-01:00", "+03:00");
					System.out.println("Output with offset -01:00 and additional offset  +03:00: " + conversionResult);
				}
				System.out.println();
			}
			
		} catch (ConverterException e) {
			e.printStackTrace();
		}

	}

}
