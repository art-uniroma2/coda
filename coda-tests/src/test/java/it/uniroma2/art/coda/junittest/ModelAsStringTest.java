package it.uniroma2.art.coda.junittest;

import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ModelAsStringTest extends AbstractTest {

    // test the base PEARL structure
    @Test
    public void baseTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "prefix my: <http://art.uniroma2.it/>";
        String textToContains2 = "$personName  a  my:Person .";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));
        assertTrue(inputText.contains(textToContains2));

        assertTrue(modelAsString.contains(textToContains1));
        assertTrue(modelAsString.contains(textToContains2));

    }

    // test the base PEARL structure with an OPTIONAL
    @Test
    public void baseOptionalTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseOptionalUnbound1.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "OPTIONAL {";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));

        assertTrue(modelAsString.contains(textToContains1));
    }

    // test the base PEARL structure with an OPTIONAL
    @Test
    public void wherelTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/where/pearlBaseWhere1.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "where =";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));

        assertTrue(modelAsString.contains(textToContains1));
    }

    @Test
    public void baseConvArgsTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConvWithArgs.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "cityName uri(coda:randIdGen(\"concept\", {})) .";
        String textToContains2 = "xlabelName uri(coda:randIdGen(\"xLabel\", {lexicalizedResource = $cityName, lexicalForm = $nameLabel})) .";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));
        assertTrue(inputText.contains(textToContains2));

        assertTrue(modelAsString.contains(textToContains1));
        assertTrue(modelAsString.contains(textToContains2));
    }

    @Test
    public void annotationLiteralAndStringFromPlaceholderTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/literalAndStringFromPlaceholder.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "@Target({subject, object, triple})";
        String textToContains2 = "String valueS1();";
        String textToContains3 = "@Retained";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));
        assertTrue(inputText.contains(textToContains2));
        assertTrue(inputText.contains(textToContains3));

        assertTrue(modelAsString.contains(textToContains1));
        assertTrue(modelAsString.contains(textToContains2));
        assertTrue(modelAsString.contains(textToContains3));
    }

    @Test
    public void variousParamAnnotationTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/variousParamAnnotationTest.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "String stringValue() default 'N/A';";
        String textToContains2 = "@Number(value=123456)";
        String textToContains3 = "@Retained";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));
        assertTrue(inputText.contains(textToContains2));
        assertTrue(inputText.contains(textToContains3));

        assertTrue(modelAsString.contains(textToContains1));
        assertTrue(modelAsString.contains(textToContains2));
        assertTrue(modelAsString.contains(textToContains3));
    }

    @Test
    public void moreNodesAnnotationTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/moreNodesAnnotationTest.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "@plcAnn1";
        String textToContains2 = "@plcAnn2";
        String textToContains3 = "@Target({node})";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));
        assertTrue(inputText.contains(textToContains2));
        assertTrue(inputText.contains(textToContains3));

        assertTrue(modelAsString.contains(textToContains1));
        assertTrue(modelAsString.contains(textToContains2));
        assertTrue(modelAsString.contains(textToContains3));
    }

    @Test
    public void pearlFormatterIriTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterIri.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "uri(coda:formatter(\"%s/!s/%n/%n/%l/%s/!s\", <http://test>, <http://test/cat>, $cityName1, \"Italia\"@it, \"city\"))";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));

        assertTrue(modelAsString.contains(textToContains1));
    }

    @Test
    public void annotationForCustomFormAllAnnotationsTest()  throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/annotationForCustomFormAllAnnotations.pr";
        String defPearlFileName = "it/uniroma2/art/coda/junittest/annotation/annDef.pr";
        setProjectionRule(defPearlFileName, pearlFileName);

        annDefToExludeList.addAll(getAnnotationsDefFromFile(defPearlFileName));

        String textToContains1 = "@ObjectOneOf(value={my:Rome, my:Milan})";
        String textToContains2 = "@Role(value={'Class', 'Concept'})";
        String textToContains3 = "@Range(value=my:MyClass)";
        String textToContains4 = "@RangeList(value={my:MyClass1, my:MyClass2})";
        String textToContains5 = "@Collection(min=3, max=5)";
        String textToContains6 = "@DataOneOf(value={\"Milan\"@it, \"Rome\", \"Firenze\"})";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains5));

        assertTrue(modelAsString.contains(textToContains1));
        assertTrue(modelAsString.contains(textToContains2));
        assertTrue(modelAsString.contains(textToContains3));
        assertTrue(modelAsString.contains(textToContains4));
        assertTrue(modelAsString.contains(textToContains5));
        assertTrue(modelAsString.contains(textToContains6));
    }

    @Test
    public void insDelBaseTest() throws PRParserException, IOException {
        List<String> annDefToExludeList = new ArrayList<>();
        annDefToExludeList.add("Memoized");
        annDefToExludeList.add("Confidence");
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/insertDelete/insDelBase.pr";
        setProjectionRule(pearlFileName);

        String textToContains1 = "insert =";
        String textToContains2 = "delete =";

        String inputText = getStringFromFile(pearlFileName);

        String modelAsString = codaCore.getProjRuleModel().getModelAsString(annDefToExludeList);

        assertTrue(inputText.contains(textToContains1));
        assertTrue(inputText.contains(textToContains2));

        assertTrue(modelAsString.contains(textToContains1));
        assertTrue(modelAsString.contains(textToContains2));
    }

}
