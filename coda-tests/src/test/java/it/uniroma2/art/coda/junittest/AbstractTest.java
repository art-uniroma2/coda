package it.uniroma2.art.coda.junittest;

import it.uniroma2.art.coda.converters.impl.TemplateBasedRandomIdGenerator;
import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.PlaceholderValueWrongTypeForParamInAnnotationException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceGenericErrorException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceNotDoubleNumException;
import it.uniroma2.art.coda.exception.parserexception.ConfidenceValueNotAcceptableException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.exception.parserexception.TypeOfParamInAnnotationWrongTypeException;
import it.uniroma2.art.coda.exception.parserexception.UnsupportedTypeInParamDefinitionException;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.pearl.model.RegexProjectionRule;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.FSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.StateFSA;
import it.uniroma2.art.coda.pearl.parser.antlr4.regex.structures.TransitionFSA;
import it.uniroma2.art.coda.pf4j.PF4JComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.structures.CODATriple;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import it.uniroma2.art.coda.test.ae.AnimalAnnotator;
import it.uniroma2.art.coda.test.ae.CityAnnotator;
import it.uniroma2.art.coda.test.ae.ComplexPersonAnnotator;
import it.uniroma2.art.coda.test.ae.DatetimeAnnotator;
import it.uniroma2.art.coda.test.ae.GenericWordsAnnotator;
import it.uniroma2.art.coda.test.ae.MultiWordAnnotator;
import it.uniroma2.art.coda.test.ae.PeopleAnnotator;
import it.uniroma2.art.coda.test.ae.PersonAnnotator;
import it.uniroma2.art.coda.test.ae.PlantAnnotator;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.factory.TypeSystemDescriptionFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.metadata.TypeDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.pf4j.DefaultPluginDescriptor;
import org.pf4j.DefaultPluginManager;
import org.pf4j.Plugin;
import org.pf4j.PluginDescriptor;
import org.pf4j.PluginManager;
import org.pf4j.PluginWrapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractTest {

	public static final String TEST_DEFAULT_NS = "http://art.uniroma2.it/";

	protected boolean debug = false;
	protected boolean printModelInDebugMode = false;
	protected boolean printQueryInDebugMode = false;
	protected boolean printFSAInDebug = false;
	protected boolean printAllAnn = false;

	protected static RepositoryConnection connection;
	protected static CODACore codaCore;

	//the name of the Annotations and their FS
	private final String singleWordAnnTypeName = "it.uniroma2.art.coda.test.ae.type.SingleWord";
	private final String singleWordWordFeatName = "word";

	private final String animalAnnTypeName = "it.uniroma2.art.coda.test.ae.type.Animal";
	private final String animalNameFeatName = "name"; //string

	private final String cityAnnTypeName = "it.uniroma2.art.coda.test.ae.type.City";
	private final String cityNameFeatName = "name"; // string
	private final String cityConfidenceFeatName = "confidence"; // double

	private final String personAnnTypeName = "it.uniroma2.art.coda.test.ae.type.Person";
	private final String personNameFeatName = "name"; //string

	private final String completenameAnnTypeName = "it.uniroma2.art.coda.test.ae.type.CompleteName";
	private final String completenameFirstnameFeatName = "firstName"; //string
	private final String completenameLastnameFeatName = "lastName"; //string

	private final String complexpersonAnnTypeName = "it.uniroma2.art.coda.test.ae.type.ComplexPerson";
	private final String complexpersonNameFeatName = "name"; //CompleteName

	private final String peopleAnnTypeName = "it.uniroma2.art.coda.test.ae.type.People";
	private final String peoplePersonlistFeatName = "personList"; //FSList
	private final String peopleNumberFeatName = "number"; //Integer

	private final String plantAnnTypeName = "it.uniroma2.art.coda.test.ae.type.Plant";
	private final String plantNameFeatName = "name"; //string

	private final String dateAnnTypeName = "it.uniroma2.art.coda.test.ae.type.Date";
	private final String dateValueFeatName = "value"; //string

	private final String timeAnnTypeName = "it.uniroma2.art.coda.test.ae.type.Time";
	private final String timeValueFeatName = "value"; //string

	private final String datetimeAnnTypeName = "it.uniroma2.art.coda.test.ae.type.Datetime";
	private final String datetimeValueFeatName = "value"; //string

	private final String multiwordAnnTypeName = "it.uniroma2.art.coda.test.ae.type.MultiWord";
	private final String multiwordFirstwordFeatName = "firstWord"; //string

	@BeforeClass
	public static void initializeModel() throws Exception {

		Repository rep = new SailRepository(new MemoryStore());
		rep.init();
		connection = rep.getConnection();
		connection.setNamespace("", TEST_DEFAULT_NS);

		// OLD version, with the OWLART
		// initialize the model
		// OWLArtModelFactory<Sesame2ModelConfiguration> modelFact = OWLArtModelFactory
		// .createModelFactory(new ARTModelFactorySesame2Impl());
		// owlModel = modelFact.loadOWLModel("http://art.uniroma2.it/",
		// Files.createTempDir().getAbsolutePath());
		// owlModel.setDefaultNamespace("http://art.uniroma2.it/");

		PluginManager pluginManager = new DefaultPluginManager();
		pluginManager.loadPlugins();
		pluginManager.startPlugins();

		PF4JComponentProvider cp = new PF4JComponentProvider(pluginManager, true);
		codaCore = new CODACore(cp);

//		CODAOSGiRegistrationUtils.registerConverter(bundleContext, CompleteNameConverter.class);
//		CODAOSGiRegistrationUtils.registerConverter(bundleContext, NoFeatPathConverter.class);
//		CODAOSGiRegistrationUtils.registerConverter(bundleContext, AdditionalURIConverter.class);
//

		codaCore.initialize(connection);

		Properties templateBasedConverterProperties = new Properties();
		templateBasedConverterProperties.setProperty("xLabel",
				"${lexicalizedResource.localName}/xl_${lexicalForm.language}_${rand()}");
		codaCore.setConverterProperties(TemplateBasedRandomIdGenerator.CONVERTER_URI,
				templateBasedConverterProperties);
	}

	@AfterClass
	public static void closeModel() throws RepositoryException {
		// close CODA (which closes the rdf model )
		connection.close();
	}

	@Before
	public void setUp() throws Exception {
		connection.clear();
		//set the LexModelForSearch so that each test can set it as they need (and its default value is ALL_MODEL)
		codaCore.getCODAContext().setLexModelForSearch(UIMACODAUtilities.LexModelForSearch.ALL_MODEL);
	}

	/*****************************************************
	 * private functions used by the tests
	 * @return
	 *
	 * @throws PRParserException
	 ******************************************************/

	protected ProjectionRulesModel setProjectionRule(String projRuleFile) throws PRParserException {
		return codaCore.setProjectionRulesModel(getStreamOfAFile(projRuleFile));
	}

	protected ProjectionRulesModel setProjectionRule(String file1, String file2)
			throws PRParserException {

		List<InputStream> inputStreamList = new ArrayList<>();
		inputStreamList.add(getStreamOfAFile(file1));
		inputStreamList.add(getStreamOfAFile(file2));

		return codaCore.setAllProjectionRulelModelFromInputStreamList(inputStreamList);
	}

	protected ProjectionRulesModel addProjectionRule(String projRuleFile) throws PRParserException {
		return codaCore.addProjectionRuleModel(getStreamOfAFile(projRuleFile));
	}

	protected void addProjectionRules(String projRuleDir, boolean isRecursive)
			throws NotDirectoryException, PRParserException {
		codaCore.cleanProjectionRulesModel();
		codaCore.addAllRulesFromDirectory(new File(projRuleDir), isRecursive);
	}

	protected JCas createAndExecuteAE(String text) throws UIMAException {
		String[] personArray = {"Andrea", "Armando", "Manuel", "Anonymous", "Wolfeschlegelsteinhausenbergerdorff"};
		AnalysisEngine aePerson = AnalysisEngineFactory.createEngine(PersonAnnotator.class,
				PersonAnnotator.PERSON_ARRAY, personArray,
				PersonAnnotator.PERSON_ANNOTATION_TYPE_STRING, personAnnTypeName,
				PersonAnnotator.NAME_STRING, personNameFeatName,
				PersonAnnotator.WORD_STRING, singleWordWordFeatName // since SingleWordAnnotation is the usertype of AnimalAnnotation
				);

		AnalysisEngine aePeople = AnalysisEngineFactory.createEngine(PeopleAnnotator.class,
				PeopleAnnotator.PERSON_ARRAY, personArray,
				PeopleAnnotator.PERSON_ANNOTATION_TYPE_STRING, personAnnTypeName,
				PeopleAnnotator.PEOPLE_ANNOTATION_TYPE_STRING, peopleAnnTypeName,
				PeopleAnnotator.PERSON_LIST_FSLIST, peoplePersonlistFeatName,
				PeopleAnnotator.NUMBER_INTEGER, peopleNumberFeatName,
				PeopleAnnotator.NAME_STRING, personNameFeatName,
				PeopleAnnotator.WORD_STRING, singleWordWordFeatName
				);

		String[] plantArray = {"rose", "daisy"};
		AnalysisEngine aePlant = AnalysisEngineFactory.createEngine(PlantAnnotator.class,
				PlantAnnotator.PLANT_ARRAY, plantArray,
				PlantAnnotator.PLANT_ANNOTATION_TYPE_STRING, plantAnnTypeName,
				PlantAnnotator.NAME_STRING, plantNameFeatName,
				PlantAnnotator.WORD_STRING, singleWordWordFeatName // since SingleWordAnnotation is the usertype of AnimalAnnotation
				);

		String[] animalArray = {"dog", "cat"};
		AnalysisEngine aeAnimal = AnalysisEngineFactory.createEngine(AnimalAnnotator.class,
				AnimalAnnotator.ANIMAL_ARRAY, animalArray,
				AnimalAnnotator.ANIMAL_ANNOTATION_TYPE_STRING, animalAnnTypeName,
				AnimalAnnotator.NAME_STRING, animalNameFeatName,
				AnimalAnnotator.WORD_STRING, singleWordWordFeatName // since SingleWordAnnotation is the usertype of AnimalAnnotation
		);

		String[] cityArray = {"Rome", "Milan", "Florence"};
		AnalysisEngine aeCity = AnalysisEngineFactory.createEngine(CityAnnotator.class,
				CityAnnotator.CITY_ARRAY, cityArray,
				CityAnnotator.CITY_ANNOTATION_TYPE_STRING, cityAnnTypeName,
				CityAnnotator.NAME_STRING, cityNameFeatName,
				CityAnnotator.WORD_STRING, singleWordWordFeatName, // since SingleWordAnnotation is the usertype of CityAnnotation
				CityAnnotator.CONFIDENCE_DOUBLE, cityConfidenceFeatName
		);

		AnalysisEngine aeMultiWord = AnalysisEngineFactory.createEngine(MultiWordAnnotator.class,
				MultiWordAnnotator.NUMBEROFWORD_INT, 5,
				MultiWordAnnotator.MULTI_WORD_ANNOTATION_TYPE_STRING, multiwordAnnTypeName,
				MultiWordAnnotator.FIRST_WORD_STRING, multiwordFirstwordFeatName
		);

		String[] complextPersonArray = {"Andrea Turbati", "Armando Stellato", "Manuel Fiorelli",
				"Tiziano Lorenzetti"};
		AnalysisEngine aeComplextPerson = AnalysisEngineFactory.createEngine(ComplexPersonAnnotator.class,
				ComplexPersonAnnotator.PERSON_ARRAY, complextPersonArray,
				ComplexPersonAnnotator.COMPLETE_NAME_ANNOTATION_TYPE_STRING, completenameAnnTypeName,
				ComplexPersonAnnotator.FIRST_NAME_STRING, completenameFirstnameFeatName,
				ComplexPersonAnnotator.LAST_NAME_STRING, completenameLastnameFeatName,
				ComplexPersonAnnotator.COMPLEX_PERSON_ANNOTATION_TYPE_STRING, complexpersonAnnTypeName,
				ComplexPersonAnnotator.NAME_COMPLETENAME, complexpersonNameFeatName
				);

		AnalysisEngine aeDatetime = AnalysisEngineFactory.createEngine(DatetimeAnnotator.class,
				DatetimeAnnotator.DATE_ANNOTATION_TYPE_STRING, dateAnnTypeName,
				DatetimeAnnotator.TIME_ANNOTATION_TYPE_STRING, timeAnnTypeName,
				DatetimeAnnotator.DATE_TIME_ANNOTATION_TYPE_STRING, datetimeAnnTypeName,
				DatetimeAnnotator.VALUE_STRING, dateValueFeatName // used only one of the three, since they have the same name
		);

		String [] genericWordsArray = {" Pinco", "Tizio e       Caio", " Caio   e      Sempronio ", "ciao*",
				"a.b,c:d;e!f?g. h.", "cia*o", "ciaiaiaiao", "first", "second*"};
		AnalysisEngine aeGenericWordsAnnotator = AnalysisEngineFactory.createEngine(GenericWordsAnnotator.class,
				GenericWordsAnnotator.WORDS_ARRAY, genericWordsArray,
				MultiWordAnnotator.MULTI_WORD_ANNOTATION_TYPE_STRING, multiwordAnnTypeName,
				MultiWordAnnotator.FIRST_WORD_STRING, multiwordFirstwordFeatName
		);

		//JCas jcas = JCasFactory.createJCas();
		JCas jcas = createTypeSystemInJCas();
		jcas.setDocumentText(text);

		SimplePipeline.runPipeline(jcas, aePerson, aePeople, aePlant, aeAnimal, aeCity, aeMultiWord,
				aeComplextPerson, aeDatetime, aeGenericWordsAnnotator);

		return jcas;
	}

	private JCas createTypeSystemInJCas() throws UIMAException {

		TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription();

		//add the SingleWord Annotation Type and its FS (this annotation is used as super type for some of the other annotations)
		TypeDescription singleWordAnnTypeDescription = tsd.addType(singleWordAnnTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		singleWordAnnTypeDescription.addFeature(singleWordWordFeatName, "", CAS.TYPE_NAME_STRING);

		//add the Animal Annotation Type and its FS
		TypeDescription animalAnnTypeDescription = tsd.addType(animalAnnTypeName, "", singleWordAnnTypeName);
		animalAnnTypeDescription.addFeature(animalNameFeatName, "", CAS.TYPE_NAME_STRING);

		//add the City Annotation Type and its FS
		TypeDescription cityAnnTypeDescription = tsd.addType(cityAnnTypeName, "", singleWordAnnTypeName);
		cityAnnTypeDescription.addFeature(cityNameFeatName, "", CAS.TYPE_NAME_STRING);
		cityAnnTypeDescription.addFeature(cityConfidenceFeatName, "", CAS.TYPE_NAME_DOUBLE);

		//add the Person Annotation Type and its FS
		TypeDescription personAnnTypeDescription = tsd.addType(personAnnTypeName, "", singleWordAnnTypeName);
		personAnnTypeDescription.addFeature(personNameFeatName, "", CAS.TYPE_NAME_STRING);

		//add the CompleteName Annotation Type and its FS
		TypeDescription completenameAnnTypeDescription = tsd.addType(completenameAnnTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		completenameAnnTypeDescription.addFeature(completenameFirstnameFeatName, "", CAS.TYPE_NAME_STRING);
		completenameAnnTypeDescription.addFeature(completenameLastnameFeatName, "", CAS.TYPE_NAME_STRING);

		//add the ComplexPerson Annotation Type and its FS
		TypeDescription complexpersonAnnTypeDescription = tsd.addType(complexpersonAnnTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		complexpersonAnnTypeDescription.addFeature(complexpersonNameFeatName, "", completenameAnnTypeName);

		//add the People Annotation Type and its FS
		TypeDescription peopleAnnTypeDescription = tsd.addType(peopleAnnTypeName, "", singleWordAnnTypeName);
		peopleAnnTypeDescription.addFeature(peopleNumberFeatName, "", CAS.TYPE_NAME_INTEGER);
		peopleAnnTypeDescription.addFeature(peoplePersonlistFeatName, "", CAS.TYPE_NAME_FS_LIST).setElementType(personAnnTypeName);

		//add the Time Annotation Type and its FS
		TypeDescription timeAnnTypeDescription = tsd.addType(timeAnnTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		timeAnnTypeDescription.addFeature(timeValueFeatName, "", CAS.TYPE_NAME_STRING);

		//add the Date Annotation Type and its FS
		TypeDescription dateAnnTypeDescription = tsd.addType(dateAnnTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		dateAnnTypeDescription.addFeature(dateValueFeatName, "", CAS.TYPE_NAME_STRING);

		//add the Datetime Annotation Type and its FS
		TypeDescription datetimeAnnTypeDescription = tsd.addType(datetimeAnnTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		datetimeAnnTypeDescription.addFeature(datetimeValueFeatName, "", CAS.TYPE_NAME_STRING);

		//add the MultiWord Annotation Type and its FS
		TypeDescription multiwordAnnTypeDescription = tsd.addType(multiwordAnnTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		multiwordAnnTypeDescription.addFeature(multiwordFirstwordFeatName, "", CAS.TYPE_NAME_STRING);

		//add the Plant Annotation Type and its FS
		TypeDescription plantAnnTypeDescription = tsd.addType(plantAnnTypeName, "", singleWordAnnTypeName);
		plantAnnTypeDescription.addFeature(plantNameFeatName, "", CAS.TYPE_NAME_STRING);

		JCas jcas = JCasFactory.createJCas(tsd);
		return jcas;
	}

	private TypeSystemDescription createTypeSystemDescription(Map<String, List<String>> annNameToFeatureMap) throws UIMAException {
		TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription();

		for (String annName : annNameToFeatureMap.keySet()){
			TypeDescription annTypeDescription = tsd.addType(annName, "", CAS.TYPE_NAME_ANNOTATION);
			for (String featName : annNameToFeatureMap.get(annName)) {
				annTypeDescription.addFeature(featName, "", CAS.TYPE_NAME_STRING);
			}
		}

		return tsd;
	}

	protected InputStream getStreamOfAFile(String filePath) {
		return AbstractTest.class.getClassLoader().getResourceAsStream(filePath);
	}

	protected void printAllAnnotations(JCas jcas) {
		AnnotationIndex<Annotation> index = jcas.getAnnotationIndex();
		System.out.println("printing all annotations");
		for (Annotation ann : index) {
			System.out.println(ann);
		}

	}

	protected List<SuggOntologyCoda> executeCODA(JCas jcas, String testName)
			throws ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {
		return executeCODA(jcas, testName, false, false);
	}

	protected List<SuggOntologyCoda> executeCODA(JCas jcas, String testName, boolean addNodeAssignmentMap,
												 boolean addNodeNamesInGraph )
			throws ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			UnsupportedTypeInParamDefinitionException, TypeOfParamInAnnotationWrongTypeException,
			ConfidenceNotDoubleNumException, ConfidenceValueNotAcceptableException, ConfidenceGenericErrorException,
			PlaceholderValueWrongTypeForParamInAnnotationException, ValueNotPresentDueToConfigurationException {

		if (printAllAnn) {
			printAllAnnotations(jcas);
		}

		codaCore.setJCas(jcas, true);
		List<SuggOntologyCoda> suggOntCodaList = new ArrayList<SuggOntologyCoda>();
		SuggOntologyCoda suggOntCoda;
		if (debug) {
			System.out.println("\n******* " + testName + " *********\n");
		}

		// print all the regexes (NFSA e DFSA)
		if (debug && printFSAInDebug) {
			Map<String, RegexProjectionRule> regexesMap = codaCore.getProjRuleModel().getRegexRuleMap();
			for (String regexId : regexesMap.keySet()) {
				RegexProjectionRule regexPr = codaCore.getProjRuleModel().getRegexRule(regexId);
				FSA nfsa = regexPr.getSingleRegexStruct().getNfsa();
				FSA dfsa = regexPr.getSingleRegexStruct().getDfsa();
				String nfsaString = printFSAasString(nfsa, regexId + " NFSA");
				System.out.println(nfsaString);
				String dfsaString = printFSAasString(dfsa, regexId + " DFSA");
				System.out.println(dfsaString);
			}

		}

		if (debug && printModelInDebugMode) {
			System.out.println(codaCore.getProjRuleModel().getModelAsStringForDebug());
		}
		while (codaCore.isAnotherAnnotationPresent()) {
			suggOntCoda = codaCore.processNextAnnotation(addNodeAssignmentMap, addNodeNamesInGraph);
			suggOntCodaList.add(suggOntCoda);
			if (debug) {
				System.out.println("\nAnnotation type = " + suggOntCoda.getAnnotation().getType().getName());
				System.out.println("\tCoveredText = " + suggOntCoda.getAnnotation().getCoveredText() + "\n");
			}
			Set<String> basePrRuleIdList = suggOntCoda.getBaseProjectionRulesIdList();
			for (String basePrRuleId : basePrRuleIdList) {
				// add the triples from the Insert Triple List
				List<CODATriple> triples = suggOntCoda.getInsertARTTripleListFromBaseProjRuleId(basePrRuleId);
				for (CODATriple triple : triples) {
					Statement stat = connection.getValueFactory().createStatement(triple.getSubject(),
							triple.getPredicate(), triple.getObject());
					connection.add(stat);
					if (debug) {
						System.out.println("\tAdding Triple from rule " + basePrRuleId + ":");
						System.out.println("\t\t" + NTriplesUtil.toNTriplesString(triple.getSubject()));
						System.out.println("\t\t" + NTriplesUtil.toNTriplesString(triple.getPredicate()));
						System.out.println("\t\t" + NTriplesUtil.toNTriplesString(triple.getObject()));
					}
				}
				// remove the triples form the Delete Triple List
				triples = suggOntCoda.getDeleteARTTripleListFromBaseProjRuleId(basePrRuleId);
				for (CODATriple triple : triples) {
					Statement stat = connection.getValueFactory().createStatement(triple.getSubject(),
							triple.getPredicate(), triple.getObject());
					connection.remove(stat);
					if (debug) {
						System.out.println("\tRemoving Triple from rule " + basePrRuleId + ":");
						System.out.println("\t\t" + NTriplesUtil.toNTriplesString(triple.getSubject()));
						System.out.println("\t\t" + NTriplesUtil.toNTriplesString(triple.getPredicate()));
						System.out.println("\t\t" + NTriplesUtil.toNTriplesString(triple.getObject()));
					}
				}
			}
		}

		return suggOntCodaList;
	}

	protected boolean executeAskQuery(String triples) throws QueryEvaluationException {
		String query = "ASK {\n" + triples + "\n}";
		if (debug && printQueryInDebugMode) {
			System.out.println("QUERY : " + query);
		}

		BooleanQuery bQuery = connection.prepareBooleanQuery(query);
		bQuery.setIncludeInferred(false);
		return bQuery.evaluate();
	}

	protected int getNumOfTripleInModel() {
		String query = "CONSTRUCT { ?s ?p ?o .}  " +
				"\nWHERE {" +
				"?s ?p ?o ." +
				"\n}";
		if (debug && printQueryInDebugMode) {
			System.out.println("QUERY : " + query);
		}
		GraphQuery graphQuery = connection.prepareGraphQuery(query);
		graphQuery.setIncludeInferred(false);
		GraphQueryResult graphQueryResult = graphQuery.evaluate() ;
		int count = 0;
		while (graphQueryResult.hasNext()) {
			Statement statement = graphQueryResult.next();
			++count;
		}
		graphQueryResult.close();
		return count;
	}

	protected Model getModel() {
		Model model = new LinkedHashModel();
		connection.export(new StatementCollector(model));
		return model;
	}

	protected void addTriple(String rdfTriple) {
		String query = "INSERT DATA {" + rdfTriple + " }";
		if (debug && printQueryInDebugMode) {
			System.out.println("QUERY : " + query);
		}
		Update update = connection.prepareUpdate(query);
		update.execute();
	}

	protected List<String> executeTupleQuery(String query, boolean infer) throws QueryEvaluationException {
		List<String> results = new ArrayList<>();

		TupleQuery tupleQuery = connection.prepareTupleQuery(query);
		tupleQuery.setIncludeInferred(infer);
		TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
		while (tupleQueryResult.hasNext()) {
			BindingSet bindingSet = tupleQueryResult.next();
			Set<String> names = bindingSet.getBindingNames();
			Iterator<String> iterNames = names.iterator();
			while (iterNames.hasNext()) {
				String name = iterNames.next();
				Value artNode = bindingSet.getBinding(name).getValue();
				String value = name + " = " + artNode.stringValue();
				results.add(value);
			}
		}

		return results;
	}

	protected int numberOfTrueValues(List<Boolean> booleanList) {
		int trueValues = 0;
		for (Boolean booleanValue : booleanList) {
			if (booleanValue == true) {
				trueValues++;
			}
		}
		return trueValues;
	}

	protected int numberOfFalseValues(List<Boolean> booleanList) {
		int falseValues = 0;
		for (Boolean booleanValue : booleanList) {
			if (booleanValue == false) {
				falseValues++;
			}
		}
		return falseValues;
	}

	private String printFSAasString(FSA fsa, String title) {
		StringBuilder sb = new StringBuilder();

		sb.append(title + ":");

		sb.append("\n\tStates:");
		for (StateFSA state : fsa.getStateMap().values()) {
			sb.append("\n\t\t" + state.getStateId());
			if (state.isStartState()) {
				sb.append("\tstartState");
			}
			if (state.isEndState()) {
				sb.append("\tendState");
			}
			// sb.append("\t#transition = "+state.getNumberTransition());
			// sb.append("\ttransitionId = "+state.getAllTransitionId());
		}

		sb.append("\n\tTransitions:");
		for (String transitionId : fsa.getTransitionId()) {
			TransitionFSA transition = fsa.getTransition(transitionId);
			sb.append("\n\t\t" + transition.getStartingState().getStateId());
			sb.append(" --- ");
			sb.append(transition.getTransitionId());
			if (!transition.isEpsilonTransition()) {
				sb.append(", " + transition.getInternalId());
			} else {
				sb.append(", Epsilon");
			}
			if (transition.isMaxDistancePresent()) {
				sb.append(", " + transition.getMaxDistance());
			}
			sb.append(" --> ");
			sb.append(transition.getEndingState().getStateId());
		}

		return sb.toString();
	}

	protected String getStringFromFile(String filePath) throws IOException {

		InputStream inputStream = AbstractTest.class.getClassLoader().getResourceAsStream(filePath);

		String text = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines().collect(Collectors
				.joining(System.lineSeparator()));
		return text;
	}

	protected List<String> getAnnotationsDefFromFile(String filePath) throws IOException {
		List<String> annDefList = new ArrayList<>();

		InputStream inputStream = AbstractTest.class.getClassLoader().getResourceAsStream(filePath);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (line.trim().startsWith("Annotation")) {
					String annName = line.trim().split(" ")[1];
					if (annName.endsWith("{")) {
						annName = annName.substring(0, annName.length() - 1);

					}
					annDefList.add(annName);
				}
			}
		}
		return annDefList;
	}
}