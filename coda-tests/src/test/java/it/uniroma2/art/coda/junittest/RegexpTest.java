package it.uniroma2.art.coda.junittest;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RegexpTest extends AbstractTest{

    @Test
    public void baseRegexpTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlBaseRegexp.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://art.uniroma2.it/ome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void baseRegexpTest2() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlBaseRegexp2.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://test/org/ome/R> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void baseRegexpTest3() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlBaseRegexp3.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        assertTrue(getNumOfTripleInModel()==0);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple

        //only one RDF triple should be present in the repository, since the OPTIONAL did not produce any triple
        // (the regex did not match the input. so the node had NULL as value, so no value)
        assertTrue(getNumOfTripleInModel()==1);
    }

    @Test
    public void baseRegexpTestIRI() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlRegexpIri.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://art.uniroma2.it/om/caput> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void baseRegexpTestILiteralAndIri() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlRegexpLiteralAndIri.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://art.uniroma2.it/om/caput> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        String rdfTriple3 = "<http://art.uniroma2.it/om/caput> "
                + "<http://www.w3.org/2000/01/rdf-schema#label> \"om is the center or Rome\"@en";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
    }

    @Test
    public void baseRegexpTestIRIandFormatter() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlRegexIriAndFormatter.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://test/http://art.uniroma2.it/ome/Milan> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void manyGroupsRegexpTest1() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlRegexpManyGroups1.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Wolfeschlegelsteinhausenbergerdorff> "
                + "<http://art.uniroma2.it/contains> \"contains letters e and l\"@en";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Wolfeschlegelsteinhausenbergerdorff does not lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
    }

    @Test
    public void manyGroupsRegexpTest2() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlRegexpManyGroups2.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://art.uniroma2.it/contains> \"contains letters A0 and d\"@en";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
    }

    /*
    // this test is no longer valid, since now the converter coda:regexp does not throw the exception ConverterException
    // anymore when the regex does not match
    @Test
    public void exceptionRegexpTest2() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlExceptionRegexp.pr";
        setProjectionRule(pearlFileName);


        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        try {
            // pass the jcas to CODA and execute to populate/enrich the ontology
            executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
            assertTrue(false);
        } catch (ConverterException e) {
            assertTrue(true);
        }
        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

    }

     */





    @Test
    public void pearlBaseRegexpEscapeTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlBaseRegexpEscapeTest.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/ia*> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";
        String rdfTriple2 = "<http://art.uniroma2.it/ia*> "
                + "<http://art.uniroma2.it/name> \"http://test\\\\.it\"@en";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Test cia*o end";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void pearlBaseRegexpNoEscapeTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlBaseRegexpNoEscapeTest.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/iaiaiaia> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";
        String rdfTriple2 = "<http://art.uniroma2.it/iaiaiaia> "
                + "<http://art.uniroma2.it/name> \"http://test\\\\.it\"@en";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Test ciaiaiaiao end";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void pearlBaseRegexpWithOrWithoutTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/regexp/pearlBaseRegexpWithOrWithout.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/first> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";
        String rdfTriple2 = "<http://art.uniroma2.it/second> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "first and second*";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

}
