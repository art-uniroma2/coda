package it.uniroma2.art.coda.junittest;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import it.uniroma2.art.coda.core.UIMACODAUtilities;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.WrongAskedValueException;
import it.uniroma2.art.coda.exception.parserexception.MultipleResourcesRetrieved;
import it.uniroma2.art.coda.structures.CODATriple;
import it.uniroma2.art.coda.structures.NodeAssignment;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import org.apache.uima.UIMAException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.EmptyFSList;
import org.apache.uima.jcas.tcas.Annotation;
import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.junit.Test;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.Assert.*;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.impl.TemplateBasedRandomIdGenerator;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.NodeNotDefinedException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.exception.parserexception.PRSyntaxException;
import it.uniroma2.art.coda.pearl.model.ConverterArgumentExpression;
import it.uniroma2.art.coda.pearl.model.ConverterMapArgument;
import it.uniroma2.art.coda.pearl.model.ConverterMention;
import it.uniroma2.art.coda.pearl.model.ConverterPlaceholderArgument;
import it.uniroma2.art.coda.pearl.model.ConverterRDFLiteralArgument;
import it.uniroma2.art.coda.pearl.model.ConverterRDFURIArgument;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import it.uniroma2.art.coda.pearl.model.ProjectionOperator;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.test.contracts.AdditionalURIConverter;

public class BaseTest extends AbstractTest {

	/*****************************************************
	 * TEST BASE CODA
	 *
	 * @throws RDFModelNotSetException
	 * @throws UnassignableFeaturePathException
	 * @throws ProjectionRuleModelNotSet
	 * @throws
	 ******************************************************/

	// test the base PEARL structure
	@Test
	public void baseTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the base PEARL structure
	@Test
	public void baseTest2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	@Test
	public void baseAccentsTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseAccents.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://art.uniroma2.it/età_teorica> \"42\"";


		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	@Test
	public void baseBnodeTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBnodeBase.pr";
		setProjectionRule(pearlFileName);

		//SAME BNODE 1
		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> _:b0 ." +
				"\n_:b0 <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://art.uniroma2.it/Person>";

		//SAME BNODE 2
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> _:b0 ." +
				"\n_:b0 <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://art.uniroma2.it/City>";


		//DIFFERENT BNODEs
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> _:b0 ." +
				"\n_:b1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure where in a placeholder will be placed multiple values
	@Test
	public void baseMultipleValuesTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseMultipleValues.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea Armando Manuel live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		//check the annotation produced by CODA
		AnnotationIndex<Annotation> annotationIndex = jcas.getAnnotationIndex();
		FSIterator<Annotation> iter = annotationIndex.iterator();
		int countAnnPeople = 0;
		assertTrue(countAnnPeople==0);
		System.out.println("PRE countAnnPeople: "+countAnnPeople);
		while(iter.hasNext()){
			Annotation uimaAnnotation = iter.next();
			if(uimaAnnotation.getType().toString().equals("it.uniroma2.art.coda.test.ae.type.People")){ // old was People.class.getName()
				//increment the count of People Annotation (which is the only UIMA annotation type in the PEARL file)
				++countAnnPeople;
			}
		}
		assertTrue(countAnnPeople==1);

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure with condition IN
	@Test
	public void baseTestConditionIn()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConditionIn.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	@Test
	public void baseTestOptionalUnbound1()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseOptionalUnbound1.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://hello> <http://world> <http://!>";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Anonymous"; // anonymous does not have a name
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
	}

    /*
	@Test
	public void baseTestOptionalUnbound2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException, 
			  ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseOptionalUnbound2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://hello> <http://world> <http://!>";
		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Manuel Armando";
		JCas jcas = createAndExecuteAE(text);

		People peopleAnnotation = JCasUtil.selectSingle(jcas, People.class);

		// clear the person list
		peopleAnnotation.setPersonList(new EmptyFSList(jcas));
		peopleAnnotation.setNumber(0);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
	}
	 */

	@Test
	public void baseTestOptionalUnbound3()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseOptionalUnbound3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://hello> <http://world> <http://!>";
		String rdfTriple2 = NTriplesUtil.toNTriplesString(AdditionalURIConverter.constantURI)
				+ " <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Anonymous"; // anonymous does not have a name
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the base PEARL structure with condition IN (2)
	@Test
	public void baseTestConditionIn2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConditionIn2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure with condition IN (3)
	@Test
	public void baseTestConditionIn3()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConditionIn3.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure with condition IN (4)
	@Test
	public void baseTestConditionIn4()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConditionIn4.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure with condition IN (2)
	@Test
	public void baseTestConditionInCoveredText()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConditionInCoveredText.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure with condition NOT IN
	@Test
	public void baseTestConditionNotIn()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConditionNotIn.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure with condition NOT IN (2)
	@Test
	public void baseTestConditionNotIn2()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConditionNotIn2.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertFalse(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // the model should not contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the base PEARL structure with literals in the node section
	@Test
	public void baseTestNodeLiteral()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseNodeLiteral.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> " + "<http://art.uniroma2.it/name1> \"Rome\"@it";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://art.uniroma2.it/name2> \"Rome\"^^<http://www.w3.org/2001/XMLSchema#string>";
		String rdfTriple4 = "<http://art.uniroma2.it/Rome> "
				+ "<http://art.uniroma2.it/name3> \"Rome\"^^<http://www.w3.org/2001/XMLSchema#string>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	// test the base PEARL structure with literal in the graph section
	@Test
	public void baseTestLiteral()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseLiteral.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/name1> \"nome\"";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/name2> \"nome\"@it";
		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/name3> \"nome\"@it-IT";
		String rdfTriple4 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://art.uniroma2.it/name4> \"nome\"^^<http://www.w3.org/2001/XMLSchema#string>";
		String rdfTriple5 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://art.uniroma2.it/name5> \"nome\"^^<http://www.w3.org/2001/XMLSchema#string>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple5)); // now the model should contain the triple

	}

	// test the base PEARL structure with the "feature" coveredText
	@Test
	public void baseCoveredTextTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseCoveredText.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea Turbati lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// / test the base PEARL structure and passing to a converter not just a single string,
	// but a feature structure
	@Test
	public void baseFeatStructToConverterTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseFeatureValue.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/AndreaTurbati> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/ArmandoStellato> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea Turbati and Armando Stellato live in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the base PEARL structure and passing:
	// - a string to a converter which is expecting a string;
	// - a feature structure CompleteName to a converter which is CompleteName or a String;
	// - a String to a converter which is CompleteName or a String;
	@Test
	public void baseFeatStructToConverterCompleteTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseFeatureValueComplete.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/AndreaTurbati> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/AndreaTurbati> "
				+ "<http://art.uniroma2.it/firstName> \"Andrea\"";
		String rdfTriple3 = "<http://art.uniroma2.it/AndreaTurbati> "
				+ "<http://art.uniroma2.it/firstNameURI> <http://art.uniroma2.it/Andrea>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea Turbati lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	// test the super types enable feature
	@Test
	public void baseSuperTypesEnable()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseSingleWord.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	// test the super types disable feature
	@Test
	public void baseSuperTypesDisable()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseSingleWord.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Word>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// disable the super types feature
		codaCore.setUseSuperTypes(false);
		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
		// enable the super types feature for the future tests
		codaCore.setUseSuperTypes(true);

		assertFalse(executeAskQuery(rdfTriple1)); // the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // the model should not contain the triple
	}


	@Test
	public void baseDeterministicGenIDSKOSConcept()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseDetGenId.pr";
		setProjectionRule(pearlFileName);

		// Reference value for the HMAC based on SHA1SUM with key "9cf5e3ac9bc37a523024a884a03e7f6241ad26b8"
		// of "Rome" is xxx

		// This value can be computed with the following command (take only the first 16 hex digits):
		// echo -n "Rome" | openssl dgst -sha1 -mac hmac -macopt
		// hexkey:9cf5e3ac9bc37a523024a884a03e7f6241ad26b8

		String rdfTriple1 = "<http://art.uniroma2.it/c_22c44247ec5b9417> a <http://art.uniroma2.it/City>";
		String rdfTriple2 = "<http://art.uniroma2.it/c_22c44247ec5b9417> <http://www.w3.org/2008/05/skos-xl#prefLabel> <http://art.uniroma2.it/xl_22c44247ec5b9417>";
		String rdfTriple3 = "<http://art.uniroma2.it/xl_22c44247ec5b9417> <http://www.w3.org/2008/05/skos-xl#literalForm> \"Rome\"";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));

	}

	@Test
	public void baseDeterministicGenIDSKOSConceptDirectInvocation()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException {

		ConverterMention converterMention = new ConverterMention(
				ContractConstants.CODA_CONTRACTS_BASE_URI + "detGen-ConceptId");
		IRI uri = codaCore.executeURIConverter(converterMention, "Rome");

		assertThat(uri.stringValue(), is("http://art.uniroma2.it/c_22c44247ec5b9417"));
	}

	@Test
	public void baseRandomGenIDSKOSConceptDirectInvocation()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException {

		ConverterMention converterMention = new ConverterMention(
				ContractConstants.CODA_CONTRACTS_BASE_URI + "randGen-ConceptId");
		IRI uri = codaCore.executeURIConverter(converterMention, null);

		assertTrue(uri != null);
		assertTrue(uri.stringValue().matches("http://art.uniroma2.it/c_[0-9a-fA-F]+"));
	}

	@Test
	public void baseConverterWithArguments()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConvWithArgs.pr";
		setProjectionRule(pearlFileName);

		ProjectionRulesModel prModel = codaCore.getProjRuleModel();
		ProjectionRule projRuleModel = prModel.getProjRuleFromId("city");
		Map<String, PlaceholderStruct> ph2struct = projRuleModel.getPlaceholderMap();

		PlaceholderStruct namePhStruct = ph2struct.get("cityName");
		List<ConverterMention> nameConvList = namePhStruct.getConverterList();

		assertTrue(nameConvList.size() == 1);
		ConverterMention nameConvMention = nameConvList.get(0);
		assertTrue(nameConvMention.getURI().equals("http://art.uniroma2.it/coda/contracts/randIdGen"));
		assertTrue(nameConvMention.getAdditionalArguments().size() == 2);
		assertTrue(nameConvMention.getAdditionalArguments().get(0) instanceof ConverterRDFLiteralArgument);
		assertTrue(ConverterRDFLiteralArgument.class.cast(nameConvMention.getAdditionalArguments().get(0))
				.getLiteralValue().getLabel().equals("concept"));

		PlaceholderStruct xlabelNamePhStruct = ph2struct.get("xlabelName");
		List<ConverterMention> xlabelNameConvList = xlabelNamePhStruct.getConverterList();

		assertTrue(xlabelNameConvList.size() == 1);
		ConverterMention xlabelNameConvMention = xlabelNameConvList.get(0);
		assertTrue(xlabelNameConvMention.getURI().equals("http://art.uniroma2.it/coda/contracts/randIdGen"));
		assertTrue(xlabelNameConvMention.getAdditionalArguments().size() == 2);
		assertTrue(
				xlabelNameConvMention.getAdditionalArguments().get(0) instanceof ConverterRDFLiteralArgument);
		assertTrue(xlabelNameConvMention.getAdditionalArguments().get(1) instanceof ConverterMapArgument);
		assertTrue(
				ConverterRDFLiteralArgument.class.cast(xlabelNameConvMention.getAdditionalArguments().get(0))
						.getLiteralValue().getLabel().equals("xLabel"));
		@SuppressWarnings("unchecked")
		Map<String, ConverterArgumentExpression> args = ConverterMapArgument.class
				.cast(xlabelNameConvMention.getAdditionalArguments().get(1)).getMap();
		assertTrue(args.containsKey("lexicalForm")
				&& args.get("lexicalForm") instanceof ConverterPlaceholderArgument && "nameLabel".equals(
				ConverterPlaceholderArgument.class.cast(args.get("lexicalForm")).getPlaceholderId()));

		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		IRI cityResource = (IRI) connection.getStatements(
				null,
				RDF.TYPE,
				SimpleValueFactory.getInstance().createIRI("http://art.uniroma2.it/City")).next().getSubject();

		assertTrue(cityResource.getLocalName().startsWith("c_"));

		IRI cityXLabel = (IRI) connection.getStatements(
				cityResource,
				SKOSXL.PREF_LABEL,
				null).next().getObject();

		assertTrue(cityXLabel.getLocalName().startsWith("xl_"));
		assertTrue(cityXLabel.getLocalName().indexOf("en") != -1);
	}

	@Test
	public void pearlBaseConvWithMultivaluedPlaceholderArgs()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseConvWithMultivaluedPlaceholderArgs.pr";
		setProjectionRule(pearlFileName);

		ProjectionRulesModel prModel = codaCore.getProjRuleModel();
		ProjectionRule projRuleModel = prModel.getProjRuleFromId("people");
		Map<String, PlaceholderStruct> ph2struct = projRuleModel.getPlaceholderMap();

		PlaceholderStruct xlabelNamePhStruct = ph2struct.get("xlabelName");
		List<ConverterMention> xlabelNameConvList = xlabelNamePhStruct.getConverterList();

		assertTrue(xlabelNameConvList.size() == 1);
		ConverterMention xlabelNameConvMention = xlabelNameConvList.get(0);
		assertTrue(xlabelNameConvMention.getURI().equals("http://art.uniroma2.it/coda/contracts/randIdGen"));
		assertTrue(xlabelNameConvMention.getAdditionalArguments().size() == 2);
		assertTrue(
				xlabelNameConvMention.getAdditionalArguments().get(0) instanceof ConverterRDFLiteralArgument);
		assertTrue(xlabelNameConvMention.getAdditionalArguments().get(1) instanceof ConverterMapArgument);
		assertTrue(
				ConverterRDFLiteralArgument.class.cast(xlabelNameConvMention.getAdditionalArguments().get(0))
						.getLiteralValue().getLabel().equals("xLabel"));
		@SuppressWarnings("unchecked")
		Map<String, ConverterArgumentExpression> args = ConverterMapArgument.class
				.cast(xlabelNameConvMention.getAdditionalArguments().get(1)).getMap();

		assertTrue(args.containsKey("lexicalForm")
				&& args.get("lexicalForm") instanceof ConverterRDFLiteralArgument
				&& "en".equals(ConverterRDFLiteralArgument.class.cast(args.get("lexicalForm"))
				.getLiteralValue().getLanguage().get()));
		assertTrue(args.containsKey("lexicalizedResource")
				&& args.get("lexicalizedResource") instanceof ConverterPlaceholderArgument
				&& "personName".equals(ConverterPlaceholderArgument.class
				.cast(args.get("lexicalizedResource")).getPlaceholderId()));

		String text = "Andrea Armando Manuel";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		int numberOfTriples = 0;

		int manuelCounter = 0;
		int andreaCounter = 0;
		int armandoCounter = 0;

		RepositoryResult<Statement> repositoryResult = connection.getStatements(null, RDF.TYPE, SKOSXL.LABEL);

		while (repositoryResult.hasNext()) {
			Value node = repositoryResult.next().getSubject();
			numberOfTriples++;

			if (!(node instanceof IRI)) {
				continue;
			}

			IRI uriRes = (IRI) node;

			String namespace = uriRes.getNamespace();

			if (namespace.endsWith("/Manuel/")) {
				manuelCounter++;
			} else if (namespace.endsWith("/Andrea/")) {
				andreaCounter++;
			} else if (namespace.endsWith("/Armando/")) {
				armandoCounter++;
			}
		}

		assertTrue(numberOfTriples == 9);
		assertTrue(manuelCounter == 3);
		assertTrue(andreaCounter == 3);
		assertTrue(armandoCounter == 3);

	}

	@Test
	public void baseRandomGenIDSKOSConcept()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseRandGenId.pr";
		setProjectionRule(pearlFileName);

		// Checks precondintions
		RepositoryResult<Statement> conceptRepositoryResult = connection.getStatements(null, RDF.TYPE,
				connection.getValueFactory().createIRI("http://art.uniroma2.it/City"));
		assertTrue(conceptRepositoryResult.hasNext() == false);

		// Assert that two different xlabels have been defined
		RepositoryResult<Statement> xlabelsRepositoryResult = connection.getStatements(null, SKOSXL.LITERAL_FORM,
				null);
		assertTrue(xlabelsRepositoryResult.hasNext() == false);


		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// Assert that two different concepts (cities) have been defined
		conceptRepositoryResult = connection.getStatements(null, RDF.TYPE,
				connection.getValueFactory().createIRI("http://art.uniroma2.it/City"));
		List<Statement> statementsList = Iterations.asList(conceptRepositoryResult);
		assertTrue(statementsList.size()==2);

		// Assert that the two concept identifiers matches the required pattern
		for(Statement statement : statementsList){
			assertThat(statement.getSubject().stringValue(),
					matchesPattern(Pattern.quote("http://art.uniroma2.it/") + "c_[0-9a-f]{16}"));
		}

		// Assert that two different xlabels have been defined
		xlabelsRepositoryResult = connection.getStatements(null, SKOSXL.LITERAL_FORM,
				null);
		statementsList = Iterations.asList(xlabelsRepositoryResult);
		assertTrue(statementsList.size()==2);

		// Assert that the two xlabel identifiers matches the required pattern
		for(Statement statement : statementsList){
			assertThat(statement.getSubject().stringValue(),
					matchesPattern(Pattern.quote("http://art.uniroma2.it/") + "xl_[0-9a-f]{16}"));
		}

	}

	@Test
	public void baseNoFeatPath()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseNoFeatPath.pr";
		setProjectionRule(pearlFileName);

		// Checks precondintions
		RepositoryResult<Statement> conceptRepositoryResult = connection.getStatements(null, RDF.TYPE,
				connection.getValueFactory().createIRI("http://art.uniroma2.it/Time"));
		List<Statement> statementsList = Iterations.asList(conceptRepositoryResult);
		assertTrue(statementsList.size()==0);

		RepositoryResult<Statement> hasTimeRepositoryResult = connection.getStatements(null,
				connection.getValueFactory().createIRI("http://art.uniroma2.it/hasTime"), null);
		statementsList = Iterations.asList(conceptRepositoryResult);
		assertTrue(statementsList.size()==0);

		// create and execute the UIMA AE
		String text = "Rome is the capital of Italy";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// Assert that new triples has been created
		conceptRepositoryResult = connection.getStatements(null, RDF.TYPE,
				connection.getValueFactory().createIRI("http://art.uniroma2.it/Time"));
		statementsList = Iterations.asList(conceptRepositoryResult);
		assertTrue(statementsList.size()==1);

		hasTimeRepositoryResult = connection.getStatements(null,
				connection.getValueFactory().createIRI("http://art.uniroma2.it/hasTime"), null);
		statementsList = Iterations.asList(hasTimeRepositoryResult);
		assertTrue(statementsList.size()==2);
	}

	@Test
	public void baseMandatoryPlaceholder()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseMandatoryPlaceholder.pr";
		setProjectionRule(pearlFileName);

		// create and execute the UIMA AE
		String text = "Rome is the capital of Italy";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		Map<String, Boolean> plchldMandatoryMap = new HashMap<String, Boolean>();
		plchldMandatoryMap.put("cityName1", true);
		plchldMandatoryMap.put("cityName2", true);
		plchldMandatoryMap.put("cityName3", true);
		plchldMandatoryMap.put("cityName4", false);
		plchldMandatoryMap.put("cityName5", false);
		plchldMandatoryMap.put("cityName6", false);
		plchldMandatoryMap.put("cityName7", false);

		// Assert the mandatory state for each plaholder
		ProjectionRule projRule = codaCore.getProjRuleModel().getProjRule().get("city");
		Map<String, PlaceholderStruct> plchldMap = projRule.getPlaceholderMap();
		for (String plchldName : plchldMap.keySet()) {
			assertTrue(plchldMap.get(plchldName).isMandatoryInGraphSection() == plchldMandatoryMap
					.get(plchldName));
		}

	}

	@Test
	public void baseBindingsPlaceholder()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseBinding.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/name> \"Andrea\"";
		String rdfTriple4 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/name> \"Armando\"";
		String rdfTriple5 = "<http://art.uniroma2.it/Armando> " + "<http://art.uniroma2.it/name> \"Andrea\"";
		String rdfTriple6 = "<http://art.uniroma2.it/Armando> " + "<http://art.uniroma2.it/name> \"Armando\"";

		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

		assertFalse(executeAskQuery(rdfTriple1));
		assertFalse(executeAskQuery(rdfTriple2));
		assertFalse(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4));
		assertFalse(executeAskQuery(rdfTriple5));
		assertFalse(executeAskQuery(rdfTriple6));

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4));
		assertFalse(executeAskQuery(rdfTriple5));
		assertTrue(executeAskQuery(rdfTriple6));

	}

	@Test
	public void baseDatetimePlaceholder()
			throws PRParserException, UIMAException, ComponentProvisioningException, ConverterException,
			DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlDatetime.pr";
		setProjectionRule(pearlFileName);

		String rdfTripleDate = "<http://art.uniroma2.it/date> <" + RDF.VALUE + "> \"2016-07-22\"^^<" + XSD.DATE + ">";

		String rdfTripleTimeUndefined = "<http://art.uniroma2.it/timeUndefined> <" + RDF.VALUE + "> \"15:39:00\"^^<" + XSD.TIME + ">";
		String rdfTripleTimeZ = "<http://art.uniroma2.it/timeZ> <" + RDF.VALUE + "> \"15:39:00Z\"^^<" + XSD.TIME + ">";
		String rdfTripleTimeReuse = "<http://art.uniroma2.it/timeReuse> <" + RDF.VALUE + "> \"15:39:00+02:00\"^^<" + XSD.TIME + ">";
		String rdfTripleTimeReuseAdd = "<http://art.uniroma2.it/timeReuseAdd> <" + RDF.VALUE + "> \"15:39:00+05:00\"^^<" + XSD.TIME + ">";
		String rdfTripleTimeOffset = "<http://art.uniroma2.it/timeOffset> <" + RDF.VALUE + "> \"15:39:00+01:00\"^^<" + XSD.TIME + ">";
		String rdfTripleTimeOffsetAdd = "<http://art.uniroma2.it/timeOffsetAdd> <" + RDF.VALUE + "> \"15:39:00+04:00\"^^<" + XSD.TIME + ">";

		String rdfTripleDatetimeUndefined = "<http://art.uniroma2.it/datetimeUndefined> <" + RDF.VALUE + "> \"2016-07-22T15:39:00\"^^<" + XSD.DATETIME + ">";
		String rdfTripleDatetimeZ = "<http://art.uniroma2.it/datetimeZ> <" + RDF.VALUE + "> \"2016-07-22T15:39:00Z\"^^<" + XSD.DATETIME + ">";
		String rdfTripleDatetimeReuse = "<http://art.uniroma2.it/datetimeReuse> <" + RDF.VALUE + "> \"2016-07-22T15:39:00\"^^<" + XSD.DATETIME + ">";
		String rdfTripleDatetimeReuseAdd = "<http://art.uniroma2.it/datetimeReuseAdd> <" + RDF.VALUE + "> \"2016-07-22T15:39:00+03:00\"^^<" + XSD.DATETIME + ">";
		String rdfTripleDatetimeOffset = "<http://art.uniroma2.it/datetimeOffset> <" + RDF.VALUE + "> \"2016-07-22T15:39:00+01:00\"^^<" + XSD.DATETIME + ">";
		String rdfTripleDatetimeOffsetAdd = "<http://art.uniroma2.it/datetimeOffsetAdd> <" + RDF.VALUE + "> \"2016-07-22T15:39:00+04:00\"^^<" + XSD.DATETIME + ">";

		// create and execute the UIMA AE
		String text = "Today is 22 july 2016, it's 3:39 PM of the afternoon, 3:39 PM +02:00 with timezone."
				+ " Datetime is 2016-07-22T15:39:00";
		JCas jcas = createAndExecuteAE(text);

		assertFalse(executeAskQuery(rdfTripleDate));
		assertFalse(executeAskQuery(rdfTripleTimeUndefined));
		assertFalse(executeAskQuery(rdfTripleTimeZ));
		assertFalse(executeAskQuery(rdfTripleTimeReuse));
		assertFalse(executeAskQuery(rdfTripleTimeReuseAdd));
		assertFalse(executeAskQuery(rdfTripleTimeOffset));
		assertFalse(executeAskQuery(rdfTripleTimeOffsetAdd));
		assertFalse(executeAskQuery(rdfTripleDatetimeUndefined));
		assertFalse(executeAskQuery(rdfTripleDatetimeZ));
		assertFalse(executeAskQuery(rdfTripleDatetimeReuse));
		assertFalse(executeAskQuery(rdfTripleDatetimeReuseAdd));
		assertFalse(executeAskQuery(rdfTripleDatetimeOffset));
		assertFalse(executeAskQuery(rdfTripleDatetimeOffsetAdd));

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTripleDate));
		assertTrue(executeAskQuery(rdfTripleTimeUndefined));
		assertTrue(executeAskQuery(rdfTripleTimeZ));
		assertTrue(executeAskQuery(rdfTripleTimeReuse));
		assertTrue(executeAskQuery(rdfTripleTimeReuseAdd));
		assertTrue(executeAskQuery(rdfTripleTimeOffset));
		assertTrue(executeAskQuery(rdfTripleTimeOffsetAdd));
		assertTrue(executeAskQuery(rdfTripleDatetimeUndefined));
		assertTrue(executeAskQuery(rdfTripleDatetimeZ));
		assertTrue(executeAskQuery(rdfTripleDatetimeReuse));
		assertTrue(executeAskQuery(rdfTripleDatetimeReuseAdd));
		assertTrue(executeAskQuery(rdfTripleDatetimeOffset));
		assertTrue(executeAskQuery(rdfTripleDatetimeOffsetAdd));
	}

	// test the langString converter
	@Test
	public void langStringTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, IOException, ProjectionRuleModelNotSet,
			UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlLiteralWithPar.pr";
		setProjectionRule(pearlFileName);
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/2000/01/rdf-schema#label> 'Rome'@en";
		String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/2000/01/rdf-schema#label> 'Rome'@Rome";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
	}

	@Test
	public void specificConverterInvocationTest() throws ComponentProvisioningException, ConverterException {
		Value node = codaCore.executeURIConverter(TemplateBasedRandomIdGenerator.CONVERTER_URI);
		assertTrue(node.stringValue().startsWith(codaCore.getCODAContext().getDefaultNamespace()));
	}

	@Test
	public void parseConveterMentionTest() throws PRParserException {

		Map<String, String> prefixMapping = new HashMap<>();
		prefixMapping.put("coda", "http://art.uniroma2.it/coda/contracts/");
		ConverterMention converterMention = codaCore.parseConverterMention(
				"coda:randIdGen(\"concept\", {label = \"dog\"@en, scheme = <http://example.org/scheme>})",
				prefixMapping);

		assertEquals("http://art.uniroma2.it/coda/contracts/randIdGen", converterMention.getURI());
		assertEquals(2, converterMention.getAdditionalArguments().size());

		ConverterArgumentExpression firstArgumentExprTemp = converterMention.getAdditionalArguments().get(0);
		ConverterArgumentExpression secondArgumentExprTemp = converterMention.getAdditionalArguments().get(1);

		assertThat(firstArgumentExprTemp, is(instanceOf(ConverterRDFLiteralArgument.class)));
		ConverterRDFLiteralArgument firstArgumentExpr = (ConverterRDFLiteralArgument) firstArgumentExprTemp;
		assertThat(firstArgumentExpr.getLiteralValue(), is(equalTo(SimpleValueFactory.getInstance().createLiteral("concept"))));

		assertThat(secondArgumentExprTemp, is(instanceOf(ConverterMapArgument.class)));
		ConverterMapArgument secondArgumentExpr = (ConverterMapArgument) secondArgumentExprTemp;

		Map<String, ConverterArgumentExpression> underlyingMapArgument = secondArgumentExpr.getMap();

		assertEquals(2, underlyingMapArgument.size());
		assertThat(underlyingMapArgument, hasKey("label"));
		assertThat(underlyingMapArgument, hasKey("scheme"));

		assertThat(underlyingMapArgument.get("label"), both(is(instanceOf(ConverterRDFLiteralArgument.class)))
				.and(hasProperty("literalValue", equalTo(SimpleValueFactory.getInstance().createLiteral("dog", "en")))));
		assertThat(underlyingMapArgument.get("scheme"), both(is(instanceOf(ConverterRDFURIArgument.class)))
				.and(hasProperty("URI", equalTo(SimpleValueFactory.getInstance().createIRI("http://example.org/scheme")))));
	}

	@Test(expected = NodeNotDefinedException.class)
	public void parseNonGroundConveterMentionFailureTest() throws PRParserException {
		Map<String, String> prefixMapping = new HashMap<>();
		prefixMapping.put("coda", "http://art.uniroma2.it/coda/contracts/");

		@SuppressWarnings("unused")
		ConverterMention converterMention = codaCore.parseConverterMention(
				"coda:randIdGen(\"concept\", {label = $label, scheme = <http://example.org/scheme>})",
				prefixMapping);
	}

	@Test
	public void parseProjectionOperatorTest() throws PRParserException {

		Map<String, String> prefixMapping = new HashMap<>();
		prefixMapping.put("coda", "http://art.uniroma2.it/coda/contracts/");
		ProjectionOperator projectionOperator = codaCore.parseProjectionOperator(
				"uri(coda:randIdGen(\"concept\", {label = \"dog\"@en, scheme = <http://example.org/scheme>}))",
				prefixMapping);

		assertEquals(ProjectionOperator.NodeType.uri, projectionOperator.getNodeType());
		assertFalse(projectionOperator.getDatatype().isPresent());
		assertFalse(projectionOperator.getLanguage().isPresent());

		List<ConverterMention> converterMentions = projectionOperator.getConverterMentions();

		assertTrue(converterMentions.size() == 1);

		ConverterMention converterMention = converterMentions.get(0);

		assertEquals("http://art.uniroma2.it/coda/contracts/randIdGen", converterMention.getURI());
		assertEquals(2, converterMention.getAdditionalArguments().size());

		ConverterArgumentExpression firstArgumentExprTemp = converterMention.getAdditionalArguments().get(0);
		ConverterArgumentExpression secondArgumentExprTemp = converterMention.getAdditionalArguments().get(1);

		assertThat(firstArgumentExprTemp, is(instanceOf(ConverterRDFLiteralArgument.class)));
		ConverterRDFLiteralArgument firstArgumentExpr = (ConverterRDFLiteralArgument) firstArgumentExprTemp;
		assertThat(firstArgumentExpr.getLiteralValue(), is(equalTo(SimpleValueFactory.getInstance().createLiteral("concept"))));

		assertThat(secondArgumentExprTemp, is(instanceOf(ConverterMapArgument.class)));
		ConverterMapArgument secondArgumentExpr = (ConverterMapArgument) secondArgumentExprTemp;

		Map<String, ConverterArgumentExpression> underlyingMapArgument = secondArgumentExpr.getMap();

		assertEquals(2, underlyingMapArgument.size());
		assertThat(underlyingMapArgument, hasKey("label"));
		assertThat(underlyingMapArgument, hasKey("scheme"));

		assertThat(underlyingMapArgument.get("label"), both(is(instanceOf(ConverterRDFLiteralArgument.class)))
				.and(hasProperty("literalValue", equalTo(SimpleValueFactory.getInstance().createLiteral("dog", "en")))));
		assertThat(underlyingMapArgument.get("scheme"),
				both(is(instanceOf(ConverterRDFURIArgument.class))).and(hasProperty("URI",
						equalTo(SimpleValueFactory.getInstance().createIRI("http://example.org/scheme")))));
	}

	@Test
	public void projectionOperatorExecutionTest_simpleLiteral()
			throws PRParserException, ComponentProvisioningException, ConverterException {
		ProjectionOperator projectionOperator = codaCore.parseProjectionOperator("literal",
				Collections.emptyMap());
		Value actualNode = codaCore.executeProjectionOperator(projectionOperator, "dog");

		assertThat(actualNode, is(equalTo(SimpleValueFactory.getInstance().createLiteral("dog"))));
	}

	@Test
	public void projectionOperatorExecutionTest_languageTaggerLiteral()
			throws PRParserException, ComponentProvisioningException, ConverterException {
		ProjectionOperator projectionOperator = codaCore.parseProjectionOperator("literal@en",
				Collections.emptyMap());
		Value actualNode = codaCore.executeProjectionOperator(projectionOperator, "dog");

		assertThat(actualNode, is(equalTo(SimpleValueFactory.getInstance().createLiteral("dog", "en"))));
	}

	@Test
	public void projectionOperatorExecutionTest_typedLiteral()
			throws PRParserException, ComponentProvisioningException, ConverterException {
		Map<String, String> prefixMapping = new HashMap<>();
		prefixMapping.put("xsd", XSD.NAMESPACE);

		ProjectionOperator projectionOperator = codaCore.parseProjectionOperator("literal^^xsd:string", prefixMapping);
		Value actualNode = codaCore.executeProjectionOperator(projectionOperator, "dog");

		assertThat(actualNode, is(equalTo(
				SimpleValueFactory.getInstance().createLiteral("dog", XSD.STRING))));
	}

	@Test
	public void projectionOperatorExecutionTest_uri()
			throws PRParserException, ComponentProvisioningException, ConverterException {
		ProjectionOperator projectionOperator = codaCore.parseProjectionOperator("uri", Collections.emptyMap());
		Value actualNode = codaCore.executeProjectionOperator(projectionOperator, "dog");

		assertTrue(actualNode instanceof IRI);
		assertTrue(((IRI)actualNode).getLocalName().equals("dog"));
	}

	@Test(expected = PRSyntaxException.class)
	public void parseNonWellFormedProjectonOperatorFailureTest() throws PRParserException {
		Map<String, String> prefixMapping = new HashMap<>();
		prefixMapping.put("coda", "http://art.uniroma2.it/coda/contracts/");

		@SuppressWarnings("unused")
		ConverterMention converterMention = codaCore.parseConverterMention("iri@en(coda:default)",
				prefixMapping);
	}

	@Test
	public void baseTestTurtleCollectionConverter() throws PRParserException, UIMAException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseTurtleCollectionConverter.pr";
		setProjectionRule(pearlFileName);

//		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
//				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
//		String rdfTriple2 = "<http://art.uniroma2.it/Armando> "
//				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
//		String rdfTriple3 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/name> \"Andrea\"";
//		String rdfTriple4 = "<http://art.uniroma2.it/Andrea> " + "<http://art.uniroma2.it/name> \"Armando\"";
//		String rdfTriple5 = "<http://art.uniroma2.it/Armando> " + "<http://art.uniroma2.it/name> \"Andrea\"";
//		String rdfTriple6 = "<http://art.uniroma2.it/Armando> " + "<http://art.uniroma2.it/name> \"Armando\"";
//
		// create and execute the UIMA AE
		String text = "Andrea and Armando live in Rome";
		JCas jcas = createAndExecuteAE(text);

//		assertFalse(executeAskQuery(rdfTriple1));
//		assertFalse(executeAskQuery(rdfTriple2));
//		assertFalse(executeAskQuery(rdfTriple3));
//		assertFalse(executeAskQuery(rdfTriple4));
//		assertFalse(executeAskQuery(rdfTriple5));
//		assertFalse(executeAskQuery(rdfTriple6));

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());


		assertTrue(executeAskQuery("_:temp <http://www.w3.org/1999/02/22-rdf-syntax-ns#value> \"(" +
				"\\\"Rome\\\" \\\"Rome\\\" \\\"Rome\\\")\" . "));

//		assertTrue(executeAskQuery(rdfTriple3));
//		assertFalse(executeAskQuery(rdfTriple4));
//		assertFalse(executeAskQuery(rdfTriple5));
//		assertTrue(executeAskQuery(rdfTriple6));
	}

	@Test
	public void baseTestGetUsedPrefixes() throws PRParserException, UIMAException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

		String prefixT1 = "skos";
		String prefixT2 = "my";
		String prefixT3 = "xsd";

		String prefixF1 = "skosxl";
		String prefixF2 = "coda";
		String prefixF3 = "rdf";


		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBaseUsedPrefixes.pr";
		ProjectionRulesModel prModel = setProjectionRule(pearlFileName);

		List<String> usedPrefixList = prModel.getUsedPrefixedList();

		assertTrue(usedPrefixList.contains(prefixT1));
		assertTrue(usedPrefixList.contains(prefixT2));
		assertTrue(usedPrefixList.contains(prefixT3));

		assertFalse(usedPrefixList.contains(prefixF1));
		assertFalse(usedPrefixList.contains(prefixF2));
		assertFalse(usedPrefixList.contains(prefixF3));


	}


	@Test
	public void overrideDefaultNSTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet,
			UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/overrideDefaultNS.pr";
		setProjectionRule(pearlFileName);
		String rdfTriple1 = "<http://overridden.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		// System.out.println("************ PROJECTION RULE MODEL
		// ************\n"+codaCore.getProjRuleModel().getModelAsString());

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
	}

	@Test
	public void NodeAssignmentNotPresent1() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		int countStep = 0;
		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			++countStep;
			SuggOntologyCoda suggOntologyCoda = suggOntologyCodaList.get(0);
			++countStep;
			suggOntologyCoda.getNodeNameToNodeAssignmentMap();
			++countStep;
			assertTrue(false);
		} catch (ValueNotPresentDueToConfigurationException e) {
			assertTrue(countStep==2);
		}
	}

	@Test
	public void NodeAssignmentNotPresent2() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		int countStep = 0;
		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName(),
					false, true);
			++countStep;
			SuggOntologyCoda suggOntologyCoda = suggOntologyCodaList.get(0);
			++countStep;
			suggOntologyCoda.getNodeNameToNodeAssignmentMap();
			++countStep;
			assertTrue(false);
		} catch (ValueNotPresentDueToConfigurationException e) {
			assertTrue(countStep==2);
		}
	}

	@Test
	public void NodeAssignmentPresent1() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName(),
					true, false);
			IRI iri=null;
			for (SuggOntologyCoda suggOntologyCoda : suggOntologyCodaList) {
				Map<String, NodeAssignment> nodeNameToNodeAssignmentMap = suggOntologyCoda.getNodeNameToNodeAssignmentMap();
				NodeAssignment nodeAssignment = nodeNameToNodeAssignmentMap.get("cityName");
				if(nodeAssignment!=null) {
					iri = (IRI) nodeAssignment.getSingleValue();
				}
			}
			assertTrue(iri.stringValue().equals("http://art.uniroma2.it/Rome"));
		} catch (ValueNotPresentDueToConfigurationException | WrongAskedValueException e) {
			assertTrue(false);
		}
	}

	@Test
	public void NodeAssignmentPresent2() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName(),
					true, true);
			IRI iri=null;
			for (SuggOntologyCoda suggOntologyCoda : suggOntologyCodaList) {
				Map<String, NodeAssignment> nodeNameToNodeAssignmentMap = suggOntologyCoda.getNodeNameToNodeAssignmentMap();
				NodeAssignment nodeAssignment = nodeNameToNodeAssignmentMap.get("cityName");
				if(nodeAssignment!=null) {
					iri = (IRI) nodeAssignment.getSingleValue();
				}
			}
			assertTrue(iri.stringValue().equals("http://art.uniroma2.it/Rome"));
		} catch (ValueNotPresentDueToConfigurationException | WrongAskedValueException e) {
			assertTrue(false);
		}
	}

	@Test
	public void SubjectNameNotPresent1() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		int countStep = 0;
		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			++countStep;
			SuggOntologyCoda suggOntologyCoda = suggOntologyCodaList.get(3);
			++countStep;
			List<CODATriple> codaTripleList = suggOntologyCoda.getInsertARTTripleListFromBaseProjRuleId("city");
			++countStep;
			CODATriple codaTriple = codaTripleList.get(0);
			++countStep;
			codaTriple.getSubjectNameInGraph();
			++countStep;
			assertTrue(false);
		} catch (ValueNotPresentDueToConfigurationException e) {
			assertTrue(countStep==4);
		}
	}

	@Test
	public void SubjectNameNotPresent2() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		int countStep = 0;
		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName(),
					true, false);
			++countStep;
			SuggOntologyCoda suggOntologyCoda = suggOntologyCodaList.get(3);
			++countStep;
			List<CODATriple> codaTripleList = suggOntologyCoda.getInsertARTTripleListFromBaseProjRuleId("city");
			++countStep;
			CODATriple codaTriple = codaTripleList.get(0);
			++countStep;
			codaTriple.getSubjectNameInGraph();
			++countStep;
			assertTrue(false);
		} catch (ValueNotPresentDueToConfigurationException e) {
			assertTrue(countStep==4);
		}
	}

	@Test
	public void SubjectNamePresent1() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		int countStep = 0;
		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName(),
					false, true);
			SuggOntologyCoda suggOntologyCoda = suggOntologyCodaList.get(3);
			List<CODATriple> codaTripleList = suggOntologyCoda.getInsertARTTripleListFromBaseProjRuleId("city");

			String subjectName = null;
			for(CODATriple codaTriple : codaTripleList) {
				subjectName = codaTriple.getSubjectNameInGraph();
			}
			assertTrue(subjectName.equals("$cityName"));
		} catch (ValueNotPresentDueToConfigurationException e) {
			assertTrue(false);
		}
	}

	@Test
	public void SubjectNamePresent2() throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/pearlBase.pr";
		setProjectionRule(pearlFileName);

		String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
		String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		int countStep = 0;
		try {
			// pass the jcas to CODA and execute to populate/enrich the ontology
			List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName(),
					true, true);
			SuggOntologyCoda suggOntologyCoda = suggOntologyCodaList.get(3);
			List<CODATriple> codaTripleList = suggOntologyCoda.getInsertARTTripleListFromBaseProjRuleId("city");

			String subjectName = null;
			for(CODATriple codaTriple : codaTripleList) {
				subjectName = codaTriple.getSubjectNameInGraph();
			}
			assertTrue(subjectName.equals("$cityName"));
		} catch (ValueNotPresentDueToConfigurationException e) {
			assertTrue(false);
		}
	}

	@Test
	public void propPathIDResolver1Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/propPathIDResolver.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present


		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	@Test
	public void propPathIDResolver2Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/propPathIDResolver.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.ALT_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.ALT_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present


		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	@Test
	public void propPathIDResolverChainTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/propPathIDResolver_Chain.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.HIDDEN_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		IRI knownAsIRI = SimpleValueFactory.getInstance().createIRI(knowAsUriString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.HIDDEN_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present


		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present since it was used the
		// value from the first converter (coda:randIdGen) and not the one from the FS

		IRI subject = (IRI) connection.getStatements(null, knownAsIRI, andreaItLiteral).stream().iterator().next().getSubject();

		assertNotEquals(andreaURIString, subject.stringValue());
	}

	@Test
	public void propPathIDResolver3Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/propPathIDResolver.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.HIDDEN_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		IRI knownAsIRI = SimpleValueFactory.getInstance().createIRI(knowAsUriString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.HIDDEN_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present


		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // this triple now it is present in the model, even if coda:propPathIDResolver
		// was not able to retrieve any resource, the converter used the FS to create the URI in the default way

		IRI subject = (IRI) connection.getStatements(null, knownAsIRI, andreaItLiteral).stream().iterator().next().getSubject();

		assertEquals(andreaURIString, subject.stringValue());
	}


	@Test
	public void propPathIDResolver4Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/propPathIDResolver2.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String fallbackUriString = "http://test.it/fallback/Andrea";
		String andreaItLiteralString = "\"Andrea\"@it";
		String andreaEnLiteralString = "\"Andrea\"@en";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+fallbackUriString+"> <"+knowAsUriString+">" +andreaEnLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		IRI fallbackUri = SimpleValueFactory.getInstance().createIRI(fallbackUriString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present


		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	@Test
	public void propPathIDResolverExceptionTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/propPathIDResolverException.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present


		try {
			// create and execute the UIMA AE
			//String text = "Andrea and Tiziano work on CODA";
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);

			// pass the jcas to CODA and execute to populate/enrich the ontology
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

			fail();
		} catch (ConverterException e) {
			assertTrue(true);
		}
	}

	@Test
	public void propPathIDResolverException2Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/propPathIDResolver.pr";
		setProjectionRule(pearlFileName);


		String andrea1URIString = "http://art.uniroma2.it/Andrea1";
		String andrea2URIString = "http://art.uniroma2.it/Andrea2";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabel1URIString = "http://art.uniroma2.it/xl_it_123-1";
		String xLabel2URIString = "http://art.uniroma2.it/xl_it_123-2";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";

		String rdfTriple1  = "<"+andrea1URIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andrea1URIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabel1URIString+">";
		String rdfTriple3  = "<"+xLabel1URIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andrea1URIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		String rdfTriple5  = "<"+andrea2URIString+"> a <"+personUriString+">";
		String rdfTriple6  = "<"+andrea2URIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabel2URIString+">";
		String rdfTriple7  = "<"+xLabel2URIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple8  = "<"+andrea2URIString+"> <"+knowAsUriString+">" +andreaItLiteralString;


		IRI andrea1Uri = SimpleValueFactory.getInstance().createIRI(andrea1URIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabel1URI = SimpleValueFactory.getInstance().createIRI(xLabel1URIString);
		IRI andrea2Uri = SimpleValueFactory.getInstance().createIRI(andrea2URIString);
		IRI xLabel2URI = SimpleValueFactory.getInstance().createIRI(xLabel2URIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple5)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple6)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple7)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple8)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andrea1Uri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andrea1Uri, SKOSXL.PREF_LABEL, xLabel1URI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabel1URI, SKOSXL.LITERAL_FORM, andreaItLiteral));
		connection.add(SimpleValueFactory.getInstance().createStatement(andrea2Uri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andrea2Uri, SKOSXL.PREF_LABEL, xLabel2URI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabel2URI, SKOSXL.LITERAL_FORM, andreaItLiteral));

		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present
		assertTrue(executeAskQuery(rdfTriple5));
		assertTrue(executeAskQuery(rdfTriple6));
		assertTrue(executeAskQuery(rdfTriple7));
		assertFalse(executeAskQuery(rdfTriple8)); // this triple should still be not present


		try {
			// create and execute the UIMA AE
			//String text = "Andrea and Tiziano work on CODA";
			String text = "Andrea lives in Rome";
			JCas jcas = createAndExecuteAE(text);

			// pass the jcas to CODA and execute to populate/enrich the ontology
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

			fail();
		} catch (ConverterException e) {
			assertTrue(true);
			assertTrue(e instanceof MultipleResourcesRetrieved);
			assertTrue(e.getMessage().contains(andreaItLiteralString));
		}
	}


	@Test
	public void lexiconIDResolver1Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/lexiconIDResolver1.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present


		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	@Test
	public void lexiconIDResolverChain1Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/lexiconIDResolverChain1.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present

		//set the LexModelForSearch as RDFS_MODEL
		codaCore.getCODAContext().setLexModelForSearch(UIMACODAUtilities.LexModelForSearch.RDFS_MODEL);

		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple4)); // the model should not contain the triple
	}

	@Test
	public void lexiconIDResolver3Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/lexiconIDResolver1.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present

		//set the LexModelForSearch as RDFS_MODEL
		codaCore.getCODAContext().setLexModelForSearch(UIMACODAUtilities.LexModelForSearch.SKOSXL_MODEL);

		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	@Test
	public void lexiconIDResolver4Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/lexiconIDResolver2.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present

		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertFalse(executeAskQuery(rdfTriple4)); // the model should not contain the triple
	}

	@Test
	public void lexiconIDResolver5Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/lexiconIDResolver3.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String fallbackUriString = "http://test.it/fallback/Andrea";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+fallbackUriString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present

		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}

	@Test
	public void lexiconIDResolver6Test()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/lexiconIDResolver4.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present

		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

		assertTrue(executeAskQuery(rdfTriple4)); // now the model should contain the triple
	}


	@Test
	public void lexiconIDResolverExceptionTest()
			throws UIMAException, PRParserException, ComponentProvisioningException,
			ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException  {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/base/lexiconIDResolverException.pr";
		setProjectionRule(pearlFileName);


		String andreaURIString = "http://art.uniroma2.it/Andrea";
		String personUriString = "http://art.uniroma2.it/Person";
		String xLabelURIString = "http://art.uniroma2.it/xl_it_123";
		String knowAsUriString = "http://art.uniroma2.it/knownAs";
		String andreaItLiteralString = "\"Andrea\"@it";
		String rdfTriple1  = "<"+andreaURIString+"> a <"+personUriString+">";
		String rdfTriple2  = "<"+andreaURIString+"> <"+SKOSXL.PREF_LABEL.stringValue()+"> <" +xLabelURIString+">";
		String rdfTriple3  = "<"+xLabelURIString+"> <"+SKOSXL.LITERAL_FORM.stringValue()+">" +andreaItLiteralString;
		String rdfTriple4  = "<"+andreaURIString+"> <"+knowAsUriString+">" +andreaItLiteralString;

		IRI andreaUri = SimpleValueFactory.getInstance().createIRI(andreaURIString);
		IRI personUri = SimpleValueFactory.getInstance().createIRI(personUriString);
		IRI xLabelURI = SimpleValueFactory.getInstance().createIRI(xLabelURIString);
		Literal andreaItLiteral = SimpleValueFactory.getInstance().createLiteral("Andrea", "it");

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple
		assertFalse(executeAskQuery(rdfTriple4)); // at the beginning the model should not contain the triple

		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, RDF.TYPE, personUri));
		connection.add(SimpleValueFactory.getInstance().createStatement(andreaUri, SKOSXL.PREF_LABEL, xLabelURI));
		connection.add(SimpleValueFactory.getInstance().createStatement(xLabelURI, SKOSXL.LITERAL_FORM, andreaItLiteral));


		assertTrue(executeAskQuery(rdfTriple1));
		assertTrue(executeAskQuery(rdfTriple2));
		assertTrue(executeAskQuery(rdfTriple3));
		assertFalse(executeAskQuery(rdfTriple4)); // this triple should still be not present

		// create and execute the UIMA AE
		//String text = "Andrea and Tiziano work on CODA";
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		try {
			executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
			fail();
		} catch (ConverterException e) {
			assertTrue(e.getMessage().contains("WRONG"));
		}
	}

}
