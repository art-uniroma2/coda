package it.uniroma2.art.coda.junittest;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.exception.parserexception.PRSyntaxException;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.pearl.parser.antlr4.AntlrParserRuntimeException;

public class UtilsTest extends AbstractTest {

	
	@Test
	public void parseWithoutRDFModelTest() {
		
		String pearlFileName = "it/uniroma2/art/coda/junittest/utils/pearlBase.pr";
		CODACore codaCodeForTest = new CODACore(null);
		
		try {
			ProjectionRulesModel projectionRulesModel = 
					codaCodeForTest.setProjectionRulesModel(getStreamOfAFile(pearlFileName));
			assertTrue(true);
		} catch (PRParserException e) {
			assertTrue(false);
		}
		
	}
	
	@Test
	public void parsePEARLFileWithSyntacticProblemTest(){
		String pearlFileName = "it/uniroma2/art/coda/junittest/utils/wrongSyntax.pr";
		CODACore codaCodeForTest = new CODACore(null);
		
		try {
			ProjectionRulesModel projectionRulesModel = 
					codaCodeForTest.setProjectionRulesModel(getStreamOfAFile(pearlFileName));
			assertTrue(false);
			
		} catch (AntlrParserRuntimeException e) {
			assertTrue(false);
		} catch (PRSyntaxException e){
			assertTrue(true);
			//System.out.println(e.getErrorAsString());
		} catch (PRParserException e) {
			assertTrue(false);
		} 
		
	}
	

	
	
}
