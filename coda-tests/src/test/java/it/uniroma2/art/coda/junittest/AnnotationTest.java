package it.uniroma2.art.coda.junittest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.AnnotationTargetNotCompatibleException;
import it.uniroma2.art.coda.exception.parserexception.MissingMandatoryParamInAnnotationException;
import it.uniroma2.art.coda.exception.parserexception.TypeOfParamInAnnotationWrongTypeException;
import it.uniroma2.art.coda.pearl.model.PlaceholderStruct;
import it.uniroma2.art.coda.pearl.model.annotation.MetaAnnotationGeneric;
import it.uniroma2.art.coda.pearl.model.annotation.param.ParamValueInterface;
import it.uniroma2.art.coda.pearl.parser.antlr4.PearlParserDescription;
import it.uniroma2.art.coda.structures.CODATriple;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.pearl.model.GraphStruct;
import it.uniroma2.art.coda.pearl.model.ProjectionRule;
import it.uniroma2.art.coda.pearl.model.ProjectionRulesModel;
import it.uniroma2.art.coda.pearl.model.annotation.Annotation;
import it.uniroma2.art.coda.pearl.model.annotation.AnnotationDefinition;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;

public class AnnotationTest extends AbstractTest {

    private final int NUM_ANNOTATION_HARDCODED = 12;

    // the number of implicit annotation defined in CODA (Confidence, Memoized and DefaultNamespace)
    private int implicitAnnotationNum = 3;


    private List<String> getAnnotationForATriple(String triple, List<SuggOntologyCoda> suggOntoCodaList) {
        List<String> annotationsList = new ArrayList<>();

        for (SuggOntologyCoda suggOntologyCoda : suggOntoCodaList) {
            for (CODATriple codaTriple : suggOntologyCoda.getAllInsertARTTriple()) {
                String artTripleString = "<" + codaTriple.getSubject().stringValue() + "> <"
                        + codaTriple.getPredicate() + "> ";
                if (codaTriple.getObject() instanceof IRI) {
                    artTripleString += "<" + codaTriple.getObject().stringValue() + ">";
                } else if (codaTriple.getObject() instanceof Literal) {
                    Literal objLit = (Literal) codaTriple.getObject();
                    artTripleString += "\"" + objLit.stringValue() + "\"";
                    if (objLit.getLanguage().isPresent()) {
                        artTripleString += "@" + objLit.getLanguage();
                    } else if (objLit.getDatatype() != null) {
                        artTripleString += "^^<" + objLit.getDatatype().stringValue() + ">";
                    }
                }

                if (triple.equals(artTripleString)) {
                    List<Annotation> gsaList = codaTriple.getListAnnotations();
                    if (gsaList == null)
                        continue;
                    for (Annotation gsa : gsaList) {
                        String annotation = gsa.getName() + "(" + getParametersFromAnnotation(gsa) + ")";
                        annotationsList.add(annotation);
                    }
                }
                //annotationsList.add(artTripleString);
            }
        }
        return annotationsList;
    }

    private String getParametersFromAnnotation(Annotation annotation) {
        String paramListAsString = "";
        boolean first = true;
        //get the target parameters from the annotation
        List<String> targetList = annotation.getTargetList();
        if (targetList.size() > 0) {
            first = false;
            paramListAsString += PearlParserDescription.TARGET_PARAM + "=";
        }
        if (targetList.size() == 1) {
            paramListAsString += targetList.get(0);
        } else {
            boolean firstValue = true;
            paramListAsString += "{";
            for (String target : targetList) {
                if (!firstValue) {
                    paramListAsString += ",";
                }
                firstValue = false;
                paramListAsString += target;
            }
            paramListAsString += "}";
        }
        //get the standard paramaters from the annotation
        Map<String, List<ParamValueInterface>> paramMap = annotation.getParamMap();
        for (String paramName : paramMap.keySet()) {
            if (!first) {
                paramListAsString += ",";
            }
            first = false;
            List<ParamValueInterface> paramValueList = annotation.getParamValueList(paramName);
            paramListAsString += paramName + "=";
            if (paramValueList.size() == 1) {
                paramListAsString += paramValueList.get(0);
            } else {
                paramListAsString += "{";
                boolean firstValue = true;
                for (ParamValueInterface paramValue : paramValueList) {
                    if (!firstValue) {
                        paramListAsString += ",";
                    }
                    firstValue = false;
                    paramListAsString += paramValue;
                }
                paramListAsString += "}";
            }
        }

        return paramListAsString;
    }

    private double getConfidenceForATriple(String triple, List<SuggOntologyCoda> suggOntoCodaList) {

        for (SuggOntologyCoda suggOntologyCoda : suggOntoCodaList) {
            for (CODATriple codaTriple : suggOntologyCoda.getAllInsertARTTriple()) {
                String artTripleString = "<" + codaTriple.getSubject().stringValue() + "> <"
                        + codaTriple.getPredicate() + "> ";
                if (codaTriple.getObject() instanceof IRI) {
                    artTripleString += "<" + codaTriple.getObject().stringValue() + ">";
                } else if (codaTriple.getObject() instanceof Literal) {
                    Literal objLit = (Literal) codaTriple.getObject();
                    artTripleString += "\"" + objLit.stringValue() + "\"";
                    if (objLit.getLanguage().isPresent()) {
                        artTripleString += "@" + objLit.getLanguage();
                    } else if (objLit.getDatatype() != null) {
                        artTripleString += "^^<" + objLit.getDatatype().stringValue() + ">";
                    }
                }

                if (triple.equals(artTripleString)) {
                    return codaTriple.getConfidence();
                }
            }
        }

        // the confidence was not found, so return 0
        return 0;
    }

    // this test does not really use the annotation in any way, it just check if CODA is able to parse the
    // file
    @Test
    public void baseAnnotationTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/pearlBaseAnnotation.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void metaAnnotationTestRightTarget() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException {
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/metaAnnotationTestRightTarget.pr";
        setProjectionRule(pearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");
        AnnotationDefinition plcAnn = prModel.getAnnotationDefinition("plcAnn");

        assertTrue(isClassAnn != null);
        assertTrue(plcAnn != null);

        assertTrue(isClassAnn.hasRetention());
        assertFalse(plcAnn.hasRetention());

        assertTrue(isClassAnn.checkTargetCompatibilityWithGraph());
        assertTrue(isClassAnn.checkTargetValueCompatibility("subject"));
        assertTrue(isClassAnn.checkTargetValueCompatibility("object"));
        assertFalse(isClassAnn.checkTargetValueCompatibility("predicate"));

        assertTrue(plcAnn.checkTargetCompatibilityWithNodes());

        ProjectionRule projRule = codaCore.getProjRuleModel().getProjRuleFromId("city");
        List<Annotation> nsaList = projRule.getInsertGraphList().iterator().next().asGraphStruct().getAnnotationList();
        assertTrue(nsaList.size() == 1);
    }

    @Test
    public void nodeAnnotationTestWrongTarget() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/metaAnnotationTestWrongTarget.pr";
        try {
            setProjectionRule(pearlFileName);
            assertTrue(false);
        } catch (AnnotationTargetNotCompatibleException e) {
            assertTrue(e.getAnnName().equals("isClass"));
            assertTrue(e.getRuleId().equals("city"));
            assertTrue(e.getTargetNotCompatible().equals("object"));
        }
    }

    @Test
    public void nodeAnnotationAnnPlc() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/nodeAnnotationAnnPlc.pr";
        setProjectionRule(pearlFileName);
        ProjectionRule projRule1 = codaCore.getProjRuleModel().getProjRuleFromId("city");
        List<Annotation> annList1 = projRule1.getPlaceholderMap().get("cityName").getAnnotationList();

        ProjectionRule projRule2 = codaCore.getProjRuleModel().getProjRuleFromId("person");
        List<Annotation> annList2 = projRule2.getPlaceholderMap().get("personName").getAnnotationList();

        assertTrue(annList1.size() == 1);
        assertTrue(annList2.isEmpty());
    }

    @Test
    public void metaAnnotationDefaultTarget() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/metaAnnotationDefaultTarget.pr";
        setProjectionRule(pearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");

        assertTrue(isClassAnn != null);

        assertTrue(isClassAnn.hasRetention());

        assertTrue(isClassAnn.checkTargetCompatibilityWithGraph());
        assertTrue(isClassAnn.checkTargetValueCompatibility("subject"));
        assertTrue(isClassAnn.checkTargetValueCompatibility("object"));
        assertTrue(isClassAnn.checkTargetValueCompatibility("triple"));
        assertFalse(isClassAnn.checkTargetValueCompatibility("predicate"));

        ProjectionRule projRule = codaCore.getProjRuleModel().getProjRuleFromId("city");
        GraphStruct graph = projRule.getInsertGraphList().iterator().next().asGraphStruct();
        List<Annotation> nsaList = graph.getAnnotationList();
        assertTrue(nsaList.size() == 1);
    }

    @Test
    public void metaAnnotationDefaultTarget2() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/metaAnnotationDefaultTarget2.pr";
        setProjectionRule(pearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");

        assertTrue(isClassAnn != null);

        assertTrue(isClassAnn.hasRetention());

        assertTrue(isClassAnn.checkTargetCompatibilityWithGraph());
        assertTrue(isClassAnn.checkTargetValueCompatibility("subject"));
        assertFalse(isClassAnn.checkTargetValueCompatibility("object"));
        assertFalse(isClassAnn.checkTargetValueCompatibility("predicate"));

        ProjectionRule projRule = codaCore.getProjRuleModel().getProjRuleFromId("city");
        GraphStruct graph = projRule.getInsertGraphList().iterator().next().asGraphStruct();
        List<Annotation> nsaList = graph.getAnnotationList();
        assertTrue(nsaList.size() == 1);
    }

    @Test
    public void baseGraphAnnotationTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/pearlGraphAnnotation.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        String annotation = "isClass(target=object)";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        GraphStruct graphStruct = prModel.getProjRuleFromId("city").getInsertGraphList().iterator().next()
                .asGraphStruct();
        List<Annotation> nsaList = graphStruct.getAnnotationList();
        assertTrue(nsaList.size() == 1);

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        List<String> annList = getAnnotationForATriple(rdfTriple1, suggOntologyCodaList);

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(annList.contains(annotation));
    }

	/*@Test
	public void baseGraphAnnotationTest2() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
			ProjectionRuleModelNotSet, UnassignableFeaturePathException {

		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/pearlGraphAnnotation2.pr";
		setProjectionRule(pearlFileName);
		String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
				+ "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
		String annotation = "isClass(target=object)";

		assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

		ProjectionRulesModel prModel = codaCore.getProjRuleModel();
		GraphStruct graphStruct = prModel.getProjRuleFromId("city").getInsertGraphList().iterator().next()
				.asGraphStruct();
		List<Annotation> nsaList = graphStruct.getAnnotationList();
		assertTrue(nsaList.size() == 0);

		// create and execute the UIMA AE
		String text = "Andrea lives in Rome";
		JCas jcas = createAndExecuteAE(text);

		// pass the jcas to CODA and execute to populate/enrich the ontology
		List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
				Thread.currentThread().getStackTrace()[1].getMethodName());
		List<String> annList = getAnnotationForATriple(rdfTriple1, suggOntologyCodaList);

		assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
		assertTrue(!annList.contains(annotation));
	}*/

    @Test
    public void checkTargetFromDefinitionTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/checkTargetFromDefinition.pr";
        setProjectionRule(pearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        GraphStruct graphStruct = prModel.getProjRuleFromId("city").getInsertGraphList().iterator().next()
                .asGraphStruct();
        List<Annotation> nsaList = graphStruct.getAnnotationList();
        assertTrue(nsaList.size() == 1);

        Annotation annotation = nsaList.get(0);
        assertTrue(annotation.getName().equals("isClass"));
        List<String> targetList = annotation.getTargetList();
        assertTrue(targetList.size() == 2);
        for (String target : targetList) {
            assertTrue(target.equals("subject") || target.equals("object"));
        }
    }

    @Test
    public void moreNodesAnnotationTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException {
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/moreNodesAnnotationTest.pr";
        setProjectionRule(pearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");
        AnnotationDefinition plcAnn1 = prModel.getAnnotationDefinition("plcAnn1");
        AnnotationDefinition plcAnn2 = prModel.getAnnotationDefinition("plcAnn2");

        assertTrue(isClassAnn != null);
        assertTrue(plcAnn1 != null);
        assertTrue(plcAnn2 != null);

        assertTrue(isClassAnn.hasRetention());
        assertFalse(plcAnn1.hasRetention());

        assertFalse(isClassAnn.checkTargetCompatibilityWithNodes());
        assertTrue(plcAnn1.checkTargetCompatibilityWithNodes());
        assertTrue(plcAnn2.checkTargetCompatibilityWithNodes());

        ProjectionRule projRule = codaCore.getProjRuleModel().getProjRuleFromId("city");
        List<Annotation> annList = projRule.getPlaceholderMap().get("cityName").getAnnotationList();
        assertTrue(annList.size() == 2);
    }

    @Test
    public void moreGraphAnnotationTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/moreGraphAnnotationTest.pr";
        setProjectionRule(pearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");
        AnnotationDefinition plcAnn1 = prModel.getAnnotationDefinition("plcAnn1");
        AnnotationDefinition plcAnn2 = prModel.getAnnotationDefinition("plcAnn2");

        assertTrue(isClassAnn != null);
        assertTrue(plcAnn1 != null);
        assertTrue(plcAnn2 != null);

        assertTrue(isClassAnn.hasRetention());

        assertTrue(isClassAnn.checkTargetCompatibilityWithGraph());
        assertTrue(plcAnn1.checkTargetCompatibilityWithGraph());
        assertFalse(plcAnn2.checkTargetCompatibilityWithGraph());

        ProjectionRule projRule = codaCore.getProjRuleModel().getProjRuleFromId("city");
        List<Annotation> annList = projRule.getInsertGraphList().iterator().next().asGraphStruct()
                .getAnnotationList();
        assertTrue(annList.size() == 2);
    }

    @Test
    public void variousParamAnnotationTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {
        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/variousParamAnnotationTest.pr";
        setProjectionRule(pearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");
        assertTrue(isClassAnn != null);
        assertTrue(isClassAnn.hasRetention());
        assertTrue(isClassAnn.checkTargetCompatibilityWithGraph());
        MetaAnnotationGeneric genericMetaAnn = (MetaAnnotationGeneric) isClassAnn.getMetaAnnotation("Number");
        assertTrue("123456".equals(genericMetaAnn.getValueList("value").get(0).toString()));

        AnnotationDefinition isClassAnn2 = prModel.getAnnotationDefinition("isClass2");
        assertTrue(isClassAnn2 != null);
        assertFalse(isClassAnn2.hasRetention());
        assertTrue(isClassAnn2.checkTargetCompatibilityWithGraph());
        MetaAnnotationGeneric genericMetaAnn2 = (MetaAnnotationGeneric) isClassAnn.getMetaAnnotation("Number");
        assertTrue("123456".equals(genericMetaAnn2.getValueList("value").get(0).toString()));

        ProjectionRule projRule = codaCore.getProjRuleModel().getProjRuleFromId("city");
        List<Annotation> annList = projRule.getInsertGraphList().iterator().next().asGraphStruct()
                .getAnnotationList();
        assertTrue(annList.size() == 1);
        Annotation ann = annList.get(0);

        assertTrue("135".equals(ann.getParamValueList("num").get(0).toString()));
        assertTrue("120G".equals(ann.getParamValueList("mix").get(0).toString()));
        assertTrue("test".equals(ann.getParamValueList("stringValue").get(0).toString()));
        assertTrue("try".equals(ann.getParamValueList("plain").get(0).toString()));
    }

	/*@Test
	public void variousParamAnnotationWrongTypeTest() throws UIMAException, PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException {
		// set the projection rules
		String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/variousParamAnnotationWrongTypeTest.pr";
		setProjectionRule(pearlFileName);

		ProjectionRulesModel prModel = codaCore.getProjRuleModel();

		AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");
		assertTrue(isClassAnn != null);
		assertTrue(isClassAnn.hasRetention());
		assertTrue(isClassAnn.checkContextCompatibilityWithGraph());
		MetaAnnotationGeneric genericMetaAnn = (MetaAnnotationGeneric) isClassAnn.getMetaAnnotation("Number");
		assertTrue("123456".equals(genericMetaAnn.getValueList("value").get(0)));

		AnnotationDefinition isClassAnn2 = prModel.getAnnotationDefinition("isClass2");
		assertTrue(isClassAnn2 != null);
		assertFalse(isClassAnn2.hasRetention());
		assertTrue(isClassAnn2.checkContextCompatibilityWithGraph());
		MetaAnnotationGeneric genericMetaAnn2 = (MetaAnnotationGeneric) isClassAnn.getMetaAnnotation("Number");
		assertTrue("123456".equals(genericMetaAnn2.getValueList("value").get(0)));

		ProjectionRule projRule = codaCore.getProjRuleModel().getProjRuleFromId("city");
		List<Annotation> annList = projRule.getInsertGraphList().iterator().next().asGraphStruct()
				.getAnnotationList();
		assertTrue(annList.size() == 1);
		Annotation ann = annList.get(0);

		assertTrue(ann.getParamList("num") == null);
		assertTrue("120G".equals(ann.getParamList("mix").get(0)));
		assertTrue("test".equals(ann.getParamList("stringValue").get(0)));
		assertTrue("try".equals(ann.getParamList("plain").get(0)));
	}*/

    @Test
    public void baseGraphAnnotationRetentionTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/pearlGraphAnnotationRetention.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        String annotation = "isClass(target=object)";

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");

        assertTrue(isClassAnn.hasRetention());

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        List<String> annList = getAnnotationForATriple(rdfTriple1, suggOntologyCodaList);

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(annList.contains(annotation));
    }

    @Test
    public void baseGraphAnnotationNoRetentionTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/pearlGraphAnnotationNoRetention.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        String annotation = "isClass(Object)";

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");

        assertFalse(isClassAnn.hasRetention());

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        List<String> annList = getAnnotationForATriple(rdfTriple1, suggOntologyCodaList);

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertFalse(annList.contains(annotation));
    }

    @Test
    public void baseConfidenceAnnotationTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/pearlConfidenceAnnotation.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        double confidence = getConfidenceForATriple(rdfTriple1, suggOntologyCodaList);
        assertTrue(confidence == 0.5); // the confidence should be the value

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
    }

    @Test
    public void baseConfidencePlchldAnnotationTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/pearlConfidencePlchldAnnotation.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        double confidence = getConfidenceForATriple(rdfTriple1, suggOntologyCodaList);
        assertTrue(confidence == 0.7); // the confidence should be the value

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
    }

    @Test
    public void defaultValueForParamInAnnotationTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/defaultValueForParamInAnnotation.pr";
            setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        double confidence = getConfidenceForATriple(rdfTriple1, suggOntologyCodaList);

        //get the annotation for the only triple produces
        for (SuggOntologyCoda suggOntologyCoda : suggOntologyCodaList) {
            if (suggOntologyCoda.getAllInsertARTTriple().size() == 0) {
                continue;
            }
            List<Annotation> annotationList = suggOntologyCoda.getAllInsertARTTriple().get(0).getListAnnotations();
            Annotation annotation = annotationList.get(0);
            assertTrue(annotation.getParamMap().size() == 4);
            assertTrue(annotation.getParamValueList("num").get(0).toString().equals("135"));
            assertTrue(annotation.getParamValueList("mix").get(0).toString().equals("120G"));
            assertTrue(annotation.getParamValueList("stringValue").get(0).toString().equals("default text"));
            assertTrue(annotation.getParamValueList("plain").get(0).toString().equals("N/A"));
        }
    }

    @Test
    public void memoizationTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/memoizationTest.pr";
        setProjectionRule(pearlFileName);
        // String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
        // + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        //
        // ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        //
        // assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the
        // triple
        //
        // create and execute the UIMA AE
        String text = "dog cat dog cat";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, OWL.SAMEAS, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        // For each annotated animal, create the following triples:
        // - <random IRI> owl:sameAs <memoized IRI>
        // - <random IRI> owl:sameAs <memoized IRI in the map named "default">
        // - <random IRI> owl:sameAs <memoized IRI in the map named "other">
        // Because of memoization, we expect different annotations of the same animal produces the same URI given the
        // same "map" name. Since the use of the "default" map is equivalent to not mention a map at all, for a given
        // annotation the two triples above are the same.
        Model model = getModel().filter(null, OWL.SAMEAS, null);

        assertThat(model.subjects(), hasSize(4));
        assertThat(model.objects(), hasSize(4));
    }

    @Test
    public void memoizationTest2() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/memoizationTest2.pr";
        setProjectionRule(pearlFileName);
        // String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
        // + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        //
        // ProjectionRulesModel prModel = codaCore.getProjRuleModel();
        //
        // assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the
        // triple
        //
        // create and execute the UIMA AE
        String text = "the dog is an animal";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.VALUE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        // 8 triples will be created (4 for each rule for "dog"):

        // from "dog"
        // - <random_IRI_1> rdf:value "animalId"@en
        // - <random_IRI_2> rdf:value "animalMemoizedId"@en
        // - <random_IRI_3> rdf:value "animalDefaultMapMemoizedId"@en
        // - <random_IRI_4> rdf:value "animalOtherMapMemoizedId"@en

        // form "cat"
        // - <random_IRI_5> rdf:value "animalId_2"@en
        // - <random_IRI_6> rdf:value "animalMemoizedId_2"@en
        // - <random_IRI_7> rdf:value "animalDefaultMapMemoizedId_2"@en
        // - <random_IRI_8> rdf:value "animalOtherMapMemoizedId_2"@en

        // Because of memoization, we expect different annotations of the same animal produces the same URI given the
        // same "map" name. Since the use of the "default" map is equivalent to not mention a map at all, for a given
        // annotation the two triples above are the same. In particular, it should be:

        // <random_IRI_1> same as <random_IRI_5>
        // <random_IRI_2> same as <random_IRI_6>
        // <random_IRI_3> same as <random_IRI_7>
        // <random_IRI_4> same as <random_IRI_8>

        // <random_IRI_2> same as <random_IRI_3>
        // <random_IRI_6> same as <random_IRI_7>
        // <random_IRI_2> same as <random_IRI_6>

        // <random_IRI_1> different from <random_IRI_2>
        // <random_IRI_1> different from <random_IRI_4>
        // <random_IRI_2> different from <random_IRI_4>


        ValueFactory vf = connection.getValueFactory();

        assertThat(getModel().filter(null, RDF.VALUE, null).subjects(), hasSize(3));
        assertThat(getModel().filter(null, RDF.VALUE, null).objects(), hasSize(8));

        Literal literal_1 = vf.createLiteral("animalId", "en");
        IRI res_1 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_1).iterator().next().getSubject();

        Literal literal_2 = vf.createLiteral("animalMemoizedId", "en");
        IRI res_2 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_2).iterator().next().getSubject();

        Literal literal_3 = vf.createLiteral("animalDefaultMapMemoizedId", "en");
        IRI res_3 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_3).iterator().next().getSubject();

        Literal literal_4 = vf.createLiteral("animalOtherMapMemoizedId", "en");
        IRI res_4 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_4).iterator().next().getSubject();

        Literal literal_5 = vf.createLiteral("animalId_2", "en");
        IRI res_5 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_5).iterator().next().getSubject();

        Literal literal_6 = vf.createLiteral("animalMemoizedId_2", "en");
        IRI res_6 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_6).iterator().next().getSubject();

        Literal literal_7 = vf.createLiteral("animalDefaultMapMemoizedId_2", "en");
        IRI res_7 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_7).iterator().next().getSubject();

        Literal literal_8 = vf.createLiteral("animalOtherMapMemoizedId_2", "en");
        IRI res_8 = (IRI) getModel().getStatements(null, RDF.VALUE, literal_8).iterator().next().getSubject();

        Assert.assertEquals(res_1.stringValue(), res_5.stringValue());
        Assert.assertEquals(res_2.stringValue(), res_6.stringValue());
        Assert.assertEquals(res_3.stringValue(), res_7.stringValue());
        Assert.assertEquals(res_4.stringValue(), res_8.stringValue());

        Assert.assertEquals(res_2.stringValue(), res_3.stringValue());
        Assert.assertEquals(res_6.stringValue(), res_7.stringValue());
        Assert.assertEquals(res_2.stringValue(), res_6.stringValue());

        Assert.assertNotEquals(res_1.stringValue(), res_2.stringValue());
        Assert.assertNotEquals(res_1.stringValue(), res_4.stringValue());
        Assert.assertNotEquals(res_2.stringValue(), res_4.stringValue());

    }

    @Test
    public void memoizationIgnoreCaseTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/memoizationIgnoreCaseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a Cat is a cat ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        Model model = getModel().filter(null, RDF.TYPE, null);

        assertThat(model.subjects(), hasSize(1));
    }

    @Test
    public void memoizationConsiderCaseTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/memoizationConsiderCaseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a Cat is a cat ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        Model model = getModel().filter(null, RDF.TYPE, null);

        assertThat(model.subjects(), hasSize(2));
    }

    @Test
    public void trimTrue1Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimTrue1Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Is Pinco a person?";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("Pinco", valueFromObejct);
    }

    @Test
    public void trimTrue2Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimTrue2Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Is Pinco a person?";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "Pinco");
    }

    @Test
    public void trimTrue3Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimTrue3Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Is Pinco a person?";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "Pinco");
    }

    @Test
    public void trimFalseTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimFalseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Is Pinco a person?";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, " Pinco");
    }

    @Test
    public void removeMultipleSpacesTrueTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removeMultipleSpacesTrueTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Are Tizio e       Caio two people? ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "Tizio e Caio");
    }

    @Test
    public void removeMultipleSpacesFalseTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removeMultipleSpacesFalseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Are Tizio e       Caio two people? ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "Tizio e       Caio");
    }

    @Test
    public void trimAndremoveMultipleSpacesBothFalseTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimAndremoveMultipleSpacesFalseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Are  Caio   e      Sempronio  two people? ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, " Caio   e      Sempronio ");
    }

    @Test
    public void trimAndremoveMultipleSpacesBothTrueTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimAndremoveMultipleSpacesTrueTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Are  Caio   e      Sempronio  two people? ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "Caio e Sempronio");
    }


    @Test
    public void passedAnnotationDefinitionTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/metaAnnotationTestRightTarget.pr";
        String defPearlFileName = "it/uniroma2/art/coda/junittest/annotation/annDef.pr";
        setProjectionRule(pearlFileName, defPearlFileName);

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        //part related to metaAnnotationTestRightTarget.pr
        AnnotationDefinition isClassAnn = prModel.getAnnotationDefinition("isClass");

        assertTrue(isClassAnn != null);

        assertTrue(isClassAnn.hasRetention());

        assertTrue(isClassAnn.checkTargetCompatibilityWithGraph());
        assertTrue(isClassAnn.checkTargetValueCompatibility("subject"));
        assertTrue(isClassAnn.checkTargetValueCompatibility("object"));
        assertFalse(isClassAnn.checkTargetValueCompatibility("predicate"));

        //part related to annDef.pr
        AnnotationDefinition objectOneOf = prModel.getAnnotationDefinition("ObjectOneOf");
        assertTrue(objectOneOf != null);
        AnnotationDefinition objectOneOfNE = prModel.getAnnotationDefinition("ObjectOneOfNE");
        assertTrue(objectOneOfNE == null);

        assertTrue(prModel.getAnnotationDefinitionList().size() == (implicitAnnotationNum+2+NUM_ANNOTATION_HARDCODED));
    }

    @Test
    public void annotationForCustomFormAllAnnotationsTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/annotationForCustomFormAllAnnotations.pr";
        String defPearlFileName = "it/uniroma2/art/coda/junittest/annotation/annDef.pr";
        setProjectionRule(defPearlFileName, pearlFileName);
        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        //part related to annDef.pr (annotation definition)
        assertTrue(prModel.getAnnotationDefinitionList().size() == (implicitAnnotationNum+NUM_ANNOTATION_HARDCODED));

        //get the annotation used in the rule section
        ProjectionRule cityRule = prModel.getProjRuleFromId("city");
        Map<String, PlaceholderStruct> plcMap = cityRule.getPlaceholderMap();

        //the annotation used in the placeholder cityName1
        PlaceholderStruct cityNamePlch = plcMap.get("cityName1");
        List<Annotation> cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        Annotation objectOneOfAnn = cityNamePlchAnnList.get(0);
        assertTrue(objectOneOfAnn.getName().equals("ObjectOneOf"));
        List<ParamValueInterface> paramValueList = objectOneOfAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 2);
        assertTrue(paramValueList.get(0).toString().equals("<http://art.uniroma2.it/Rome>") || paramValueList.get(0).toString().equals("<http://art.uniroma2.it/Milan>"));
        assertTrue(paramValueList.get(1).toString().equals("<http://art.uniroma2.it/Rome>") || paramValueList.get(1).toString().equals("<http://art.uniroma2.it/Milan>"));

        //the annotation used in the placeholder cityName2
        cityNamePlch = plcMap.get("cityName2");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        Annotation roleAnn = cityNamePlchAnnList.get(0);
        assertTrue(roleAnn.getName().equals("Role"));
        paramValueList = roleAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 2);
        assertTrue(paramValueList.get(0).toString().equals("Class") || paramValueList.get(0).toString().equals("Concept"));
        assertTrue(paramValueList.get(1).toString().equals("Class") || paramValueList.get(1).toString().equals("Concept"));

        //the annotation used in the placeholder cityName3
        cityNamePlch = plcMap.get("cityName3");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        Annotation rangeAnn = cityNamePlchAnnList.get(0);
        assertTrue(rangeAnn.getName().equals("Range"));
        paramValueList = rangeAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 1);
        assertTrue(paramValueList.get(0).toString().equals("<http://art.uniroma2.it/MyClass>"));

        //the annotation used in the placeholder cityName4
        cityNamePlch = plcMap.get("cityName4");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        Annotation rangeListAnn = cityNamePlchAnnList.get(0);
        assertTrue(rangeListAnn.getName().equals("RangeList"));
        paramValueList = rangeListAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 2);
        assertTrue(paramValueList.get(0).toString().equals("<http://art.uniroma2.it/MyClass1>") || paramValueList.get(0).toString().equals("<http://art.uniroma2.it/MyClass2>"));
        assertTrue(paramValueList.get(1).toString().equals("<http://art.uniroma2.it/MyClass1>") || paramValueList.get(1).toString().equals("<http://art.uniroma2.it/MyClass2>"));

        //the annotation used in the placeholder cityName5
        cityNamePlch = plcMap.get("cityName5");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        Annotation foreignAnn = cityNamePlchAnnList.get(0);
        assertTrue(foreignAnn.getName().equals("Foreign"));
        paramValueList = foreignAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 1);
        assertTrue(paramValueList.get(0).toString().equals("otherProject"));

        //the annotation used in the placeholder cityName6
        cityNamePlch = plcMap.get("cityName6");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        Annotation collectionAnn = cityNamePlchAnnList.get(0);
        assertTrue(collectionAnn.getName().equals("Collection"));
        List<ParamValueInterface> paramMinValueList = collectionAnn.getParamValueList("min");
        assertTrue(paramMinValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMinValueList.get(0).toString()) == 0);
        List<ParamValueInterface> paramMaxValueList = collectionAnn.getParamValueList("max");
        assertTrue(paramMaxValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMaxValueList.get(0).toString()) == 0);

        //the annotation used in the placeholder cityName7
        cityNamePlch = plcMap.get("cityName7");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        collectionAnn = cityNamePlchAnnList.get(0);
        assertTrue(collectionAnn.getName().equals("Collection"));
        paramMinValueList = collectionAnn.getParamValueList("min");
        assertTrue(paramMinValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMinValueList.get(0).toString()) == 1);
        paramMaxValueList = collectionAnn.getParamValueList("max");
        assertTrue(paramMaxValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMaxValueList.get(0).toString()) == 0);

        //the annotation used in the placeholder cityName8
        cityNamePlch = plcMap.get("cityName8");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        collectionAnn = cityNamePlchAnnList.get(0);
        assertTrue(collectionAnn.getName().equals("Collection"));
        paramMinValueList = collectionAnn.getParamValueList("min");
        assertTrue(paramMinValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMinValueList.get(0).toString()) == 0);
        paramMaxValueList = collectionAnn.getParamValueList("max");
        assertTrue(paramMaxValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMaxValueList.get(0).toString()) ==2);

        //the annotation used in the placeholder cityName9
        cityNamePlch = plcMap.get("cityName9");
        cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        collectionAnn = cityNamePlchAnnList.get(0);
        assertTrue(collectionAnn.getName().equals("Collection"));
        paramMinValueList = collectionAnn.getParamValueList("min");
        assertTrue(paramMinValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMinValueList.get(0).toString()) == 3);
        paramMaxValueList = collectionAnn.getParamValueList("max");
        assertTrue(paramMaxValueList.size() == 1);
        assertTrue(Integer.parseInt(paramMaxValueList.get(0).toString()) == 5);


        //the annotation used in the palceholder cityName
        PlaceholderStruct cityNameLitPlch = plcMap.get("cityNameLit");
        List<Annotation> cityNameLitPlchAnnList = cityNameLitPlch.getAnnotationList();
        assertTrue(cityNameLitPlchAnnList.size() == 1);
        Annotation dataOneOfAnn = cityNameLitPlchAnnList.get(0);
        assertTrue(dataOneOfAnn.getName().equals("DataOneOf"));
        paramValueList = dataOneOfAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 3);
        assertTrue(paramValueList.get(0).toString().equals("\"Rome\"") || paramValueList.get(0).toString().equals("\"Milan\"@it")
                || paramValueList.get(0).toString().equals("\"Firenze\"^^<http://www.w3.org/2001/XMLSchema#string>"));
        assertTrue(paramValueList.get(1).toString().equals("\"Rome\"") || paramValueList.get(0).toString().equals("\"Milan\"@it")
                || paramValueList.get(1).toString().equals("\"Firenze\"^^<http://www.w3.org/2001/XMLSchema#string>"));
        assertTrue(paramValueList.get(2).equals("\"Rome\"") || paramValueList.get(0).toString().equals("\"Milan\"@it")
                || paramValueList.get(2).toString().equals("\"Firenze\"^^<http://www.w3.org/2001/XMLSchema#string>"));

    }

    @Test
    public void annotationForCustomFormTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/annotationForCustomForm.pr";
        String defPearlFileName = "it/uniroma2/art/coda/junittest/annotation/annDef.pr";
        setProjectionRule(defPearlFileName, pearlFileName);
        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        //part related to annDef.pr (annotation definition)
        assertTrue(prModel.getAnnotationDefinitionList().size() == (implicitAnnotationNum+NUM_ANNOTATION_HARDCODED));

        AnnotationDefinition objectOneOf = prModel.getAnnotationDefinition("ObjectOneOf");
        assertTrue(objectOneOf != null);
        AnnotationDefinition objectOneOfNE = prModel.getAnnotationDefinition("ObjectOneOfNE");
        assertTrue(objectOneOfNE == null);


        //get the annotation used in the rule section
        ProjectionRule cityRule = prModel.getProjRuleFromId("city");
        Map<String, PlaceholderStruct> plcMap = cityRule.getPlaceholderMap();
        //the annotation used in the palceholder cityName
        PlaceholderStruct cityNamePlch = plcMap.get("cityName");
        List<Annotation> cityNamePlchAnnList = cityNamePlch.getAnnotationList();
        assertTrue(cityNamePlchAnnList.size() == 1);
        Annotation objectOneOfAnn = cityNamePlchAnnList.get(0);
        assertTrue(objectOneOfAnn.getName().equals("ObjectOneOf"));
        List<ParamValueInterface> paramValueList = objectOneOfAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 2);
        assertTrue(paramValueList.get(0).toString().equals("<http://art.uniroma2.it/Rome>") || paramValueList.get(0).toString().equals("<http://art.uniroma2.it/Milan>"));
        assertTrue(paramValueList.get(1).toString().equals("<http://art.uniroma2.it/Rome>") || paramValueList.get(1).toString().equals("<http://art.uniroma2.it/Milan>"));

        //the annotation used in the palceholder cityName
        PlaceholderStruct cityNameLitPlch = plcMap.get("cityNameLit");
        List<Annotation> cityNameLitPlchAnnList = cityNameLitPlch.getAnnotationList();
        assertTrue(cityNameLitPlchAnnList.size() == 1);
        Annotation dataOneOfAnn = cityNameLitPlchAnnList.get(0);
        assertTrue(dataOneOfAnn.getName().equals("DataOneOf"));
        paramValueList = dataOneOfAnn.getParamValueList("value");
        assertTrue(paramValueList.size() == 3);
        assertTrue(paramValueList.get(0).toString().equals("\"Rome\"") || paramValueList.get(0).toString().equals("\"Milan\"@it")
                || paramValueList.get(0).toString().equals("\"Firenze\"^^<http://www.w3.org/2001/XMLSchema#string>"));
        assertTrue(paramValueList.get(1).toString().equals("\"Rome\"") || paramValueList.get(0).toString().equals("\"Milan\"@it")
                || paramValueList.get(1).toString().equals("\"Firenze\"^^<http://www.w3.org/2001/XMLSchema#string>"));
        assertTrue(paramValueList.get(2).equals("\"Rome\"") || paramValueList.get(0).toString().equals("\"Milan\"@it")
                || paramValueList.get(2).toString().equals("\"Firenze\"^^<http://www.w3.org/2001/XMLSchema#string>"));

    }

    @Test
    public void annotationForCustomFormTestWrongType1() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/annotationForCustomFormWrongType1.pr";
        String defPearlFileName = "it/uniroma2/art/coda/junittest/annotation/annDef.pr";
        try {
            setProjectionRule(defPearlFileName, pearlFileName);
            ProjectionRulesModel prModel = codaCore.getProjRuleModel();
            assertTrue(false);
        } catch (TypeOfParamInAnnotationWrongTypeException e) {
            assertTrue(e.getAnnName().equals("ObjectOneOf"));
            assertTrue(e.getParamName().equals("value"));
            assertTrue(e.getTypeExpected().toLowerCase().equals(PearlParserDescription.TYPE_IRI_ARRAY));
        }
    }

    @Test
    public void annotationForCustomFormTestWrongType2() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/annotationForCustomFormWrongType2.pr";
        String defPearlFileName = "it/uniroma2/art/coda/junittest/annotation/annDef.pr";
        try {
            setProjectionRule(defPearlFileName, pearlFileName);
            ProjectionRulesModel prModel = codaCore.getProjRuleModel();
            assertTrue(false);
        } catch (TypeOfParamInAnnotationWrongTypeException e) {
            assertTrue(e.getAnnName().equals("DataOneOf"));
            assertTrue(e.getParamName().equals("value"));
            assertTrue(e.getTypeExpected().toLowerCase().equals(PearlParserDescription.TYPE_LITERAL_ARRAY));
        }
    }

    @Test
    public void missingParamWithDefaultInAnnotationTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/missingParamWithDefaultInAnnotation.pr";
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        // pass the jcas to CODA and execute to populate/enrich the ontology
        try {
            setProjectionRule(pearlFileName);
            String text = "Andrea lives in Rome";
            JCas jcas = createAndExecuteAE(text);
            // pass the jcas to CODA and execute to populate/enrich the ontology
            List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
            assertTrue(true);
            for (SuggOntologyCoda suggOntologyCoda : suggOntologyCodaList) {
                if (suggOntologyCoda.getAllInsertARTTriple().size() > 0) {
                    CODATriple codaTriple = suggOntologyCoda.getAllInsertARTTriple().get(0);
                    Annotation isClassAnn = codaTriple.getAnnotation("isClass");
                    assertTrue(isClassAnn.getParamValueList("max").get(0).toString().equals("5"));
                    assertTrue(isClassAnn.getParamValueList("text").get(0).toString().equals("plain text"));
                }
            }
        } catch (MissingMandatoryParamInAnnotationException e) {
            // should not arrive at this point
            assertTrue(false);
        }
    }


    @Test
    public void literalAndStringTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/literalAndString.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        double confidence = getConfidenceForATriple(rdfTriple1, suggOntologyCodaList);

        //get the annotation for the only triple produces
        for (SuggOntologyCoda suggOntologyCoda : suggOntologyCodaList) {
            if (suggOntologyCoda.getAllInsertARTTriple().size() == 0) {
                continue;
            }
            List<Annotation> annotationList = suggOntologyCoda.getAllInsertARTTriple().get(0).getListAnnotations();
            Annotation annotation = annotationList.get(0);
            assertTrue(annotation.getParamMap().size() == 5);
            assertTrue(annotation.getParamValueList("valueS1").get(0).toString().equals("value1"));
            assertTrue(annotation.getParamValueList("valueS2").get(0).toString().equals("value2"));
            assertTrue(annotation.getParamValueList("valueL1").get(0).toString().equals("\"value3\""));
            assertTrue(annotation.getParamValueList("valueL2").get(0).toString().equals("\"value4\""));
            assertTrue(annotation.getParamValueList("valueL3").get(0).toString().equals("\"value5\"@it"));
        }
    }

    @Test
    public void literalAndStringFromPlaceholderTest() throws UIMAException, PRParserException,
            ComponentProvisioningException, ConverterException, DependencyException, RDFModelNotSetException,
            ProjectionRuleModelNotSet, UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/literalAndStringFromPlaceholder.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        ProjectionRulesModel prModel = codaCore.getProjRuleModel();

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());
        double confidence = getConfidenceForATriple(rdfTriple1, suggOntologyCodaList);

        //get the annotation for the only triple produces
        for (SuggOntologyCoda suggOntologyCoda : suggOntologyCodaList) {
            if (suggOntologyCoda.getAllInsertARTTriple().size() == 0) {
                continue;
            }
            List<Annotation> annotationList = suggOntologyCoda.getAllInsertARTTriple().get(0).getListAnnotations();
            Annotation annotation = annotationList.get(0);
            assertTrue(annotation.getParamMap().size() == 2);
            assertTrue(annotation.getParamValueList("valueS1").get(0).toString().equals("Rome"));
            assertTrue(annotation.getParamValueList("valueL1").get(0).toString().equals("\"Rome\""));
        }
    }


    @Test
    public void baseRuleAnnotation1Test() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimFalseRuleTest1.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Is Pinco a person?";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, " Pinco");
    }

    @Test
    public void baseRuleAnnotation2Test() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimFalseRuleTest2.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Is Pinco a person?";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "Pinco");
    }

    @Test
    public void baseRuleAnnotation3Test() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/trimFalseRuleTest3.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Is Pinco a person?";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, " Pinco");
    }

    @Test
    public void removeMultipleSpacesRuleTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removeMultipleSpacesRuleTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Are Tizio e       Caio two people? ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "Tizio e       Caio");
    }

    @Test
    public void lowerCaseRuleTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/lowerCaseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Andrea is a person ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "andrea");
    }

    @Test
    public void capitalizeRuleTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/capitalizeTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "Andrea is a person ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals(valueFromObejct, "ANDREA");
    }

    @Test
    public void removePunctuation1Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removePunctuation1Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a.b,c:d;e!f?g. h. is a sequence ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("a b c d e f g h", valueFromObejct);
    }

    @Test
    public void removePunctuation2Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removePunctuation2Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a.b,c:d;e!f?g. h. is a sequence ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("a b,c:d e!f g h", valueFromObejct);
    }

    @Test
    public void removePunctuation3Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removePunctuation3Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a.b,c:d;e!f?g. h. is a sequence ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("a b,c:d e!f g h", valueFromObejct);
    }



    @Test
    public void removePunctuationFalseTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removePunctuationFalseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a.b,c:d;e!f?g. h. is a sequence ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("a.b,c:d;e!f?g. h.", valueFromObejct);
    }

    @Test
    public void removePunctuationFalseTrue1Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removePunctuationFalseTrue1Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a.b,c:d;e!f?g. h. is a sequence ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("a b c d e f g h", valueFromObejct);
    }

    @Test
    public void removePunctuationFalseTrue2Test() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removePunctuationFalseTrue2Test.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a.b,c:d;e!f?g. h. is a sequence ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("a b,c:d e!f g h", valueFromObejct);
    }

    @Test
    public void removePunctuationTrueFalseTest() throws PRParserException, UIMAException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/annotation/removePunctuationTrueFalseTest.pr";

        setProjectionRule(pearlFileName);

        // create and execute the UIMA AE
        String text = "a.b,c:d;e!f?g. h. is a sequence ";
        JCas jcas = createAndExecuteAE(text);

        assertThat(getModel().filter(null, RDF.TYPE, null), empty());

        // pass the jcas to CODA and execute to populate/enrich the ontology
        List<SuggOntologyCoda> suggOntologyCodaList = executeCODA(jcas,
                Thread.currentThread().getStackTrace()[1].getMethodName());

        ValueFactory vf = connection.getValueFactory();

        Literal object = (Literal) getModel().filter(null, vf.createIRI("http://art.uniroma2.it/name"), null).iterator().next().getObject();

        String valueFromObejct = object.getLabel();

        Assert.assertEquals("a.b,c:d;e!f?g. h.", valueFromObejct);
    }

}
