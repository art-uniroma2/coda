package it.uniroma2.art.coda.junittest;

import it.uniroma2.art.coda.exception.*;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FormatterTest extends AbstractTest {

    @Test
    public void baseFormatterTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlBaseFormatter.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://test/Rome/Milan> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void baseFormatterTest2() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlBaseFormatter2.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://test/Rome/Milan> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void formatterLiteralTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterLiteral.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/2000/01/rdf-schema#label> \"Rome capital of Italy in en in the world\"@en";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
    }

    @Test
    public void formatterIriTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterIri.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://test/Rome/cat/Rome/it/city/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void formatterExceptionTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterException.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://test/Rome/Milan> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        try {
            executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
            assertTrue(false);
        } catch (ConverterException e ){
            assertTrue(true);
        }
    }

    @Test
    public void formatterChainTestUri() throws UIMAException, PRParserException, ComponentProvisioningException,
            DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException, ConverterException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterChainUri.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome/city1/city2> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
    }

    @Test
    public void formatterChainTestLiteral() throws UIMAException, PRParserException, ComponentProvisioningException,
            DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException, ConverterException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterChainLiteral.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome> "
                + "<http://art.uniroma2.it/defaultfakeProp> \"Rome/city1/city2\"@it";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
    }

    @Test
    public void formatterChainTestUriAndLiteral() throws UIMAException, PRParserException, ComponentProvisioningException,
            DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException, ConverterException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterChainUriLiteral.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Rome/city1/city2> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        String rdfTriple2 = "<http://art.uniroma2.it/Rome/city1/city2> "
                + "<http://art.uniroma2.it/defaultfakeProp> \"Rome/city1/city2\"@it";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());
        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
    }

    @Test
    public void formatterLiteralCapitalTest() throws UIMAException, PRParserException, ComponentProvisioningException,
            ConverterException, DependencyException, RDFModelNotSetException, ProjectionRuleModelNotSet,
            UnassignableFeaturePathException, ValueNotPresentDueToConfigurationException {

        // set the projection rules
        String pearlFileName = "it/uniroma2/art/coda/junittest/formatter/pearlFormatterLiteralCapital.pr";
        setProjectionRule(pearlFileName);
        String rdfTriple1 = "<http://art.uniroma2.it/Andrea> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/Person>";
        String rdfTriple2 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://art.uniroma2.it/City>";
        String rdfTriple3 = "<http://art.uniroma2.it/Rome> "
                + "<http://www.w3.org/2000/01/rdf-schema#label> \"ROME capital of ITALY in EN in the world\"@en";

        assertFalse(executeAskQuery(rdfTriple1)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple2)); // at the beginning the model should not contain the triple
        assertFalse(executeAskQuery(rdfTriple3)); // at the beginning the model should not contain the triple

        // create and execute the UIMA AE
        String text = "Andrea lives in Rome";
        JCas jcas = createAndExecuteAE(text);

        // pass the jcas to CODA and execute to populate/enrich the ontology
        executeCODA(jcas, Thread.currentThread().getStackTrace()[1].getMethodName());

        // System.out.println("************ PROJECTION RULE MODEL
        // ************\n"+codaCore.getProjRuleModel().getModelAsString());

        assertTrue(executeAskQuery(rdfTriple1)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple2)); // now the model should contain the triple
        assertTrue(executeAskQuery(rdfTriple3)); // now the model should contain the triple
    }

}
