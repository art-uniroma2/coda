package ${package}.converters.impls;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.easymock.EasyMockRule;
import org.easymock.Mock;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.junit.Rule;
import org.junit.Test;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;

/**
 * TODO update test for converter {@code ${converterName}}
 */
public class ${converterName}Test {

	@Rule
	public EasyMockRule rule = new EasyMockRule(this);

	@Mock
	private CODAContext mockedCODAContext;

	@Test
	public void testProduceURI() throws ConverterException {
		
		expect(mockedCODAContext.getDefaultNamespace()).andReturn("http://example.org/");
		replay(mockedCODAContext);
		
		${converterName} conv = new ${converterName}();

		IRI uriResource = conv.produceURI(mockedCODAContext, "hello");

		verify(mockedCODAContext);
		
		assertThat(uriResource.stringValue(), is(equalTo("http://example.org/hello")));
	}

	@Test
	public void testProduceLiteral_string() throws ConverterException {
		
		replay(mockedCODAContext);
		
		${converterName} conv = new ${converterName}();

		Literal literalValue = conv.produceLiteral(mockedCODAContext, XMLSchema.STRING.stringValue(), null, "hello");
		
		verify(mockedCODAContext);

		assertThat(literalValue, both(hasProperty("datatype", equalTo(XMLSchema.STRING)))
				.and(hasProperty("label", equalTo("hello"))));
	}
	
	@Test
	public void testProduceLiteral_en() throws ConverterException {
		
		replay(mockedCODAContext);
		
		${converterName} conv = new ${converterName}();

		Literal literalValue = conv.produceLiteral(mockedCODAContext, null, "en", "hello");
		
		verify(mockedCODAContext);

		assertThat(literalValue, both(hasProperty("language", equalTo(Optional.of("en"))))
				.and(hasProperty("label", equalTo("hello"))));
	}
	
}
