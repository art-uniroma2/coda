package ${package}.converters.contracts;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

/**
 * TODO: provide a description for the contract ${contractName}
 */
public interface ${contractName} extends Converter {

	String CONTRACT_URI = "${contractURI}";

	/**
	 * TODO: define an appropriate signature
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	IRI produceURI(CODAContext ctx, String value) throws ConverterException;


	/**
	 * TODO: define an appropriate signature
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) throws ConverterException;

}
