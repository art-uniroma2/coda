package ${package}.converters.impls;

import java.util.Map;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.pf4j.Extension;

import ${package}.converters.contracts.${contractName};

/**
 * An implementation of the contract {@code ${contractName}}
 */
@Extension(points = {Converter.class})
public class ${converterName} implements ${contractName} {

	public static final String CONVERTER_URI = "${converterURI}";

	private ValueFactory fact = SimpleValueFactory.getInstance();
	
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	public IRI produceURI(CODAContext ctx, String value) throws ConverterException {
		return fact.createIRI(ctx.getDefaultNamespace() + value);
	}

	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) throws ConverterException {
		if (datatype != null) {
			return fact.createLiteral(value, fact.createIRI(datatype));
		} else if (lang != null) {
			return fact.createLiteral(value, lang);
		} else {
			return fact.createLiteral(value);
		}
	}
}
