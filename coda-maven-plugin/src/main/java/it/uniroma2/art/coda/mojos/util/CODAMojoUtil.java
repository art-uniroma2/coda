package it.uniroma2.art.coda.mojos.util;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collection;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;

/**
 * Utility class for the CODA Mojo. The available operations usually needs a a {@link ClassLoader} that is
 * initialized with the classes produced by the current project (i.e. the project in which the mojo has been
 * used) as well as with the classes available in the compilation classpath.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class CODAMojoUtil {
	/**
	 * Checks whether the given class represents a converter.
	 * 
	 * @param componentLoader
	 *            the class loader associated with the project classes and its dependencies
	 * @param clazz
	 *            the class to test
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static boolean isConverter(ClassLoader componentLoader, Class<?> clazz)
			throws ClassNotFoundException {
		Class<?> converterInterface = componentLoader.loadClass("it.uniroma2.art.coda.interfaces.Converter");

		return converterInterface.isAssignableFrom(clazz) && !clazz.isInterface()
				&& !Modifier.isAbstract(clazz.getModifiers());
	}

	/**
	 * Returns the collection of contracts implementated by the supplied converter.
	 * 
	 * @param componentLoader
	 * @param clazz
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static Collection<String> getImplementedContracts(ClassLoader componentLoader, Class<?> clazz)
			throws ClassNotFoundException {
		Class<?> converterInterface = componentLoader.loadClass("it.uniroma2.art.coda.interfaces.Converter");

		return Arrays.stream(clazz.getInterfaces())
				.filter(c -> converterInterface.isAssignableFrom(c) && c != converterInterface).map(c -> {
					try {
						Field contractURIField = c.getField("CONTRACT_URI");
						return (String)contractURIField.get(null);
					} catch (Exception e) {
						return null;
					}
				}).filter(u -> u != null).collect(toList());
	}

	/**
	 * Returns a classloader initialized with the elements of the project compilation classpath.
	 * 
	 * @param project
	 * @param log
	 * @return
	 * @throws DependencyResolutionRequiredException
	 */
	public static URLClassLoader getClassloader(MavenProject project, Log log)
			throws DependencyResolutionRequiredException {
		URL[] urls = project.getCompileClasspathElements().stream().map(path -> {
			try {
				return new File(path).toURI().toURL();
			} catch (MalformedURLException e) {
				return null;
			}
		}).toArray(URL[]::new);
		return new URLClassLoader(urls);
	}

	/**
	 * Returns the name of the class defined by the supplied file.
	 * 
	 * @param project
	 * @param aFile
	 * @return
	 */
	public static String getClassName(MavenProject project, String aFile) {
		String base = aFile.substring(0, aFile.length() - 6);
		String clazzPath = base.substring(project.getBuild().getOutputDirectory().length() + 1);
		return clazzPath.replace(File.separator, ".");
	}
}
