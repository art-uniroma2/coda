package it.uniroma2.art.coda.mojos;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLClassLoader;
import java.nio.charset.Charset;

import org.apache.maven.artifact.DependencyResolutionRequiredException;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.FileUtils;
import org.sonatype.plexus.build.incremental.BuildContext;

import it.uniroma2.art.coda.mojos.util.CODAMojoUtil;

/**
 * This goal generates the file <code>converters.rdf</code> describing the converters defined by the current
 * Maven project.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
@Mojo(name = "generate-rdf-description", defaultPhase = LifecyclePhase.PROCESS_CLASSES, requiresProject = true, requiresDependencyResolution = ResolutionScope.COMPILE, requiresDependencyCollection = ResolutionScope.COMPILE)
public class GenerateRDFDescription extends AbstractMojo {

	private static final String FINAL_NAME = "coda-metadata.ttl";

	@Component
	private BuildContext buildContext;

	@Parameter(property = "project", required = true, readonly = true)
	private MavenProject project;

	@Parameter(defaultValue = "${project.build.directory}/classes", property = "outputDir", required = true)
	private File outputDirectory;

	@Parameter(defaultValue = "${project.build.sourceEncoding}", required = true)
	private String encoding;

	public void execute() throws MojoExecutionException {
		try {
			if (!outputDirectory.exists()) {
				outputDirectory.mkdirs();
				buildContext.refresh(outputDirectory);
			}
			File rdfDescription = new File(outputDirectory, FINAL_NAME);
			rdfDescription.createNewFile();

			// Get the compiled classes from this project
			String[] files = FileUtils.getFilesFromExtension(project.getBuild().getOutputDirectory(),
					new String[] { "class" });
			URLClassLoader componentLoader = CODAMojoUtil.getClassloader(project, getLog());
			Class<?> componentIndexClass = componentLoader
					.loadClass("it.uniroma2.art.coda.provisioning.ComponentIndex");
			Object componentIndex = componentIndexClass.newInstance();

			Method indexConverterMethod = componentIndexClass.getMethod("indexConverter", Class.class);
			Method writeRDFMethod = componentIndexClass.getMethod("writeRDF", Writer.class);

			for (String aFile : files) {
				String className = CODAMojoUtil.getClassName(project, aFile);
				Class<?> clazz = componentLoader.loadClass(className);

				if (!CODAMojoUtil.isConverter(componentLoader, clazz))
					continue;

				indexConverterMethod.invoke(componentIndex, clazz);
			}

			try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(rdfDescription),
					Charset.forName(encoding))) {
				writeRDFMethod.invoke(componentIndex, writer);
			}
		} catch (SecurityException | IOException | ClassNotFoundException | IllegalAccessException
				| DependencyResolutionRequiredException | NoSuchMethodException | InvocationTargetException
				| InstantiationException e) {
			e.printStackTrace();
			throw new MojoExecutionException("Could not write RDF description", e);
		}
	}
}
