package it.uniroma2.art.coda.converters.impl;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.RandomIdGenerator;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Objects;
import com.google.common.net.UrlEscapers;

/**
 * Template-based implementation of the {@link RandomIdGenerator} contract.
 *
 */
public class TemplateBasedRandomIdGenerator implements RandomIdGenerator {

	public static final String CONVERTER_URI = ContractConstants.CODA_CONVERTERS_BASE_URI
			+ "templateBasedRandIdGen";

	private static Properties defaultPropertyValues;

	static {
		defaultPropertyValues = new Properties();
		defaultPropertyValues.setProperty("concept", "c_${rand()}");
		defaultPropertyValues.setProperty("xLabel", "xl_${lexicalForm.language}_${rand()}");
		defaultPropertyValues.setProperty("xNote", "xDef_${rand()}");
		defaultPropertyValues.setProperty("fallback", "${xRole}_${rand()}");
	}

	public enum RandCode {
		DATETIMEMS, UUID, TRUNCUUID4, TRUNCUUID8, TRUNCUUID12;
	}

	// private static final String VALUE_REGEX = "[a-zA-Z0-9-_]*"; // regex for
	// // admitted
	// // value of
	// // placeholder
	private static final String RAND_REGEX = "rand\\((" + RandCode.DATETIMEMS + "|" + RandCode.UUID + "|"
			+ RandCode.TRUNCUUID4 + "|" + RandCode.TRUNCUUID8 + "|" + RandCode.TRUNCUUID12 + ")?\\)";
	/*
	 * this regex matches every string that contains one ${rand()} with an optional argument (datetimems,
	 * uuid, truncuuid4, truncuuid8, truncuuid12). Before and after this part, eventually there could be some
	 * placeholders (${...}) or alphanumeric characters and _ character
	 */
	// private static final String TEMPLATE_REGEX = "([A-Za-z0-9_]*(\\$\\{[A-Za-z0-9]+\\})*[A-Za-z0-9_]*)*"
	// + "\\$\\{" + RAND_REGEX + "\\}" + "([A-Za-z0-9_]*(\\$\\{[A-Za-z0-9]+\\})*[A-Za-z0-9_]*)*";

	private static final String XROLE = "xRole";

	private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("([a-zA-Z]+)(?:\\.(([a-zA-Z]+)))?");
	private static final Pattern PLACEHOLDER_START_PATTERN = Pattern.compile("\\$(\\$)?\\{");

	private String getRandomPart(String placheholder) {
		String DEFAULT_VALUE = RandCode.TRUNCUUID8.name();

		String randomCode = placheholder.substring(placheholder.indexOf("(") + 1, placheholder.indexOf(")"));
		// if in the template there's no rand code, try to get it from project
		// property
		// if (randomCode.length() == 0) {
		// try {
		// randomCode = ProjectManager.getProjectProperty(stServiceContext.getProject().getName(),
		// "uriRndCodeGenerator");
		// } catch (IOException | InvalidProjectNameException | ProjectInexistentException e) {
		// }
		// }
		// If the property is not found in the project truncuuid8 is assumed as default
		if (randomCode == null) {
			randomCode = DEFAULT_VALUE;
		}

		String randomValue;
		if (randomCode.equalsIgnoreCase(RandCode.DATETIMEMS.name())) {
			randomValue = new java.util.Date().getTime() + "";
		} else if (randomCode.equalsIgnoreCase(RandCode.UUID.name())) {
			randomValue = UUID.randomUUID().toString();
		} else if (randomCode.equalsIgnoreCase(RandCode.TRUNCUUID4.name())) {
			randomValue = UUID.randomUUID().toString().substring(0, 4);
		} else if (randomCode.equalsIgnoreCase(RandCode.TRUNCUUID12.name())) {
			randomValue = UUID.randomUUID().toString().substring(0, 13);
		} else {// default value TRUNCUUID8
			randomValue = UUID.randomUUID().toString().substring(0, 8);
		}
		return randomValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.coda.contracts.RandomIdGenerator#produceURI(it.uniroma2.art.coda.interfaces.CODAContext
	 * , java.lang.String, java.util.Map)
	 */
	@Override
	public ARTURIResource produceURI(CODAContext ctx, String value, String xRole, Map<String, ARTNode> args) {
		// // validate template
		// if (!template.matches(TEMPLATE_REGEX)) {
		// throw new IllegalArgumentException("The template \"" + template
		// + "\" is not valid");
		// }

		// ARTURIResAndRandomString resRand = new ARTURIResAndRandomString();
		ARTURIResource uriRes = null; // null should never be returned

		boolean newConceptGenerated = false;
		while (!newConceptGenerated) {
			String localName = "";

			String currentTemplate = getTemplate(ctx, xRole);

			while (currentTemplate.length() > 0) {
				// S{ is an escaped placeholder, $${ is a not escaped placeholder
				if (currentTemplate.startsWith("${") || currentTemplate.startsWith("$${")) {
					// get placeholder
					// If ${, then escaped with 2 characters to be skipped
					boolean phEscaped = true;
					int phBegin = 2;
					
					// If $${, then not escaped with 3 characters to be skipped
					if (currentTemplate.startsWith("$${")) {
						phEscaped = false;
						phBegin = 3;
					}
					
					int phEnd = currentTemplate.indexOf("}");
					
					if (phEnd == -1) {
						throw new IllegalArgumentException("Missing closing brace");
					}
					
					String ph = currentTemplate.substring(phBegin, phEnd);

					// retrieve the value to replace the placeholder
					String phSubst;
					if (ph.matches(RAND_REGEX)) {
						phSubst = getRandomPart(ph);
						// resRand.setRandomValue(phSubst);
					} else {
						if (ph.equals(XROLE)) {
							phSubst = xRole;
						} else {
							phSubst = getPlaceholderValue(ph, args);
						}
						if (phSubst == null)
							throw new IllegalArgumentException("The placeholder \"" + ph
									+ "\" is not present into the valueMapping");
						// if (!phSubst.matches(VALUE_REGEX))
						// throw new IllegalArgumentException("The value \"" + phSubst
						// + "\" for the placeholder \"" + ph + "\" is not valid");
					}
					if (phEscaped) {
						phSubst = escapeValue(phSubst);
					}
					localName = localName + phSubst; // compose the result
					// remove the parsed part
					currentTemplate = currentTemplate.substring(phEnd + 1);
				} else {
					// concat the fixed part of the template
					Matcher m = PLACEHOLDER_START_PATTERN.matcher(currentTemplate);
					
					int literalEnd;
					
					if (m.find()) {
						literalEnd = m.start();
					} else {
						literalEnd = currentTemplate.length();
					}
					
					localName = localName
							+ currentTemplate.substring(0,literalEnd);
					currentTemplate = currentTemplate.substring(literalEnd);
				}
			}

			RDFModel model = ctx.getModel();
			// ARTResource[] graphs = ctx.getRGraphs();
			uriRes = model.createURIResource(model.getDefaultNamespace() + localName);
			// resRand.setArtURIResource(uriRes);

			try {
				if (!model.existsResource(uriRes /* , graphs */)) {
					newConceptGenerated = true;
				}
			} catch (ModelAccessException e) {
				// throw new URIGenerationException(e);
				throw new RuntimeException(e);
			}
		}

		return uriRes;
	}
	
	private String escapeValue(String rawString) {
		return UrlEscapers.urlPathSegmentEscaper().escape(rawString.trim().replaceAll("\\s+", "_"));
	}

	private String getPlaceholderValue(String ph, Map<String, ARTNode> args) {
		Matcher matcher = PLACEHOLDER_PATTERN.matcher(ph);
		if (!matcher.matches())
			return null;

		String firstLevel = matcher.group(1);
		String secondLevel = matcher.group(2);

		ARTNode firstLevelObj = args.get(firstLevel);

		if (firstLevelObj == null)
			return null;

		if (secondLevel == null)
			return object2string(firstLevelObj);

		try {
			Method[] methods = firstLevelObj.getClass().getMethods();

			String nameWithHas = "get" + Character.toUpperCase(secondLevel.charAt(0))
					+ secondLevel.substring(1);

			Method methodWithLiteralName = null;
			Method methodWithHasName = null;

			for (Method m : methods) {
				if (m.getParameterTypes().length != 0)
					continue;

				String methodName = m.getName();
				if (Objects.equal(methodName, secondLevel)) {
					methodWithLiteralName = m;
					break;
				} else if (Objects.equal(methodName, nameWithHas)) {
					methodWithHasName = m;
				}
			}

			Method m = methodWithLiteralName != null ? methodWithLiteralName : methodWithHasName;

			if (m == null)
				return null;

			Object secondLevelObject = m.invoke(firstLevelObj);

			return object2string(secondLevelObject);
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			return null;
		}
	}

	private String object2string(Object obj) {
		String rawString = null;
		if (obj instanceof ARTNode) {
			ARTNode artNode = (ARTNode) obj;
			if (artNode.isURIResource()) {
				rawString = artNode.asURIResource().getLocalName();
			} else if (artNode.isLiteral()) {
				rawString = artNode.asLiteral().getLabel();
			} else {
				rawString = artNode.getNominalValue();
			}
		} else if (obj != null) {
			rawString = obj.toString();
		} else {
			return null;
		}

		return rawString;
	}

	/**
	 * Returns the template associated with the given <code>xRole</code>.
	 * 
	 * @param ctx
	 * @param xRole
	 * @return
	 */
	private String getTemplate(CODAContext ctx, String xRole) {
		Properties props = ctx.getConverterProperties(CONVERTER_URI);

		String template = null;

		if (props != null) {
			template = props.getProperty(xRole, defaultPropertyValues.getProperty(xRole));
		} else {
			template = defaultPropertyValues.getProperty(xRole);
		}

		if (template == null) {
			template = defaultPropertyValues.getProperty("fallback");
		}

		return template;
	}

	@Override
	public ARTURIResource produceURI(CODAContext ctx, String value, String xRole) throws ConverterException {
		return produceURI(ctx, value, xRole, Collections.<String, ARTNode> emptyMap());
	}

	@Override
	public ARTURIResource produceURI(CODAContext ctx, String value) throws ConverterException {
		return produceURI(ctx, value, "undetermined");
	}

}
