package it.uniroma2.art.coda.converters.contracts;

import java.util.Map;

import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.coda.interfaces.Converter;
import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;
import it.uniroma2.art.coda.interfaces.annotations.converters.Parameter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.RDFResourceRolesEnum;

/**
 * A converter compliant with the {@link RandomIdGenerator} contract generates resource identifiers in a
 * random manner, i.e. producing different outputs for the same arguments.
 * 
 * The contract identifier is: <code>http://art.uniroma2.it/coda/contracts/randIdGen</code>
 * 
 */
public interface RandomIdGenerator extends Converter {

	String CONTRACT_URI = ContractConstants.CODA_CONTRACTS_BASE_URI + "randIdGen";

	
	public static class XRoles {
		public static final String CONCEPT = "concept";
		public static final String CONCEPTSCHEME = "conceptScheme";
		public static final String XLABEL = "xLabel";
		public static final String XNOTE = "xNote";
		public static final String SKOSCOLLECTION = "skosCollection";
	};

	public static class PARAMETERS {
		public static final String LABEL = "label";
		public static final String VALUE = "value";
		public static final String SCHEME = "scheme";
		public static final String LEXICALFORM = "lexicalForm";
		public static final String LEXICALIZEDRESOURCE = "lexicalizedResource";
		public static final String ANNOTATEDRESOURCE = "annotatedResource";
		public static final String TYPE = "type";
	};

	/**
	 * Returns a resource identifier produced randomly from the given inputs. The parameter {@code xRole}
	 * holds the nature of the resource that will be identified with the given URI. Depending on the value of
	 * the parameter {@code xRole}, a conforming converter may generate differently shaped URIs, possibly
	 * using specific arguments passed via the map {@code args}.
	 * 
	 * For each specific {@code xRole}, the client should provide some agreed-upon parameters to the
	 * converters. This contract defines the following parameters:
	 * 
	 * <ul>
	 * <li><code>concept</code> (for <code>skos:Concept</code>s)
	 * <ul>
	 * <li><code>label</code> (optional): the accompanying preferred label of the <i>skos:Concept</i></li>
	 * <i>skos:Concept</i></li>
	 * <li><code>scheme</code> (optional): the local name of the concept scheme to which the concept is being attached at the moment of
	 * its creation</li>
	 * </ul>
	 * </li>
	 * <li><code>conceptScheme</code> (for <code>skos:ConceptScheme</code>s)
	 * <ul>
	 * <li><code>label</code> (optional): the accompanying preferred label of the <i>skos:Concept</i></li>
	 * <i>skos:Concept</i></li>
	 * </ul>
	 * </li>
	 * <li><code>skosCollection</code> (for <code>skos:Collection</code>s)
	 * <ul>
	 * <li><code>label</code> (optional): the accompanying preferred label of the <i>skos:Collection</i></li>
	 * </ul>
	 * </li>
	 * <li><code>xLabel</code> (for <code>skosxl:Labels</code>s)
	 * <ul>
	 * <li><code>lexicalForm</code>: the lexical form of the <i>skosxl:Label</i></li>
	 * <li><code>lexicalizedResource</code>: the resource to which the <i>skosxl:Label</i> will be attached to
	 * </li>
	 * <li><code>type</code>: the property used for attaching the label</li>
	 * </ul>
	 * </li>
	 * <li><code>xNote</code></li> (for reified <code>skos:note</code>s)
	 * <ul>
	 * <li><code>value</code>: the content of the note</li>
	 * <li><code>annotatedResource</code>: the resource being annotated</li>
	 * <li><code>type</code>: the property used for annotation</li>
	 * </ul>
	 * </li> </li> </ul>
	 *
	 * The parameters requested by additional <code>xRole</code>s are defined elsewhere by the party defining
	 * that <code>xRole</code>.
	 * 
	 * Users of this contract should always supply values for the required parameters associated with an
	 * <code>xRole</code>; therefore, they should not attempt to generate a URI for an <code>xRole</code>
	 * unless they known what arguments are requested.
	 * 
	 * Conversely, it is a duty of the specific converter implementing this contract to verify that all
	 * relevant information has been provided by the client. In fact, it is suggested that converters are
	 * implemented defensively, that is to say they should:
	 * 
	 * <ul>
	 * <li>complain only about the absence of absolutely required parameters</li>
	 * <li>handle unknown <code>xRole</code>s gracefully, by means some fallback strategy</li>
	 * </ul>
	 *
	 * @param ctx
	 *            the conversion context
	 * @param value
	 *            the value seeding the generation of the identifier (ignored in this case)
	 * @param xRole
	 *            an extension of the notion of role (see {@link RDFResourceRolesEnum}), allowing for finer
	 *            grained distinctions
	 * @param args
	 *            additional arguments, depending on the specific type of identifier
	 * @return
	 * @throws ConverterException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.IGNORED)
	ARTURIResource produceURI(CODAContext ctx, String value, @Parameter(name = "xRole") String xRole, @Parameter(name = "args") Map<String, ARTNode> args)
			throws ConverterException;

	/**
	 * An overload of {@link #produceURI(CODAContext, String, String, Map)} without an explicit parameter map.
	 * 
	 * @param ctx
	 * @param value
	 * @param xRole
	 * @return
	 * @throws ConverterException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.IGNORED)
	ARTURIResource produceURI(CODAContext ctx, String value, @Parameter(name = "xRole") String xRole) throws ConverterException;

	/**
	 * An overload of {@link #produceURI(CODAContext, String, String, Map)} without an explicit xRole and
	 * parameter map.
	 * 
	 * @param ctx
	 * @param value
	 * @return
	 * @throws ConverterException
	 */
	@FeaturePathArgument(requirementLevel = RequirementLevels.IGNORED)
	ARTURIResource produceURI(CODAContext ctx, String value) throws ConverterException;

}
