package it.uniroma2.art.coda.converters.contracts;

public interface ContractConstants {
	String CODA_CONTRACTS_BASE_URI = "http://art.uniroma2.it/coda/contracts/";
	String CODA_CONVERTERS_BASE_URI = "http://art.uniroma2.it/coda/converters/";
}
