# README #

CODA (Computer-aided Ontology Development Architecture) is an architecture and an associated Java framework for the [RDF](http://www.w3.org/RDF/) triplification of [UIMA](http://uima.apache.org/) results from analysis of unstructured content.

You may learn more about CODA on its home site: http://art.uniroma2.it/coda/

